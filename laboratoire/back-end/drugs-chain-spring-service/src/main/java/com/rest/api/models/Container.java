package com.rest.api.models;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Container {

    @Id
    private String id;
private    String email;

    public String getTagContainer() {
        return tagContainer;
    }

    public void setTagContainer(String tagContainer) {
        this.tagContainer = tagContainer;
    }

    @NotBlank
private String tagContainer;
private String laboAddress;
    private String grossisteAddress;
    private String commandeRef;

    private ArrayList<String> temperature=new ArrayList<>();
    private String seuilTemperature;
    private String seuilHumidite;

    private  ArrayList<String> humidity=new ArrayList<>();
    private  Status state;
    private List<Medicament> medicaments =new ArrayList<>();
private String transporteurAddress;
private String transactionInTransit;
private String transactionReceived;
private  String affectationTransaction;
private  String destinationAddress;
    @DateTimeFormat(style = "M-")
    @CreatedDate
    private Date createdDate;

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public String getSeuilTemperature() {
        return seuilTemperature;
    }

    public void setSeuilTemperature(String seuilTemperature) {
        this.seuilTemperature = seuilTemperature;
    }

    public String getSeuilHumidite() {
        return seuilHumidite;
    }

    public void setSeuilHumidite(String seuilHumidite) {
        this.seuilHumidite = seuilHumidite;
    }

    public String getTransactionInTransit() {
        return transactionInTransit;
    }

    public void setTransactionInTransit(String transactionInTransit) {
        this.transactionInTransit = transactionInTransit;
    }

    public String getTransactionReceived() {
        return transactionReceived;
    }

    public void setTransactionReceived(String transactionReceived) {
        this.transactionReceived = transactionReceived;
    }

    public String getAffectationTransaction() {
        return affectationTransaction;
    }

    public void setAffectationTransaction(String affectationTransaction) {
        this.affectationTransaction = affectationTransaction;
    }

    public List<Medicament> getMedicaments() {
        return medicaments;
    }

    public void setMedicaments(List<Medicament> medicaments) {
        this.medicaments = medicaments;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLaboAddress() {
        return laboAddress;
    }

    public void setLaboAddress(String laboAddress) {
        this.laboAddress = laboAddress;
    }

    public String getGrossisteAddress() {
        return grossisteAddress;
    }

    public void setGrossisteAddress(String grossisteAddress) {
        this.grossisteAddress = grossisteAddress;
    }

    public ArrayList<String> getTemperature() {
        return temperature;
    }

    public ArrayList<String> getHumidity() {
        return humidity;
    }

    public Status getState() {
        return state;
    }

    public void setState(Status state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "Container{" +
                "id='" + id + '\'' +
                ", grossisteAddress='" + grossisteAddress + '\'' +
                ", state='" + state + '\'' +
                '}';
    }

    public void setTemperature(ArrayList<String> temperature) {
        this.temperature = temperature;
    }

    public void setHumidity(ArrayList<String> humidity) {
        this.humidity = humidity;
    }

    public String getCommandeRef() {
        return commandeRef;
    }

    public void setCommandeRef(String commandeRef) {
        this.commandeRef = commandeRef;
    }

    public String getTransporteurAddress() {
        return transporteurAddress;
    }

    public void setTransporteurAddress(String transporteurAddress) {
        this.transporteurAddress = transporteurAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
