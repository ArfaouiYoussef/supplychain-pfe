package com.rest.api.controllers;

import com.rest.api.models.ERole;
import com.rest.api.models.Role;
import com.rest.api.models.User;
import com.rest.api.payload.request.ActivateRequest;
import com.rest.api.payload.request.BlockRequest;
import com.rest.api.payload.request.GetUserRequest;
import com.rest.api.payload.response.MessageResponse;
import com.rest.api.payload.response.UserResponse;
import com.rest.api.repository.RoleRepository;
import com.rest.api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/transporteur")
public class TransporteurController {

@Autowired
    UserRepository userRepository;
@Autowired
    RoleRepository roleRepository;



@GetMapping()
    public ResponseEntity<?> getAll(){
    Role transporteurRole = roleRepository.findByName(ERole.ROLE_TRANSPORTEUR)
            .orElseThrow(() -> new RuntimeException("Error: Role is not found."));

    List<User> users=userRepository.findByRoles(transporteurRole);

    return 	 	ResponseEntity.ok(users); }

    @GetMapping("/activeTransporteur")
    public ResponseEntity<?> getclientActive() {
        Role transporteurRole = roleRepository.findByName(ERole.ROLE_TRANSPORTEUR)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));

        List<User> users = userRepository.findByisAccountActiveAndRoles(true,transporteurRole);


        return ResponseEntity.ok(users);
    }
    @GetMapping("/demande")
    public ResponseEntity<?> getClient() {
        Role transporteurRole = roleRepository.findByName(ERole.ROLE_TRANSPORTEUR)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));

        List<User> users = userRepository.findByisAccountActiveAndRoles(false,transporteurRole);

        return ResponseEntity.ok(users);
    }

    @GetMapping("/unblocked")
    public ResponseEntity<?> getClientUnBlocked(){
        Role grossisteRole = roleRepository.findByName(ERole.ROLE_TRANSPORTEUR)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));

        List<User> users=userRepository.findByisAccountActiveAndRoles(true,grossisteRole);

        return 	 	ResponseEntity.ok(users);

    }
    @GetMapping("/blocked")
    public ResponseEntity<?> getClientBlocked(){
        Role role = roleRepository.findByName(ERole.ROLE_TRANSPORTEUR)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));

        List<User> users=userRepository.findByisBlockedAndRoles(true,role);

        return 	 	ResponseEntity.ok(users);

    }
    @PostMapping("/blocked")
    public ResponseEntity<?> Block(@Valid @RequestBody BlockRequest blockRequest){
        if(userRepository.existsByAccountAddress(blockRequest.getAccountAddress())){
            User user=userRepository.findByAccountAddress(blockRequest.getAccountAddress()).get();
            user.setBlocked(blockRequest.isBlock());
            userRepository.save(user);
            return 	 	ResponseEntity.ok().body(new MessageResponse("success"));

        };

        return 	 	ResponseEntity.badRequest().body(new MessageResponse("user not found"));

    }

    @PostMapping("/activateAccount")
    public ResponseEntity<?> activateAccount(@Valid @RequestBody ActivateRequest activateRequest){
        if(userRepository.existsByisBlockedAndId(false,activateRequest.getId())){
            if(userRepository.findById(activateRequest.getId()).isPresent()) {
                User user = userRepository.findById(activateRequest.getId()).get();
                user.setSmartContract(activateRequest.getSmartContract());
                user.setAccountActive(activateRequest.isActive());
                userRepository.save(user);
                return ResponseEntity.ok().body(new MessageResponse("success"));
            }
            else
                return 	 	ResponseEntity.badRequest().body(new MessageResponse("user not found"));

        };

        return 	 	ResponseEntity.badRequest().body(new MessageResponse("user not found"));

    }

}
