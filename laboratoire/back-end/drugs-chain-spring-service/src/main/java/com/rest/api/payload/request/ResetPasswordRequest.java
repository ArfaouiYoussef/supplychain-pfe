package com.rest.api.payload.request;

import org.springframework.data.annotation.Id;

import javax.validation.constraints.Size;

public class ResetPasswordRequest {
private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
@Size(min = 8,max =100 )
    private  String oldPassword;
    @Size(min = 8,max =100 )

    private String newPassword;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
