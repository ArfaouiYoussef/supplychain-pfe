package com.rest.api.service;

import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.Request;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.protocol.http.HttpService;

import java.util.concurrent.ExecutionException;

public class SendTransactionService {

    public Request<?, EthSendTransaction> sendRawTransaction(String transaction) throws ExecutionException, InterruptedException {
        Web3j web3 = Web3j.build(new HttpService("http://5.135.52.74:8545"));
       return    web3.ethSendRawTransaction(transaction);




    }
}
