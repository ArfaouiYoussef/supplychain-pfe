package com.rest.api.payload.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class ClassThUpdateRequest {

    @NotBlank
    @Size(min = 3, max = 70)
    private String oldName;
    @NotBlank
    @Size(min = 3, max = 70)
    private String newName;

    public String getOldName() {
        return oldName;
    }

    public void setOldName(String oldName) {
        this.oldName = oldName;
    }

    public String getNewName() {
        return newName;
    }

    public void setNewName(String newName) {
        this.newName = newName;
    }
}
