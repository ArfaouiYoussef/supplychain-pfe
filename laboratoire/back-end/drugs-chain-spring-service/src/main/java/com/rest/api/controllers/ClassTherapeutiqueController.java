package com.rest.api.controllers;

import com.rest.api.models.ClasseTherapeutique;
import com.rest.api.payload.request.ClassThRequest;
import com.rest.api.payload.request.ClassThUpdateRequest;
import com.rest.api.payload.response.MessageResponse;
import com.rest.api.repository.ClassTherapeutiqueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/classTherapeutique")
public class ClassTherapeutiqueController {

    @Autowired
private ClassTherapeutiqueRepository therapeutiqueRepository;

    @GetMapping()
    public ResponseEntity<?> getAll(){
      return   ResponseEntity.ok(therapeutiqueRepository.findAll());
    }

@PostMapping("/{id}")
    public  ResponseEntity<?> getById(@PathVariable String id){

        if(therapeutiqueRepository.findById(id).isPresent())
        {
          ClasseTherapeutique ct= therapeutiqueRepository.findById(id).get();
            return ResponseEntity.ok(ct);

        }
        else
            return ResponseEntity.badRequest().body("class not found");
}

    @PostMapping()
    public  ResponseEntity<?> addClass(@Valid @RequestBody ClassThRequest classThRequest) {
        if (!therapeutiqueRepository.findByName(classThRequest.getName().trim()).isPresent()) {
            ClasseTherapeutique classeTherapeutique = new ClasseTherapeutique();
            classeTherapeutique.setName(classThRequest.getName());
            therapeutiqueRepository.save(classeTherapeutique);
            return ResponseEntity.ok(new MessageResponse("success"));
        }
        else
            return ResponseEntity.badRequest().body(new MessageResponse("class already exist"));
    }
@PutMapping()
public  ResponseEntity<?> updateClass(@Valid @RequestBody ClassThUpdateRequest classThRequest) {

{
    if (therapeutiqueRepository.findByName(classThRequest.getOldName().trim()).isPresent() && !therapeutiqueRepository.findByName(classThRequest.getNewName().trim()).isPresent() ) {
ClasseTherapeutique classe=therapeutiqueRepository.findByName(classThRequest.getOldName().trim()).get();
classe.setName(classThRequest.getNewName().trim());
therapeutiqueRepository.save(classe);
        return ResponseEntity.ok(new MessageResponse("success"));
    }
    else
        return ResponseEntity.badRequest().body(new MessageResponse("class/new name already exist"));
}


}

    @DeleteMapping("/{id}")
    public  ResponseEntity<?> delete(@PathVariable String id){

        if(therapeutiqueRepository.findByName(id).isPresent())
        {
             therapeutiqueRepository.deleteByName(id);
            return ResponseEntity.ok("success");

        }
        else
            return ResponseEntity.badRequest().body(new MessageResponse("class not found"));
    }


}
