package com.rest.api.repository;

import com.rest.api.models.MedicamentInBasket;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;


public interface MedicamentInBasketRepository extends MongoRepository<MedicamentInBasket, String> {
MedicamentInBasket findByMedicamentIdAndUserId(String medId, String id);
Boolean existsByMedicamentIdAndUserId(String medId,String id);
}
