package com.rest.api.models;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.sql.Date;
import java.util.HashMap;

@Document(collection = "commande")
public class Commande {
    @Id
    private String id;
private String lat;
private String lng;
private String publicAddress;
private String telephone;
private String email;
    private String transactionInTransit;
    private String transactionReceived;
    private  String affectationTransaction;



    public HashMap<String,String> containers=new HashMap<>();

    public HashMap<String, String> getContainers() {
        return containers;
    }

    public void setContainers(HashMap<String, String> containers) {
        this.containers = containers;
    }

    public Panier getPanier() {
        return panier;
    }

    public void setPanier(Panier panier) {
        this.panier = panier;
    }

    private Panier panier;

    private String name;
    public HashMap<String, Medicament> products=new HashMap<>();

    public String getPublicAddress() {
        return publicAddress;
    }

    public void setPublicAddress(String publicAddress) {
        this.publicAddress = publicAddress;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Enumerated(EnumType.STRING)
private Status status=Status.EnAttente;

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @CreatedDate
    private Date createdDate;
    public Commande() {

    }

    public String getTransactionInTransit() {
        return transactionInTransit;
    }

    public void setTransactionInTransit(String transactionInTransit) {
        this.transactionInTransit = transactionInTransit;
    }

    public String getTransactionReceived() {
        return transactionReceived;
    }

    public void setTransactionReceived(String transactionReceived) {
        this.transactionReceived = transactionReceived;
    }

    public String getAffectationTransaction() {
        return affectationTransaction;
    }

    public void setAffectationTransaction(String affectationTransaction) {
        this.affectationTransaction = affectationTransaction;
    }

    public HashMap<String, Medicament> getProducts() {
        return products;
    }

    public void setProducts(HashMap<String, Medicament> products) {
        this.products = products;
    }

    public HashMap<String, Integer> getProductQuantities() {
        return productQuantities;
    }

    public void setProductQuantities(HashMap<String, Integer> productQuantities) {
        this.productQuantities = productQuantities;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getAddressCommande() {
        return addressCommande;
    }

    public void setAddressCommande(String addressCommande) {
        this.addressCommande = addressCommande;
    }

    private String addressCommande;
   public HashMap<String, Integer> productQuantities=new HashMap<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
