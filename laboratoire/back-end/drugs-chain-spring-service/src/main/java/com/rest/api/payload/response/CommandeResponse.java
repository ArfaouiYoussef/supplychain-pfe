package com.rest.api.payload.response;

public class CommandeResponse {
	private String message;

	public CommandeResponse(String message) {
	    this.message = message;
	  }

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
