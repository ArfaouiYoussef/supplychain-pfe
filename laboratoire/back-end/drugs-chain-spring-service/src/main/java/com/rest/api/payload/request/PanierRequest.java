package com.rest.api.payload.request;

import com.rest.api.models.Medicament;
import com.rest.api.models.MedicamentInBasket;
import com.rest.api.models.User;

import java.util.List;

public class PanierRequest {
private String prixTotal;
Medicament medicament;
private Number quantite;
private Number dispo;
List<MedicamentInBasket>medicamentList;

    public List<MedicamentInBasket> getMedicamentList() {
        return medicamentList;
    }

    public void setMedicamentList(List<MedicamentInBasket> medicamentList) {
        this.medicamentList = medicamentList;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    private String userId;
    public String getPanierId() {
        return panier;
    }
    public void setPanier(String panierId) {
        this.panier = panierId;
    }

    private String panier;
    public Medicament getMedicament() {
        return medicament;
    }
private User user;
    public void setMedicament(Medicament medicament) {
        this.medicament = medicament;
    }

    public String getPrixTotal() {
        return prixTotal;
    }

    public void setPrixTotal(String prixTotal) {
        this.prixTotal = prixTotal;
    }

    public Number getQuantite() {
        return quantite;
    }

    public void setQuantite(Number quantite) {
        this.quantite = quantite;
    }

    public String getPanier() {
        return panier;
    }

    public Number getDispo() {
        return dispo;
    }

    public void setDispo(Number dispo) {
        this.dispo = dispo;
    }

    @Override
    public String toString() {
        return "PanierRequest{" +
                "prixTotal='" + prixTotal + '\'' +
                ", medicament=" + medicament +
                ", quantite=" + quantite +
                ", dispo=" + dispo +
                ", medicamentList=" + medicamentList +
                ", userId='" + userId + '\'' +
                ", panier='" + panier + '\'' +
                '}';
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
