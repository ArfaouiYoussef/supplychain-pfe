
package com.rest.api.models;

import java.util.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "users")
public class User {
    @Id
    private String id;

    @NotBlank
    @Size(max = 20)
    private String username;
    @NotBlank
    @Size(max = 70)
    private String accountAddress;
    private String photoUrl;
private String smartContract;

    public String getSmartContract() {
        return smartContract;
    }

    public void setSmartContract(String smartContract) {
        this.smartContract = smartContract;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    @NotBlank
    @Size(max = 70)
    private String address;
    @NotBlank
    @Size(max = 50)
    @Email
    private String email;
    @Size(max=10)
private String telephone;
    @NotBlank
    @Size(max = 120)
    private String password;
    @Size(max = 120)
    private String lng;
@DBRef
    List<Commande> commandes=new ArrayList<>();

    public List<Commande> getCommandes() {
        return commandes;
    }

    public void setCommandes(List<Commande> commandes) {
        this.commandes = commandes;
    }

    private boolean isBlocked =false;

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isAccountActive() {
        return isAccountActive;
    }

    public void setAccountActive(boolean accountActive) {
        isAccountActive = accountActive;
    }

    public String getUserAddresse() {
        return userAddresse;
    }

    public void setUserAddresse(String userAddresse) {
        this.userAddresse = userAddresse;
    }

    public String getUserSite() {
        return userSite;
    }

    public void setUserSite(String userSite) {
        this.userSite = userSite;
    }

    public String getUserPicture() {
        return userPicture;
    }

    public void setUserPicture(String userPicture) {
        this.userPicture = userPicture;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    @Size(max = 120)
    private String lat;
    @Size(max = 12)
    private String userPhone;

    private String userPicture;
    @Size(max = 120)
    private String userSite;

    @Size(max = 150)
    private String userAddresse;

    private boolean isAccountActive;


    @CreatedDate
    private Date createdDate;
    @LastModifiedDate
    private Date lastModifiedDate;








@DBRef
Panier panier;

    public Panier getPanier() {
        return panier;
    }

    public void setPanier(Panier panier) {
        this.panier = panier;
    }

    @DBRef
    private Set<Role> roles = new HashSet<>();

    public User() {
    }

    public User(String username, String email,String accountAddress, String password,boolean isAccountActive) {
        this.username = username;
        this.email = email;
        this.accountAddress=accountAddress;

        this.password = password;
    this.isAccountActive=isAccountActive;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }


    public String getAccountAddress() {
        return accountAddress;
    }

    public void setAccountAddress(String accountAddress) {
        this.accountAddress = accountAddress;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", accountAddress='" + accountAddress + '\'' +
                ", address='" + address + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", lng='" + lng + '\'' +
                ", commandes=" + commandes +
                ", isBlocked=" + isBlocked +
                ", lat='" + lat + '\'' +
                ", userPhone='" + userPhone + '\'' +
                ", userPicture='" + userPicture + '\'' +
                ", userSite='" + userSite + '\'' +
                ", userAddresse='" + userAddresse + '\'' +
                ", isAccountActive=" + isAccountActive +
                ", createdDate=" + createdDate +
                ", lastModifiedDate=" + lastModifiedDate +
                ", panier=" + panier +
                ", roles=" + roles +
                '}';
    }
}
