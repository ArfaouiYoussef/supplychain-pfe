
const config = {
    // Ethereum node and operations
    eth: {
        protocol: process.env.ETH_PROTOCOL || 'http',
        host: process.env.ETH_HOST || '5.135.52.74',
        port: parseInt(process.env.ETH_PORT) || 8546,
        chain_id: parseInt(process.env.ETH_CHAIN_ID) || 1515,
        gas_price: 0,
        gas_limit:  'auto'
    },
    // HTTP RESTful API
    api: {
        port: parseInt(process.env.APP_PORT) || 3000
    },
    // list of contracts
    contract: {
        'SampleToken': {
            abi_file: require('./contracts/ABI')
        }
    }
}

module.exports = config;
