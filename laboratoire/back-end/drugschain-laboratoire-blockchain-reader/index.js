const express = require('express');
const app = express();
const config = require('./config');
const web3 = require('./ethconn');
const cors = require('cors');
const PORT =  8100;
const eurekaHelper = require('./eureka-helper');
const bodyParser = require('body-parser');
app.use(bodyParser.json());
require('./router')(app, web3);

app.listen(PORT, () => {
  console.log("user-service on 3000");
})

app.get('/', (req, res) => {
 res.json("welcome to blockchain-reader service")
})

eurekaHelper.registerWithEureka('laboratoire-blockchain-reader-service', PORT);
