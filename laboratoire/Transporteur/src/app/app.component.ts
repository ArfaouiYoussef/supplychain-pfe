import {Component, Inject} from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { FCM } from '@ionic-native/fcm/ngx';
import {NativeAudio} from '@ionic-native/native-audio/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})

export class AppComponent {
     audio2 = new Audio("../assets/alert.mp3");

    constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
   @Inject(NativeAudio) private nativeAudio: NativeAudio,
    private fcm: FCM,
  ) {
    this.initializeApp();

    }

  initializeApp() {
    this.platform.ready().then(() => {
        this.statusBar.styleDefault();
        this.splashScreen.hide();

    });
  }
}

