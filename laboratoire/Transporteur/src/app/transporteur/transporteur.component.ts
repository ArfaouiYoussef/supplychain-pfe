import { Component, OnInit } from '@angular/core';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import {FCM} from '@ionic-native/fcm/ngx';
import {ToastController} from '@ionic/angular';
import {Router} from '@angular/router';
import {MessageService} from '../services/shared.service';

@Component({
  selector: 'app-transporteur',
  templateUrl: './transporteur.component.html',
  styleUrls: ['./transporteur.component.scss'],
})
export class TransporteurComponent implements OnInit {
    private audio2: HTMLAudioElement;
constructor(private nativeAudio: NativeAudio,
            private messageService:MessageService,
            private fcm: FCM,private toastController: ToastController,private router:Router
,

) {


}
    appPages = [
        {
            title: 'Livraison',
            url: '/dashboard/container',
            direct: 'root',
            icon: 'cube-outline'
        }
,
        {
            title: 'Configuration appareils iot ',
            url: '/dashboard/configuration',
            direct: 'forward',
            icon: 'construct-outline'
        },

        {
            title: 'Paramètres',
            url: '/dashboard/parametres',
            direct: 'forward',
            icon: 'cog'
        },
        {title: "Déconnexion",
url: 'logout'
        }

    ];
    loggedIn = false;
    dark = false;
    logout() {
        localStorage.clear()
        this.router.navigate(["/auth/login"])

    }

    ngOnInit(): void {
        // subscribe to a topic
            this.fcm.subscribeToTopic(JSON.parse(localStorage.getItem("currentUser")).accountAddress);

        // get FCM token
        this.fcm.getToken().then(token => {
            console.log(token);
        });

        // ionic push notification example
        this.fcm.onNotification().subscribe(async data => {
            console.log(data);
            if (data.wasTapped) {
                if(data.alert===true){

                    this.audio2=null;

                    this.audio2 = new Audio("../assets/alertDanger.mp3");
                    this.audio2.play();


                    console.log('Received in background');

                }
                else {

                    this.audio2=null;
                    this.audio2 = new Audio("../assets/notif.mp3");
                    this.audio2.play();

                    const toast = await this.toastController.create({
                        message: 'Nouveau conteneur disponible. Tag:'+data.tagContainer,
                        duration: 10000,
                        color:'success'
                    });

                    toast.present();

                    console.log('Received in background');
                }

            } else {

                if(data.alert===true){
                    this.audio2=null;

                    this.audio2 = new Audio("../assets/alertDanger.mp3");
                    this.audio2.play();

                    const toast = await this.toastController.create({
                        message: 'Conteneur depasse le seuil maximum! . Tag:'+data.tagContainer,
                        duration: 10000,
                        color:'danger'
                    });
                    toast.present();


                }
                else{
                this.audio2=null;
                this.audio2 = new Audio("../assets/notif.mp3");
                this.audio2.play();
                    const toast = await this.toastController.create({
                        message: 'Nouveau conteneur disponible. Tag! . Tag:'+data.tagContainer,
                        duration: 5000,
                        color:'success'
                    });
                    this.messageService.sendMessage(data.container);
                    toast.present();
                console.log('Received in foreground');}
            }
        });

        // refresh the FCM token
        this.fcm.onTokenRefresh().subscribe(token => {
            console.log(token);
        });

        // unsubscribe from a topic
        // this.fcm.unsubscribeFromTopic('offers');

    }

}
