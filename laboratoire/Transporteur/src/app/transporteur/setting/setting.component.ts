import { Component, OnInit } from '@angular/core';
import {isAction} from '@angular-devkit/schematics';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../services/user.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss'],
})
export class SettingComponent implements OnInit {
isActive:any;
profil:FormGroup;
    passwordForm: FormGroup;
    userDetails: any;
    oldForm: any;
    private isChange: boolean=false;

constructor(private _fb:FormBuilder,
            private userService:UserService,
           private toastController: ToastController) {
    this.profil=this._fb.group({
        accountAddress:['',Validators.required],
        username:['',Validators.required],
        email:['',Validators.required],
    })
    this.passwordForm=this._fb.group({
        oldPassword:['',Validators.required],
        newPassword:['',Validators.required]

    })

}
  ngOnInit() {

    this.userDetails=JSON.parse(localStorage.getItem("currentUser"));
this.userService.getUser(this.userDetails.id).subscribe((response:any)=>{
this.profil.patchValue({
    accountAddress:!!response.accountAddress?response.accountAddress:'',
    username:!!response.username?response.username:'',
    email:!!response.email?response.email:'',
})
    this.oldForm=this.profil.value;

})
  }
onChange(){

    this.isChange= (  JSON.stringify(this.profil.value)===(JSON.stringify(this.oldForm)));
 console.log(this.isChange)
}
    editPassWord() {
        this.isActive=!this.isActive
    }
    async changePassword() {

        let request = {...this.passwordForm.value, id: this.userDetails.id};
        if (this.passwordForm.controls["newPassword"].value.toString() === this.passwordForm.controls["oldPassword"].value) {
            const toast = await this.toastController.create({
                message: 'nouveau mot de passe  est identique au mot de passe actuelle.',
                duration: 1000,
                color:'danger',
            });
toast.present();
this.passwordForm.reset();
return false;
        }
        this.userService.changePassword(request).subscribe(async response => {

            const toast = await this.toastController.create({
                message: 'success.',
                duration: 1000,
                color:'success'
            });
            toast.present();


        }, async error => {
            const toast = await this.toastController.create({
                message: 'echec.',
                duration: 1000,
                color:'danger'
            });
            toast.present();
        })

    }

    onSave() {
console.log(this.profil.value)
        let request ={...this.profil.value,id:this.userDetails.id}
        this.userService.updateUser(request).subscribe(async response=>{
            const toast = await this.toastController.create({
                message: 'success',
                duration: 1000,
                color:'success',
            });
            toast.present();
            this.isChange=true;
            this.oldForm=this.profil.value;
        },async error => {
            const toast = await this.toastController.create({
                message: 'success',
                duration: 1000,
                color:'echec',
            });
            toast.present();
        })
    }
}
