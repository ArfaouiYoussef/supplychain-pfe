import {Observable} from "rxjs";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";

@Injectable()
export class LoginActivate implements CanActivate {
    constructor( private router: Router) {}
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean>|Promise<boolean>|boolean {

let user=localStorage.getItem('currentUser');
        if (user && JSON.parse(user )!==null) {
            console.log(user)
        this.router.navigate(['/dashboard']);

          return false;
        }
        else
        return true;
    }
}
