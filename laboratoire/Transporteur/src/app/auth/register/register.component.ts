import { File } from '@ionic-native/file/ngx';


import * as saveAs from 'file-saver';

import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, Validators, FormGroup, FormBuilder, ValidatorFn, AbstractControl, ValidationErrors} from "@angular/forms";
import {EthereumProvider} from "../../ethereumProvider/ethereum";
import {takeUntil, timestamp} from "rxjs/operators";
import {Subject} from 'rxjs';
import {AuthService} from "../../services/auth.service";
import {Router} from '@angular/router';
import {ToastController} from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls:['./register.component.scss']
})
export class RegisterComponent {

  registerForm: FormGroup;
  errors: string[] = [];

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
      private file:File,
    private _auth:AuthService,
    private _formBuilder: FormBuilder ,
    private _web3:EthereumProvider ,
    private  _router:Router,
      private toastController: ToastController

  )
  {

    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------



  createAccount(){

    this._web3.generateAccount();

    let data=JSON.stringify({address:this._web3.getAccount(),privateKey:this._web3.getPrivateKey()});
    let file= this.createFile(data);
    let timestamp = Math.round(+new Date()/1000);

    this.registerForm.controls["roles"].patchValue([this.registerForm.controls["userrole"].value]);

    this.registerForm.controls["accountAddress"].patchValue(this._web3.getAccount());
    this._auth.signup(this.registerForm.value).subscribe(async res=>{
      console.log(res)
      this._router.navigate(["/auth/login"]);
      saveAs(file, timestamp+'key.json')
        const toast = await this.toastController.create({
            message: 'Success',
            duration: 10000,
            color:'success'
        });
        toast.present();
        this.file.createFile(this.file.externalDataDirectory,timestamp+"key.json" , true);
        this.file.writeFile(this.file.externalDataDirectory, timestamp+"key.json" , file, {replace: true, append: false}).then(

            console.log

        );

console.log(this.file.dataDirectory);
    },async error1 => {
        this.errors = ['invalid email'];
        const toast = await this.toastController.create({
            message: 'Echec'+error1,
            duration: 10000,
            color:'success'
        });
        toast.present();

    })


  }

  createFile(data) {

    let file = new Blob([data], { type: 'text/text;charset=utf-8' });
    return file;
  }

  ngOnInit(): void
  {
    this.registerForm = this._formBuilder.group({
      username           : ['', Validators.required],
      email          : ['', [Validators.required, Validators.email]],
      accountAddress:[''],
      password       : ['', Validators.required],
      roles      : ['transporteur'],
      userrole      : ['transporteur'],
    });
    this._web3.generateAccount();



    // Update the validity of the 'passwordConfirm' field
    // when the 'password' field changes

  }
  onRegister(){

  }
  ngOnDestroy(): void
  {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

  if ( !control.parent || !control )
  {
    return null;
  }

  const password = control.parent.get('password');
  const passwordConfirm = control.parent.get('passwordConfirm');

  if ( !password || !passwordConfirm )
  {
    return null;
  }

  if ( passwordConfirm.value === '' )
  {
    return null;
  }

  if ( password.value === passwordConfirm.value )
  {
    return null;
  }

  return {passwordsNotMatching: true};
};





