import { Component } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';

@Component({
    selector: 'page-splash',
    templateUrl: 'splash.html',
})
export class Splash {

    constructor( public splashScreen: SplashScreen) {

    }

    ionViewDidEnter() {

        this.splashScreen.hide();

        setTimeout(() => {

        }, 4000);

    }

}
