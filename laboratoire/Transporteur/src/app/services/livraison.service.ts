import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {config} from '../../config.js'
@Injectable({
  providedIn: 'root'
})
export class LivraisonService {



    constructor(private _http:HttpClient) { }

    getcontainers(){
        return this._http.get(config.apiUrl+"/api/container/transporteur/"+JSON.parse(localStorage.getItem("currentUser")).accountAddress);
    }

    shippingContianer(data) {
    return this._http.post(config.apiUrl+"/api/container/start",data);
}

delivredContianer(data) {
  return this._http.post(config.apiUrl+"/api/container/end",data);
}


getContainerById(id){
        return this._http.get(config.apiUrl+"/api/container/"+id);
}

}
