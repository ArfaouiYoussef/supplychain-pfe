/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from './@core/utils/analytics.service';
import { NbThemeService } from '@nebular/theme';
import { StorageService } from './service/secure-storage.service';


@Component({
  selector: 'ngx-app',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit {

  constructor(private analytics: AnalyticsService,
              private secureStorageService:StorageService,
              private theme:NbThemeService) {




  }
  message;





  ngOnInit(): void {
    const userId = 'user001';
this.theme.changeTheme("corporate");
    this.analytics.trackPageViews();

  }
}
