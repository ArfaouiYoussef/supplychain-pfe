import {Observable} from "rxjs";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {AuthService} from "./auth-service.service";
import {LocalstorageService} from "../service/localstorage.service";

@Injectable()
export class LoginActivate implements CanActivate {
  constructor(private authService: AuthService, private router: Router,
              private localstorageService:LocalstorageService) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean>|Promise<boolean>|boolean {
    let user= JSON.parse(this.localstorageService.getItem('currentUser'));

    if(!user) {
      this.router.navigate(['/auth/client']);
      return true;}
    if (user.roles[0]==="ROLE_LABORATOIRE") {
      this.router.navigate(['/dashboard/client']);
return false;
    }
   else if (user.roles[0]==="ROLE_GROSSISTE") {
      this.router.navigate(['/dashboard-grossiste/produit']);
      return false;
    }


  }
}
