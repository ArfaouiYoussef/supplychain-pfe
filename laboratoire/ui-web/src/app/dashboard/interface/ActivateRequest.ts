export interface ActivateRequest {
  accountAddress: string;
  active: boolean;
}
