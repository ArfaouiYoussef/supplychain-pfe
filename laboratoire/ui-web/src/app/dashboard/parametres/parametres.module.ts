import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ParametresRoutingModule } from './parametres-routing.module';
import { ParametresComponent } from './parametres.component';
import { ProfileComponent } from './profile/profile.component';
import {NbButtonModule, NbCardModule, NbIconModule, NbInputModule} from "@nebular/theme";
import {AvatarModule} from "ngx-avatar";
import {MatFormFieldModule} from "@angular/material/form-field";
import {ReactiveFormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {MatTooltipModule} from '@angular/material/tooltip';

@NgModule({
  declarations: [ParametresComponent,ProfileComponent],
  imports: [
    CommonModule,
    ParametresRoutingModule,
    NbCardModule,
    AvatarModule,
    MatTooltipModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatIconModule,
    
    NbInputModule,
    NbButtonModule,
    NbIconModule
  ]
})
export class ParametresModule { }
