import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListApprobationComponent } from './list-approbation.component';

describe('ListApprobationComponent', () => {
  let component: ListApprobationComponent;
  let fixture: ComponentFixture<ListApprobationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListApprobationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListApprobationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
