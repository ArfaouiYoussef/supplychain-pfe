import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTagRfidComponent } from './list-tag-rfid.component';

describe('ListTagRfidComponent', () => {
  let component: ListTagRfidComponent;
  let fixture: ComponentFixture<ListTagRfidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTagRfidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTagRfidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
