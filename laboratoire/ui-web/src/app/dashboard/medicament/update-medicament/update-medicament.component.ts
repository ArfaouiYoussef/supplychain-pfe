import { Component, OnInit } from '@angular/core';
import {LocalDataSource} from "ng2-smart-table";
import {FileQueueObject, ImageUploaderOptions} from "ngx-image-uploader";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ClassTh} from "../../../model/ClassTh";
import {MedicamentService} from "../../../service/medicament.service";
import {Router, ActivatedRoute} from "@angular/router";
import {medicament_validation_messages} from "../../Validators/medicament_validation_messages";
import Swal from "sweetalert2";

@Component({
  selector: 'ngx-update-medicament',
  templateUrl: './update-medicament.component.html',
  styleUrls: ['./update-medicament.component.scss']
})
export class UpdateMedicamentComponent implements OnInit{
  updateMedicament: FormGroup;
  class: ClassTh[];
  validation_messages: any;
  medicament_id: any;
  selectedFile: any;
  picture: any;


  constructor(private _formBuilder:FormBuilder,private _serviceMedicament:MedicamentService,private _route:Router,
              private activatedRoute: ActivatedRoute,
  ){
    this.updateMedicament = this._formBuilder.group({
      id:[''],
      name: ['', [Validators.required]],
      indication : ['', [Validators.required]],
      photoUrl:[''  ],
      dosage:['', [Validators.required]],
      formegalenique:['', [Validators.required]],
      composition:['', [Validators.required]],
      classeTherapeutique:['', [Validators.required]],
      presentation:['', [Validators.required]],
      stock:['', [Validators.required]],
      marque:['', [Validators.required]],
      medicamentPrice:['', [Validators.required]],
      type:'model',
    });

    this._serviceMedicament.getAllClass().subscribe(res=>{
      this.class=res;
    })
  }



  options: ImageUploaderOptions = {
    thumbnailHeight: 150,

    thumbnailWidth: 150,
    autoUpload:false,
    uploadUrl: 'http://some-server.com/upload',
    allowedImageTypes: ['image/png', 'image/jpeg'],
    maxImageSize: 3,
    fieldName:"image medicament",

  };

  onUpload(file: FileQueueObject) {
    console.log(file.response);
  }
  onFileChanged(event) {
    this.selectedFile = event.target.files[0]
    console.log(this.selectedFile);
    const uploadData = new FormData();
    uploadData.append('file', this.selectedFile, this.selectedFile.name);
    console.log(event);
    if (event.target.files.length > 0) {
      const file = event.target.files[0];


      const formData = new FormData();
      formData.append('file', file);
      this._serviceMedicament.upload(formData).subscribe((res:any)=>{
        this.picture=res.fileDownloadUri;
        this.updateMedicament.controls["photoUrl"].setValue(this.picture);


      })
    }


  }

  ngOnInit(): void {
     this.activatedRoute.params.subscribe(res=>{
    this.medicament_id=res.id;
     });
console.log(this.medicament_id);
    this._serviceMedicament.getMedicamentById(this.medicament_id).subscribe((result: any) => {
      this.picture= !!result.photoUrl ?result.photoUrl: '';
        this.updateMedicament.setValue({
        id: !!result.id ?result.id: '',

        name: !!result.name ?result.name: '',
        indication: !!result.indication ?result.indication : '',
        photoUrl: !!result.photoUrl ?result.photoUrl : '',
        dosage: !!result.dosage ?result.dosage : '',
        formegalenique: !!result.formegalenique ?result.formegalenique : '',
        composition: !!result.composition ?result.composition : '',
        classeTherapeutique: !!result.classeTherapeutique.id ?result.classeTherapeutique.id : '',
        presentation: !!result.presentation ?result.presentation : '',
          stock: !!result.stock ?result.stock : '',
        marque:!!result.marque ?result.marque : '',
          medicamentPrice:!!result.medicamentPrice ?result.medicamentPrice : '',
        type: !!result.type ?result.type : '',
      });


      this.validation_messages = medicament_validation_messages;

    })
  }

  onSave() {

    if (this.updateMedicament.invalid) {
      console.log('invalid');
      Object.keys(this.updateMedicament.controls).forEach(field => {
        const control = this.updateMedicament.get(field);
        control.markAsTouched({onlySelf: true});
      });
    }else{

      this._serviceMedicament.updateMedicament(this.updateMedicament.value).subscribe(response=>{
        Swal.fire(
          'Success!',
          'Medicament Ajouté avec succès!',
          'success'
        )
        this._route.navigate(["/dashboard/medicament/"])
      });
      return;
    }

    console.log(this.updateMedicament.value) }
}

