import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClasseTherapeutiqueComponent } from './classe-therapeutique.component';

describe('ClasseTherapeutiqueComponent', () => {
  let component: ClasseTherapeutiqueComponent;
  let fixture: ComponentFixture<ClasseTherapeutiqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClasseTherapeutiqueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClasseTherapeutiqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
