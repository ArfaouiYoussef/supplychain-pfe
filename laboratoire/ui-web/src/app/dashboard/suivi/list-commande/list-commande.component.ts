import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {User} from "../../../model/user";
import {ActivateRequest} from "../../interface/ActivateRequest";
import {DeleteRequest} from "../../interface/DeleteRequest";
import {ClientService} from "../../../service/client.service";
import {NbDialogService} from "@nebular/theme";
import {Observable, of} from "rxjs";
import {map} from "rxjs/operators";
import Swal from "sweetalert2";
import {CommandService} from "../../../service/command.service";
import {container} from "../../../model/Container";
import {Container} from "@angular/compiler/src/i18n/i18n_ast";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NgxSpinnerService} from "ngx-spinner";
import {EthereumProvider} from "../../../ethereumProvider/ethereum";
import {NotificationService} from "../../../service/notification.service";

@Component({
  selector: 'ngx-list-commande',
  templateUrl: './list-commande.component.html',
  styleUrls: ['./list-commande.component.scss']
})
export class ListCommandeComponent implements OnInit {


  view: any[] = [250, 150];
  selectedItem:any;
  // options for the chart
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Temperature';
  xAxisLabelH = 'Humidité';

  showYAxisLabel = true;
  yAxisLabel = 'Valeur';
  timeline = true;

  colorScheme = {
    domain: [this.getRandomColor(),this.getRandomColor(), this.getRandomColor(), this.getRandomColor(), this.getRandomColor(), this.getRandomColor()]
  };
  humidites: any[];
  getRandomColor() {
    var length = 6;
    var chars = '0123456789ABCDEF';
    var hex = '#';
    while(length--) hex += chars[(Math.random() * 16) | 0];
    return hex;
  }
  @ViewChild('item', { static: true }) accordion;
  thresholdConfig = {
    '0': {color: 'green'},
    '40': {color: 'orange'},
    '75.5': {color: 'red'}
  };
  container: container;
  gaugeType = "full";
  gaugeValue = 28.3;
  gaugeLabel = "Temperature \n";
  gaugeAppendText = "°C";
  gaugeAppendTextHumidity = "%";

  transactionInTransit: string;
  transactionReceived: string;
  affectationTransaction: string;
  transporteurAddress: string;
  firstFormGroup: any;
  seuilHumidite: string;
  seuilTemperature: string;
  sensorSeuil:FormGroup;
  disableSave: boolean=true;
  decodeAffectationTx: Object;
  decodeIntransit: any;
  decodeReceived: any;
  oldValueSensor: any;
  status: any;
  currentContainer: any[];
  humiditeArea: boolean=false;
  single: any[];
  toggle() {
  }

  private users:User[];
  private  request:ActivateRequest;
  private deleteRequest:DeleteRequest;
  filteredItems: any[] & container[];
 sensorContianers:any[]=[];
  constructor(private _commandeService:CommandService,
              private dialogService: NbDialogService,private  _fb:FormBuilder,
              private spinner:NgxSpinnerService,
              private  notification:NotificationService,
              private  _web3:EthereumProvider,
              ) {
    this.filteredItems=[];

  }

  ngOnInit() {

    this.notification.getContainers().subscribe(res=>{
this.sensorContianers[res.tagContainer]=res;
console.log(this.sensorContianers);

    })
    this.sensorSeuil=this._fb.group({
      seuilTemperature:['', Validators.required],
      seuilHumidite:['',Validators.required],
      tagContainer:['',Validators.required]

    })
    this.firstFormGroup=this._fb.group({
      transactionHash:[''],
      timeStamp:[''],
      tagContainer:[''],
      ownerAddress:[''],
    })
    this.options = ['Option 1', 'Option 2', 'Option 3'];
    this.filteredOptions$ = of(this.options);

    this._commandeService.getAllContainer().subscribe((res:any)=>{

      this.container=res.filter(item=>item.state!==null);
      console.log("-container-------------")
      console.log(res)
      this.assignCopy();
    },error1 => this.assignCopy())


  }

  options: string[];
  filteredOptions$: Observable<string[]>;



  private filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(optionValue => optionValue.toLowerCase().includes(filterValue));
  }

  getFilteredOptions(value: string): Observable<string[]> {
    return of(value).pipe(
      map(filterString => this.filter(filterString)),
    );
  }
  assignCopy(){
    this.filteredItems = Object.assign([], this.container);
  }
  filterItem(value){

  }

selectContainer(res){
console.log(res)
  this.transporteurAddress=!!res.transporteurAddress?res.transporteurAddress:"";
let i=0;
  this.single = []
  const distinctTemperature = res.temperature.filter((n, i) => res.temperature.indexOf(n) === i);

  distinctTemperature.map(item=>{
  this.single=[...this.single, {
    "name": i++,
    "value": item
  }];

})
  let j=0;
  this.humidites=[];
  const distinctHumidite = res.humidity.filter((n, i) => res.humidity.indexOf(n) === i);

  distinctHumidite.map(item=>{
    this.humidites=[...this.humidites, {
      "name": j++,
      "value": item
    }];

    console.log(item)
  })

let data = [
    {
      "name": "Temperature",
      "series": [
        {
          "name": "",
          "value": 7300000
        },
        {
          "name": "2011",
          "value": 8940000
        }
      ]
    },


      ]


    }

search(event){
  let value=event.target.value;
  if(!value){
    console.log(this.status)

    this.filteredItems= Object.assign([], this.container).filter(item=>item.state===this.status)
    console.log(this.filteredItems)
  }
  else {
    this.filteredItems=  Object.assign([], this.container).filter(item=>item.state==this.status)

    this.filteredItems = Object.assign([], this.filteredItems).filter(
      item =>  item.tagContainer.toLowerCase().indexOf(value.toLowerCase()) > -1
    )
    console.log(this.filteredItems)
  }
}
  onChange(event) {

  }

  onSelectionChange($event) {
    this.filteredOptions$ = this.getFilteredOptions($event);
  }
  Onchange(event){
    this.disableSave=(JSON.stringify(this.sensorSeuil.value)===JSON.stringify(this.oldValueSensor));
    console.log(this.sensorSeuil.value);
    console.log(this.oldValueSensor);


  }
 updateSeuilSensor(){

   if(this.sensorSeuil.valid) {
      this.spinner.show().then(console.log);
      this._web3.updateContainerSensorSeuil(this.sensorSeuil.value).then(signed=>{
        let data={...this.sensorSeuil.value,transaction:signed.rawTransaction}
this._commandeService.updateSensorContainer(data).subscribe(res=>{

  console.log(data)
  this.disableSave=false;
  Swal.fire('Success', '', 'success')

  this.spinner.hide();
  },err=>{      Swal.fire('Echec', '', 'error')
  }
)
      })
    }

}



  open(dialog: TemplateRef<any>) {

    this.dialogService.open(dialog);
  }

  confirm(id: any) {
    this.request = { accountAddress: id ,active:false};
    // @ts-ignore
    this._clientService.activateAccount(this.request).subscribe(response=>{
      Swal.fire('Oops...', 'Compte activé!', 'success')
      this.users=this.users.filter(user=> user.id.toString()!==id);

    })

  }


  selectContainerTransaction(res) {
this.decodeAffectationTx=null;
this.decodeIntransit=null;
this.decodeReceived=null;

    console.log("hello");
    console.log(res);
    console.log(res.affectationTransaction)
    if(!!res.affectationTransaction){
      this._commandeService.getTransactionDetails(res.affectationTransaction).subscribe(res=>{
        this.decodeAffectationTx=res;
console.log(res)

      })
      if(!!res.transactionInTransit){
        this._commandeService.getTransactionDetails(res.transactionInTransit).subscribe(res=>{
          this.decodeIntransit=res;
  console.log(this.decodeIntransit)

        })

    }
  }
  if(!!res.transactionReceived){
    this._commandeService.getTransactionDetails(res.transactionReceived).subscribe(res=>{
      this.decodeReceived=res;
console.log(this.decodeReceived)

    })

}
    this.transactionInTransit=!!res.transactionInTransit?res.transactionInTransit:"";
    this.transactionReceived=!!res.transactionReceived?res.transactionReceived:"";
    this.affectationTransaction=!!res.affectationTransaction?res.affectationTransaction:"";

  }
selectTemperatureContainer(container:container){

 this.currentContainer=   this.sensorContianers.filter(item=> item.tagContainer.toString().toLowerCase()===container.tagContainer.toString().toLowerCase())
}
  selectContainerSeuil(container:container){

    this._commandeService.getContainerById(container.commandeRef).subscribe((container:container)=>{
      this.sensorSeuil.controls["tagContainer"].patchValue(container.tagContainer);

      this.sensorSeuil.controls["seuilTemperature"].patchValue(parseInt(container.seuilTemperature));
      this.sensorSeuil.controls["seuilHumidite"].patchValue(parseInt(container.seuilHumidite));
this.oldValueSensor=this.sensorSeuil.value;

      console.log(container)

    });

  }

  state(event) {
    this.status=event;
    console.log(this.status)
    this.assignCopy();
   this.filteredItems= this.filteredItems.filter(item=>item.state===event);

  }

  activateHumidite(event) {
console.log(event);
if(event.tabTitle==='Temperature'){
  this.humiditeArea=false;

}
else if(event.tabTitle==='Humidité'){
  this.humiditeArea=true;
}

  }

  onSelect($event: any) {

  }
}
