import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {MedicamentService} from "../../../service/medicament.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Medicament} from "../../../model/Medicament";
import {ActivateRequest} from "../../interface/ActivateRequest";
import {DeleteRequest} from "../../interface/DeleteRequest";
import {NbDialogService} from "@nebular/theme";
import {Observable, of} from "rxjs";
import {map} from "rxjs/operators";
import {Store} from "@ngrx/store";
import {ActivatedRoute} from "@angular/router";
import {EthereumProvider} from "../../../ethereumProvider/ethereum";
import {CommandService} from "../../../service/command.service";
import {Spinner} from "ngx-spinner/lib/ngx-spinner.enum";
import {container_validation_messages} from 'app/dashboard/Validators/container_validation_message';

import {NgxSpinnerService} from "ngx-spinner";
import {FindValueOperator} from "rxjs/internal/operators/find";
import {container} from "../../../model/Container";
import {MessageService} from "../../../service/message.service";
import {CarteRfidService} from "../../../service/carte-rfid.service";
import Swal from 'sweetalert2';

@Component({
  selector: 'ngx-preparation',
  templateUrl: './preparation.component.html',
  styleUrls: ['./preparation.component.scss']
})
export class PreparationComponent implements OnInit {

  @ViewChild('item', { static: true }) accordion;
tagContainer:any;
  isDisable:any=true;
  userAddress: any;
  tag:any;
  commandeId: any;
  data: { tagContainer: any; grossisteAddress: any; commandeId: any; laboAddress: any; destinationAddress :any};
  seuilHumidite: string;
  errors: string;
  seuilTemperature: string;

  containerForm: FormGroup;
  validation_messages: any;
  tags: Object;

  toggle() {
    this.accordion.toggle();
  }

  private medicaments:Medicament[];
  private  request:ActivateRequest;
  private deleteRequest:DeleteRequest;
  filteredItems: any[] & Medicament[];

  constructor(private _medicamentService:MedicamentService,
              private dialogService: NbDialogService,private store:Store<any>,
              private spinner:NgxSpinnerService,
              private rfidTagService:CarteRfidService,
              private _fb:FormBuilder,
              private tagRfidService:CarteRfidService,
              private msg:MessageService,
              private route:ActivatedRoute,private _web3:EthereumProvider,
              private commandService:CommandService      ) {
    this.filteredItems=[];
    this.containerForm=this._fb.group({
      tagContainer:['',Validators.compose([Validators.required,Validators.minLength(11),Validators.maxLength(11)])],

      seuilHumidite:['',Validators.required],
      seuilTemperature:['',Validators.required],
      destinationAddress:['']
    })

    this.validation_messages=container_validation_messages;
  }

  ngOnInit() {
this.tagRfidService.getAllCard().subscribe((done:any)=>{
  this.tags=done.filter(item=>item.active===true);

  }
)

    this.route.queryParams
      .subscribe(params =>{
        console.log(params);
        console.log(params.commandeId);
        this.commandeId=params.commandeId;



      });
    this.route.params
      .subscribe(params =>{
        console.log(params)
      this.userAddress=(params.id)     });
this.commandService.getCommandeById(this.commandeId).subscribe((res:any)=>{this.medicaments=[];
    Object.keys(res.panier.products).forEach((key) => {
      this.containerForm.controls['destinationAddress'].patchValue(res.addressCommande);

      this.medicaments.push( {...res.panier.products[key],quantity:res.panier.productQuantities[key]})
      console.log(this.medicaments)
    });

this.assignCopy();
  console.log(this.medicaments)

  }

,error=>this.assignCopy());
    this.commandService.getContainerById(this.commandeId).subscribe((res:any )=>{
      this.tagContainer=res.tagContainer;
      }
    );

    this.store.select("command").subscribe(res=>{
      console.log(res);
    });
    this.options = ['Option 1', 'Option 2', 'Option 3'];
    this.filteredOptions$ = of(this.options);




  }

  options: string[];
  filteredOptions$: Observable<string[]>;

checkTagInput(){
  if(this.tag.length<6 )
  {
    this.errors='Tag container doit etre sup a 6 caractéres'
    return false;
  }
  this.errors='';
  return true;

}
  private filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(optionValue => optionValue.toLowerCase().includes(filterValue));
  }

  getFilteredOptions(value: string): Observable<string[]> {
    return of(value).pipe(
      map(filterString => this.filter(filterString)),
    );
  }
  assignCopy(){
    this.filteredItems = Object.assign([], this.medicaments);
  }
  filterItem(value){

  }

addContainer()
{
  this.spinner.show().then(console.log);

  if(this.containerForm.valid){

   let laboAddress=JSON.parse(localStorage.getItem("currentUser")).accountAddress;
    console.log("start")
     this.data={...this.containerForm.value,grossisteAddress:this.userAddress,commandeId:this.commandeId,laboAddress:laboAddress}
this._web3.createContainer(this.data).then(signed=>{
  let data={...this.data,transaction:signed.rawTransaction};
  this.commandService.createContainer(data).subscribe(response=>{
    this.tagContainer=this.containerForm.controls['tagContainer'].value;
this.tagRfidService.disableTagCard(this.tagContainer).subscribe(res=>{


  Swal.fire(
    'Success!',
    '',
    'success'
  )
    this.spinner.hide();

},error => {
  this.spinner.hide()
    Swal.fire(
      'Echec ',
      '',
      'error'
    )

}
)

    console.log(response);
   this.tagContainer= this.containerForm.controls["tagContainer"].value;

  },error => {Swal.fire(
    'Echec!',
    error,
    'error'
  )}); this.spinner.hide()
},)
  }

 else
   this.spinner.hide().then(console.log);

}
  onChange(event) {
    let value=event.target.value;

    if(!value){
      this.assignCopy();
    }
    this.filteredItems = Object.assign([], this.medicaments).filter(
      item => item.name.toLowerCase().indexOf(value.toLowerCase()) > -1
    )


  }

  onSelectionChange($event) {
    this.filteredOptions$ = this.getFilteredOptions($event);
  }




  open(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog);
  }

  confirm(id: any) {

    this._medicamentService.deleteMedicament(id).subscribe(response=>{
      console.log(response)
      this.filteredItems=this.filteredItems.filter( item => (item.id)!==id);

    });





  }






}
