import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LivraisonComponent} from "./livraison/livraison.component";
import {CommandeComponent} from "./commande.component";
import {ListCommandeComponent} from "./list-commande/list-commande.component";
import {PreparationComponent} from "./preparation/preparation.component";
import {MedicamentCommandeComponent} from "./medicament-commande/medicament-commande.component";

const routes: Routes = [{
  path:"",
  component:CommandeComponent,
  children:[{
    path:'',
    component:ListCommandeComponent
  },
    {path:":id/preparation",
    component:PreparationComponent,
    },
    {path:":id/preparation/:id/add",
    component:MedicamentCommandeComponent}]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommandeRoutingModule { }
