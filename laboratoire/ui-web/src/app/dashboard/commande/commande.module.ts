import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommandeRoutingModule } from './commande-routing.module';
import { LivraisonComponent } from './livraison/livraison.component';
import {
  MatBadgeModule,
  MatButtonModule,
  MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatTooltipModule
} from "@angular/material";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {
  NbAccordionModule,
  NbActionsModule, NbButtonModule,
  NbCardModule, NbIconModule,
  NbInputModule,
  NbSelectModule,
  NbUserModule
} from "@nebular/theme";
import { CommandeComponent } from './commande.component';
import {Ng2SmartTableModule} from "ng2-smart-table";
import { CommandesComponent } from './commandes/commandes.component';
import {AvatarModule} from "ngx-avatar";
import {ListCommandeComponent} from "./list-commande/list-commande.component";
import { PreparationComponent } from './preparation/preparation.component';
import { MedicamentCommandeComponent } from './medicament-commande/medicament-commande.component';
import {NgxSpinnerModule} from "ngx-spinner";
import { SuiviComponent } from './suivi/suivi.component';
import { ListCommandesComponent } from './list-commandes/list-commandes.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { SmartTableDatePickerComponent } from './medicament-commande/smart-table-date-picker/smart-table-date-picker.component';
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule} from "@angular/material/core";

@NgModule({
  declarations: [LivraisonComponent, ListCommandeComponent,CommandeComponent, CommandesComponent, PreparationComponent, MedicamentCommandeComponent, SuiviComponent, ListCommandesComponent, SmartTableDatePickerComponent],
    imports: [
        CommonModule,
        CommandeRoutingModule,
        MatFormFieldModule,
        MatIconModule,
        NbCardModule,
        ReactiveFormsModule,
        NbInputModule,
        NbSelectModule,
        NbCardModule,
        MatIconModule,
        Ng2SmartTableModule,
        NbActionsModule,
        MatButtonModule,
        NbAccordionModule,
        AvatarModule,
        MatDividerModule,
        MatInputModule,
        NbUserModule,
        MatBadgeModule,
        NbIconModule,
        MatTooltipModule,
        NbButtonModule,
        FormsModule,
      MatDatepickerModule,
      MatNativeDateModule,
        NgxSpinnerModule
    ],
  entryComponents:[SmartTableDatePickerComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

})
export class CommandeModule { }
