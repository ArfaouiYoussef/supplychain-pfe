import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicamentCommandeComponent } from './medicament-commande.component';

describe('MedicamentCommandeComponent', () => {
  let component: MedicamentCommandeComponent;
  let fixture: ComponentFixture<MedicamentCommandeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicamentCommandeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicamentCommandeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
