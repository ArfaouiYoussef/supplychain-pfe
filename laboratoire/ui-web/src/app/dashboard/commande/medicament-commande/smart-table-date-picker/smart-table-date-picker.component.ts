import { Component, AfterViewInit } from '@angular/core';

import { Cell, DefaultEditor, Editor } from 'ng2-smart-table';

@Component({
  template:'  <input  [(ngModel)]="cell.newValue" [name]="cell.getId()" [disabled]="!cell.isEditable()" (click)="onClick.emit($event)" (keydown.enter)="onEdited.emit($event)" (keydown.esc)="onStopEditing.emit()" nbInput [min]="today"  [matDatepicker]="myDatepicker">' +
    '  <mat-datepicker-toggle  matSuffix [for]="myDatepicker"></mat-datepicker-toggle>' +
    '  <mat-datepicker #myDatepicker></mat-datepicker>' })
export class SmartTableDatePickerComponent extends DefaultEditor implements AfterViewInit {
  today = new Date();

  constructor() {
    super();
  }

  ngAfterViewInit() {
    this.today = JSON.parse(JSON.stringify(this.today)).substring(0, 10);

  }
}
