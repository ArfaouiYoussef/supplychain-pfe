import { Component, OnInit } from '@angular/core';
import {takeWhile} from "rxjs/operators";
import { StatsProgressBarData, ProgressInfo } from '../../@core/data/stats-progress-bar';
import {CommandService} from "../../service/command.service";
import {MessageService} from "../../service/message.service";
import {calculateSizes} from "@angular-devkit/build-angular/src/angular-cli-files/utilities/bundle-calculator";
import {ClientService} from "../../service/client.service";
import {TransporteurService} from "../../service/transporteur.service";

@Component({
  selector: 'drugschain-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent  {
  private alive = true;

  title = 'Angular Charts';

  view: any[] = [1000, 300];

  // options for the chart
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Commandes';
  showYAxisLabel = true;
  yAxisLabel = 'Nombre';
  timeline = true;

  colorScheme = {
    domain: [this.getRandomColor(),this.getRandomColor(), this.getRandomColor(), this.getRandomColor(), this.getRandomColor(), this.getRandomColor()]
  };




  //pie
  showLabels = true;

  progressInfoData: Object;
  list: any[]=[];
  enAttenteCommande:number;
  enExpedierCommande: number;
  enTransitCommande: number;
  single: { "name": string; "value": number; }[];
  transporteurData: { "name": string; "value": number; }[];
  clientData: { "name": string; "value": number; }[];
  transporteurArea: boolean;
  clientArea: boolean;
  commandeArea: boolean=true;
  numberDemande: number;
  numberBlockedUser: any;
  numberTotalUser: any;
  activeUser: any;
  activeTransporteur: number;
  blockedTransporteur: number;
  numberDemandeTransporteur: number;
  enCoursCommande: number;
  ReceivedCommande: number;

  constructor(private commandeService: CommandService,
              private messageService:MessageService
  ,private  clientService:ClientService,
              private transporteurService:TransporteurService) {


  }

  ngOnDestroy() {
    this.alive = true;
  }

  ngOnInit(): void {
    this.transporteurService.getAllTransporteur().subscribe(res=>{

this.activeTransporteur=res.filter(item=>item.accountActive===true &&item.blocked===false).length;
this.blockedTransporteur=res.filter(item=>item.blocked===true).length;
this.numberDemandeTransporteur=res.filter(item=>item.active===false ).length;
      this.transporteurData = [
        {
          "name": "Demande D'approbation",
          "value": !!this.numberDemandeTransporteur?this.numberDemandeTransporteur:0
        },
        {
          "name": "Activer",
          "value": !!this.activeTransporteur?this.activeTransporteur:0
        },
        {
          "name": "Bloquer",
          "value": !!this.blockedTransporteur?this.blockedTransporteur:0
        },

        {
          "name": "Total",
          "value": res.length
        }
      ];
    })
    this.clientService.getAllDemand().subscribe(res=>{
      this.numberDemande=res.length;


      this.clientData = [
        {
          "name": "Demande D'approbation",
          "value": !!this.numberDemande?this.numberDemande:0
        },
        {
          "name": "Activer",
          "value": !!this.activeUser?this.activeUser:0
        },
        {
          "name": "Bloquer",
          "value": !!this.numberBlockedUser?this.numberBlockedUser:0
        },

        {
          "name": "Total",
          "value": this.numberBlockedUser+this.numberDemande+this.activeUser
        }
      ];
    })

    this.clientService.BlockedClient().subscribe(res=>{
      this.numberBlockedUser=res.length;
      this.clientData = [
        {
          "name": "Demande D'approbation",
          "value": !!this.numberDemande?this.numberDemande:0
        },
        {
          "name": "Activer",
          "value": !!this.activeUser?this.activeUser:0
        },
        {
          "name": "Bloquer",
          "value": !!this.numberBlockedUser?this.numberBlockedUser:0
        },

        {
          "name": "Total",
          "value": this.numberBlockedUser+this.numberDemande+this.activeUser
        }
      ];
    });
    this.clientService.getClient().subscribe((res:any)=>{
this.activeUser=res.length;
      this.clientData = [
        {
          "name": "Demande D'approbation",
          "value": !!this.numberDemande?this.numberDemande:0
        },
        {
          "name": "Activer",
          "value": !!this.activeUser?this.activeUser:0
        },
        {
          "name": "Bloquer",
          "value": !!this.numberBlockedUser?this.numberBlockedUser:0
        },

        {
          "name": "Total",
          "value": this.numberBlockedUser+this.numberDemande+this.activeUser
        }
      ];
    })


    this.commandeService.getAllCommand()
      .pipe(takeWhile(() => this.alive))
      .subscribe((data) => {
        this.progressInfoData = data;
        console.log(data)
      });


    this.messageService.getMessage().subscribe(res=>
    {
      this.numberDemande++;
      this.numberTotalUser++;
      this.clientData = [
        {
          "name": "Demande D'approbation",
          "value": !!this.numberDemande?this.numberDemande:0
        },
        {
          "name": "Activer",
          "value": !!this.activeUser?this.activeUser:0
        },
        {
          "name": "Bloquer",
          "value": !!this.numberBlockedUser?this.numberBlockedUser:0
        },

        {
          "name": "Total",
          "value": this.numberBlockedUser+this.numberDemande+this.activeUser
        }
      ];

    })
    this.messageService.getMessageCommande().subscribe(done=>{
      console.log(done);
      if(!!done) {
        this.list.push(done);
        this.enAttenteCommande = Object.assign([], this.list).filter(item => item.status === "EnAttente").length;

        this.enExpedierCommande = Object.assign([], this.list).filter(item => item.status === "Expedie").length;
        this.enTransitCommande = Object.assign([], this.list).filter(item => item.status === "InTransit").length;
        this.ReceivedCommande = Object.assign([], this.list).filter(item => item.status === "Received").length;

        this.single = [
          {
            "name": "En Attente",
            "value": !!this.enAttenteCommande?this.enAttenteCommande:0
          },
          {
            "name": "En Cours",
            "value": !!this.enExpedierCommande?this.enExpedierCommande:0
          },
          {
            "name": "En Transit",
            "value": !!this.enTransitCommande?this.enTransitCommande:0
          },
          {
            "name": "Livrée",
            "value":!! this.ReceivedCommande?this.ReceivedCommande:0
          },
          {
            "name": "Total",
            "value": this.list.length - this.enExpedierCommande
          }
        ];
      }

    })
    this.commandeService.getAllCommand().subscribe((res:any)=>{
      this.list=res;

      this.enAttenteCommande=Object.assign([], this.list).filter(item=>item.status==="EnAttente").length;
      this.enExpedierCommande=Object.assign([], this.list).filter(item=>item.status==="Expedie").length;
      this.enTransitCommande=Object.assign([], this.list).filter(item=>item.status==="InTransit").length;
      this.enCoursCommande=Object.assign([], this.list).filter(item=>item.status==="Encours").length;
      this.ReceivedCommande = Object.assign([], this.list).filter(item => item.status === "Received").length;

      console.log(this.enTransitCommande)
      this.single = [
        {
          "name": "En Attente",
          "value": !!this.enAttenteCommande?this.enAttenteCommande:0
        },



        {
          "name": "En Cours",
          "value": !!this.enCoursCommande?this.enCoursCommande:0
        },
        {
          "name": "En Transit",
          "value": !!this.enTransitCommande?this.enTransitCommande:0
        },
        {
          "name": "Livrée",
          "value":!! this.ReceivedCommande?this.ReceivedCommande:0
        },
        {
          "name": "Total",
          "value": this.list.length - this.enExpedierCommande
        }
      ];
      this.clientData = [
        {
          "name": "Demande D'approbation",
          "value": !!this.numberDemande?this.numberDemande:0
        },
        {
          "name": "Activer",
          "value": !!this.activeUser?this.activeUser:0
        },
        {
          "name": "Bloquer",
          "value": !!this.numberBlockedUser?this.numberBlockedUser:0
        },

        {
          "name": "Total",
          "value": this.numberBlockedUser+this.numberDemande+this.activeUser
        }
      ];

    })

  }


  onSelect($event: any) {

  }
  getRandomColor() {
    var length = 6;
    var chars = '0123456789ABCDEF';
    var hex = '#';
    while(length--) hex += chars[(Math.random() * 16) | 0];
    return hex;
  }

  activate(data) {
    this.colorScheme = {
      domain: [this.getRandomColor(),this.getRandomColor(), this.getRandomColor(), this.getRandomColor(), this.getRandomColor(), this.getRandomColor()]
    };
    console.log(data)
  switch (data.tabTitle.toLowerCase()) {
    case "commandes":
      this.clientArea=false;
      this.transporteurArea=false;
      this.commandeArea=true;
      break;
    case "clients": this.clientArea=true;
    this.transporteurArea=false;
    this.commandeArea=false;
    break;
    case "transporteurs":
      this.clientArea=false;
      this.transporteurArea=true;
      this.commandeArea=false;
    break;

  }
  }
}
