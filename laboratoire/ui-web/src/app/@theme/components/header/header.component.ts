import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import { NbMediaBreakpointsService, NbMenuService, NbSidebarService, NbThemeService } from '@nebular/theme';
import { NB_WINDOW } from '@nebular/theme';
import {Actions, ofType} from "@ngrx/effects";

import { UserData } from '../../../@core/data/users';
import { LayoutService } from '../../../@core/utils';
import {filter, map, takeUntil} from 'rxjs/operators';
import {Subject, Subscription} from 'rxjs';
import {Router} from "@angular/router";
import {MessageService} from "../../../service/message.service";
import {AdminServiceService} from "../../../service/admin-service.service";
import {UserService} from "../../../service/user-service.service";
import {Store} from "@ngrx/store";
import * as Paniers from '../../../dashboard-grossiste/redux/common/store/panier/panier.actions';
import * as Users from '../../../dashboard-grossiste/redux/common/store/notification/notification.actions';

import {AuthService} from "../../../service/auth.service";
import {PushNotificationsService} from 'ng-push';
import {ClientService} from "../../../service/client.service";
import * as Panier from "../../../dashboard-grossiste/redux/common/models/panier";
import {NotificationService} from "../../../service/notification.service";
import {CommandService} from "../../../service/command.service";

export interface DialogData {
  animal: string;
  name: string;
}


@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {
role:boolean;
  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: any;
picture= 'https://www.samaauto.sn/img/logos/admin.png'
  themes = [
    {
      value: 'default',
      name: 'Light',
    },
    {
      value: 'dark',
      name: 'Dark',
    },
    {
      value: 'cosmic',
      name: 'Cosmic',
    },
    {
      value: 'corporate',
      name: 'Corporate',
    },
  ];

  currentTheme = 'default';
  item = [ { title: 'Profile' }, { title: 'Log out' } ];
  itemm=[];

  numberBag:string="0";
  message: any;
  subscription: Subscription;
  userDetails: any;
  list: any;
  users: any;
  clientNumber: any;
clientList:any=[];
  commandeNumber: any;
  commandeList: any=[];
  constructor(private sidebarService: NbSidebarService,
              private _auth:AuthService,
              private menuService: NbMenuService,
              private themeService: NbThemeService,
              private userService: UserData,
              private service:UserService,
              private clientService:ClientService,
              private layoutService: LayoutService,
              private adminService:AdminServiceService,@Inject(NB_WINDOW) private window,
              private router:Router,
              private commandeService:CommandService,
              private notificationService:NotificationService,
              private _pushNotifications: PushNotificationsService,
              private nbMenuService: NbMenuService,
              private store: Store<any>,private action:Actions,
              private breakpointService: NbMediaBreakpointsService,private messageService:MessageService) {
    this.userDetails=this.message=JSON.parse(localStorage.getItem("currentUser"));
    this.clientNumber=0;
    this.commandeNumber=0;

    this.users=[];
    if(this.userDetails.roles[0]==="ROLE_LABORATOIRE"){

      this._pushNotifications.requestPermission();
      this.commandeService.getAllCommand().subscribe((res:any)=>{
this.commandeList=res.filter(item=>item.status==="EnAttente")
        this.commandeNumber=this.commandeList.length;

      })

      this.notificationService.getCommande().subscribe(res=>{
        this.commandeNumber++;
        this.messageService.sendCommandeMessage(res);
        console.log(res);
this.commandeList.push(res)
console.log(res)
        this.notify(res.name,"une nouvelle Commande  \n client:  ");
      })

      this.clientService.getAllDemand().subscribe((res:any)=>{
        if(res.length>0)
        {this.clientNumber+=res.length;

        console.log(res)
        this.clientList=res;}
      })
this.messageService.getUserToRemove().subscribe(id=>{
console.log(id);
  this.clientList=Object.assign([],this.clientList).filter(item=>item.id!==id.id)
console.log(this.clientList.length)
})
      this.messageService.getCommandeToRemove().subscribe(res=>{
        this.commandeList=this.commandeList.filter(item=>item.id!==res.id);
      })


    }




//----------------

    this.action.pipe(
      ofType('ADD_COMMAND_SUCCESS'))
      .
      subscribe(   { next: (value:any)=>{
this.list=[];
this.itemm=[];
this.numberBag='0';
        },

        error: err => console.error(err),
        complete: () => {
      }});


    this.action.pipe(
      ofType('GET_USERS_SUCCESS'))
      .
    subscribe(   { next: (value:any)=>{ if(!!value.payload){
    //  this.numberBag=this.users.length;
          this.numberBag=this.users.length;

          this.notify(value.payload.username,"'Vous avez une nouvelle demande d'ajout  '")
          this.messageService.sendMessage(value.payload);
console.log(value.payload)
          if(value.payload.roles[0].name==="ROLE_GROSSISTE") {
            this.clientList.push(value.payload)
          }
          }},

      error: err => console.error(err),
      complete: () => {
       console.log("good job man")}});
  }
  notify(username,text){ //our function to be called on click
    let options = { //set options
      body: ""+username,
      icon: "https://www.icone-png.com/png/8/7523.png" //adding an icon
    }
    this._pushNotifications.create(text, options).subscribe( //creates a notification
      res => console.log(res),
      err => console.log(err)
    );
  }

  ngOnInit() {

    this.clientService.getUserById(this.userDetails.id).subscribe((response:any)=>{
      this.picture=response.photoUrl;
      console.log(this.picture)
      this.userDetails.username=response.username;
    })
    this.messageService.getProfile().subscribe(res=>{

      if(!!res )
{this.picture=res.user.photoUrl;
      this.userDetails.username=res.user.username;
console.log(res.user.photoUrl)}
    })
    this.role=this.userDetails.roles[0];

    this.numberBag=!!this.users?this.users.length.toString():"0";
    this.store.dispatch(new Paniers.GetPanierById(JSON.parse(localStorage.getItem("currentUser")).panier.id));
this.store.dispatch(new Users.GetUsers());



console.log(this.users)
    this.store.select("panier").subscribe(res=> {

    console.log(res.panier.products);
      this.itemm=[];
      if(!!res.panier.products) {


console.log([res.panier.productQuantities]);
        Object.keys(res.panier.products).forEach((key) => {
          this.itemm.push( {...res.panier.products[key],count:res.panier.productQuantities[key]})
        });
        this.numberBag=Array.from(this.itemm).length.toString()

      }});





    this.nbMenuService.onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'my-context-menu'),
        map(({ item: { title } }) => title),
      )
      .subscribe(title => {
      switch(title){
        case"Profile":
        if(this.userDetails.roles[0]==="ROLE_LABORATOIRE")  this.router.navigate(["/dashboard/compte"]);
        else this.router.navigate(["/dashboard-grossiste/compte"]);
        break;
        case"Log out":this.logOut();break;

      }
      });

    this.message=this.userDetails;
    this.currentTheme = this.themeService.currentTheme;

    this.userService.getUsers()
      .pipe(takeUntil(this.destroy$))
      .subscribe((users: any) => this.user = users.nick);

    const { xl } = this.breakpointService.getBreakpointsMap();
    this.themeService.onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$),
      )
      .subscribe((isLessThanXl: boolean) => this.userPictureOnly = isLessThanXl);

    this.themeService.onThemeChange()
      .pipe(
        map(({ name }) => name),
        takeUntil(this.destroy$),
      )
      .subscribe(themeName => this.currentTheme = themeName);

  this.messageService.getMessage().subscribe((msg:any)=>{
 this.message=msg.token;
  console.log(msg)})
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  changeTheme(themeName: string) {
    this.themeService.changeTheme(themeName);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  navigateHome() {
    this.menuService.navigateHome();
    return false;
  }
  logOut(){
    this.store.dispatch(new Paniers.Destruct());
    this._auth.logout();
   this.router.navigate(['/auth/login']);
  }


}
