import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardGrossisteComponent} from "./dashboard-grossiste.component";

const routes: Routes = [
  {
    path:'',
    component:DashboardGrossisteComponent,
    children:[
      {path:'produit',
      loadChildren:()=>import("./produit/produit.module").then(m=>m.ProduitModule)},
      {path:'compte',
      loadChildren:()=>import("../dashboard/parametres/parametres.module").then(m=>m.ParametresModule)}
    ]

  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardGrossisteRoutingModule { }
