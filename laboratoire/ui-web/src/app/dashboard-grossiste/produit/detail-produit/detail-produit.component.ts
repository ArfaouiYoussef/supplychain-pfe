import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import * as Medicaments from "../../redux/common/store/products/products.actions";
import {ActivatedRoute, Router} from "@angular/router";
import {Medicament} from "../../redux/common/models/medicament";
import * as Paniers from "../../redux/common/store/panier/panier.actions";
import {PanierEffects} from "../../redux/common/store/panier/panier.effects";
import {NbToastrService} from '@nebular/theme';
import {Actions, ofType} from "@ngrx/effects";
import {Observable} from 'rxjs';
import "rxjs-compat/add/operator/take";
import {FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'ngx-detail-produit',
  templateUrl: './detail-produit.component.html',
  styleUrls: ['./detail-produit.component.scss']
})
export class DetailProduitComponent implements OnInit {
  medicament:Medicament;
  index: number;
  quantity:FormControl = new FormControl([],Validators.required);
  submit:boolean=false;
  data: any;
  count:any=0;
  products$: Observable<any>;
  msg: string;

  constructor(private store: Store<any>,private toastrService: NbToastrService,private activatedRoute:ActivatedRoute,
              private panierEffects:PanierEffects,private router:Router,
              private action:Actions) {

  }

  ngOnInit() {

    this.action.pipe(
      ofType('ADD_PANIER_SUCCESS'))
      .take(1).subscribe(   { next: value => console.log("next"+value),

  error: err => console.error(err),
      complete: () => {this.showToast();
this.router.navigate(["/dashboard-grossiste/produit/panier"])}});

    this.activatedRoute.params.subscribe(res => {
      this.store.dispatch(new Medicaments.GetMedicamentById(res.id));



      this.store.select('medicament').subscribe(response => {
        this.medicament = response.medicament;

      }, error => {
      });

    });

  }
  remove() {

  }

  showToast() {
    this.index += 1;
    this.toastrService.show(
      status || 'Success',
      `Produit a été ajouté à votre panier  `,
    )
  }
add(){
if(this.quantity.valid) {
  if(this.quantity.value<=this.medicament.stock){
    this.msg='';
  this.submit = true;
  this.store.dispatch(new Paniers.AddPanier({
    medicament: this.medicament,
    prixTotal: 500,
    panier: JSON.parse(localStorage.getItem("currentUser")).panier.id,
    quantite: this.quantity.value,
    userId: JSON.parse(localStorage.getItem("currentUser")).id
    , dispo: this.medicament.quantity

  }));}
  else {
    this.msg="Qte non disponible"
  }
}
else {
  this.msg='Qte vide !! '
}
}




}
