import {Component, OnInit, TemplateRef} from '@angular/core';
import {Store} from "@ngrx/store";
import * as Command from "../../redux/common/store/commande/commande.actions";
import {Actions, ofType} from '@ngrx/effects';
import {CommandService} from "../../../service/command.service";
import {NbDialogService} from "@nebular/theme";

@Component({
  selector: 'ngx-commande',
  templateUrl: './commande.component.html',
  styleUrls: ['./commande.component.scss']
})
export class CommandeComponent implements OnInit {
  listCommande: any;
  selectedItem:any;
  decodeAffectationTx: any;
  decodeIntransit: any;
  decodeReceived: any;
  transactionInTransit: any;
  transactionReceived: any;
  affectationTransaction: any;
  filteredItems: any=[];

  constructor(private commandeService:CommandService,
               private  dialogService:NbDialogService) {

  }
  selectContainerTransaction(res) {
    this.decodeAffectationTx=null;
    this.decodeIntransit=null;
    this.decodeReceived=null;

    console.log("hello");
    console.log(res);
    if(!!res.affectationTransaction){
      this.commandeService.getTransactionDetails(res.affectationTransaction).subscribe(res=>{
        this.decodeAffectationTx=res;
        console.log(res)

      })
    }

    if(!!res.transactionInTransit){
      this.commandeService.getTransactionDetails(res.transactionInTransit).subscribe(res=>{
        this.decodeIntransit=res;
        console.log(res)

      })
    }
    if(!!res.transactionReceived){
      this.commandeService.getTransactionDetails(res.transactionReceived).subscribe(res=>{
        this.decodeReceived=res;
        console.log(res)

      })
    }
    this.transactionInTransit=!!res.transactionInTransit?res.transactionInTransit:"";
    this.transactionReceived=!!res.transactionInTransit?res.transactionInTransit:"";
    this.affectationTransaction=!!res.affectationTransaction?res.affectationTransaction:"";

  }
  open(dialog: TemplateRef<any>) {

    this.dialogService.open(dialog);
  }



  ngOnInit(): void {
this.commandeService.getCommandByPublicAdress().subscribe(res=>{
  this.listCommande=res;
this.assignCopy();
})
  }


  onChange(event) {
    let value=event.target.value;

    if(!value){
      this.assignCopy();
    }
    this.filteredItems = Object.assign([], this.listCommande).filter(
      item => item.id.toLowerCase().indexOf(value.toLowerCase()) > -1
    )


  }
  state(event) {
    console.log(event)
    this.status=event;
    this.assignCopy();
    this.filteredItems= this.listCommande.filter(item=>item.status==event)
  }
  search(event){
    let value=event.target.value;
    if(!value){
      console.log(this.status)

      this.filteredItems= Object.assign([], this.listCommande).filter(item=>item.state===this.status)
      console.log(this.filteredItems)
    }
    else {
      this.filteredItems=  Object.assign([], this.listCommande).filter(item=>item.state==this.status)

      this.filteredItems = Object.assign([], this.filteredItems).filter(
        item =>  item.tagContainer.toLowerCase().indexOf(value.toLowerCase()) > -1
      )
      console.log(this.filteredItems)
    }
  }

  status(status: any) {
        throw new Error("Method not implemented.");
    }
  assignCopy(){
    this.filteredItems = Object.assign([], this.listCommande);
  }
}
