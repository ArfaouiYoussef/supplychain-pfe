import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardGrossisteRoutingModule } from './dashboard-grossiste-routing.module';
import { DashboardGrossisteComponent } from './dashboard-grossiste.component';
import {ThemeModule} from "../@theme/theme.module";
import {NbMenuModule} from "@nebular/theme";
import { ProfilComponent } from './profil/profil.component';


@NgModule({
  declarations: [DashboardGrossisteComponent, ProfilComponent],
  imports: [
    CommonModule,
    DashboardGrossisteRoutingModule,
    ThemeModule,
    NbMenuModule,

  ]
})
export class DashboardGrossisteModule { }
