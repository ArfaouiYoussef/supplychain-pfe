import { Injectable } from '@angular/core';
import {Actions, createEffect, Effect, ofType} from '@ngrx/effects';


import {
  PanierActions,

  GetPanierFailed,

  GetPanierById,
  PanierActionType,
  GetPanierSuccess,
  GetPanierByIdSuccess,
  AddPanierSuccess,
  AddPanierFailed,
  DeletePanierSuccess, DeletePanierFailed, UpdatePanier, UpdatePanierFailed, UpdatePanierSuccess,
} from './panier.actions';
import {switchMap, catchError, map, mergeMap, tap} from 'rxjs/operators';
import { of } from 'rxjs';

import { Panier } from '../../models/panier';
import {TodosService} from '../../services/todos/todos.service';
import {PanierService} from "../../../../../service/panier.service";

@Injectable()
export class PanierEffects {

  constructor(
    private actions$: Actions,
    private todosService: TodosService,
    private medicamentService:PanierService,

  ) { }

  getPanier$ =createEffect(() => this.actions$.pipe(
    ofType(PanierActionType.GET_PANIER),
    mergeMap((action) =>
      this.medicamentService.getMedicament(action['payload']).pipe(

        map((products: Array<Panier>) => new GetPanierSuccess(products),console.log(action["payload"])),
        catchError(error => of(new GetPanierByIdSuccess(error)))
      )
    )
  ));




  getPANIERById$ =createEffect(() => this.actions$.pipe(
    ofType(PanierActionType.GET_PANIER_BY_ID),
    mergeMap((action) => this.medicamentService.getPanierById(action["payload"]).pipe(
        map((products: Panier) => new GetPanierByIdSuccess(products)),
        catchError(error => of(new GetPanierFailed(error)))
      )

    )

));



  addPANIER$ =createEffect(() => this.actions$.pipe(
    ofType(PanierActionType.ADD_PANIER),
    mergeMap((action) => {

      return this.medicamentService.addPanier(action['payload']).pipe(
        map((panier: Panier) => new AddPanierSuccess(panier)), tap(() => {
          console.log('getMockDataEffect Finished')
        }),
        catchError(error => of(new AddPanierFailed(error))),
      )
    }),
  ));
  deleteMedicamentInBasket$ =createEffect(() => this.actions$.pipe(
    ofType(PanierActionType.DELETE_PANIER),
    mergeMap((action) =>
      this.medicamentService.deleteMedicamentInBasket(action['payload']).pipe(
        map((medicamentInBasketId: string) => new DeletePanierSuccess(medicamentInBasketId)),
        catchError(error => of(new DeletePanierFailed(error)))
      )
    )
  ));
  updatePANIER$ =createEffect(()=> this.actions$.pipe(
    ofType(PanierActionType.UPDATE_PANIER),
    mergeMap((action) =>
      this.medicamentService.updateMedicamentInBasket(action['payload']).pipe(
        map((panier: any) => new UpdatePanierSuccess(panier)),
        catchError(error => of(new UpdatePanierFailed(error)))
      )
    )
));

/*  @Effect()


  @Effect()
  deletePANIER$ = this.actions$.pipe(
    ofType(PANIERActionType.DELETE_PANIER),
    switchMap((action) =>
      this.todosService.updateAPITodo(action['payload']).pipe(
        map((todoId: number) => new DeleteMedicmanetSuccess(todoId)),
        catchError(error => of(new DeleteMedicmanetFailed(error)))
      )
    )
  );
*/
}
