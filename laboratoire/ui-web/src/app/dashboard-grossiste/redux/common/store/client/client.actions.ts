import {Action, createAction, props} from '@ngrx/store';
import {User} from "../../../../../model/user";

const API_SUCCESS_ACTION = 'ADD_ USERS_SUCCESS';

// this will be dispatched from some component or service
// these will be dispatched by the Effect result
export const ApiSuccess = createAction(API_SUCCESS_ACTION, props<{ data: any }>());

export enum UsersActionType {
  GET_USERS = 'GET_USERS',
  GET_USERS_SUCCESS = 'GET_USERS_SUCCESS',
  GET_USERS_FAILED = 'GET_ USERS_FAILED',
  DESTRUCT = 'DESTRUCT',

}

export class GetUsers implements Action {
  readonly type = UsersActionType.GET_USERS;
}




export class Destruct implements Action {
  readonly type = UsersActionType.DESTRUCT;
}

export class GetUsersSuccess implements Action {
  readonly type = UsersActionType.GET_USERS_SUCCESS;
  constructor(public payload: Array<User>) { }
}

export class GetUsersFailed implements Action {
  readonly type = UsersActionType.GET_USERS_FAILED;
  constructor(public payload: string) { }
}


export  type  UsersActions = GetUsersSuccess |GetUsersFailed|GetUsersFailed|GetUsers

