import { Injectable, NgZone } from '@angular/core';
import {Observable, throwError} from "rxjs";
import {environment} from "../../environments/environment.prod";
import {config} from "../config";

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private zone: NgZone){}

  /**
   * Subscribe to the teams update Server Sent Event stream
   */
  getUsers(): Observable<any> {
    return new Observable((observer) => {
      let url =(config.notification+'/api/auth/demande');
      let eventSource = new EventSource(url);

      eventSource.onmessage = (event) => {
        let json = JSON.parse(event.data);
        if (json !== undefined && json !== '') {
          this.zone.run(() => observer.next(json));
        }
      };

      eventSource.onerror = (error) => {
        if (eventSource.readyState === 0) {
          console.log('The stream has been closed by the server.');
          eventSource.close();
          observer.complete();
        } else {
          observer.error('EventSource error: ' + error);
        }
      }
    });
  }

  getCommande(): Observable<any> {
    return new Observable((observer) => {
      let url = (config.notification+'/api/command/notification');
      let eventSource = new EventSource(url);

      eventSource.onmessage = (event) => {
        let json = JSON.parse(event.data);
        if (json !== undefined && json !== '') {
          this.zone.run(() => observer.next(json));
        }
      };

      eventSource.onerror = (error) => {
        if (eventSource.readyState === 0) {
          console.log('The stream has been closed by the server.');
          eventSource.close();
          observer.complete();
        } else {
          observer.error('EventSource error: ' + error);
        }
      }
    });
  }


  getContainers(): Observable<any> {
    return new Observable((observer) => {
      let url = (config.notification+'/api/container/notification');
      let eventSource = new EventSource(url);

      eventSource.onmessage = (event) => {
        let json = JSON.parse(event.data);
        if (json !== undefined && json !== '') {
          this.zone.run(() => observer.next(json));
        }
      };

      eventSource.onerror = (error) => {
        if (eventSource.readyState === 0) {
          console.log('The stream has been closed by the server.');
          eventSource.close();
          observer.complete();
        } else {
          observer.error('EventSource error: ' + error);
        }
      }
    });
  }
}
