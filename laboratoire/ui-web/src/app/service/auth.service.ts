import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject} from "rxjs";
import {config} from "../config";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
    currentUserSubject: any;
    currentUser:any;
  constructor(private _http:HttpClient) {
      this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
      this.currentUser = this.currentUserSubject.asObservable();

  }
    public get currentUserValue(): any {
        return this.currentUserSubject.value;
    }



signin(data){
 delete data.privateKey;
    return this._http.post(config.apiUrl1+"/api/auth/signin",data);

}
  signup(data){

      return this._http.post<any>(config.apiUrl1+"/api/auth/signup",data);

}
  signup2(data,tx){

    return this._http.post<any>(config.apiUrl1+"/api/auth/signup",data);

  }
    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.clear();

        this.currentUserSubject.next(null);
    }
getUser(data){
    return this._http.post<any>(config.apiUrl1+"/api/auth/user",data);

}
}
