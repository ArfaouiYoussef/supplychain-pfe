const config=require('../config');
const InputDataDecoder = require('ethereum-input-data-decoder');

module.exports =(app,web3)=>(
    {
    
        getContainerData: async (req, res, next) => {
            try {
                let contractAddress= req.body.contractAddress;
                let tagContainer=req.body.tagContainer;

                let contract = new web3.eth.Contract(config.contract.SampleToken.abi_file, contractAddress);
                contract.methods.getContainer(tagContainer).call().then(function(response) {
                  const data={
                    id: response.id,
                    name: response.name,
                    dateImport: response.dateImport,
                    dateImport: response.dateImport,
                    dateExport: response.dateExport,
                    temprature: response.temprature,
                    humidite:response.humidite,
                    owner:response.owner,
                    saler: response.saler,
                    medicaments: response.medicaments,
                    state: response.state,
                    transporteur: response.transporteur,
                    device: response.device,
                  }
                        res.status(200).json(data)

                 });
      
      
      
            } catch (error) {
                console.log(error.message);
            }
        },
    
        getTransactionDetails: async (req, res) => {
            try {
                const decoder = new InputDataDecoder(config.contract.SampleToken.abi_file);


                console.log(req);
                let transactionHash=req.body.transactionHash;
                console.log(transactionHash);
                
       web3.eth.getTransaction(req.body.transactionHash).then(data=>{
        
        const result = decoder.decodeData(data.input);
    
        
        web3.eth.getBlock(data.blockNumber).then(block=>{
web3.eth.getTransactionReceipt(transactionHash).then(receipt=>{

    let response={...result,data,block,receipt}


    res.status(200).send(response);

})
           
        }
        );
      

     })
    




      
      
      
            } catch (error) {
                console.log(error.message);
            }
        },
    
    });
    