pragma solidity ^0.4.20;

contract Counter {
    int counter; // Global state

    event CounterIncremented(address indexed _by, int _newValue);
    event CounterDecremented(address indexed _by, int _newValue);

    function increment(int _value) public {
        counter = counter + _value;
        emit CounterIncremented(msg.sender, counter);
    }

    function deincrement(int _value) public {
        counter = counter - _value;
        emit CounterDecremented(msg.sender, counter);
    }
}
