package com.rest.api.payload.request;

import javax.validation.constraints.NotBlank;

public class DeleteMedicamentInBasketRequest {
    @NotBlank
    private String panierId;
    @NotBlank
    private String medicamentInBasket;

    public String getPanierId() {
        return panierId;
    }

    public void setPanierId(String panierId) {
        this.panierId = panierId;
    }

    public String getMedicamentInBasket() {
        return medicamentInBasket;
    }

    public void setMedicamentInBasket(String medicamentInBasket) {
        this.medicamentInBasket = medicamentInBasket;
    }

    @Override
    public String toString() {
        return "DeleteMedicamentInBasketRequest{" +
                "panierId='" + panierId + '\'' +
                ", medicamentInBasket='" + medicamentInBasket + '\'' +
                '}';
    }
}
