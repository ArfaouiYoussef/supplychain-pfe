package com.rest.api.repository;

import com.rest.api.models.ClasseTherapeutique;
import org.springframework.data.mongodb.repository.MongoRepository;

import javax.jws.soap.SOAPBinding;
import java.util.Optional;


public interface ClassTherapeutiqueRepository extends MongoRepository<ClasseTherapeutique, String> {
Optional<ClasseTherapeutique> findByName(String name);
void deleteByName(String name);
}
