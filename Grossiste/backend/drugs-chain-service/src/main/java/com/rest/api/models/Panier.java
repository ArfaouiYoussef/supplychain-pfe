package com.rest.api.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Document(collection = "panier")

public class Panier {
    @Id
    private String id;
    @DateTimeFormat(style = "M-")
    @CreatedDate
    private Date createdDate;

    public List<MedicamentInBasket> getMedicamentInBaskets() {
        return medicamentInBaskets;
    }

    public void setMedicamentInBaskets(List<MedicamentInBasket> medicamentInBaskets) {
        this.medicamentInBaskets = medicamentInBaskets;
    }


    @DBRef
    private List<MedicamentInBasket> medicamentInBaskets = new ArrayList<>();
    private Number prixTotal=0;
    @CreatedDate
    private Timestamp createdAt;
    @LastModifiedDate
    private Timestamp lastModified;

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    private int quantite;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



    public Number getPrixTotal() {
        return prixTotal;
    }

    public void setPrixTotal(Number prixTotal) {
        this.prixTotal = prixTotal;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getLastModified() {
        return lastModified;
    }

    public void setLastModified(Timestamp lastModified) {
        this.lastModified = lastModified;
    }

    public HashMap<String, Medicament> products=new HashMap<>();

    public HashMap<String, Medicament> getProducts() {
        return products;
    }

    public void setProducts(HashMap<String, Medicament> products) {
        this.products = products;
    }

    public HashMap<String, Integer> getProductQuantities() {
        return productQuantities;
    }

    public void setProductQuantities(HashMap<String, Integer> productQuantities) {
        this.productQuantities = productQuantities;
    }

    public HashMap<String, Integer> productQuantities=new HashMap<>();

    public void addProduct(Medicament product){

        //Check if the product is in the HashMap
System.out.println(this.products);
if(product!=null)
        this.products.put(product.getId(), product);

    }

    public void setProductQuantity(Medicament product,int qte){
        String productId = product.getId();

        if(this.productQuantities.containsKey(productId) ){

            this.productQuantities.put(productId, qte);

        }
        else {
            // init the product quantities if key not found
            this.productQuantities.put(productId, qte);
        }
        this.prixTotal = this.prixTotal.doubleValue() + (product.getMedicamentPrice().doubleValue()*qte);
    }


    public void addProductQuantity(Medicament product,int qte){
        String productId = product.getId();
        if(this.productQuantities.containsKey(productId)){
            int quantity = this.productQuantities.get(productId);
            quantity=quantity+qte;
            this.productQuantities.put(productId, quantity);

        }
        else {
            // init the product quantities if key not found
            this.productQuantities.put(productId, qte);
        }
        this.prixTotal = this.prixTotal.doubleValue() + (product.getMedicamentPrice().doubleValue()*qte);
    }

    public void removeProduct(String productId){
        if(this.products.containsKey(productId)){
            this.products.remove(productId);
        }
    }
public  void clear(){
        this.productQuantities.clear();
        this.getProducts().clear();
        this.prixTotal=0;
        this.quantite=0;
}
    public void removeProductQuantity(Medicament product,int qte){

        String productId = product.getId();
        if(this.productQuantities.containsKey(productId)){
            int quantity = this.productQuantities.get(productId);
           quantity= quantity-qte;
           System.out.println(quantity);
            //remove datas from the HashMaps when quantity is too low
            if(quantity <1){
                this.productQuantities.remove(productId);
                this.removeProduct(productId);
            }
            else {
                this.productQuantities.put(productId, quantity);
            }
            this.prixTotal =    this.prixTotal.doubleValue() -product.getMedicamentPrice().doubleValue();
        }
    }


    @Override
    public String toString() {
        return "Panier{" +
                "id='" + id + '\'' +
                ", medicamentInBaskets=" + medicamentInBaskets +
                ", prixTotal='" + prixTotal + '\'' +
                ", createdAt=" + createdAt +
                ", lastModified=" + lastModified +
                ", quantite=" + quantite +
                '}';
    }
}

