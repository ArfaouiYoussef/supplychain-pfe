package com.rest.api.models;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Date;
import java.util.List;

@Document(collection = "panierMedicament")
public class MedicamentPanier {
    @Id
    private String id;
    @DateTimeFormat(style = "M-")
    @CreatedDate
    private Date createdDate;
@DBRef
    Medicament  medicament;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Medicament getMedicament() {
        return medicament;
    }

    public void setMedicament(Medicament medicament) {
        this.medicament = medicament;
    }

    public Long getQuantite() {
        return quantite;
    }

    public void setQuantite(Long quantite) {
        this.quantite = quantite;
    }

    private  Long quantite;
}
