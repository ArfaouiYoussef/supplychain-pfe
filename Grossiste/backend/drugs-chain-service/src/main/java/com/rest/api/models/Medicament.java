package com.rest.api.models;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.util.ArrayList;

@Document(collection = "medicament")

public class Medicament {
    @Id
    private String id;
    @NotBlank
    @Size(max = 20)
    private String name;
    @NotBlank
    private String  indication;
    @NotBlank
    private String  composition;
    @NotBlank
    @Size(max = 20)
    private String dosage;

    public String getOriginTransaction() {
        return originTransaction;
    }

    public void setOriginTransaction(String originTransaction) {
        this.originTransaction = originTransaction;
    }

    @NotBlank
    @Size(max = 120)

private String originTransaction;
    private ArrayList<String> NumeroLots=new ArrayList<>();
    private String formegalenique;
    @DateTimeFormat(style = "M-")
    @CreatedDate
    private Date createdDate;
    public String getDateExpiration() {
        return dateExpiration;
    }

    public void setDateExpiration(String dateExpiration) {
        this.dateExpiration = dateExpiration;
    }

    private  String dateExpiration;
    private  String amm;

private int stock=0;
private  String numeroLot;

private String serialNumber;

    public String getNumeroLot() {
        return numeroLot;
    }

    public void setNumeroLot(String numeroLot) {
        this.numeroLot = numeroLot;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndication() {
        return indication;
    }

    public void setIndication(String indication) {
        this.indication = indication;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getFormegalenique() {
        return formegalenique;
    }

    public void setFormegalenique(String formegalenique) {
        this.formegalenique = formegalenique;
    }

    public ClasseTherapeutique getClasseTherapeutique() {
        return classeTherapeutique;
    }

    public void setClasseTherapeutique(ClasseTherapeutique classeTherapeutique) {
        this.classeTherapeutique = classeTherapeutique;
    }

    public String getPresentation() {
        return presentation;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }



    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }



    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @DBRef
    private ClasseTherapeutique classeTherapeutique;
    @NotBlank
    @Size(max = 200)
    private String presentation;
    public MultipartFile getPhoto() {
        return photo;
    }

    public void setPhoto(MultipartFile photo) {
        this.photo = photo;
    }

    private MultipartFile photo;

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    private double quantity;
    @Size(max = 200)
    private String marque;

    public Number getMedicamentPrice() {
        return medicamentPrice;
    }

    public void setMedicamentPrice(Number medicamentPrice) {
        this.medicamentPrice = medicamentPrice;
    }

    private Number medicamentPrice;
    @Size(max = 200)
    private String type ="model";
    private String photoUrl;




    public String getComposition() {
        return composition;
    }

    public void setComposition(String composition) {
        this.composition = composition;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }



    public int getStock() { return this.stock; }

    public void setStock(int value) { this.stock = value; }

    public boolean removeStock(){
        if(this.stock > 0){
            this.stock--;
            return true;
        }
        else {
            return false;
        }
    }

    public boolean removeStock(int value){
        if(this.stock - value >= 0 ){
            this.stock -= value;
            return true;
        }
        else {
            return false;
        }
    }

    public void addStock(){
        this.stock++;
    }

    public void addStock(int value){
        this.stock += value;
    }



    public String getAmm() {
        return amm;
    }

    public void setAmm(String amm) {
        this.amm = amm;
    }

    public ArrayList<String> getNumeroLots() {
        return NumeroLots;
    }

    public void setNumeroLots(ArrayList<String> numeroLots) {
        NumeroLots = numeroLots;
    }
}
