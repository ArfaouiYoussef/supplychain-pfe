package com.rest.api.models;

public enum ERole {
  ROLE_PATIENT,
  ROLE_PHARMACIE,
  ROLE_GROSSISTE,
  ROLE_TRANSPORTEUR,
  ROLE_LABORATOIRE
}



