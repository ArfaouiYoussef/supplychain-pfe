package com.rest.api.repository;

import com.rest.api.models.Container;
import com.rest.api.models.TagRfid;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TagRfidRepository extends MongoRepository<TagRfid, String> {
Boolean existsByTag(String tag);
Optional<TagRfid> findByTag(String address);

}
