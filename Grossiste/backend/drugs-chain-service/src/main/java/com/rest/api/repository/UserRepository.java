package com.rest.api.repository;

import java.util.List;
import java.util.Optional;

import com.rest.api.models.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.rest.api.models.User;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends MongoRepository<User, String> {
  Optional<User> findByUsername(String username);
  Optional<User> findByEmail(String username);
  List<User> findByisAccountActive(boolean active);

  List<User> findByisBlockedAndRoles(boolean active,Role roles);

  Boolean existsByAccountAddress(String address);
List<User> findByisAccountActiveAndRoles(boolean active,Role roles);
Optional<User> findByAccountAddress(String address);
  Optional<User> findByAccountAddressAndEmail(String address,String email);

    Boolean existsByUsername(String username);
Boolean existsByAccountAddressAndEmail(String address,String email);
  Boolean existsByEmail(String email);
  List<User> findByRoles(Role roles);

  Boolean existsByisBlockedAndId(boolean active,String id );


}
