package com.rest.api.payload.response;

import com.rest.api.models.Panier;

import java.util.List;

public class JwtResponse {
	private String token;
	private String type = "Bearer";
	private String id;
	private String accountAddress;
	private String username;
	private String email;
	private List<String> roles;
	private String smartContract;

	public String getSmartContract() {
		return smartContract;
	}

	public void setSmartContract(String smartContract) {
		this.smartContract = smartContract;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public Panier getPanier() {
		return panier;
	}

	public void setPanier(Panier panier) {
		this.panier = panier;
	}

	private Panier panier;
	public JwtResponse(String accessToken, String id, String username, String email,String smartContract,String accountAddress, List<String> roles,Panier panier) {
		this.token = accessToken;
		this.id = id;
		this.username = username;
		this.email = email;
		this.smartContract=smartContract;
		this.accountAddress=accountAddress;
		this.roles = roles;
	this.panier=panier;
	}

	public String getAccessToken() {
		return token;
	}

	public void setAccessToken(String accessToken) {
		this.token = accessToken;
	}

	public String getTokenType() {
		return type;
	}

	public void setTokenType(String tokenType) {
		this.type = tokenType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<String> getRoles() {
		return roles;
	}

	public String getAccountAddress() {
		return accountAddress;
	}

	public void setAccountAddress(String accountAddress) {
		this.accountAddress = accountAddress;
	}
}
