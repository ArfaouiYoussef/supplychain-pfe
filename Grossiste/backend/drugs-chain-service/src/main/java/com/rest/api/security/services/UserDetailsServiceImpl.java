package com.rest.api.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rest.api.models.User;
import com.rest.api.repository.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	UserRepository userRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String accountAddress) throws UsernameNotFoundException {
		User user = userRepository.findByAccountAddress(accountAddress)
				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + accountAddress));

		return UserDetailsImpl.build(user);
	}

}
