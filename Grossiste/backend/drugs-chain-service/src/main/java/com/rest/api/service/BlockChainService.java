package com.rest.api.service;


import com.rest.api.payload.response.MessageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.ECKeyPair;
import org.web3j.crypto.RawTransaction;
import org.web3j.crypto.TransactionEncoder;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.core.methods.response.EthGetTransactionReceipt;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.exceptions.TransactionException;
import org.web3j.tx.Transfer;
import org.web3j.tx.gas.DefaultGasProvider;
import org.web3j.utils.Convert;
import org.web3j.utils.Numeric;

import javax.xml.ws.ServiceMode;
import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

@Service
public class BlockChainService {
    @Autowired
    private Web3j web3j;
    public TransactionReceipt sendTransaction(String transaction) throws InterruptedException, ExecutionException, IOException {


        EthSendTransaction ethSendTransaction = web3j.ethSendRawTransaction(transaction).sendAsync().get();
if(ethSendTransaction.hasError())
        System.out.println(ethSendTransaction.getError().getMessage());


        Optional<TransactionReceipt> transactionReceipt = null;
        String transactionHash = ethSendTransaction.getTransactionHash();
        do {
            System.out.println("checking if transaction " + transactionHash + " is mined....");
            EthGetTransactionReceipt ethGetTransactionReceiptResp = web3j.ethGetTransactionReceipt(transactionHash).send();
            transactionReceipt = ethGetTransactionReceiptResp.getTransactionReceipt();
        } while (!transactionReceipt.isPresent());

        System.out.println("Transaction " + transactionHash + " was mined in block # " + transactionReceipt.get().isStatusOK());
return transactionReceipt.get();




    }

}