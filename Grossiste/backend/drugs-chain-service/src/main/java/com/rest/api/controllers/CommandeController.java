package com.rest.api.controllers;

import com.rest.api.models.Commande;
import com.rest.api.models.Panier;
import com.rest.api.models.Status;
import com.rest.api.models.User;
import com.rest.api.payload.request.CommandeRequest;
import com.rest.api.payload.response.MessageResponse;
import com.rest.api.repository.CommandeRepository;
import com.rest.api.repository.PanierRepository;
import com.rest.api.repository.UserRepository;
import com.rest.api.service.CommandeNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@RestController
@RequestMapping("/api/command")
public class CommandeController {
    public static final Map<String, SseEmitter> sses = new ConcurrentHashMap<>();

@Autowired
    CommandeRepository commandeRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    PanierRepository panierRepository;
@Autowired
    CommandeNotificationService commandeNotificationService;

@PostMapping()
    public ResponseEntity<?> addCommande( @RequestBody CommandeRequest commandeRequest){
    System.out.println(commandeRequest.getName());
  if(userRepository.findById(commandeRequest.getUserId()).isPresent()){
User user=userRepository.findById(commandeRequest.getUserId()).get();
System.out.println(user);
Commande commande =new Commande();

commande.setPanier(commandeRequest.getPanier());
commande.setAddressCommande(commandeRequest.getAddressCommande());
commande.setLat(commandeRequest.getLat());
commande.setName(commandeRequest.getName());
commande.setLng(commandeRequest.getLng());
commande.setPublicAddress(commandeRequest.getPublicAddress());
commande.setTelephone(commandeRequest.getTelephone());
commande.setEmail(commandeRequest.getEmail());
      commandeRepository.save(commande);
      user.getCommandes().add(commande);

      commandeNotificationService.sendMessage(commandeRepository.save(commande));

Panier panier=panierRepository.findById(commandeRequest.getPanier().getId()).get();
panier.clear();
panierRepository.save(panier);
return   ResponseEntity.ok(new MessageResponse("success"));

  }

    return ResponseEntity.badRequest().body(new MessageResponse("user not found"));

}
    @CrossOrigin(value = "*",maxAge = 30000)
    @GetMapping("/notification")
    SseEmitter messages() {
        SseEmitter.SseEventBuilder eventBuilder = SseEmitter.event();
        SseEmitter emitter = new SseEmitter();

        SseEmitter sseEmitter = new SseEmitter(6 *0L);

        sseEmitter.onError((t) -> sses.remove("4"));
        sseEmitter.onCompletion(() -> sses.remove("4"));
        sses.put("4", sseEmitter);

        return sseEmitter;

    }

@GetMapping("/{id}")
    public ResponseEntity<?> getCommandById(@PathVariable String id) {
    if (commandeRepository.findById(id).isPresent()) {
        Optional<Commande> commandeList = commandeRepository.findById(id);
        return ResponseEntity.ok(commandeList);
    } else {
   return      ResponseEntity.badRequest().body(new MessageResponse("commande not found"));

    }
}
 @PostMapping("/disable")
 public ResponseEntity<?> disableCommande(@RequestBody CommandeRequest commandeRequest){
   if( commandeRepository.findById(commandeRequest.getId()).isPresent()){
     Commande commande=  commandeRepository.findById(commandeRequest.getId()).get();
   commande.setStatus(Status.DESACTIVER);
     commandeRepository.save(commande);
 return     ResponseEntity.ok(new MessageResponse("commande desactiver"));
   }
    return  ResponseEntity.badRequest().body(new MessageResponse("Failed"));
 }
    @GetMapping("client/{id}")
    public ResponseEntity<?> getCommandByAccountAddress(@PathVariable String id) {
        if (!commandeRepository.findByPublicAddress(id).isEmpty()) {
            List<Commande> commandeList = commandeRepository.findByPublicAddress(id);
            return ResponseEntity.ok(commandeList);
        } else {
            return      ResponseEntity.badRequest().body(new MessageResponse("commande not found"));

        }
    }

@GetMapping()
    public  ResponseEntity<?> getAllCommand(){
   return ResponseEntity.ok(commandeRepository.findAll());
}
}
