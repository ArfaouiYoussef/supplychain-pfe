package com.rest.api.repository;

import com.rest.api.models.Commande;
import com.rest.api.models.Message;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.mongodb.repository.Tailable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommandeRepository extends MongoRepository<Commande, String> {
    List<Commande> findByPublicAddress(String address);
}
