package com.rest.api.repository;

import com.rest.api.models.Medicament;
import org.springframework.data.mongodb.repository.MongoRepository;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;


public interface MedicamentRepository extends MongoRepository<Medicament, String> {
List<Medicament> findByClasseTherapeutiqueId(String id);
    List<Medicament> findByType(String type);
Optional<Medicament> findByName(String name);
}
