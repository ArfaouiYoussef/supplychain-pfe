package com.rest.api.payload.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class ActivateRequest {

    @Size(min = 3, max = 70)
    private String id;

    private boolean active;
    private String transaction;
    private  String smartContract;
    private String owner;

    public String getSmartContract() {
        return smartContract;
    }

    public void setSmartContract(String smartContract) {
        this.smartContract = smartContract;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
