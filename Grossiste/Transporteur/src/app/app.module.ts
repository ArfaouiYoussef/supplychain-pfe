// app.module.ts

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

// FCM
import { FCM } from '@ionic-native/fcm/ngx';
import { NetworkInterface } from '@ionic-native/network-interface/ngx';
import {AuthGuardService} from './auth/guards/auth-guard.service';
import {LoginComponent} from './auth/login/login.component';
import {RegisterComponent} from './auth/register/register.component';
import {AuthComponent} from './auth/auth.component';
import {ReactiveFormsModule} from '@angular/forms';
import {EthereumProvider} from './ethereumProvider/ethereum';
import {ConfigurationIotService} from './services/configuration-iot.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NativeAudio} from '@ionic-native/native-audio/ngx';
import {ErrorInterceptor,JwtInterceptor} from './_helpers';
import { File } from "@ionic-native/file/ngx";
import {StorageService} from './services/secure-storage.service';

@NgModule({
  declarations: [AppComponent,LoginComponent,RegisterComponent,AuthComponent],
  entryComponents: [],
    imports: [BrowserModule, IonicModule.forRoot(),
        HttpClientModule,AppRoutingModule, ReactiveFormsModule],
  providers: [
    StatusBar,EthereumProvider,ConfigurationIotService,
      SplashScreen,NetworkInterface,File,StorageService,
    FCM,AuthGuardService,NativeAudio,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
      { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
      { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],

  bootstrap: [AppComponent]
})

export class AppModule {}
