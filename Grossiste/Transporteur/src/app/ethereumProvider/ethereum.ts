
import { Injectable } from '@angular/core';
const  abi = require('./contracts/abi.json');
const  laboratoire = require('./contracts/abi.json');
const privateKeyToAddress = require('ethereum-private-key-to-address')
const Tx = require('ethereumjs-tx')

import { AppConfig } from "../../../env";
import {StorageService} from '../services/secure-storage.service';


const Web3 = require('web3');
@Injectable()
export class EthereumProvider {
    private web3: any;
    private accountAddress: string;
    private privateKey: string;
    private publicKey: string;
    private interface = (<any>abi).interface;

    private mnemonic: string;
    byteCode="";
    smartContractAddress: any;
    contract: any;
    userDetails: any;
    key: string;

    constructor(private secureStorage:StorageService
    ) {
        this.web3 = new Web3(new Web3.providers.HttpProvider("http://5.135.52.74:8546"));
        this.web3.eth.net.getNetworkType(function(err, res){
            console.log("Network Type: "+res);
        });
        this.accountAddress = AppConfig.ethereum.account;
        this.privateKey = AppConfig.ethereum.privateKey;
        this.userDetails = JSON.parse(localStorage.getItem('currentUser'))

        if(!!this.userDetails) {
            let data = JSON.parse(localStorage.getItem("currentUser")).smartContract;
            this.smartContractAddress = data;
            console.log(this.smartContractAddress);

            let key=this.secureStorage.encrypt("prvKey");
            let value=localStorage.getItem(key);
            this.key="0x"+this.secureStorage.decrypt(value)

            this.contract = new this.web3.eth.Contract(laboratoire, this.smartContractAddress);



        }


    }





    public generateAccount() {
        const account = this.web3.eth.accounts.create();

        this.accountAddress = account.address;
        this.privateKey = account.privateKey;



    }



    public get(){
        const contractTest = new this.web3.eth.Contract(laboratoire,"0x94Fc59c49a72621605Ff5b87613ba84a3073c1f6");

        let encoded = contractTest.methods.getContainer("1515").encodeABI();
        const tx = {
            to:"0x94Fc59c49a72621605Ff5b87613ba84a3073c1f6",
            value: '0',
            gas: 447940
            ,
            gasPrice: '0',
            nonce:this.web3.eth.getTransactionCount(JSON.parse(localStorage.getItem("currentUser")).accountAddress, 'pending'),

            chainId: 1515,

            data : encoded
        };

        return  this.web3.eth.accounts.signTransaction(tx, this.key);



    }
    public ReceivedContainer(data){
console.log(data);
     const contract = new this.web3.eth.Contract(laboratoire,this.smartContractAddress);
console.log(data)
        let encoded = contract.methods.ReceivedContainer(data.tagContainer,data.temperature,data.humidite).encodeABI();
        let tx =
            {
            to : this.smartContractAddress,
            value: '0',
            gas: 4000000 ,
            gasPrice: '0',
            nonce:this.web3.eth.getTransactionCount(JSON.parse(localStorage.getItem("currentUser")).accountAddress, 'pending'),

            chainId: 1515,

            data : encoded
        };

        return  this.web3.eth.accounts.signTransaction(tx, this.key);


    }





    public removeUser(data) {
        let encoded = this.contract.methods.removeUser(data.address).encodeABI();
        const tx = {
            to : this.smartContractAddress,
            value: '0',
            gas: 4000000,
            gasPrice: '0',
            nonce:this.web3.eth.getTransactionCount(JSON.parse(localStorage.getItem("currentUser")).accountAddress, 'pending'),

            chainId: 1515,

            data : encoded
        };

        return  this.web3.eth.accounts.signTransaction(tx, this.key);
    }
    data(){

        this.web3.eth.getTransactionReceipt('0x4f7dda82fcc24c84c35a7efff3c6b2bd4b21ef1577a793ed426e729b3a92bdfe').then(console.log);

    }




    public loadContainer(data){
        console.log(data)
        const contract = new this.web3.eth.Contract(laboratoire,this.smartContractAddress);
//
        let encoded = contract.methods.StateContainer(data.tagContainer,data.temperature,data.humidite).encodeABI();
        let tx =
            {
                to : this.smartContractAddress,
                value: '0',
                gas: 4000000 ,
                gasPrice: '0',
                nonce:this.web3.eth.getTransactionCount(JSON.parse(localStorage.getItem("currentUser")).accountAddress, 'pending'),

                chainId: 1515,

                data : encoded
            };

        return  this.web3.eth.accounts.signTransaction(tx, this.key);


    }

    public deliveredContainer(data){
        const contract = new this.web3.eth.Contract(laboratoire,this.smartContractAddress);

        let encoded = contract.methods.ReceivedContainer(data.tagContainer,data.temperature,data.humidite).encodeABI();
        let tx =
            {
            to : this.smartContractAddress,
            value: '0',
            gas: 4000000 ,
            gasPrice: '0',
            nonce:this.web3.eth.getTransactionCount(JSON.parse(localStorage.getItem("currentUser")).accountAddress, 'pending'),

            chainId: 1515,

            data : encoded
        };

        return  this.web3.eth.accounts.signTransaction(tx, this.key);


    }












    public getPrivateKey() {
        return this.privateKey;
    }


    public getAccount () {
        return this.accountAddress;
    }
    public getMnemonic () {
        return this.mnemonic;
    }

    // @ts-ignore
    public async getGasPrice() {
        return await this.web3.eth.getGasPrice();
    }
    public verify(privateKey){
        let data= this.web3.utils.utf8ToHex("hello");

        let signed_data = this.web3.eth.accounts.sign(data,privateKey);

        return signed_data;/*
let v =signed_data.v;
  let r=signed_data.r;
  let s=signed_data.s;
  let l=this.web3.eth.accounts.recover({
    messageHash: signed_data.messageHash,
    v: v,
    r: r,
    s: s
  });
  console.log("result account"+l)
  console.log("account"+this.getAccount());

  console.log(JSON.stringify(signed_data))
  this.web3.eth.personal.ecRecover(data, signed_data.signature).then(console.log)
*/

    }

    getPublicKeyFromPrivateKey(privateKey){

        return   privateKeyToAddress(privateKey);


    }



    /*





    public verify(){
      var privateKey = eccrypto.generatePrivate();
      var publicKey = eccrypto.getPublic(privateKey);
    console.log("account")
      this.ge
      console.log(this.getAccount())
      var str = "message to sign";
      var msg = crypto.createHash("sha256").update(str).digest();

      eccrypto.sign(privateKey, msg).then(function(sig) {
        console.log("Signature in DER format:", sig);
        eccrypto.verify(publicKey, msg, sig).then(function() {
          console.log("Signature is OK");
        }).catch(function() {
          console.log("Signature is BAD");
        });
      });




     */


    async   send(transaction) {
        this.generateAccount();
        let gas = await transaction.estimateGas({from: this.getAccount()});
        let options = {
            to: transaction._parent._address,
            data: transaction.encodeABI(),
            gas: gas
        };
        let signedTransaction = await this.web3.eth.accounts.signTransaction(options, this.getPrivateKey());
        return await this.web3.eth.sendSignedTransaction(signedTransaction.rawTransaction);
    }


}
