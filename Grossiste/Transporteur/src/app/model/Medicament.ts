export class Medicament {
  id: string;
  classeTherapeutique: string
  composition: string
  dosage: string
  formegalenique: string
  indication: string
  marque: string
  name: string
  photoUrl: string
  presentation: string
  prix: string
  quantite: string
  type: string
}

