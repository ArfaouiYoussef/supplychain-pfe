import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationIotService {

  constructor(private _http:HttpClient) { }

 checkNetwork(){

return this._http.get("/device");
 }

}
