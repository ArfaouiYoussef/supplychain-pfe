import { TestBed } from '@angular/core/testing';

import { ConfigurationIotService } from './configuration-iot.service';

describe('ConfigurationIotService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConfigurationIotService = TestBed.get(ConfigurationIotService);
    expect(service).toBeTruthy();
  });
});
