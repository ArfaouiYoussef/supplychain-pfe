import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject} from "rxjs";
import {User} from "../model/user";
import {config} from "../config";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  currentUserSubject: any;
  currentUser:any;
  constructor(private _http:HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();

  }
  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }



  signin(data){
delete data.privateKey;
    return this._http.post(config.apiUrl+"/api/auth/signin",data);

  }
  signup(data){

    return this._http.post<any>(config.apiUrl+"/api/auth/signup",data);

  }
  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

}
