import { Injectable } from "@angular/core";
import { Router, CanActivate, ActivatedRouteSnapshot } from "@angular/router";

@Injectable({
    providedIn: "root"
})
export class AuthGuardService implements CanActivate {
    constructor(private router: Router) {}

    canActivate(route: ActivatedRouteSnapshot): boolean {
        console.log(route);

        let user=localStorage.getItem('currentUser');


        if (!user ) {
            this.router.navigate(["/auth/login"]);
            return false;
        }

        return true;
    }
}
