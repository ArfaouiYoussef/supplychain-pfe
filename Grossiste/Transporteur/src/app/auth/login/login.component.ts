import { Component, OnInit } from '@angular/core';
import {EthereumProvider} from "../../ethereumProvider/ethereum";
import {AuthService} from "../../services/auth.service";
import {FormBuilder, Validators, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {FCM} from '@ionic-native/fcm/ngx';
import {ToastController} from '@ionic/angular';
import generateHash from 'random-hash';
import {NgxSpinnerService} from 'ngx-spinner';
var crypto = require('crypto');

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {


  loginForm: FormGroup;
  formData: FormData;
  humanizeBytes: Function;
  dragOver: boolean;
  jsonFile: any;
  isvalid: boolean;
  error: any;

  /**
   * Constructor
   *
   * @param {FormBuilder} _formBuilder
   * @param _router
   * @param _web3
   * @param _auth
   */
   constructor(
      private _formBuilder: FormBuilder,
      private _router: Router,
      private _web3: EthereumProvider,
      private _auth: AuthService,
      private _spinner:NgxSpinnerService,
      private toastController: ToastController,
  ) {




      // Configure the layout
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void
  {
    this.loginForm = this._formBuilder.group({
      email   : ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
        accountAddress:[''],
      privateKey: ['', Validators.required],
    });
  }

  openFile(event) {
    let input = event.target;
    for (var index = 0; index < input.files.length; index++) {
      let reader = new FileReader();
      reader.onload = () => {
        // this 'text' is the content of the file
        var text = reader.result;
        this.jsonFile=JSON.parse(text.toString());
        this.loginForm.controls["privateKey"].patchValue(this.jsonFile.privateKey)


      }
      reader.readAsText(input.files[index]);
    };
  }

  onLogin(){
      this._spinner.show().then(console.log)
console.log(this.loginForm.value);
if(!this.jsonFile)
{
  this.error=["clé privée invalide"]
this._spinner.hide().then(console.log)
}
    let pubKey=this._web3.getPublicKeyFromPrivateKey(this.jsonFile.privateKey);
this.loginForm.controls['accountAddress'].patchValue(pubKey);
this._auth.signin(this.loginForm.value).subscribe((res:any)=>{

       this._spinner.hide().then(console.log)
        localStorage.setItem('currentUser', JSON.stringify(res));
        this._router.navigate(["/dashboard"]).then(console.log);
        let hash=generateHash({length:100});

        var mykey = crypto.createCipher('aes-128-cbc',hash);
        var mystr = mykey.update("prvKey", 'utf8', 'hex')
        mystr += mykey.final('hex');
        let privateKey=this.jsonFile.privateKey.substr(2,this.jsonFile.privateKey.length);
        var key = crypto.createCipher('aes-128-cbc', hash);
        var str = key.update(privateKey, 'utf8', 'hex')
        str += key.final('hex');
        localStorage.setItem("hash",hash);
        localStorage.setItem(mystr,str)
      if(res.accountAddress===pubKey){
        this.isvalid=true;
 this.loginForm.controls["privateKey"].reset();
          this._spinner.hide().then(console.log)

      }


      else {
        this.loginForm.controls["privateKey"].reset();

        this.isvalid=false;
          this._spinner.hide().then(console.log)

      }


    },async error=>{
        const toast = await this.toastController.create({
            message: 'Echec:'+error ,
            duration: 5000,
            color: 'danger'
        });
        toast.present();
      console.log(error)
        this._spinner.hide().then(console.log)
      this.error=[(error)]});


    if(this.loginForm.valid) {
      let signedData=this._web3.verify(this.loginForm.controls["privateKey"].value);
      console.log(signedData);
      console.log(this.loginForm.value)
        this._spinner.hide().then(console.log)

    }
  }





}
