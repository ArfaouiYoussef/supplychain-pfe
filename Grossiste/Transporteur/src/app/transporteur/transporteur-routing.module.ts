import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SettingComponent} from './setting/setting.component';
import {TransporteurComponent} from './transporteur.component';
import {ContainerComponent} from './container/container.component';
import {DetailsContainerComponent} from './details-container/details-container.component';
import {AboutComponent} from './about/about.component';
import {ConfigurationComponent} from './configuration/configuration.component';


const routes: Routes = [
    {
        path: '',
        component: TransporteurComponent,
        children:[
            { path:'setting',
                component:SettingComponent},
            { path:'container',
            component:ContainerComponent},{
            path:"container/:id/detail",
                component:DetailsContainerComponent
            },{
            path:"about",
                component: AboutComponent
            },{
                path:"parametres",
                component:SettingComponent
            }

            ,{
            path:"configuration",
                component:ConfigurationComponent
            },
            {
                path:''
                ,redirectTo:'container'
            }
        ]
    }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransporteurRoutingModule { }
