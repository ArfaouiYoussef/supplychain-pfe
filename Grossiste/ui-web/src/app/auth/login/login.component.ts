import { Router } from '@angular/router';
import { getDeepFromObject } from '@nebular/auth/helpers';
import { NB_AUTH_OPTIONS } from '@nebular/auth';

import {Component, Inject, OnInit, EventEmitter} from '@angular/core';
import { NbLoginComponent } from '@nebular/auth';
import {FormControl, Validators, FormGroup, FormBuilder} from "@angular/forms";
import {GlobalsService} from "../../service/globals.service";
import {UserService} from "../../service/user-service.service";
import {EthereumProvider} from "../../ethereumProvider/ethereum";
import {UploaderOptions, UploadFile, UploadInput, humanizeBytes, UploadOutput} from 'ngx-uploader';
import {AuthService} from '../../service/auth.service';
import {StorageService} from "../../service/secure-storage.service";
import {environment} from "../../../environments/environment.prod";
const Cryptr = require('cryptr');
const cryptr = new Cryptr(environment.SECRET_KEY);
import generateHash from 'random-hash';
import {NgxSpinnerService} from "ngx-spinner";


function readBase64(file) {
  var reader  = new FileReader();
  var future = new Promise((resolve, reject) => {
    reader.addEventListener("load", function () {
      resolve(reader.result);
    }, false);

    reader.addEventListener("error", function (event) {
      reject(event);
    }, false);

    reader.readAsDataURL(file);
  });
}
var crypto = require('crypto');


@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']

})
export class NgxLoginComponent {


  loginForm: FormGroup;
  options: UploaderOptions;
  formData: FormData;
  files: UploadFile[];
  uploadInput: EventEmitter<UploadInput>;
  humanizeBytes: Function;
  dragOver: boolean;
   jsonFile: any;
   isvalid: boolean;
  error: any;

  /**
   * Constructor
   *
   * @param {FuseConfigService} _fuseConfigService
   * @param {FormBuilder} _formBuilder
   */
  constructor(
    private _formBuilder: FormBuilder,
    private _router:Router,
    private _web3:EthereumProvider,
    private _auth:AuthService,
    private spinner:NgxSpinnerService,
    private secureStorageService:StorageService,
  )

  {
    this.files = []; // local uploading files array
    this.uploadInput = new EventEmitter<UploadInput>(); // input events, we use this to emit data to ngx-uploader
    this.humanizeBytes = humanizeBytes;
    // Configure the layout
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void
  {
    this.loginForm = this._formBuilder.group({
      email   : ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      privateKey: ['', Validators.required],
      accountAddress:['']
    });
  }

  openFile(event) {
    let input = event.target;
    for (var index = 0; index < input.files.length; index++) {
      let reader = new FileReader();
      reader.onload = () => {
        // this 'text' is the content of the file
        var text = reader.result;
        this.jsonFile=JSON.parse(text.toString());
        this.loginForm.controls["privateKey"].patchValue(this.jsonFile.privateKey)


      }
      reader.readAsText(input.files[index]);
    };
  }

  onLogin(){
this.spinner.show().then(console.log)
    let pubKey=this._web3.getPublicKeyFromPrivateKey(this.jsonFile.privateKey);
    this._auth.getUser(this.loginForm.value).subscribe(res=>{
      let hash=generateHash({length:100});

      var mykey = crypto.createCipher('aes-128-cbc',hash);
      var mystr = mykey.update("prvKey", 'utf8', 'hex')
      mystr += mykey.final('hex');
      let privateKey=this.jsonFile.privateKey.substr(2,this.jsonFile.privateKey.length);
      var key = crypto.createCipher('aes-128-cbc', hash);
      var str = key.update(privateKey, 'utf8', 'hex')
      str += key.final('hex');
      localStorage.setItem("hash",hash);
      localStorage.setItem(mystr,str)
      this.spinner.hide().then(console.log)

      if(res.accountAddress===pubKey){
        this.isvalid=true;
       this.loginForm.controls["accountAddress"].patchValue(pubKey);

        this._auth.signin(this.loginForm.value).subscribe(response=>{
          this.spinner.hide().then(console.log)

          localStorage.setItem('currentUser', JSON.stringify(response));
          this._auth.currentUserSubject.next(response);
          window.location.replace("/dashboard");

        },error1 => {
          this.spinner.hide().then(console.log)
          this.error = [error1]
        } );
      }


      else {
        this.loginForm.controls["privateKey"].reset();
        this.spinner.hide().then(console.log)

        this.isvalid=false;
      }


    });


    if(this.loginForm.valid) {
      let signedData=this._web3.verify(this.loginForm.controls["privateKey"].value);
      console.log(signedData);
      console.log(this.loginForm.value)

    }
  }

  onUploadOutput(output: UploadOutput): void {
    if (output.type === 'allAddedToQueue') { // when all files added in queue

    } else if (output.type === 'addedToQueue'  && typeof output.file !== 'undefined') { // add file to array when added
      this.files.push(output.file);
    } else if (output.type === 'uploading' && typeof output.file !== 'undefined') {
      // update current data in files array for uploading file
      const index = this.files.findIndex(file => typeof output.file !== 'undefined' && file.id === output.file.id);
      this.files[index] = output.file;
    } else if (output.type === 'removed') {
      // remove file from array when removed
      this.files = this.files.filter((file: UploadFile) => file !== output.file);
    } else if (output.type === 'dragOver') {
      this.dragOver = true;
    } else if (output.type === 'dragOut') {
      this.dragOver = false;
    } else if (output.type === 'drop') {
      this.dragOver = false;
    }

    console.log(this.files)
    let reader = new FileReader();
    reader.onload = () => {
      // this 'text' is the content of the file
      var text = reader.result;
    }
    reader.readAsText(output.file[0]);

  }

  startUpload(): void {
    const event: UploadInput = {
      type: 'uploadAll',
      url: 'http://ngx-uploader.com/upload',
      method: 'POST',
      data: { foo: 'bar' }
    };

    this.uploadInput.emit(event);
  }

  cancelUpload(id: string): void {
    this.uploadInput.emit({ type: 'cancel', id: id });
  }

  removeFile(id: string): void {
    this.uploadInput.emit({ type: 'remove', id: id });
  }

  removeAllFiles(): void {
    this.uploadInput.emit({ type: 'removeAll' });
  }
}


