import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {observable, Subscription} from "rxjs";
import "rxjs-compat/add/observable/of";
import {map} from "rxjs/operators";
import {GlobalsService} from "../service/globals.service";
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
export class User {
  isAccountActive: boolean
  store: boolean
  success: bigint
  token:String
  userid: bigint





}

@Injectable()
export class AuthService {
  public userDetails: any;
  user: User[];
  private handleError: any;

  constructor(private router: Router, private  httpClient: HttpClient,private globalService:GlobalsService) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT"
  });




  }

   login(email,password):   Promise<User[]>   {

   return    this.httpClient.post("/api/admin/login",
       {
         "email": email,
         "password": password

       })    .toPromise()
     .then(response => response as User[]

     )
     .catch(this.handleError);




   }
  public isAuthenticated(): boolean {
    const user = this.globalService.getUser();
    // Check whether the token is expired and return
    // true or false
    if(user!=null){
      return  true;
    }
    else
      {

      return false;

    }
  }


  logout() {
      ( this.router.navigate(['/auth/login']));
  }
}
