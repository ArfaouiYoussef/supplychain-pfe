import { Router } from '@angular/router';
import { getDeepFromObject } from '@nebular/auth/helpers';
import { NB_AUTH_OPTIONS } from '@nebular/auth';

import * as saveAs from 'file-saver';

import {Component, Inject, OnInit} from '@angular/core';
import { NbLoginComponent } from '@nebular/auth';
import {FormControl, Validators, FormGroup, FormBuilder, ValidatorFn, AbstractControl, ValidationErrors} from "@angular/forms";
import {GlobalsService} from "../../service/globals.service";
import {EthereumProvider} from "../../ethereumProvider/ethereum";
import {takeUntil, timestamp} from "rxjs/operators";
import {UserService} from "../../service/user-service.service";
import {Subject} from 'rxjs';
import {AuthService} from 'app/service/auth.service';
import {NgxSpinnerService} from "ngx-spinner";
import {errorObject} from "rxjs/internal-compatibility";

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']

})
export class NgxRegisterComponent {

  registerForm: FormGroup;
  errors: string[] = [];

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private _auth:AuthService,
    private _formBuilder: FormBuilder ,
    private _web3:EthereumProvider ,
    private  _router:Router,
    private _spinner:NgxSpinnerService,
  )
  {

    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------



  createAccount(){

    this._web3.generateAccount();

    let data=JSON.stringify({address:this._web3.getAccount(),privateKey:this._web3.getPrivateKey()});
    let file= this.createFile(data);
    let timestamp = Math.round(+new Date()/1000);

    this.registerForm.controls["roles"].patchValue([this.registerForm.controls["userrole"].value]);

    this.registerForm.controls["accountAddress"].patchValue(this._web3.getAccount());
    this._spinner.show().then(console.log)
    this._web3.createContract().then(signed=>{
      console.log(signed)
      const data={...this.registerForm.value,transaction:signed.rawTransaction}
    this._auth.signup(data).subscribe(res=>{
      this._spinner.hide();
      this._router.navigate(["/auth/login"]);
      saveAs(file, timestamp+'key.json')
    },error => {this._spinner.hide(); this.errors = [error.message];
  console.log(error);    this.errors = [error];});



    },error1 =>  {this.errors = ['invalid Transaction'];this._spinner.hide()})


  }

  createFile(data) {

    let file = new Blob([data], { type: 'text/text;charset=utf-8' });
    return file;
  }

  ngOnInit(): void
  {
    this.registerForm = this._formBuilder.group({
      username           : ['', Validators.required],
      email          : ['', [Validators.required, Validators.email]],
      accountAddress:[''],
      password       : ['', Validators.required],
      roles      : ['grossiste'],
      userrole      : ['grossiste'],

      passwordConfirm: ['', [Validators.required, confirmPasswordValidator]]
    });
    this._web3.generateAccount();



    // Update the validity of the 'passwordConfirm' field
    // when the 'password' field changes
    this.registerForm.get('password').valueChanges
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(() => {
        this.registerForm.get('passwordConfirm').updateValueAndValidity();
      });
  }
  onRegister(){

  }
  ngOnDestroy(): void
  {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

  if ( !control.parent || !control )
  {
    return null;
  }

  const password = control.parent.get('password');
  const passwordConfirm = control.parent.get('passwordConfirm');

  if ( !password || !passwordConfirm )
  {
    return null;
  }

  if ( passwordConfirm.value === '' )
  {
    return null;
  }

  if ( password.value === passwordConfirm.value )
  {
    return null;
  }

  return {passwordsNotMatching: true};
};



