import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ContainerComponent} from "./container.component";
import {ImportContainerComponent} from "./import-container/import-container.component";
import {ListContainerComponent} from "./list-container/list-container.component";

const routes: Routes = [
  {path:'',
  component:ContainerComponent,
  children:[
    {
      path:'',
      component:ImportContainerComponent,
    },
    {
      path: 'list-container',
    component:ListContainerComponent
    }
  ]
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContainerRoutingModule { }
