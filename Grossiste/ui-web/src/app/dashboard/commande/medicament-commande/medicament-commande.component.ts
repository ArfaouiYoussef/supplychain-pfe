import { Component,TemplateRef, OnInit } from '@angular/core';
import {MedicamentService} from "../../../service/medicament.service";
import {Router, ActivatedRoute} from '@angular/router';
import {EthereumProvider} from "../../../ethereumProvider/ethereum";
import {TransactionService} from "../../../service/transaction.service";
import {CommandService} from "../../../service/command.service";
import {Medicament} from 'app/dashboard-grossiste/redux/common/models/medicament';
import {TransporteurService} from 'app/service/transporteur.service';
import {NgxSpinnerService} from "ngx-spinner";
import Swal from "sweetalert2";
import { SmartTableDatePickerComponent } from './smart-table-date-picker/smart-table-date-picker.component';
import * as moment from "moment";
import {MessageService} from "../../../service/message.service";
import {NbDialogService} from "@nebular/theme";

@Component({
  selector: 'ngx-medicament-commande',
  templateUrl: './medicament-commande.component.html',
  styleUrls: ['./medicament-commande.component.scss']
})
export class MedicamentCommandeComponent implements OnInit {
  containerTag;
  medicamentId: any;
  medicament: any;
  commandeId: string;
  commande: any;
  medicamentList: any;
  qte: any;
  datas:any;
  qteDemande: any;
  transporteurList: any[];
 list:any[]=[];
  numLots:any[]=[];
  selectedItem:any;
  qteTotal: any;
  displayData:any;
  total: any;
  disable: boolean=true;
  newOwnerName: any;
  newOwner: any;
  filteredItems:any;
  transporteurAddress: any;
  medicamentName: any;

  constructor(private _serviceMedicament:MedicamentService,
              private route:ActivatedRoute,private _web3:EthereumProvider,
              private commandService:CommandService,
private router: Router,
              private msg:MessageService,
              private spinner:NgxSpinnerService,
              private transporteurService:TransporteurService,
              private dialogService: NbDialogService) {

  }

  ngOnInit(): void {
    this.route.params
      .subscribe(params =>{
        console.log(params);
this.medicamentId=params.id
      });
    console.log('--------------id---------------------')
    console.log(this.medicamentId)
      this._serviceMedicament.getMedicamentById(this.medicamentId).subscribe((res:any)=>{
        this.medicament=res;
        this.numLots=res.numeroLots;
        this.datas=[];
        this.medicamentName=res.name;
        console.log("---------------------------list---------------------------")
        console.log(res.name)
        this.commandService.getContainerById(this.commandeId).subscribe((response:any)=>{
          console.log(response);
          this.medicamentList=response.medicaments
          this.transporteurAddress=response.transporteurAddress;
          this.total=this.medicamentList.length;
          this.filteredItems=this.medicamentList;
          console.log(this.medicamentId)
       this.qte=   this.medicamentList=this.medicamentList.filter(item=>item.name===this.medicamentName)
          console.log(this.medicamentList)
          console.log(""+this.medicamentList.length)
          this.qte=!!this.medicamentList.length?this.medicamentList.length:0;
          console.log("------------------1-----------------------------")
          if(this.medicamentList.length<this.qteDemande){
            this.spinner.show().then(console.log)

            console.log(this.numLots.length>0);
        if(!!this.numLots && this.qteDemande>this.qte)
       this.addToBlockChain(this.numLots[0],0);
else{

  Swal.fire(
    'Erreur',
    'Hors Stock ',
    'error'
  )

          this.spinner.hide().then(console.log)

}


        }
          else{
            console.log('------------------3-----------------------')
            console.log(this.medicamentList)
            this.medicamentList.map(item=>{
              console.log(item);
           this.commandService.getMedicament(   item.numeroLot).subscribe(response=>{
             this.list.push(response)
             console.log("****************************************")

           })
             this.filteredItems=this.list;

            })
          }
        });




        });
    this.route.queryParams
      .subscribe(params =>{
console.log(params)
        console.log(params.tag);
        this.containerTag=params.tag;
this.commandeId=params.commandeId;
      });
    console.log(this.commandeId)

    this.transporteurService.getAllTransporteur().subscribe(res=>{
      console.log(this.transporteurAddress)
      this.transporteurList=res.filter(item=>item.accountAddress!==this.transporteurAddress)}
    )




    this.commandService.getCommandeById(this.commandeId).subscribe((res:any)=>{
this.newOwnerName=res.name;
this.newOwner=res.publicAddress;
console.log(this.newOwner);
console.log(this.newOwnerName)
      this.commande=res;
      this.qteDemande=0;
      this.qteTotal=0;
      console.log(res.panier.productQuantities)
      Object.keys(res.panier.products).forEach((key) => {
        this.qteTotal+=res.panier.productQuantities[key];
        this.total;
        console.log(this.qteTotal)
      });
      console.log(

        this.qteDemande=res.panier.productQuantities[this.medicamentId])
      console.log("--------------------panier----------------------")
      console.log(res.panier)
console.log(this.qteDemande)
    });

    this._serviceMedicament.getAllClass().subscribe(response=>{
      this.data=response;
    })

  }


  data = [
    {
      name: "classe 1 ",
    },

  ];

  onDeleteConfirm(event) {
    console.log("Delete Event In Console")
    console.log(event);
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
expandContainer(){
    this.spinner.show().then(console.log)
    this.disable=true;

    console.log(this.selectedItem)
  let data={addressTrasnporteur:this.selectedItem,tagContainer:this.containerTag,commandeId:this.commandeId };
    this._web3.expandContainer(data).then(signed=>{
      console.log(signed)
      let request={...data,transaction:signed.rawTransaction}
      this.transporteurService.expandContainer(request).subscribe(res=>{
        this.disable=false;
        this.spinner.hide();

        Swal.fire(
          'Success',
          'Conteneur   expédier ',
          'success'
        )
        this.router.navigate(["/dashboard/suivi"])
        },error => {
        this.disable = false;
        this.spinner.hide();
      });
    })


}
  onCreateConfirm(event) {
console.log(this.commande)
    console.log(event)
  let dateExpiration= moment(event.newData.dateExpiration).format("DD/MM/YYYY");

  console.log(dateExpiration);
     if(this.qteTotal==this.total){
       Swal.fire(
           'Echec!',
         'Conteneur prêt à expédier !',
         'error'
       )
     }
     else {
       this.spinner.show().then(console.log);
       const data = {...event.newData, tag: this.containerTag, medicament: this.medicament}
       console.log("Create Event In Console");
       console.log(data);
       console.log(this.medicament);
       console.log(this.commande);

       let ethRequest = {
         id:!!this.medicament.id?this.medicament.id:'',
         name: !!this.medicament.name?this.medicament.name:'',
         numeroLot: data.numeroLot,
         amm: !!this.medicament.amm ? this.medicament.amm : 0,
         serialNumber: data.serialNumber,
         dateExpiraetion: dateExpiration,
         saler: this.commande.publicAddress,
         tagContainer: this.containerTag,
         newOwnerName:this.newOwnerName,
         newOwner:this.newOwner,
         addressDestination: this.commande.addressCommande
       }
if((data.serialNumber.length!=8))
{
  Swal.fire(
    'Echec',
    'Numero  de Serie  non Valide!',
    'error'
  )
}
if((data.numeroLot.length!=5))
       {
         Swal.fire(
           'Error!',
           'Numero  de lot  non valide!',
           'success'
         )
       }


       this.medicament.numeroLot = data.numeroLot;
       this.medicament.serialNumber = data.serialNumber;
       this.medicament.dateExpiration = dateExpiration;

       console.log(this.medicament);
if(data.serialNumber.length===8 && data.numeroLot.length===5){
       this._web3.addMedicament(ethRequest).then(signed => {
         console.log(signed);
         let data = {medicament: this.medicament, transaction: signed.rawTransaction, commandeId: this.commandeId,email:this.commande.email};
         this.commandService.addMedicament(data).subscribe(response => {
           console.log(response)
           event.confirm.resolve();

           this.spinner.hide();
           Swal.fire(
             'Success!',
             'Transaction  validée  avec succès!',
             'success'
           )
           this.qte++;
           this.msg.removeCommande(this.commandeId);
           this.total++;

         }, error1 => {
           event.confirm.reject();
           this.spinner.hide();
         })
       });
     }
else {
  this.spinner.hide();
  Swal.fire(
    'Error!',
    'information non valide',
    'error'
  )
}
     }
    }


  onSaveConfirm(event) {

    const data={oldName:event.data.name,newName:event.newData.name};
    this._serviceMedicament.updateClass(data).subscribe(response=>{
      event.confirm.resolve();

    },error1 =>
      event.confirm.reject())
  }

  activeBtn(event) {
if(event!=="")
{this.disable=false;}
  }




  assignCopy(){
    this.filteredItems = Object.assign([], this.medicamentList);
  }
  filterItem(value){

  }


  onChange(event) {
    let value=event.target.value;

    if(!value){
      this.assignCopy();
    }
    this.filteredItems = Object.assign([], this.medicamentList).filter(
      item => item.name.toLowerCase().indexOf(value.toLowerCase()) > -1
    )


  }



addToBlockChain(item,index){
if(index>this.qteDemande){



  return ;
}
console.log("---------------")
console.log(item);
if(!!item && index<this.qteDemande)
  this.commandService.getMedicament(item).subscribe((response:any)=>{
    this.commande.publicAddress;


  let ethRequest={
    numLot:response.numLot,
    newOwner:this.commande.publicAddress,
    tagContainer:this.containerTag,
    nameNewOwner:this.commande.name,
  }
  if(ethRequest.numLot!='') {
    console.log(item)
    this._web3.addMedicament(ethRequest).then(signed => {
      console.log(signed);
      let data = {
        medicament: this.medicament, ...ethRequest,
        transaction: signed.rawTransaction,
        commandeId: this.commandeId,
        email: this.commande.email
      };
      this.commandService.addMedicament(data).subscribe(response => {
        console.log(response)
          this.commandService.getMedicament(item).subscribe(response => {
            this.filteredItems.push(response)

          })

        this.qte++;
        this.msg.removeCommande(this.commandeId);

        this.total++;
        this.addToBlockChain(this.numLots[index++], index++);

      }, error1 => {
        this.addToBlockChain(this.numLots[index++], index++)

      })
    });
  }
  this.spinner.hide()
  this.list.push(response)

  })
else
  this.spinner.hide()

}
































  open(dialog: TemplateRef<any>) {

    this.dialogService.open(dialog);
  }
  decodeReceived:any;
  transactionReceived:any;
tx:any;
  selectContainerTransaction(res) {
    this.decodeReceived=null;

        console.log("hello");
        console.log(res);
        console.log(res.transactionHash)
  this.tx=res.transactionHash;
      if(!!res.transactionHash){
        this.commandService.getTransactionDetails(res.transactionHash).subscribe(res=>{
          this.decodeReceived=res;
    console.log(res);
        })

    }
        this.transactionReceived=!!res.transactionReceived?res.transactionReceived:"";

      }
  selectMedicament(res) {
    this.decodeReceived=null;

    console.log("hello");
    console.log(res);
    console.log(res.transactionHash)
    this.tx=res.transactionHash;
    if(!!res.numLot){
      console.log("--------------lot------------------------")
      this.commandService.getMedicament(res.numLot).subscribe(res=>{
        this.decodeReceived=res;
        console.log(res);
      })

    }
    this.transactionReceived=!!res.transactionReceived?res.transactionReceived:"";

  }
}
