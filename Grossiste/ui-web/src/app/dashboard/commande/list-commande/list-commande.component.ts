import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import {Actions, ofType} from "@ngrx/effects";
import * as Command from "../../../dashboard-grossiste/redux/common/store/commande/commande.actions";
import {CommandService} from "../../../service/command.service";
import Swal from "sweetalert2";
import {EthereumProvider} from "../../../ethereumProvider/ethereum";
import {MessageService} from "../../../service/message.service";

@Component({
  selector: 'ngx-list-commande',
  templateUrl: './list-commande.component.html',
  styleUrls: ['./list-commande.component.scss']
})
export class ListCommandeComponent implements OnInit {
  listCommande: any=[];
  filteredItems: any;
  selectedItem:any="EnAttente";
  status: any;
  constructor(private store:Store<any>,
              private wbe3:EthereumProvider,
              private messageService:MessageService,
              private action:Actions,private commandeService:CommandService) {

  this.action.pipe(
    ofType('GET_ALL_COMMAND_SUCCESS'))
.
  subscribe(   { next: (value:any)=>{
  this.listCommande=value.payload;
  console.log(this.listCommande);
this.state("EnAttente");

  console.log(value)},

error: err => console.error(err),
  complete: () => {
  console.log("")}});
}

ngOnInit(): void {
  this.store.dispatch(new Command.GetAllCommand());
  this.status='EnAttente';
this.messageService.getMessageCommande().subscribe(res=>{
this.filteredItems.push(res.commande)
  console.log("heeelo")
  console.log(res)
})
  this.state("EnAttente")
}


  state(event) {
    console.log(event)
this.status=event;
    this.assignCopy();
 this.filteredItems= this.listCommande.filter(item=>item.status==event)
  }

  assignCopy(){
    this.filteredItems = Object.assign([], this.listCommande);
  }

  onChange(event) {
    let value=event.target.value;
    if(value===''){
      console.log(this.status)

      this.filteredItems= Object.assign([], this.listCommande).filter(item=>item.status===this.status)
console.log(this.filteredItems)
    }
    else {
      this.filteredItems= this.listCommande.filter(item=>item.status==this.status)

      this.filteredItems = Object.assign([], this.filteredItems).filter(
        item => item.id.toLowerCase().indexOf(value.toLowerCase()) > -1 || item.name.toLowerCase().indexOf(value.toLowerCase()) > -1
      )
      console.log(this.filteredItems)
    }
  }
help(){
console.log(this.wbe3.help());
this.wbe3.data();
}
  open(dialog1: any) {

  }

  disable(id: any) {
this.commandeService.disableCommande(id).subscribe(res=>{
console.log(res);
  this.store.dispatch(new Command.GetAllCommand());

  this.assignCopy();
  this.filteredItems= this.listCommande.filter(item=>item.status==this.status)

});


      }
}



