import {Component, OnInit, TemplateRef} from '@angular/core';
import {NbDialogService} from "@nebular/theme";
import {CarteRfidService} from "../../../service/carte-rfid.service";
import Swal from 'sweetalert2';

@Component({
  selector: 'ngx-list-tag-rfid',
  templateUrl: './list-tag-rfid.component.html',
  styleUrls: ['./list-tag-rfid.component.scss']
})
export class ListTagRfidComponent implements OnInit {
    tag: any;
  errors: string[];
  listTags: any;
  filteredItems: any=[];

  constructor(private dialogService:NbDialogService,private tagService:CarteRfidService) { }

  ngOnInit() {

    this.tagService.getAllCard().subscribe(tags=>{
      this.listTags=tags;
      this.assignCopy()
    })}

  open(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog);
  }
  assignCopy(){
    this.filteredItems = Object.assign([], this.listTags);
  }
  onChange(event) {
    let value=event.target.value;

    if(!value){
      this.assignCopy();
    }
    this.filteredItems = Object.assign([], this.listTags).filter(
      item => item.tag.toLowerCase().indexOf(value.toLowerCase()) > -1
    )

  }
  confirm() {
    console.log(this.tag.length)
    if(this.tag.length!==11){
      this.tag="";

      Swal.fire(
        'Echec!',
        'Tag Non valide',
        'error'
      )
    }
    else {
      this.errors=[''];
      this.tagService.addTagCard(this.tag).subscribe(done=>{

        this.tag="";
        console.log(done);
        Swal.fire(
          'Success!',
          'Tag Ajouter !',
          'success'
        )
this.filteredItems.push(done);
        this.listTags.push(done)
      },error => {
        Swal.fire(
          'Echec!',
          error,
          'error'
        )
        this.tag="";

      });
    }
  }

  confirmDelete(id: any) {
    this.tagService.removeTagCard(id).subscribe(done=>{
      Swal.fire(
        'Success!',
        ' ',
        'success'
      )
    this.listTags=  this.listTags.filter(item=>item.id != id);
      this.assignCopy();

    },error => {
      Swal.fire(
        'Echec ',
        ' ',
        'error'
      )
    })
  }
}
