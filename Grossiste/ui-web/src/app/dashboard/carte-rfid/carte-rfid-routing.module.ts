import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CarteRfidComponent} from "./carte-rfid.component";
import {ListTagRfidComponent} from "./list-tag-rfid/list-tag-rfid.component";

const routes: Routes = [{
  path:'',
  component:CarteRfidComponent,
  children:[{
    path:'',
    component:ListTagRfidComponent,
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CarteRfidRoutingModule { }
