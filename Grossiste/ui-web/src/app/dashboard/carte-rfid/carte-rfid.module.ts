import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CarteRfidRoutingModule } from './carte-rfid-routing.module';
import { ListTagRfidComponent } from './list-tag-rfid/list-tag-rfid.component';
import { CarteRfidComponent } from './carte-rfid.component';
import {
  NbAccordionModule,
  NbActionsModule,
  NbCardModule,
  NbDialogService,
  NbInputModule,
  NbUserModule
} from "@nebular/theme";
import {MatIconModule} from "@angular/material/icon";
import {NgxSpinnerModule} from "ngx-spinner";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatDividerModule} from "@angular/material/divider";
import {MatButtonModule} from "@angular/material/button";
import {MatTooltipModule} from "@angular/material/tooltip";
import {AvatarModule} from "ngx-avatar";
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [CarteRfidComponent, ListTagRfidComponent ],
  imports: [
//
    CommonModule,
    CarteRfidRoutingModule,
    NbActionsModule,
    NbCardModule,
    NbAccordionModule,
    MatIconModule,
    NgxSpinnerModule,
    MatFormFieldModule,
    MatDividerModule,
    MatButtonModule,
    MatTooltipModule,
    AvatarModule,
    NbInputModule,
    FormsModule,
    NbUserModule
  ],
})
export class CarteRfidModule { }
