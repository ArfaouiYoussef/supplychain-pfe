import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from "../../gurads/auth.gurad";
import {NotFoundComponent} from "../../not-found/not-found.component";
import {ListApprobationComponent} from "./list-approbation/list-approbation.component";
import {BlocageComponent} from "./blocage/blocage.component";
import {ListTransporteurComponent} from "./list-transporteur/list-client.component";
import {AddTrasnporteurComponent} from "./add-client/add-client.component";
import {UpdateTransporteurComponent} from "./update-client/update-client.component";
import {TransporteurComponent} from "./transporteur.component";

const routes: Routes = [
  {
    path:'',
    component:TransporteurComponent,
    children:[
      {
        path:'',
        component:ListTransporteurComponent,
      },
      {
        path:'ajouter',
        component:AddTrasnporteurComponent,
      },
      {
        path:":id/modifier",
        component:UpdateTransporteurComponent
      },{
        path: 'approbation',
        component: ListApprobationComponent,
      }
      ,{
        path: 'blocage',
        component: BlocageComponent,
      }

      ]



},



];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransporteurRoutingModule { }
