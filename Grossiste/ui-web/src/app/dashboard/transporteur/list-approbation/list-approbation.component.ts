import { Component, OnInit, TemplateRef } from '@angular/core';
import { ChangeDetectionStrategy,  ViewChild } from '@angular/core';
import { Observable, of } from 'rxjs';

import {ClientService} from "../../../service/client.service";
import {User} from "../../../model/user";
import {NbDialogService} from '@nebular/theme';
import {ActivateRequest} from 'app/dashboard/interface/ActivateRequest';
import Swal from 'sweetalert2'
import {DeleteRequest} from "../../interface/DeleteRequest";
import * as moment from "moment";
import _date = moment.unitOfTime._date;
import {map} from "rxjs/operators";
import {TransporteurService} from "../../../service/transporteur.service";
import {EthereumProvider} from "../../../ethereumProvider/ethereum";
import {NgxSpinnerService} from "ngx-spinner";
import {MessageService} from "../../../service/message.service";



@Component({
  selector: 'ngx-list-approbation',
  templateUrl: './list-approbation.component.html',
  styleUrls: ['./list-approbation.component.scss']
})
export class ListApprobationComponent implements OnInit {

  private users:User[]=[];
  private  request:ActivateRequest;
  private deleteRequest:DeleteRequest;
  filteredItems: any[] & User[]=[];

  constructor(private _transporteurService:TransporteurService,
              private dialogService: NbDialogService,
              private _clientService:ClientService,
              private messageService:MessageService,
              private _web3:EthereumProvider,
              private  spinner:NgxSpinnerService) {
    this.filteredItems=[];
  }

  ngOnInit() {
    this.options = ['Option 1', 'Option 2', 'Option 3'];
    this.filteredOptions$ = of(this.options);
this.messageService.getMessage().subscribe((res:any)=>{
let data= res.user.roles[0].name==='ROLE_TRANSPORTEUR';
if(data) {
  this.filteredItems.push(res.user);
  this.users.push(res.user);
  console.log(res.user)
}
})
    this._transporteurService.getAllDemand().subscribe(res=>{
      this.users=res.filter((item:any)=>item.blocked===false);
   this.assignCopy();
    },error1 => this.assignCopy())

  }

  options: string[];
  filteredOptions$: Observable<string[]>;



  private filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(optionValue => optionValue.toLowerCase().includes(filterValue));
  }

  getFilteredOptions(value: string): Observable<string[]> {
    return of(value).pipe(
      map(filterString => this.filter(filterString)),
    );
  }
  assignCopy(){
    this.filteredItems = Object.assign([], this.users);
  }
  filterItem(value){

  }


  onChange(event) {
    let value=event.target.value;

    if(!value){
      this.assignCopy();
    }
    this.filteredItems = Object.assign([], this.users).filter(
      item => item.username.toLowerCase().indexOf(value.toLowerCase()) > -1
    )


  }

  onSelectionChange($event) {
    this.filteredOptions$ = this.getFilteredOptions($event);
  }




  open(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog);
  }

  confirm(id: any,accountAddress:string,username:string) {
    this.spinner.show().then(console.log);
    let smartContract=JSON.parse(localStorage.getItem("currentUser")).smartContract;
    if(smartContract){
      const data = {id: id, active: true,smartContract:smartContract};
      let ethData = {id: id, accountAddress: accountAddress, name: username}
      this._web3.addGrossiste(ethData).then(signed => {
        let activateDto = {...data, transaction: signed.rawTransaction}
        this._clientService.activateAccount(activateDto).subscribe(response => {
          this.spinner.hide();
        //  this.messageService.removeUser(id);

          Swal.fire('Success', 'Compte activé!', 'success')
          this.filteredItems = this.filteredItems.filter(user => user.id.toString() !== id);
        }, error1 => this.spinner.hide())

      })
    }
    else {
      Swal.fire('Erreur', 'smart contract adresse vide actualiser votre page web!', 'success')
      this.filteredItems = this.filteredItems.filter(user => user.id.toString() !== id);


    }

  }


  delete(id: any,accountAddress:any) {
    this.deleteRequest = {id: id,accountAddress:accountAddress};

    this._clientService.delete(this.deleteRequest).subscribe(response=>{
      Swal.fire('success...', 'Compte supprimer!', 'success')
      this.filteredItems=this.filteredItems.filter( item => (item.id)!==id);
//      this.messageService.removeUser(id);
    })

  }
}
