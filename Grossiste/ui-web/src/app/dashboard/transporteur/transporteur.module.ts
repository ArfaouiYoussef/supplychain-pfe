import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TransporteurRoutingModule} from "./transporteur-routing.module";
import {ListTransporteurComponent} from "./list-transporteur/list-client.component";
import {AddTrasnporteurComponent} from "./add-client/add-client.component";
import {UpdateTransporteurComponent} from "./update-client/update-client.component";
import {TransporteurComponent} from './transporteur.component';
import {ListApprobationComponent} from "./list-approbation/list-approbation.component";
import {BlocageComponent} from "./blocage/blocage.component";
import {NotFoundComponent} from "../../not-found/not-found.component";
import {NbAccordionModule, NbActionsModule, NbCardModule, NbInputModule, NbUserModule} from "@nebular/theme";
import {
  MatButtonModule,
  MatDividerModule,
  MatFormFieldModule,
  MatIconModule, MatInputModule,
  MatTooltipModule
} from "@angular/material";
import {AvatarModule} from "ngx-avatar";
import {NgxSpinnerModule} from "ngx-spinner";

@NgModule({
  declarations: [TransporteurComponent,BlocageComponent,ListApprobationComponent,ListTransporteurComponent,AddTrasnporteurComponent,UpdateTransporteurComponent],
    imports: [
        TransporteurRoutingModule,
        CommonModule,
        NbCardModule,
        NbActionsModule,
        MatIconModule,
        NbAccordionModule,
        AvatarModule,
        MatButtonModule,
        NbInputModule,
        MatTooltipModule,
        NbUserModule,
        MatFormFieldModule,
        MatDividerModule,
        MatInputModule,
        NgxSpinnerModule
    ]
})
export class TransporteurModule { }
