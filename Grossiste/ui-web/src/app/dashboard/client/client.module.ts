import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { AvatarModule } from 'ngx-avatar';

import { ClientRoutingModule } from './client-routing.module';
import { ClientComponent } from './client.component';
import { AddClientComponent } from './add-client/add-client.component';
import { UpdateClientComponent } from './update-client/update-client.component';
import { ListClientComponent } from './list-client/list-client.component';
import { ListApprobationComponent } from './list-approbation/list-approbation.component';
import {
  NbAccordionModule, NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbInputModule,
  NbRadioModule, NbSelectModule, NbUserModule
} from "@nebular/theme";
import {NotFoundModule} from "../../not-found/not-found.module";
import {
  MatButtonModule,
  MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatTooltipModule
} from "@angular/material";
import { BlocageComponent } from './blocage/blocage.component';
import {NgxSpinnerModule} from "ngx-spinner";

@NgModule({
  declarations: [ClientComponent, AddClientComponent, UpdateClientComponent, ListClientComponent, ListApprobationComponent, BlocageComponent],
    imports: [
        CommonModule,
        NotFoundModule,
        ClientRoutingModule,

        NbInputModule,
        NbCardModule,
        NbCheckboxModule,
        NbRadioModule,
        NbButtonModule,
        NbAccordionModule,
        NbActionsModule,
        NbUserModule,
        MatIconModule,
        AvatarModule,
        MatFormFieldModule,
        MatInputModule,
        MatDividerModule,
        MatButtonModule,
        MatTooltipModule,
        NbSelectModule,
        NgxSpinnerModule
    ], schemas: [CUSTOM_ELEMENTS_SCHEMA],

})
export class ClientModule { }
