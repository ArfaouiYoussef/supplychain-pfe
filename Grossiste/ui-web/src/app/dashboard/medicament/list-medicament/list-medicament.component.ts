import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {LocalDataSource} from "ng2-smart-table";
import {ActivateRequest} from "../../interface/ActivateRequest";
import {DeleteRequest} from "../../interface/DeleteRequest";
import {ClientService} from "../../../service/client.service";
import {NbDialogService} from "@nebular/theme";
import {Observable, of} from "rxjs";
import {map} from "rxjs/operators";
import Swal from "sweetalert2";
import {Medicament} from "../../../model/Medicament";
import {MedicamentService} from "../../../service/medicament.service";

@Component({
  selector: 'ngx-list-medicament',
  templateUrl: './list-medicament.component.html',
  styleUrls: ['./list-medicament.component.scss']
})
export class ListMedicamentComponent implements OnInit {


  @ViewChild('item', { static: true }) accordion;

  toggle() {
    this.accordion.toggle();
  }

  private medicaments:Medicament[];
  private  request:ActivateRequest;
  private deleteRequest:DeleteRequest;
  filteredItems: any[] & Medicament[];

  constructor(private _medicamentService:MedicamentService,
              private dialogService: NbDialogService) {
    this.filteredItems=[];
  }

  ngOnInit() {
    this.options = ['Option 1', 'Option 2', 'Option 3'];
    this.filteredOptions$ = of(this.options);

    this._medicamentService.getMedicament().subscribe(res=>{
      this.medicaments=res;
      this.assignCopy();
    },error1 => this.assignCopy())


  }

  options: string[];
  filteredOptions$: Observable<string[]>;



  private filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(optionValue => optionValue.toLowerCase().includes(filterValue));
  }

  getFilteredOptions(value: string): Observable<string[]> {
    return of(value).pipe(
      map(filterString => this.filter(filterString)),
    );
  }
  assignCopy(){
    this.filteredItems = Object.assign([], this.medicaments);
  }
  filterItem(value){

  }


  onChange(event) {
    let value=event.target.value;

    if(!value){
      this.assignCopy();
    }
    this.filteredItems = Object.assign([], this.medicaments).filter(
      item => item.name.toLowerCase().indexOf(value.toLowerCase()) > -1
    )


  }

  onSelectionChange($event) {
    this.filteredOptions$ = this.getFilteredOptions($event);
  }




  open(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog);
  }

  confirm(id: any) {

   this._medicamentService.deleteMedicament(id).subscribe(response=>{
console.log(response)
     this.filteredItems=this.filteredItems.filter( item => (item.id)!==id);

   });





  }






}
