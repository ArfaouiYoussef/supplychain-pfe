import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {ActivateRequest} from "../../interface/ActivateRequest";
import {DeleteRequest} from "../../interface/DeleteRequest";
import {ClientService} from "../../../service/client.service";
import {NbDialogService} from "@nebular/theme";
import {Observable, of} from "rxjs";
import {map} from "rxjs/operators";
import Swal from "sweetalert2";
import {LocalDataSource} from 'ng2-smart-table';
import {MedicamentService} from "../../../service/medicament.service";

@Component({
  selector: 'ngx-classe-therapeutique',
  templateUrl: './classe-therapeutique.component.html',
  styleUrls: ['./classe-therapeutique.component.scss']
})
export class ClasseTherapeutiqueComponent implements OnInit {
  constructor(private _serviceMedicament:MedicamentService,
              private dialog:NbDialogService) {

  }

  ngOnInit(): void {
  this._serviceMedicament.getAllClass().subscribe(response=>{
    this.data=response;
    console.log(this.data)
  })

  }
  open(dialog: TemplateRef<any>) {
    this.dialog.open(dialog);
  }
  settings = {
    delete: {

      deleteButtonContent:'<i  class="nb-trash"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmDelete: true,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
confirmCreate:true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',

      confirmSave: true,
    },
    columns: {
      name: {
        title: 'Name',
      }
    },
  };

  data = [
    {
      name: "classe 1 ",
    },

  ];

  onDeleteConfirm(event) {
    if (window.confirm('Are you sure you want to delete?')) {
      this._serviceMedicament.deleteClass(event.data).subscribe(donee=>{
        console.log("done");
        Swal.fire(
          'Success!',
          ' classe supprimer  !',
          'success'
        )

      } ,error => {Swal.fire(
        'Success!',
        ' classe supprimer  !',
        'success'
        )}
      )
      event.confirm.resolve();

    } else {
      event.confirm.reject();
    }
  }

  onCreateConfirm(event) {

this._serviceMedicament.addClass(event.newData).subscribe(response=>{

  event.confirm.resolve();


},error1 => event.confirm.reject())

  }

  onSaveConfirm(event) {
    const data={oldName:event.data.name,newName:event.newData.name};
    this._serviceMedicament.updateClass(data).subscribe(response=>{
event.confirm.resolve();

    },error1 =>
      event.confirm.reject())
  }

  confirm() {

  }
}
