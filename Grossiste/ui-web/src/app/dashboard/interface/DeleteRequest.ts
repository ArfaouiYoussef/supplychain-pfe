export interface DeleteRequest {
  id: string;
  accountAddress:string;
}
