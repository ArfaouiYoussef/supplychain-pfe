import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import {AuthService} from "./auth-service.service";

@Injectable()
export class AuthGuard implements CanActivate {
test:any;
  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(): boolean {
   let user= localStorage.getItem("currentUser");
    if ( !user ) {
      this.router.navigate(['/auth/login']);
    return false;
    }

    return true;

  }
}
