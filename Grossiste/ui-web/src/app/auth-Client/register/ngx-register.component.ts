import { Router } from '@angular/router';
import { getDeepFromObject } from '@nebular/auth/helpers';
import { NB_AUTH_OPTIONS } from '@nebular/auth';

import * as saveAs from 'file-saver';

import {Component, Inject, OnInit} from '@angular/core';
import { NbLoginComponent } from '@nebular/auth';
import {FormControl, Validators, FormGroup, FormBuilder, ValidatorFn, AbstractControl, ValidationErrors} from "@angular/forms";
import {GlobalsService} from "../../service/globals.service";
import {EthereumProvider} from "../../ethereumProvider/ethereum";
import {takeUntil, timestamp} from "rxjs/operators";
import {UserService} from "../../service/user-service.service";
import {Subject} from 'rxjs';
import {AuthService} from 'app/service/auth.service';
import {NgxSpinnerService} from "ngx-spinner";
import {consoleTestResultHandler} from "tslint/lib/test";

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']

})
export class  RegisterComponent {

  registerForm: FormGroup;
  errors: string[] = [];

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private _auth:AuthService,
    private _formBuilder: FormBuilder ,
    private _web3:EthereumProvider ,
    private  _router:Router,
    private  _spinner:NgxSpinnerService,
  )
  {

    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------



  createAccount(){
    this._spinner.show().then(console.log)
    this.registerForm.controls["roles"].patchValue([this.registerForm.controls["userrole"].value]);
if(!this._web3.checkAddress(this.registerForm.controls["accountAddress"].value)){
this.errors=['Adresse publique invalide'];
this._spinner.hide()
}

else
    this._auth.signup(this.registerForm.value).subscribe(res=>{
this._spinner.hide();

      this._router.navigate(["/auth-client/login"]);


    },error1 => {
      this.errors = ['invalid email']
this._spinner.hide()
    })


  }



  ngOnInit(): void
  {
    this.registerForm = this._formBuilder.group({
      username           : ['', Validators.required],
      email          : ['', [Validators.required, Validators.email]],
      accountAddress:['',Validators.required],
      password       : ['', Validators.required],
      roles      : ['pharmacie'],
      userrole      : ['pharmacie'],

      passwordConfirm: ['', [Validators.required, confirmPasswordValidator]]
    });
    this._web3.generateAccount();



    // Update the validity of the 'passwordConfirm' field
    // when the 'password' field changes
    this.registerForm.get('password').valueChanges
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(() => {
        this.registerForm.get('passwordConfirm').updateValueAndValidity();
      });
  }
  onRegister(){

  }
  ngOnDestroy(): void
  {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

  if ( !control.parent || !control )
  {
    return null;
  }

  const password = control.parent.get('password');
  const passwordConfirm = control.parent.get('passwordConfirm');

  if ( !password || !passwordConfirm )
  {
    return null;
  }

  if ( passwordConfirm.value === '' )
  {
    return null;
  }

  if ( password.value === passwordConfirm.value )
  {
    return null;
  }

  return {passwordsNotMatching: true};
};



