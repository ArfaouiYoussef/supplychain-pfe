export class container{
  id: string;
  email: string;
  tagContainer: string;
  laboAddress: string;
  grossisteAddress: string;
  commandeRef: string;
  temperature: [];
  humidity: [];
  seuilTemperature:string;
  seuilHumidite:string;
  state: string;
  medicaments: string;
  transactionInTransit: string;
  transactionReceived: string;
  affectationTransaction: string;
  transporteurAddress: string;
}
