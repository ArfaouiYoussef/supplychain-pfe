import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {config} from "../config";
import {User} from "../model/user";
import {DeleteRequest} from "../dashboard/interface/DeleteRequest";

@Injectable({
  providedIn: 'root'
})
export class TransporteurService {

  constructor(private _http:HttpClient) { }


  getAllTransporteur(): Observable<any[]>{
    return this._http.get<any[]>(config.apiUrl1+"/api/transporteur/unblocked");
  }

  expandContainer(data){
    return this._http.post(config.apiUrl1+'/api/container/expand',data);
  }
  activateAccount(data):Observable<Object>{

    return this._http.post(config.apiUrl1+"/api/user/activateAccount",data)
  }
  getClient(): Observable<User[]>{
    return this._http.get<User[]>(config.apiUrl1+"/api/transporteur/unblocked");
  }
  BlockedClient(): Observable<User[]>{
    return this._http.get<User[]>(config.apiUrl1+"/api/transporteur/blocked");
  }






  blockClient(data){
    return this._http.post(config.apiUrl1+"/api/user/blocked",data)
  }
  delete(id:DeleteRequest):Observable<Object>{

    return this._http.delete<User>(config.apiUrl1+"/api/user/client"+ "/"+ id.id);
  }
  removeUserFromBlockChian(data):Observable<any>
  {
    return this._http.post(config.apiUrl1+"/api/user/delete",data);
  }



  getAllDemand(): Observable<User[]>{
    return this._http.get<User[]>(config.apiUrl1+"/api/transporteur/demande");
  }


}






