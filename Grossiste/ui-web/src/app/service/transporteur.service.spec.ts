import { TestBed } from '@angular/core/testing';

import { TransporteurService } from './transporteur.service';

describe('TransporteurService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TransporteurService = TestBed.get(TransporteurService);
    expect(service).toBeTruthy();
  });
});
