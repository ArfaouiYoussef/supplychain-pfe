import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError} from "rxjs/operators";
import {config} from 'app/config';
import {throwError} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CommandService {

  constructor(private _http:HttpClient) { }
  // Open connection with the back-end socket



addMedicament(data:any){
 return    this._http.post(config.apiUrl1+"/api/container/addMedicament",data);

}

  addCommand(data){
    console.log(data)
    return this._http.post(config.apiUrl1+"/api/command",data)
      .pipe(catchError((error: any) => throwError(error.message)));


  }
  getContainerById(id){
    return this._http.get(config.apiUrl1+"/api/container/"+id);
  }

  getCommandeById(id){
  return   this._http.get(config.apiUrl1+'/api/command/'+id);
  }
  createContainer(data){
 return    this._http.post(config.apiUrl1+"/api/container",data);
  }
  getCommand(){
    return this._http.get(config.apiUrl1+"/api/command/"+JSON.parse(localStorage.getItem("currentUser")).id)
      .pipe(catchError((error: any) => throwError(error.message)));
  }
  getAllCommand(){
    return this._http.get(config.apiUrl1+"/api/command/")
      .pipe(catchError((error: any) => throwError(error.message)));
  }
  getAllContainer(){
    return this._http.get(config.apiUrl1+'/api/container')
      .pipe(catchError((error: any) => throwError(error.message)));
  }
updateSensorContainer(data){
    console.log(data)
  return this._http.post(config.apiUrl1+"/api/container/updateSeuil",data)
    .pipe(catchError((error: any) => throwError(error.message)));


}
getTransactionDetails(tx){
    return this._http.post(config.apiUrl2+"/api/getTransactionDetails",{
      transactionHash:tx

    })
}
  getContainerDetails(tx){
    return this._http.post(config.apiUrl2+"/api/getContainerDetails",{
      transactionHash:tx

    })
  }

getMedicament(numLot){
  return this._http.post(config.apiUrl2+"/api/getMedicament",{
    numLot:numLot,
    contractAddress:JSON.parse(localStorage.getItem("currentUser")).smartContract
  })
}
getAllContainerExpand(){
    return this._http.get(config.apiUrl1+"/api/container/expand");
}
  getCommandByPublicAdress(){
    return this._http.get(config.apiUrl1+"/api/command/client/"+JSON.parse(localStorage.getItem("currentUser")).accountAddress)
      .pipe(catchError((error: any) => throwError(error.message)));

  }
  disableCommande(id){
    return this._http.post(config.apiUrl1+"/api/command/disable", {id:id})
  }
  submitContract(data)
  {
    return this._http.post(config.apiUrl1+"/api/addCustomer",{transaction:data});
  }
  delete(deleteRequest: any) {

  }
}
