
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { User } from 'app/model/user';

@Injectable({ providedIn: 'root' })
export class MessageService {
  private subject = new Subject<any>();
private removeSubject=new Subject<any>();
  private commandeSubject=new Subject<any>();
  private removeCommandeSubject=new Subject<any>();
  private subjectProfile = new Subject<any>();
  sendProfile(user: User) {
    this.subjectProfile.next({ user:user });
  }

  getProfile(){
    return this.subjectProfile.asObservable();

  }
  sendMessage(user: User) {
    this.subject.next({ user:user });
  }
removeUser(id:any){
    this.removeSubject.next({id:id});
}

sendCommandeMessage(commande:any){
  this.commandeSubject.next({ commande:commande });
}
removeCommande(id:any){
    this.removeCommandeSubject.next({id:id});
  }
  getCommandeToRemove(): Observable<any> {
    return this.removeCommandeSubject.asObservable();
  }
  clearMessage() {
    this.subject.next();
  }
  getUserToRemove(): Observable<any> {
    return this.removeSubject.asObservable();
  }
  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }
  getMessageCommande(): Observable<any> {
    return this.commandeSubject.asObservable();
  }
}
