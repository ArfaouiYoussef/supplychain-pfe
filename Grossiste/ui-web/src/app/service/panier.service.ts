import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {config} from "../config";
import {ClassTh} from "../model/ClassTh";
import {Medicament} from "../model/Medicament";
import {catchError} from "rxjs/operators";
import {Panier} from 'app/dashboard-grossiste/redux/common/models/panier';

@Injectable({
  providedIn: 'root'
})
export class PanierService {

  constructor(private _http:HttpClient) { }


  getAllClass(): Observable<ClassTh[]>{
    return this._http.get<ClassTh[]>(config.apiUrl1+"/api/classTherapeutique");
  }
  addClass(data:ClassTh): Observable<Object>{
    return this._http.post(config.apiUrl1+"/api/classTherapeutique",data);
  }
  deleteMedicamentInBasket(id:any){
let panierId=JSON.parse(localStorage.getItem("currentUser")).panier.id;
console.log(panierId)
    console.log(id)
    return this._http.post(config.apiUrl1+"/api/panier/delete", {medicamentInBasket:id,panierId:panierId});

  }

  deleteClass(data:ClassTh):Observable<Object>{

    return this._http.delete<ClassTh>(config.apiUrl1+"/api/classTherapeutique"+ "/"+ data.id);

  }
  updateMedicamentInBasket(data){
    return this._http.post(config.apiUrl1+"/api/panier/update",data)
      .pipe(catchError((error: any) => throwError(error.message)));


  }
  addPanier(data){
    return this._http.post(config.apiUrl1+"/api/panier",data)
      .pipe(catchError((error: any) => throwError(error.message)));


  }
  updateClass(data:any):Observable<Object>{

    return this._http.put(config.apiUrl1+"/api/classTherapeutique",data);

  }
  updateMedicament(data:any):Observable<Object>{

    return this._http.put(config.apiUrl1+"/api/medicament",data);

  }
  getMedicament(id:any){
    return this._http.get(config.apiUrl1+"/api/medicament"+"/"+id)      .pipe(catchError((error: any) => throwError(error.message)));;


  }
  getPanierById(id):Observable<Panier>{
    return this._http.get<Panier>(config.apiUrl1+"/api/panier/"+id)
      .pipe(catchError((error: any) => throwError(error.message)));
  }

  delete(deleteRequest: any) {

  }
}
