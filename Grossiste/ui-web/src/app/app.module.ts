/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CoreModule } from './@core/core.module';
import { NgxCopyToClipboardModule } from 'ngx-copy-to-clipboard';

import { ThemeModule } from './@theme/theme.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {
  NbButtonModule, NbCardModule,
  NbChatModule,
  NbDatepickerModule,
  NbDialogModule,
  NbMenuModule,
  NbSidebarModule,
  NbToastrModule,
  NbWindowModule,
} from '@nebular/theme';
import {AuthGuard} from './auth/auth-guard.service';
import {NgxLoginComponent} from './auth/login/login.component';
import {AuthService} from './auth/auth-service.service';
import {LoginActivate} from "./auth/LoginActivate";
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {NgxRegisterComponent} from './auth/register/ngx-register.component';
import {EthereumProvider} from "./ethereumProvider/ethereum";
import {MatIconModule, MatSelectModule, MatDialog} from "@angular/material";
import {NgxUploaderModule} from "ngx-uploader";
import {NbAuthModule} from "@nebular/auth";
import {NotFoundComponent} from "./not-found/not-found.component";
import {ErrorInterceptor, JwtInterceptor} from './_helpers';
import {ActionReducer, StoreModule} from "@ngrx/store";
import {panierReducer} from "./dashboard-grossiste/redux/common/store/panier/panier.reducers";
import {productReducer} from "./dashboard-grossiste/redux/common/store/products/products.reducers";
import {EffectsModule} from "@ngrx/effects";
import {ProductsEffects} from "./dashboard-grossiste/redux/common/store/products/products.effects";
import {PanierEffects} from "./dashboard-grossiste/redux/common/store/panier/panier.effects";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {storeLogger} from "ngrx-store-logger";
import {userReducer} from "./dashboard-grossiste/redux/common/store/notification/notification.reducers";
import {NotificationEffects} from "./dashboard-grossiste/redux/common/store/notification/notification.effects";
import {NgxSpinnerModule, NgxSpinnerService} from "ngx-spinner";
import {MatTooltipModule} from '@angular/material/tooltip';

export function logger(reducer: ActionReducer<any>): any {
  return storeLogger()(reducer);
}
import { PushNotificationsModule } from 'ng-push';
import {commandReducer} from "./dashboard-grossiste/redux/common/store/commande/commande.reducers";
import {CommandeEffects} from "./dashboard-grossiste/redux/common/store/commande/commande.effects";
import {AuthGuardClient}  from "./gurads-client/auth.gurad";
import {LoginComponent} from "./auth-Client/login/login.component";
import {RegisterComponent} from "./auth-Client/register/ngx-register.component";
import { NgxChartsModule } from '@swimlane/ngx-charts';
import {StorageService} from "./service/secure-storage.service";


export const metaReducers = [logger];

// @ts-ignore
@NgModule({
  declarations: [AppComponent,NgxLoginComponent,  LoginComponent,RegisterComponent,  NgxRegisterComponent,
  ],
  imports: [
    MatButtonModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatInputModule,
    NgxCopyToClipboardModule,
    HttpClientModule,
    MatFormFieldModule,
    MatTooltipModule,
    FormsModule,


    BrowserModule,
    MatCardModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ThemeModule.forRoot(),
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbWindowModule.forRoot(),
    NbToastrModule.forRoot(),
    NbChatModule.forRoot({
      messageGoogleMapKey: 'AIzaSyA_wNuCzia92MAmdLRzmqitRGvCF7wCZPY',
    }),
    CoreModule.forRoot(),
    MatSelectModule,
    MatIconModule,
    NgxUploaderModule,
    NbAuthModule,
    NbButtonModule,
    PushNotificationsModule,

    StoreModule.forFeature('users', userReducer, { metaReducers }),

    StoreModule.forFeature('panier', panierReducer, { metaReducers }),
    StoreModule.forFeature('command', commandReducer, { metaReducers }),

    StoreModule.forRoot({},{
      runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability: false,
        strictStateSerializability: true,
        strictActionSerializability: true,
        strictActionWithinNgZone: true
      },
    }),
    StoreDevtoolsModule.instrument({}),
StoreModule.forFeature('medicament', productReducer),


    EffectsModule.forRoot([]),

    EffectsModule.forFeature([PanierEffects,ProductsEffects,NotificationEffects,CommandeEffects]),


    NbCardModule,
    NgxChartsModule,
    BrowserAnimationsModule,

    NgxSpinnerModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

  bootstrap: [AppComponent],
  providers: [AuthGuard,AuthGuardClient,NgxSpinnerService ,StorageService,AuthService,EthereumProvider,LoginActivate,     { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],



})
export class AppModule {
}
