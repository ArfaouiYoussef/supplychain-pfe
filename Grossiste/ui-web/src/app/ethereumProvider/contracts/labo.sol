pragma solidity >=0.4.21 <0.7.0;
pragma experimental ABIEncoderV2;
library SharedStruct{
  enum ContainerState {
    ENCOURS,
    INTRANSIT,
    RECEIVED
  }
  struct Medicament {
    string name;
    string numLot;
    string amm;
    string serialNumber;
    string dateExpiration;
    address grossiste;
    string tagContainer;
    address owner;
    string nameOwner;
    address newOwner;
    string nameNewOwner;


    bool exists;

  }
  enum ROLE { LABORATOIRE ,TRANSPORTEUR,GROSSISTE}

  struct User{
    string name;
    string id;
    ROLE role;
    bool exists;

  }


  struct Store{
    Container[] allcontainer;
  }
  struct Container {
    string id;
    string name;
    uint256 dateExport;
    string[] temprature;
    string[] humidite;
    string seuilTemperature;
    string seuilHumidite;
    address owner;
    address grossiste;
    Medicament[] medicaments;
    ContainerState state;
    address transporteur;
    bool exists;

  }

}
contract Laboratoire {
  string[] array;
  uint nbContainer;

  mapping(string => SharedStruct.Medicament) public medicaments;
  mapping(string => SharedStruct.Container) public containers;
  mapping(address=>SharedStruct.User) public users;
  event addMedicament(address indexed owner,address indexed grossiste,string numLot,uint256 time);
  event addContainer(address indexed owner,address indexed grossiste,string tagContainer);
  event assignmentContainer(address indexed owner,address indexed transporteur,string tagContainer);
  event stateContainer(string tagContainer,string temprature,string humidite);
  event addUser(address indexed users ,SharedStruct.ROLE role);


  address public owner;


  constructor() public {
    owner = msg.sender;
  }

  modifier restrictedTransporteur() {
    require (users[msg.sender].exists) ;
    _;
  }


  modifier restricted() {
    require (msg.sender == owner);
    _;
  }

  function AddGrossiste(string memory name, string memory id,address address_user) public  restricted returns (bool){
    users[address_user].id=id;
    users[address_user].name=name;
    users[address_user].exists=true;
    users[address_user].role=SharedStruct.ROLE.GROSSISTE;
    emit addUser(address_user,SharedStruct.ROLE.GROSSISTE);

  }

  function AddTransporteur(string memory name, string memory id,address address_user) public  restricted returns (bool){
    users[address_user].id=id;
    users[address_user].name=name;
    users[address_user].exists=true;
    users[address_user].role=SharedStruct.ROLE.TRANSPORTEUR;
    emit addUser(address_user,SharedStruct.ROLE.TRANSPORTEUR);

  }

  function removeUser(address address_user) external restricted returns(bool){
    require(users[address_user].exists==true);
    users[address_user].exists=false;
    delete users[address_user];
    return true;
  }

  event updateSensorSeuil(address sender,string tagContainer ,string temprature,string humidite);
  function UpdateSensorSeuil(string calldata tagContainer,string calldata temprature,string calldata humidite)restricted external{
    require(containers[tagContainer].exists==true);
    containers[tagContainer].seuilTemperature=temprature;
    containers[tagContainer].seuilHumidite=humidite;
    emit updateSensorSeuil(msg.sender,tagContainer,temprature,humidite);


  }

  function AddMedicament(string  memory name ,string memory numLot,string memory amm,string memory serialNumber
  ,string memory dateExpiration,address grossiste,string memory tagContainer,string memory nameOwner,address newOwner,string memory nameNewOwner) restricted public  returns(bool){
    require(medicaments[numLot].exists==false);

    medicaments[numLot]=SharedStruct.Medicament(name,numLot,amm,serialNumber,dateExpiration,grossiste,tagContainer,msg.sender,nameOwner,newOwner,nameNewOwner,true);
    containers[tagContainer].medicaments.push(medicaments[numLot]);
    emit addMedicament( msg.sender,  grossiste, numLot, now);
    return true;
  }
  function AddContainer(string  calldata tagContainer,address grossite,string calldata seuilTemperature,string calldata seuilHumidite) restricted external returns(bool){
    require(containers[tagContainer].exists==false);
    containers[tagContainer].grossiste=grossite;
    containers[tagContainer].seuilTemperature=seuilTemperature;
    containers[tagContainer].seuilHumidite=seuilHumidite;
    containers[tagContainer].owner=msg.sender;
    containers[tagContainer].state=SharedStruct.ContainerState.ENCOURS;
    containers[tagContainer].exists=true;
    emit addContainer(msg.sender, grossite, tagContainer);

  }
  function AssignmentContainer(address transporteur,string calldata tagContainer)restricted external returns(bool){
    containers[tagContainer].state=SharedStruct.ContainerState.INTRANSIT;
    containers[tagContainer].transporteur=transporteur;
    emit assignmentContainer( owner, transporteur, tagContainer);
    return true;
  }
  function StateContainer(string calldata tagContainer,string calldata temprature,string calldata humidite)external restrictedTransporteur returns(bool){
    containers[tagContainer].temprature.push(temprature);
    containers[tagContainer].humidite.push(humidite);
    containers[tagContainer].transporteur=msg.sender;
    emit stateContainer(tagContainer,temprature,humidite);
  }
  function ReceivedContainer(string calldata tagContainer,string calldata temprature,string calldata humidite) external restrictedTransporteur {
    containers[tagContainer].state=SharedStruct.ContainerState.RECEIVED;
    containers[tagContainer].temprature.push(temprature);
    containers[tagContainer].humidite.push(humidite);
    containers[tagContainer].transporteur=msg.sender;
    emit stateContainer(tagContainer,temprature,humidite);

  }

  event sendContainer(SharedStruct.Container container);


  function getContainer(string memory tagContainer) view public  returns(SharedStruct.Container memory data){
    return containers[tagContainer];
  }


}
