import {Action, createAction, props} from '@ngrx/store';
import {User} from "../../../../../model/user";
import {PanierActionType} from "../panier/panier.actions";

const API_SUCCESS_ACTION = 'ADD_ COMMAND_SUCCESS';

// this will be dispatched from some component or service
// these will be dispatched by the Effect result
export const ApiSuccess = createAction(API_SUCCESS_ACTION, props<{ data: any }>());

export enum CommandActionType {

  GET_COMMAND = 'GET_COMMAND',
  GET_COMMANDByAddress = 'GET_COMMAND',

  GET_COMMAND_SUCCESS = 'GET_COMMAND_SUCCESS',
  GET_COMMAND_FAILED = 'GET_COMMAND_FAILED',
  GET_ALL_COMMAND = 'GET_ALL_COMMAND',
  GET_ALL_COMMAND_SUCCESS = 'GET_ALL_COMMAND_SUCCESS',
  GET_ALL_COMMAND_FAILED = 'GET_ALL_COMMAND_FAILED',

  ADD_COMMAND = 'ADD_COMMAND',
  ADD_COMMAND_SUCCESS = 'ADD_COMMAND_SUCCESS',
  ADD_COMMAND_FAILED = 'ADD_COMMAND_FAILED',
  DESTRUCT = 'DESTRUCT',

}

export class GetCommand implements Action {
  readonly type = CommandActionType.GET_COMMAND;
}

export class GetAllCommand implements Action {
  readonly type = CommandActionType.GET_ALL_COMMAND;
}



export class Destruct implements Action {
  readonly type = CommandActionType.DESTRUCT;
}

export class GetCommandSuccess implements Action {
  readonly type = CommandActionType.GET_COMMAND_SUCCESS;
  constructor(public payload: Array<User>) { }
}
export class GetAllCommandSuccess implements Action {
  readonly type = CommandActionType.GET_ALL_COMMAND_SUCCESS;
  constructor(public payload: Array<User>) { }
}

export class GetAllCommandFailed implements Action {
  readonly type = CommandActionType.GET_ALL_COMMAND_FAILED;
  constructor(public payload: string) { }
}

export class AddCommand implements Action {
  readonly type = CommandActionType.ADD_COMMAND;
  constructor(public payload: any) { }
}

export class AddCommandSuccess implements Action {
  readonly type = CommandActionType.ADD_COMMAND_SUCCESS;
  constructor(public payload: any) { }
}

export class AddCommandFailed implements Action {
  readonly type = CommandActionType.ADD_COMMAND_FAILED;
  constructor(public payload: string) { }
}


export class GetCommandFailed implements Action {
  readonly type = CommandActionType.GET_COMMAND_FAILED;
  constructor(public payload: string) { }
}


export  type  CommandActions = GetCommandSuccess |GetCommandFailed|GetCommandFailed|GetCommand |AddCommand|AddCommandFailed|AddCommandSuccess
|GetAllCommand|GetAllCommandSuccess|GetAllCommandFailed
