import {PanierActions, PanierActionType} from './panier.actions';
import {Panier} from '../../models/panier';
import {act} from "@ngrx/effects";
import {colorSets} from "@swimlane/ngx-charts/release/utils";

export interface IUserState {
  panier: Panier[];
  isLoading: boolean;
  message: string;
}

export const initialState :IUserState ={
  panier: [],
  isLoading: false,
  message: ''
};
let data=[]
export function panierReducer(state = initialState, action: PanierActions) {

  switch (action.type) {

    case PanierActionType.GET_PANIER: {
      return { ...state,
        isSuccess:false,
      };
    }
    case PanierActionType.GET_PANIER_BY_ID: {
      return { ...state,
        isSuccess:false,
      };
    }

    case PanierActionType.GET_PANIER_SUCCESS: {
      let msgText = '';
      let bgClass = '';

      if (action.payload.length < 1) {
        msgText = 'No data found';
        bgClass = 'bg-danger';
      } else {
        msgText = 'Loading data';
        bgClass = 'bg-info';
      }

      return {
        ...state,
        panier: action.payload,
        isSuccess:false,

        message: msgText,
        infoClass: bgClass
      };
    }

    case PanierActionType.GET_PANIER_FAILED: {
      return { ...state };
    }

    case PanierActionType.ADD_PANIER: {

      return {
        isSuccess:false,

        ...state
      };
    }

    case PanierActionType.ADD_PANIER_SUCCESS: {

      return {

    ...state,
        isSuccess: true,

        panier:action.payload,


      };
    }
          case PanierActionType.DESTRUCT :{
      state=null;
            return {
        ...state
      }
}
    case PanierActionType.GET_PANIER_BY_ID_SUCCESS: {
      let msgText = '';
      let bgClass = '';


      return {
        ...state,
        isSuccess:false,
        panier:action.payload
      };
    }

    case PanierActionType.UPDATE_PANIER: {
      const todos = state.panier;
      todos['medicamentInBaskets']=(action.payload.data);
console.log(todos)
      return {
        ...state,
      };
    }

    case PanierActionType.UPDATE_PANIER_SUCCESS: {
      return {
        ...state,
        isSuccess:true,
      };
    }

    case PanierActionType.UPDATE_PANIER_FAILED: {
      return { ...state };
    }

    case PanierActionType.DELETE_PANIER: {
      const todos = state.panier;
      todos['medicamentInBaskets'].forEach((todo: Panier, i: number) => {
        if (todo.id === action.payload) {
          todos['medicamentInBaskets'].splice(i, 1);
        }
      });

      return {
        ...state,
        message: '',
        infoClass: ''
      };
    }

    case PanierActionType.DELETE_PANIER_SUCCESS: {
      return {
        ...state,
      };
    }

    case PanierActionType.DELETE_PANIER_FAILED: {
      return { ...state };
    }

    default: {
      return state;
    }

  }

}
