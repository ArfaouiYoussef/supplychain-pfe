import { Injectable } from '@angular/core';
import {Actions, createEffect, Effect, ofType} from '@ngrx/effects';



import {switchMap, catchError, map, mergeMap, tap} from 'rxjs/operators';
import { of } from 'rxjs';

import {NotificationService} from "../../../../../service/notification.service";
import {
  AddCommandFailed,
  AddCommandSuccess,
  CommandActionType, GetAllCommandFailed, GetAllCommandSuccess,
  GetCommand,
  GetCommandSuccess
} from "./commande.actions";
import {
  AddPanierFailed,
  AddPanierSuccess,
  GetPanierByIdSuccess,
  GetPanierFailed,
  PanierActionType
} from "../panier/panier.actions";
import {Panier} from "../../models/panier";
import {CommandService} from "../../../../../service/command.service";

@Injectable()
export class CommandeEffects {

  constructor(
    private actions$: Actions,
    private notification:NotificationService,
    private commandService:CommandService,

  ) { }

  addCommand$ =createEffect(() => this.actions$.pipe(
    ofType(CommandActionType.ADD_COMMAND),
    mergeMap((action) => {

      return this.commandService.addCommand(action['payload']).pipe(
        map((command: any) => new AddCommandSuccess(command)), tap(() => {
          console.log(' Finished')
        }),
        catchError(error => of(new AddCommandFailed(error))),
      )
    }),
  ));


  getCommande$ =createEffect(() => this.actions$.pipe(
    ofType(CommandActionType.GET_COMMAND),
    mergeMap(() => this.commandService.getCommand().pipe(
      map((commandes: any) => new GetCommandSuccess(commandes)),
      catchError(error => of(new GetCommandSuccess(error)))
      )

    )

  ));

 getAllCommande= createEffect(() => this.actions$.pipe(
    ofType(CommandActionType.GET_ALL_COMMAND),
  mergeMap(() => this.commandService.getAllCommand().pipe(
    map((commandes: any) => new GetAllCommandSuccess(commandes)),
  catchError(error => of(new GetAllCommandFailed(error)))
)

)

));

getCommandByAddress=createEffect(() => this.actions$.pipe(
  ofType(CommandActionType.GET_ALL_COMMAND),
  mergeMap(() => this.commandService.getAllCommand().pipe(
    map((commandes: any) => new GetAllCommandSuccess(commandes)),
    catchError(error => of(new GetAllCommandFailed(error)))
    )

  )

));

/*  @Effect()


  @Effect()
  deletePANIER$ = this.actions$.pipe(
    ofType(PANIERActionType.DELETE_PANIER),
    switchMap((action) =>
      this.todosService.updateAPITodo(action['payload']).pipe(
        map((todoId: number) => new DeleteMedicmanetSuccess(todoId)),
        catchError(error => of(new DeleteMedicmanetFailed(error)))
      )
    )
  );
*/
}
