import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';


import {
  MedicamentsActionType,
  GetMedicamentSuccess,
  GetMedicamentFailed,
  AddMedicmanetSuccess,
  AddMedicmanetFailed,
  UpdateMedicmanetSuccess,
  DeleteMedicmanetSuccess,
  DeleteMedicmanetFailed,
  GetMedicamentById, GetMedicamentByIdSuccess,
} from './products.actions';
import { switchMap, catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';

import { Medicament } from '../../models/medicament';
import {MedicamentService} from "../../../../../service/medicament.service";
import {TodosService} from '../../services/todos/todos.service';

@Injectable()
export class ProductsEffects {

  constructor(
    private actions$: Actions,
    private todosService: TodosService,
    private medicamentService:MedicamentService
  ) { }

  @Effect()
  getMedicaments$ = this.actions$.pipe(
    ofType(MedicamentsActionType.GET_MEDICAMENTS),
    switchMap(() =>
      this.medicamentService.getMedicament().pipe(
        map((products: Array<Medicament>) => new GetMedicamentSuccess(products)),
        catchError(error => of(new GetMedicamentFailed(error)))
      )
    )
  );
  @Effect()
  getMedicamentsById$ = this.actions$.pipe(
    ofType(MedicamentsActionType.GET_MEDICAMENT_BY_ID),
    switchMap((action) =>
      this.medicamentService.getMedicamentById(action["payload"]).pipe(
        map((products: Medicament) => new GetMedicamentByIdSuccess(products)),
        catchError(error => of(new GetMedicamentFailed(error)))
      )
    )
  );
  @Effect()
  addMedicament$ = this.actions$.pipe(
    ofType(MedicamentsActionType.ADD_MEDICAMENT),
    switchMap((action) =>
      this.todosService.addAPITodo(action['payload']).pipe(
        map((products: Medicament) => new AddMedicmanetSuccess(products)),
        catchError(error => of(new AddMedicmanetFailed(error)))
      )
    )
  );

  @Effect()
  updateMedicament$ = this.actions$.pipe(
    ofType(MedicamentsActionType.UPDATE_MEDICAMENT),
    switchMap((action) =>
      this.todosService.updateAPITodo(action['payload']).pipe(
        map((todo: Medicament) => new UpdateMedicmanetSuccess(todo)),
        catchError(error => of(new UpdateMedicmanetSuccess(error)))
      )
    )
  );

  @Effect()
  deleteMedicament$ = this.actions$.pipe(
    ofType(MedicamentsActionType.DELETE_MEDICAMENT),
    switchMap((action) =>
      this.todosService.updateAPITodo(action['payload']).pipe(
        map((todoId: number) => new DeleteMedicmanetSuccess(todoId)),
        catchError(error => of(new DeleteMedicmanetFailed(error)))
      )
    )
  );

}
