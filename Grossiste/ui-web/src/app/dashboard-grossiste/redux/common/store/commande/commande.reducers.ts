import {User} from "../../../../../auth/auth-service.service";
import {CommandActions, CommandActionType} from "./commande.actions";
import {PanierActionType} from "../panier/panier.actions";


export interface IUserState {
  user: User[];
  isLoading: boolean;
  message: string;
}

export const initialState :IUserState ={
  user: [],
  isLoading: false,
  message: ''
};
let data=[];
export function commandReducer(state = initialState, action: CommandActions) {

  switch (action.type) {

    case CommandActionType.GET_COMMAND: {
      return {
        ...state,


        isSuccess: false,
      };
    }

    case CommandActionType.GET_ALL_COMMAND: {
      return {
        ...state,


        isSuccess: false,
      };
    }


    case CommandActionType.GET_COMMAND_SUCCESS: {
      let msgText = '';
      let bgClass = '';

      return {
        ...state,
        command: action.payload,
        isSuccess: false,
        message: msgText,
        infoClass: bgClass
      };
    }
    case CommandActionType.GET_ALL_COMMAND_SUCCESS: {
      let msgText = '';
      let bgClass = '';

      return {
        ...state,
        command: action.payload,
        isSuccess: false,
        message: msgText,
        infoClass: bgClass
      };
    }
    case CommandActionType.GET_ALL_COMMAND_FAILED: {
      return {...state};
    }
    case CommandActionType.GET_COMMAND_FAILED: {
      return {...state};
    }
    case CommandActionType.ADD_COMMAND: {

      return {
        isSuccess:false,

        ...state
      };
    }

    case CommandActionType.ADD_COMMAND_SUCCESS: {

      return {

        ...state,
        isSuccess: true,

        command:action.payload,


      };
    }



  }
}


