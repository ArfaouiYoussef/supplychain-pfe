import {Action, createAction, props} from '@ngrx/store';
import { Panier } from '../../models/panier';

const API_SUCCESS_ACTION = 'ADD_PANIER_SUCCESS';

// this will be dispatched from some component or service
// these will be dispatched by the Effect result
export const ApiSuccess = createAction(API_SUCCESS_ACTION, props<{ data: any }>());

export enum PanierActionType {
  GET_PANIER = 'GET_PANIER',
  GET_PANIER_BY_ID = 'GET_PANIER_BY_ID',
  GET_PANIER_BY_ID_SUCCESS = 'GET_PANIER_BY_ID_SUCCESS',
  GET_PANIER_SUCCESS = 'GET_PANIER_SUCCESS',
  GET_PANIER_FAILED = 'GET_PANIER_FAILED',
  ADD_PANIER = 'ADD_PANIER',
  ADD_PANIER_SUCCESS = 'ADD_PANIER_SUCCESS',
  ADD_PANIER_FAILED = 'ADD_PANIER_FAILED',
  UPDATE_PANIER = 'UPDATE_PANIER',
  UPDATE_PANIER_SUCCESS = 'UPDATE_PANIER_SUCCESS',
  UPDATE_PANIER_FAILED = 'UPDATE_PANIER_FAILED',
  DELETE_PANIER = 'DELETE_PANIER',
  DELETE_PANIER_SUCCESS = 'DELETE_PANIER_SUCCESS',
  DELETE_PANIER_FAILED = 'DELETE_PANIER_FAILED',
  DESTRUCT = "DESTRUCT"
}

export class GetPaniers implements Action {
  readonly type = PanierActionType.GET_PANIER;
}
export class GetPanierById implements Action {
  readonly type = PanierActionType.GET_PANIER_BY_ID;
  constructor(public payload: string) { }



}
export class Destruct implements Action {
  readonly type = PanierActionType.DESTRUCT;
}
export class GetPanierByIdSuccess implements Action {
  readonly type = PanierActionType.GET_PANIER_BY_ID_SUCCESS;
  constructor(public payload:Panier) { }
}
export class GetPanierSuccess implements Action {
  readonly type = PanierActionType.GET_PANIER_SUCCESS;
  constructor(public payload: Array<Panier>) { }
}

export class GetPanierFailed implements Action {
  readonly type = PanierActionType.GET_PANIER_FAILED;
  constructor(public payload: string) { }
}

export class AddPanier implements Action {
  readonly type = PanierActionType.ADD_PANIER;
  constructor(public payload: any) { }
}

export class AddPanierSuccess implements Action {
  readonly type = PanierActionType.ADD_PANIER_SUCCESS;
  constructor(public payload: any) { }
}

export class AddPanierFailed implements Action {
  readonly type = PanierActionType.ADD_PANIER_FAILED;
  constructor(public payload: string) { }
}

export class UpdatePanier implements Action {
  readonly type = PanierActionType.UPDATE_PANIER;
  constructor(public payload: any) { }
}

export class UpdatePanierSuccess implements Action {
  readonly type = PanierActionType.UPDATE_PANIER_SUCCESS;
  constructor(public payload: any) { }
}

export class UpdatePanierFailed implements Action {
  readonly type = PanierActionType.UPDATE_PANIER_FAILED;
  constructor(public payload: string) { }
}

export class DeletePanier implements Action {
  readonly type = PanierActionType.DELETE_PANIER;
  constructor(public payload: string) { }

}

export class DeletePanierSuccess implements Action {
  readonly type = PanierActionType.DELETE_PANIER_SUCCESS;
  constructor(public payload: string) { }
}

export class DeletePanierFailed implements Action {
  readonly type = PanierActionType.DELETE_PANIER_FAILED;
  constructor(public payload: string) { }
}


export type PanierActions = GetPaniers |GetPanierById|
  GetPanierSuccess |Destruct|
  GetPanierFailed |GetPanierByIdSuccess|
  AddPanier|UpdatePanier|
  AddPanierSuccess |
  AddPanierFailed |
  DeletePanierSuccess|
  UpdatePanierSuccess |
  UpdatePanierFailed |
  DeletePanier|
  DeletePanierSuccess |
  DeletePanierFailed
