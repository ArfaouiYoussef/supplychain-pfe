import {User} from "../../../../../auth/auth-service.service";
import {UsersActions, UsersActionType} from './client.actions';


export interface IUserState {
  user: User[];
  isLoading: boolean;
  message: string;
}

export const initialState :IUserState ={
  user: [],
  isLoading: false,
  message: ''
};
let data=[];
export function userReducer(state = initialState, action: UsersActions) {

  switch (action.type) {

    case UsersActionType.GET_USERS: {
      return {
        ...state,
        isSuccess: false,
      };
    }

    case UsersActionType.GET_USERS_SUCCESS: {
      let msgText = '';
      let bgClass = '';

      if (action.payload.length < 1) {
        msgText = 'No data found';
        bgClass = 'bg-danger';
      } else {
        msgText = 'Loading data';
        bgClass = 'bg-info';
      }
      return {
        ...state,
        user: action.payload,
        isSuccess: false,
        message: msgText,
        infoClass: bgClass
      };
    }

    case UsersActionType.GET_USERS_FAILED: {
      return {...state};
    }




  }
}


