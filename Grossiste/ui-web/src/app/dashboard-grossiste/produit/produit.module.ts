import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProduitRoutingModule } from './produit-routing.module';
import {ProduitComponent} from "./produit.component";
import {
  NbAccordionModule,
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbIconModule,
  NbInputModule, NbListModule,
  NbSelectModule, NbStepperModule, NbTooltipModule, NbUserModule
} from "@nebular/theme";
import {
  MatButtonModule,
  MatCardModule,
  MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatTooltipModule
} from "@angular/material";
import { ListProduitComponent } from './list-produit/list-produit.component';
import { DetailProduitComponent } from './detail-produit/detail-produit.component';
import { CheckoutComponent } from './checkout/checkout.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { UpdatePanierComponent } from './update-panier/update-panier.component';
import { PanierProduitComponent } from './panier-produit/panier-produit.component';
import {AvatarModule} from "ngx-avatar";
import {LeafletModule} from "@asymmetrik/ngx-leaflet";
import {FactureComponent} from "./facture/facture.component";
import { CommandeComponent } from './commande/commande.component';
import {MatStepperModule} from "@angular/material/stepper";

@NgModule({
  declarations: [ProduitComponent,CommandeComponent, FactureComponent,ListProduitComponent, DetailProduitComponent, CheckoutComponent, UpdatePanierComponent, PanierProduitComponent, CommandeComponent],
    imports: [
        CommonModule,
        ProduitRoutingModule,
        NbCardModule,
        MatCardModule,
        MatButtonModule,
        MatDividerModule,
        NbInputModule,
        NbSelectModule,
        MatIconModule,
        NbIconModule,
        NbButtonModule,
        NbActionsModule,
        NbListModule,
        NbStepperModule,
        FormsModule,
        AvatarModule,
        NbTooltipModule,
        ReactiveFormsModule,
        NbUserModule,
        LeafletModule,
        MatFormFieldModule,
        NbAccordionModule,
        MatTooltipModule,
        MatStepperModule
    ]
})
export class ProduitModule { }
