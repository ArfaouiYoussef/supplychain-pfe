import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProduitComponent} from "./produit.component";
import {ListProduitComponent} from "./list-produit/list-produit.component";
import {DetailProduitComponent} from "./detail-produit/detail-produit.component";
import {CheckoutComponent} from "./checkout/checkout.component";
import {PanierProduitComponent} from "./panier-produit/panier-produit.component";
import {FactureComponent} from "./facture/facture.component";
import {CommandeComponent} from "./commande/commande.component";

const routes: Routes = [
  {
    path:'',
    component:ProduitComponent,

    children:[
      {
        path:'',
        component:ListProduitComponent,
      },

      {path:"checkout"
        ,
        component:CheckoutComponent

      }
      ,{
        path:':id/details',
        component:DetailProduitComponent
      },{
      path:"panier",
        component:PanierProduitComponent
      },{
      path:"commande/:id/facture",
        component:FactureComponent
      },
      {
        path:"commande",
        component:CommandeComponent
      }
    ]
  }

];
///**

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProduitRoutingModule { }
