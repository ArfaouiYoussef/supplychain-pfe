import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import * as Paniers from '../../redux/common/store/panier/panier.actions';
import { ActionsSubject } from '@ngrx/store';
import { ofType } from "@ngrx/effects";
import {PanierEffects} from "../../redux/common/store/panier/panier.effects";
import {FormArray, FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {NbToastrService} from "@nebular/theme";


@Component({
  selector: 'ngx-panier-produit',
  templateUrl: './panier-produit.component.html',
  styleUrls: ['./panier-produit.component.scss']
})
export class PanierProduitComponent implements OnInit {
  panier: [];
  panierList:FormGroup;
  quantity:FormControl = new FormControl();
  price: number=0;
  totalPrice:number=0;
  fg: any = {};
  panierForm: FormArray;
  list: any;

  constructor(              private store: Store<any>, private storyEffects : PanierEffects
    ,private _formBuider:FormBuilder) {
    this.store.dispatch(new Paniers.GetPanierById(JSON.parse(localStorage.getItem("currentUser")).panier.id))

    this.panierList= this._formBuider.group({
      id:[''],

      classeTherapeutique: [''],
      composition: [''],
      dosage: [''],
      formegalenique: [''],
      indication: [''],
      marque: [''],
      medicamentId: [''],
      medicamentPrice: [''],
      name: [''],
      photoUrl: [''],
      presentation: [''],
      quantity: [''],
      totalMedicamentPrice: [''],
      userId:[''],

    });

    this.list=[]
    this.store.select("panier").subscribe(res=> {

      this.panier=[];
      if(!!res.panier.products) {

        this.list=[]

        Object.keys(res.panier.products).forEach((key) => {
          delete res.panier.products[key].numeroLots;

          // @ts-ignore
          this.list.push( {...res.panier.products[key],quantity:res.panier.productQuantities[key]})
        });

        console.log(this.list)

        this.panierForm=this._formBuider.array(this.list.map(r =>this._formBuider.group(r)))
        console.log(this.panierForm)
        this.totalPrice = this.panierForm.value.reduce((sum, item: any) => sum + item.quantity * item.medicamentPrice, 0);

      }});
console.log(this.panierForm)


  }
  get f() { return this.panierForm.controls; }

  BuildFormDynamic(ClassDatas):FormGroup{
    return this._formBuider.group({
id:[''],
      composition: [''],
      dosage: [''],
      formegalenique: [''],
      indication: [''],
      amm:[''],
      dateExpiration:[''] ,
      marque: [''],
      medicamentId: [''],
      medicamentPrice: [''],
      name: [''],
      stock:[''],
      photoUrl: [''],
      presentation: [''],
      quantity: [''],
      totalMedicamentPrice: [''],
      userId:[''],
    })
  }


  SchoolDetailsForm : FormGroup;

  ngOnInit() {

  }
  delete(item:any){
      let data = {...item};

      this.store.dispatch(new Paniers.AddPanier({
        medicament: data.value,
        panier: JSON.parse(localStorage.getItem("currentUser")).panier.id,
        quantite:-item.get('quantity').value,
        userId: JSON.parse(localStorage.getItem("currentUser")).id
        , dispo: data.controls['quantity'].value

      }));
    }

  edit(item:any) {
console.log(item.get('quantity').value);
let qte=(parseInt(this.quantity.value)-parseInt(item.get('quantity').value));

if(qte<= item.get('stock').value)
{
  let data = { ...item };
  data.controls['quantity'].patchValue(this.quantity);

  data.controls['quantity'].patchValue(qte)
  this.store.dispatch(new Paniers.AddPanier({
    medicament: data.value,
    panier: JSON.parse(localStorage.getItem("currentUser")).panier.id,
    quantite: qte,
    userId: JSON.parse(localStorage.getItem("currentUser")).id
    , dispo: data.controls['quantity'].value

  }));

console.log("dispatch")
}


  }

/*
    if(!!this.quantity.value&&this.quantity.value > 0 && this.quantity.value<=item.get('dispo').value) {
      item.controls['quantity'].patchValue(this.quantity.value)

      let data = {
        data: this.panierForm.value,
        panier: JSON.parse(localStorage.getItem("currentUser")).panier.id,
        medicament: item.value
      }
      this.store.dispatch(new Paniers.UpdatePanier(data))
      this.totalPrice = this.panierForm.value.reduce((sum, item: any) => sum + item.quantity * item.medicamentPrice, 0);
    }
    if(this.quantity.value<=0){
      this.delete(item.get('id').value);
    }

*/



  getUserActivity($event: any[] | any) {


  }

  show(cd: any) {
    console.log(cd)
  }
}
