import {NbMenuItem} from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [

  {
    title: 'Tableau de bord\n',
  },
  {
    title: 'Produit',
    icon: 'people-outline',
    children: [

      {
        title: 'produits',
        link: '/dashboard-pharmacie/produit',

      },
      {
        title: 'Commande',
        link: '/dashboard-pharmacie/produit/commande',

      }
    ],
  },


  {
    title: 'Compte ',
    icon: 'settings-2-outline',
    children: [
      {
        title: 'Paramètres',
        link: '/dashboard-pharmacie/compte',
      }
    ],
  }

];
