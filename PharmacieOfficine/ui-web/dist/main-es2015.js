(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./env.ts":
/*!****************!*\
  !*** ./env.ts ***!
  \****************/
/*! exports provided: AppConfig */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppConfig", function() { return AppConfig; });
let AppConfig = {
    ethereum: {
        provider: 'http://5.135.52.74:8501',
        privateKey: '',
        account: '',
        recipient: '0x.......',
        amount: 0.1,
    }
};


/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/@core/core.module.ts":
/*!**************************************!*\
  !*** ./src/app/@core/core.module.ts ***!
  \**************************************/
/*! exports provided: NbSimpleRoleProvider, NB_CORE_PROVIDERS, CoreModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NbSimpleRoleProvider", function() { return NbSimpleRoleProvider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NB_CORE_PROVIDERS", function() { return NB_CORE_PROVIDERS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CoreModule", function() { return CoreModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _nebular_auth__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @nebular/auth */ "./node_modules/@nebular/auth/fesm2015/index.js");
/* harmony import */ var _nebular_security__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @nebular/security */ "./node_modules/@nebular/security/fesm2015/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _module_import_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./module-import-guard */ "./src/app/@core/module-import-guard.ts");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./utils */ "./src/app/@core/utils/index.ts");
/* harmony import */ var _data_users__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./data/users */ "./src/app/@core/data/users.ts");
/* harmony import */ var _data_electricity__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./data/electricity */ "./src/app/@core/data/electricity.ts");
/* harmony import */ var _data_smart_table__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./data/smart-table */ "./src/app/@core/data/smart-table.ts");
/* harmony import */ var _data_user_activity__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./data/user-activity */ "./src/app/@core/data/user-activity.ts");
/* harmony import */ var _data_orders_chart__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./data/orders-chart */ "./src/app/@core/data/orders-chart.ts");
/* harmony import */ var _data_profit_chart__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./data/profit-chart */ "./src/app/@core/data/profit-chart.ts");
/* harmony import */ var _data_traffic_list__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./data/traffic-list */ "./src/app/@core/data/traffic-list.ts");
/* harmony import */ var _data_earning__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./data/earning */ "./src/app/@core/data/earning.ts");
/* harmony import */ var _data_orders_profit_chart__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./data/orders-profit-chart */ "./src/app/@core/data/orders-profit-chart.ts");
/* harmony import */ var _data_traffic_bar__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./data/traffic-bar */ "./src/app/@core/data/traffic-bar.ts");
/* harmony import */ var _data_profit_bar_animation_chart__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./data/profit-bar-animation-chart */ "./src/app/@core/data/profit-bar-animation-chart.ts");
/* harmony import */ var _data_temperature_humidity__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./data/temperature-humidity */ "./src/app/@core/data/temperature-humidity.ts");
/* harmony import */ var _data_solar__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./data/solar */ "./src/app/@core/data/solar.ts");
/* harmony import */ var _data_traffic_chart__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./data/traffic-chart */ "./src/app/@core/data/traffic-chart.ts");
/* harmony import */ var _data_stats_bar__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./data/stats-bar */ "./src/app/@core/data/stats-bar.ts");
/* harmony import */ var _data_country_order__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./data/country-order */ "./src/app/@core/data/country-order.ts");
/* harmony import */ var _data_stats_progress_bar__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./data/stats-progress-bar */ "./src/app/@core/data/stats-progress-bar.ts");
/* harmony import */ var _data_visitors_analytics__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./data/visitors-analytics */ "./src/app/@core/data/visitors-analytics.ts");
/* harmony import */ var _data_security_cameras__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./data/security-cameras */ "./src/app/@core/data/security-cameras.ts");
/* harmony import */ var _mock_users_service__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./mock/users.service */ "./src/app/@core/mock/users.service.ts");
/* harmony import */ var _mock_electricity_service__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./mock/electricity.service */ "./src/app/@core/mock/electricity.service.ts");
/* harmony import */ var _mock_smart_table_service__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./mock/smart-table.service */ "./src/app/@core/mock/smart-table.service.ts");
/* harmony import */ var _mock_user_activity_service__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./mock/user-activity.service */ "./src/app/@core/mock/user-activity.service.ts");
/* harmony import */ var _mock_orders_chart_service__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./mock/orders-chart.service */ "./src/app/@core/mock/orders-chart.service.ts");
/* harmony import */ var _mock_profit_chart_service__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./mock/profit-chart.service */ "./src/app/@core/mock/profit-chart.service.ts");
/* harmony import */ var _mock_traffic_list_service__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./mock/traffic-list.service */ "./src/app/@core/mock/traffic-list.service.ts");
/* harmony import */ var _mock_earning_service__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./mock/earning.service */ "./src/app/@core/mock/earning.service.ts");
/* harmony import */ var _mock_orders_profit_chart_service__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./mock/orders-profit-chart.service */ "./src/app/@core/mock/orders-profit-chart.service.ts");
/* harmony import */ var _mock_traffic_bar_service__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./mock/traffic-bar.service */ "./src/app/@core/mock/traffic-bar.service.ts");
/* harmony import */ var _mock_profit_bar_animation_chart_service__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./mock/profit-bar-animation-chart.service */ "./src/app/@core/mock/profit-bar-animation-chart.service.ts");
/* harmony import */ var _mock_temperature_humidity_service__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./mock/temperature-humidity.service */ "./src/app/@core/mock/temperature-humidity.service.ts");
/* harmony import */ var _mock_solar_service__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./mock/solar.service */ "./src/app/@core/mock/solar.service.ts");
/* harmony import */ var _mock_traffic_chart_service__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./mock/traffic-chart.service */ "./src/app/@core/mock/traffic-chart.service.ts");
/* harmony import */ var _mock_stats_bar_service__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./mock/stats-bar.service */ "./src/app/@core/mock/stats-bar.service.ts");
/* harmony import */ var _mock_country_order_service__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./mock/country-order.service */ "./src/app/@core/mock/country-order.service.ts");
/* harmony import */ var _mock_stats_progress_bar_service__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./mock/stats-progress-bar.service */ "./src/app/@core/mock/stats-progress-bar.service.ts");
/* harmony import */ var _mock_visitors_analytics_service__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./mock/visitors-analytics.service */ "./src/app/@core/mock/visitors-analytics.service.ts");
/* harmony import */ var _mock_security_cameras_service__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./mock/security-cameras.service */ "./src/app/@core/mock/security-cameras.service.ts");
/* harmony import */ var _mock_mock_data_module__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./mock/mock-data.module */ "./src/app/@core/mock/mock-data.module.ts");













































const socialLinks = [
    {
        url: 'https://github.com/akveo/nebular',
        target: '_blank',
        icon: 'github',
    },
    {
        url: 'https://www.facebook.com/akveo/',
        target: '_blank',
        icon: 'facebook',
    },
    {
        url: 'https://twitter.com/akveo_inc',
        target: '_blank',
        icon: 'twitter',
    },
];
const DATA_SERVICES = [
    { provide: _data_users__WEBPACK_IMPORTED_MODULE_6__["UserData"], useClass: _mock_users_service__WEBPACK_IMPORTED_MODULE_25__["UserService"] },
    { provide: _data_electricity__WEBPACK_IMPORTED_MODULE_7__["ElectricityData"], useClass: _mock_electricity_service__WEBPACK_IMPORTED_MODULE_26__["ElectricityService"] },
    { provide: _data_smart_table__WEBPACK_IMPORTED_MODULE_8__["SmartTableData"], useClass: _mock_smart_table_service__WEBPACK_IMPORTED_MODULE_27__["SmartTableService"] },
    { provide: _data_user_activity__WEBPACK_IMPORTED_MODULE_9__["UserActivityData"], useClass: _mock_user_activity_service__WEBPACK_IMPORTED_MODULE_28__["UserActivityService"] },
    { provide: _data_orders_chart__WEBPACK_IMPORTED_MODULE_10__["OrdersChartData"], useClass: _mock_orders_chart_service__WEBPACK_IMPORTED_MODULE_29__["OrdersChartService"] },
    { provide: _data_profit_chart__WEBPACK_IMPORTED_MODULE_11__["ProfitChartData"], useClass: _mock_profit_chart_service__WEBPACK_IMPORTED_MODULE_30__["ProfitChartService"] },
    { provide: _data_traffic_list__WEBPACK_IMPORTED_MODULE_12__["TrafficListData"], useClass: _mock_traffic_list_service__WEBPACK_IMPORTED_MODULE_31__["TrafficListService"] },
    { provide: _data_earning__WEBPACK_IMPORTED_MODULE_13__["EarningData"], useClass: _mock_earning_service__WEBPACK_IMPORTED_MODULE_32__["EarningService"] },
    { provide: _data_orders_profit_chart__WEBPACK_IMPORTED_MODULE_14__["OrdersProfitChartData"], useClass: _mock_orders_profit_chart_service__WEBPACK_IMPORTED_MODULE_33__["OrdersProfitChartService"] },
    { provide: _data_traffic_bar__WEBPACK_IMPORTED_MODULE_15__["TrafficBarData"], useClass: _mock_traffic_bar_service__WEBPACK_IMPORTED_MODULE_34__["TrafficBarService"] },
    { provide: _data_profit_bar_animation_chart__WEBPACK_IMPORTED_MODULE_16__["ProfitBarAnimationChartData"], useClass: _mock_profit_bar_animation_chart_service__WEBPACK_IMPORTED_MODULE_35__["ProfitBarAnimationChartService"] },
    { provide: _data_temperature_humidity__WEBPACK_IMPORTED_MODULE_17__["TemperatureHumidityData"], useClass: _mock_temperature_humidity_service__WEBPACK_IMPORTED_MODULE_36__["TemperatureHumidityService"] },
    { provide: _data_solar__WEBPACK_IMPORTED_MODULE_18__["SolarData"], useClass: _mock_solar_service__WEBPACK_IMPORTED_MODULE_37__["SolarService"] },
    { provide: _data_traffic_chart__WEBPACK_IMPORTED_MODULE_19__["TrafficChartData"], useClass: _mock_traffic_chart_service__WEBPACK_IMPORTED_MODULE_38__["TrafficChartService"] },
    { provide: _data_stats_bar__WEBPACK_IMPORTED_MODULE_20__["StatsBarData"], useClass: _mock_stats_bar_service__WEBPACK_IMPORTED_MODULE_39__["StatsBarService"] },
    { provide: _data_country_order__WEBPACK_IMPORTED_MODULE_21__["CountryOrderData"], useClass: _mock_country_order_service__WEBPACK_IMPORTED_MODULE_40__["CountryOrderService"] },
    { provide: _data_stats_progress_bar__WEBPACK_IMPORTED_MODULE_22__["StatsProgressBarData"], useClass: _mock_stats_progress_bar_service__WEBPACK_IMPORTED_MODULE_41__["StatsProgressBarService"] },
    { provide: _data_visitors_analytics__WEBPACK_IMPORTED_MODULE_23__["VisitorsAnalyticsData"], useClass: _mock_visitors_analytics_service__WEBPACK_IMPORTED_MODULE_42__["VisitorsAnalyticsService"] },
    { provide: _data_security_cameras__WEBPACK_IMPORTED_MODULE_24__["SecurityCamerasData"], useClass: _mock_security_cameras_service__WEBPACK_IMPORTED_MODULE_43__["SecurityCamerasService"] },
];
class NbSimpleRoleProvider extends _nebular_security__WEBPACK_IMPORTED_MODULE_2__["NbRoleProvider"] {
    getRole() {
        // here you could provide any role based on any auth flow
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])('guest');
    }
}
const NB_CORE_PROVIDERS = [
    ..._mock_mock_data_module__WEBPACK_IMPORTED_MODULE_44__["MockDataModule"].forRoot().providers,
    ...DATA_SERVICES,
    ..._nebular_auth__WEBPACK_IMPORTED_MODULE_1__["NbAuthModule"].forRoot({
        strategies: [
            _nebular_auth__WEBPACK_IMPORTED_MODULE_1__["NbDummyAuthStrategy"].setup({
                name: 'email',
                delay: 3000,
            }),
        ],
        forms: {
            login: {
                socialLinks: socialLinks,
            },
            register: {
                socialLinks: socialLinks,
            },
        },
    }).providers,
    _nebular_security__WEBPACK_IMPORTED_MODULE_2__["NbSecurityModule"].forRoot({
        accessControl: {
            guest: {
                view: '*',
            },
            user: {
                parent: 'guest',
                create: '*',
                edit: '*',
                remove: '*',
            },
        },
    }).providers,
    {
        provide: _nebular_security__WEBPACK_IMPORTED_MODULE_2__["NbRoleProvider"], useClass: NbSimpleRoleProvider,
    },
    _utils__WEBPACK_IMPORTED_MODULE_5__["AnalyticsService"],
    _utils__WEBPACK_IMPORTED_MODULE_5__["LayoutService"],
    _utils__WEBPACK_IMPORTED_MODULE_5__["PlayerService"],
    _utils__WEBPACK_IMPORTED_MODULE_5__["StateService"],
];
class CoreModule {
    constructor(parentModule) {
        Object(_module_import_guard__WEBPACK_IMPORTED_MODULE_4__["throwIfAlreadyLoaded"])(parentModule, 'CoreModule');
    }
    static forRoot() {
        return {
            ngModule: CoreModule,
            providers: [
                ...NB_CORE_PROVIDERS,
            ],
        };
    }
}


/***/ }),

/***/ "./src/app/@core/data/country-order.ts":
/*!*********************************************!*\
  !*** ./src/app/@core/data/country-order.ts ***!
  \*********************************************/
/*! exports provided: CountryOrderData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CountryOrderData", function() { return CountryOrderData; });
class CountryOrderData {
}


/***/ }),

/***/ "./src/app/@core/data/earning.ts":
/*!***************************************!*\
  !*** ./src/app/@core/data/earning.ts ***!
  \***************************************/
/*! exports provided: EarningData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EarningData", function() { return EarningData; });
class EarningData {
}


/***/ }),

/***/ "./src/app/@core/data/electricity.ts":
/*!*******************************************!*\
  !*** ./src/app/@core/data/electricity.ts ***!
  \*******************************************/
/*! exports provided: ElectricityData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ElectricityData", function() { return ElectricityData; });
class ElectricityData {
}


/***/ }),

/***/ "./src/app/@core/data/orders-chart.ts":
/*!********************************************!*\
  !*** ./src/app/@core/data/orders-chart.ts ***!
  \********************************************/
/*! exports provided: OrdersChartData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersChartData", function() { return OrdersChartData; });
class OrdersChartData {
}


/***/ }),

/***/ "./src/app/@core/data/orders-profit-chart.ts":
/*!***************************************************!*\
  !*** ./src/app/@core/data/orders-profit-chart.ts ***!
  \***************************************************/
/*! exports provided: OrdersProfitChartData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersProfitChartData", function() { return OrdersProfitChartData; });
class OrdersProfitChartData {
}


/***/ }),

/***/ "./src/app/@core/data/profit-bar-animation-chart.ts":
/*!**********************************************************!*\
  !*** ./src/app/@core/data/profit-bar-animation-chart.ts ***!
  \**********************************************************/
/*! exports provided: ProfitBarAnimationChartData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfitBarAnimationChartData", function() { return ProfitBarAnimationChartData; });
class ProfitBarAnimationChartData {
}


/***/ }),

/***/ "./src/app/@core/data/profit-chart.ts":
/*!********************************************!*\
  !*** ./src/app/@core/data/profit-chart.ts ***!
  \********************************************/
/*! exports provided: ProfitChartData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfitChartData", function() { return ProfitChartData; });
class ProfitChartData {
}


/***/ }),

/***/ "./src/app/@core/data/security-cameras.ts":
/*!************************************************!*\
  !*** ./src/app/@core/data/security-cameras.ts ***!
  \************************************************/
/*! exports provided: SecurityCamerasData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SecurityCamerasData", function() { return SecurityCamerasData; });
class SecurityCamerasData {
}


/***/ }),

/***/ "./src/app/@core/data/smart-table.ts":
/*!*******************************************!*\
  !*** ./src/app/@core/data/smart-table.ts ***!
  \*******************************************/
/*! exports provided: SmartTableData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SmartTableData", function() { return SmartTableData; });
class SmartTableData {
}


/***/ }),

/***/ "./src/app/@core/data/solar.ts":
/*!*************************************!*\
  !*** ./src/app/@core/data/solar.ts ***!
  \*************************************/
/*! exports provided: SolarData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SolarData", function() { return SolarData; });
class SolarData {
}


/***/ }),

/***/ "./src/app/@core/data/stats-bar.ts":
/*!*****************************************!*\
  !*** ./src/app/@core/data/stats-bar.ts ***!
  \*****************************************/
/*! exports provided: StatsBarData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatsBarData", function() { return StatsBarData; });
class StatsBarData {
}


/***/ }),

/***/ "./src/app/@core/data/stats-progress-bar.ts":
/*!**************************************************!*\
  !*** ./src/app/@core/data/stats-progress-bar.ts ***!
  \**************************************************/
/*! exports provided: StatsProgressBarData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatsProgressBarData", function() { return StatsProgressBarData; });
class StatsProgressBarData {
}


/***/ }),

/***/ "./src/app/@core/data/temperature-humidity.ts":
/*!****************************************************!*\
  !*** ./src/app/@core/data/temperature-humidity.ts ***!
  \****************************************************/
/*! exports provided: TemperatureHumidityData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TemperatureHumidityData", function() { return TemperatureHumidityData; });
class TemperatureHumidityData {
}


/***/ }),

/***/ "./src/app/@core/data/traffic-bar.ts":
/*!*******************************************!*\
  !*** ./src/app/@core/data/traffic-bar.ts ***!
  \*******************************************/
/*! exports provided: TrafficBarData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrafficBarData", function() { return TrafficBarData; });
class TrafficBarData {
}


/***/ }),

/***/ "./src/app/@core/data/traffic-chart.ts":
/*!*********************************************!*\
  !*** ./src/app/@core/data/traffic-chart.ts ***!
  \*********************************************/
/*! exports provided: TrafficChartData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrafficChartData", function() { return TrafficChartData; });
class TrafficChartData {
}


/***/ }),

/***/ "./src/app/@core/data/traffic-list.ts":
/*!********************************************!*\
  !*** ./src/app/@core/data/traffic-list.ts ***!
  \********************************************/
/*! exports provided: TrafficListData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrafficListData", function() { return TrafficListData; });
class TrafficListData {
}


/***/ }),

/***/ "./src/app/@core/data/user-activity.ts":
/*!*********************************************!*\
  !*** ./src/app/@core/data/user-activity.ts ***!
  \*********************************************/
/*! exports provided: UserActivityData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserActivityData", function() { return UserActivityData; });
class UserActivityData {
}


/***/ }),

/***/ "./src/app/@core/data/users.ts":
/*!*************************************!*\
  !*** ./src/app/@core/data/users.ts ***!
  \*************************************/
/*! exports provided: UserData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserData", function() { return UserData; });
class UserData {
}


/***/ }),

/***/ "./src/app/@core/data/visitors-analytics.ts":
/*!**************************************************!*\
  !*** ./src/app/@core/data/visitors-analytics.ts ***!
  \**************************************************/
/*! exports provided: VisitorsAnalyticsData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitorsAnalyticsData", function() { return VisitorsAnalyticsData; });
class VisitorsAnalyticsData {
}


/***/ }),

/***/ "./src/app/@core/mock/country-order.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/@core/mock/country-order.service.ts ***!
  \*****************************************************/
/*! exports provided: CountryOrderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CountryOrderService", function() { return CountryOrderService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _data_country_order__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data/country-order */ "./src/app/@core/data/country-order.ts");


class CountryOrderService extends _data_country_order__WEBPACK_IMPORTED_MODULE_1__["CountryOrderData"] {
    constructor() {
        super(...arguments);
        this.countriesCategories = [
            'Sofas',
            'Furniture',
            'Lighting',
            'Tables',
            'Textiles',
        ];
        this.countriesCategoriesLength = this.countriesCategories.length;
    }
    generateRandomData(nPoints) {
        return Array.from(Array(nPoints)).map(() => {
            return Math.round(Math.random() * 20);
        });
    }
    getCountriesCategories() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.countriesCategories);
    }
    getCountriesCategoriesData(country) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.generateRandomData(this.countriesCategoriesLength));
    }
}


/***/ }),

/***/ "./src/app/@core/mock/earning.service.ts":
/*!***********************************************!*\
  !*** ./src/app/@core/mock/earning.service.ts ***!
  \***********************************************/
/*! exports provided: EarningService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EarningService", function() { return EarningService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _data_earning__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data/earning */ "./src/app/@core/data/earning.ts");


class EarningService extends _data_earning__WEBPACK_IMPORTED_MODULE_1__["EarningData"] {
    constructor() {
        super(...arguments);
        this.currentDate = new Date();
        this.currentValue = Math.random() * 1000;
        this.ONE_DAY = 24 * 3600 * 1000;
        this.pieChartData = [
            {
                value: 50,
                name: 'Bitcoin',
            },
            {
                value: 25,
                name: 'Tether',
            },
            {
                value: 25,
                name: 'Ethereum',
            },
        ];
        this.liveUpdateChartData = {
            bitcoin: {
                liveChart: [],
                delta: {
                    up: true,
                    value: 4,
                },
                dailyIncome: 45895,
            },
            tether: {
                liveChart: [],
                delta: {
                    up: false,
                    value: 9,
                },
                dailyIncome: 5862,
            },
            ethereum: {
                liveChart: [],
                delta: {
                    up: false,
                    value: 21,
                },
                dailyIncome: 584,
            },
        };
    }
    getDefaultLiveChartData(elementsNumber) {
        this.currentDate = new Date();
        this.currentValue = Math.random() * 1000;
        return Array.from(Array(elementsNumber))
            .map(item => this.generateRandomLiveChartData());
    }
    generateRandomLiveChartData() {
        this.currentDate = new Date(+this.currentDate + this.ONE_DAY);
        this.currentValue = this.currentValue + Math.random() * 20 - 11;
        if (this.currentValue < 0) {
            this.currentValue = Math.random() * 100;
        }
        return {
            value: [
                [
                    this.currentDate.getFullYear(),
                    this.currentDate.getMonth(),
                    this.currentDate.getDate(),
                ].join('/'),
                Math.round(this.currentValue),
            ],
        };
    }
    getEarningLiveUpdateCardData(currency) {
        const data = this.liveUpdateChartData[currency.toLowerCase()];
        const newValue = this.generateRandomLiveChartData();
        data.liveChart.shift();
        data.liveChart.push(newValue);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(data.liveChart);
    }
    getEarningCardData(currency) {
        const data = this.liveUpdateChartData[currency.toLowerCase()];
        data.liveChart = this.getDefaultLiveChartData(150);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(data);
    }
    getEarningPieChartData() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.pieChartData);
    }
}


/***/ }),

/***/ "./src/app/@core/mock/electricity.service.ts":
/*!***************************************************!*\
  !*** ./src/app/@core/mock/electricity.service.ts ***!
  \***************************************************/
/*! exports provided: ElectricityService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ElectricityService", function() { return ElectricityService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _data_electricity__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data/electricity */ "./src/app/@core/data/electricity.ts");


class ElectricityService extends _data_electricity__WEBPACK_IMPORTED_MODULE_1__["ElectricityData"] {
    constructor() {
        super();
        this.listData = [
            {
                title: '2015',
                months: [
                    { month: 'Jan', delta: '0.97', down: true, kWatts: '816', cost: '97' },
                    { month: 'Feb', delta: '1.83', down: true, kWatts: '806', cost: '95' },
                    { month: 'Mar', delta: '0.64', down: true, kWatts: '803', cost: '94' },
                    { month: 'Apr', delta: '2.17', down: false, kWatts: '818', cost: '98' },
                    { month: 'May', delta: '1.32', down: true, kWatts: '809', cost: '96' },
                    { month: 'Jun', delta: '0.05', down: true, kWatts: '808', cost: '96' },
                    { month: 'Jul', delta: '1.39', down: false, kWatts: '815', cost: '97' },
                    { month: 'Aug', delta: '0.73', down: true, kWatts: '807', cost: '95' },
                    { month: 'Sept', delta: '2.61', down: true, kWatts: '792', cost: '92' },
                    { month: 'Oct', delta: '0.16', down: true, kWatts: '791', cost: '92' },
                    { month: 'Nov', delta: '1.71', down: true, kWatts: '786', cost: '89' },
                    { month: 'Dec', delta: '0.37', down: false, kWatts: '789', cost: '91' },
                ],
            },
            {
                title: '2016',
                active: true,
                months: [
                    { month: 'Jan', delta: '1.56', down: true, kWatts: '789', cost: '91' },
                    { month: 'Feb', delta: '0.33', down: false, kWatts: '791', cost: '92' },
                    { month: 'Mar', delta: '0.62', down: true, kWatts: '790', cost: '92' },
                    { month: 'Apr', delta: '1.93', down: true, kWatts: '783', cost: '87' },
                    { month: 'May', delta: '2.52', down: true, kWatts: '771', cost: '83' },
                    { month: 'Jun', delta: '0.39', down: false, kWatts: '774', cost: '85' },
                    { month: 'Jul', delta: '1.61', down: true, kWatts: '767', cost: '81' },
                    { month: 'Aug', delta: '1.41', down: true, kWatts: '759', cost: '76' },
                    { month: 'Sept', delta: '1.03', down: true, kWatts: '752', cost: '74' },
                    { month: 'Oct', delta: '2.94', down: false, kWatts: '769', cost: '82' },
                    { month: 'Nov', delta: '0.26', down: true, kWatts: '767', cost: '81' },
                    { month: 'Dec', delta: '1.62', down: true, kWatts: '760', cost: '76' },
                ],
            },
            {
                title: '2017',
                months: [
                    { month: 'Jan', delta: '1.34', down: false, kWatts: '789', cost: '91' },
                    { month: 'Feb', delta: '0.95', down: false, kWatts: '793', cost: '93' },
                    { month: 'Mar', delta: '0.25', down: true, kWatts: '791', cost: '92' },
                    { month: 'Apr', delta: '1.72', down: false, kWatts: '797', cost: '95' },
                    { month: 'May', delta: '2.62', down: true, kWatts: '786', cost: '90' },
                    { month: 'Jun', delta: '0.72', down: false, kWatts: '789', cost: '91' },
                    { month: 'Jul', delta: '0.78', down: true, kWatts: '784', cost: '89' },
                    { month: 'Aug', delta: '0.36', down: true, kWatts: '782', cost: '88' },
                    { month: 'Sept', delta: '0.55', down: false, kWatts: '787', cost: '90' },
                    { month: 'Oct', delta: '1.81', down: true, kWatts: '779', cost: '86' },
                    { month: 'Nov', delta: '1.12', down: true, kWatts: '774', cost: '84' },
                    { month: 'Dec', delta: '0.52', down: false, kWatts: '776', cost: '95' },
                ],
            },
        ];
        this.chartPoints = [
            490, 490, 495, 500,
            505, 510, 520, 530,
            550, 580, 630, 720,
            800, 840, 860, 870,
            870, 860, 840, 800,
            720, 200, 145, 130,
            130, 145, 200, 570,
            635, 660, 670, 670,
            660, 630, 580, 460,
            380, 350, 340, 340,
            340, 340, 340, 340,
            340, 340, 340,
        ];
        this.chartData = this.chartPoints.map((p, index) => ({
            label: (index % 5 === 3) ? `${Math.round(index / 5)}` : '',
            value: p,
        }));
    }
    getListData() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.listData);
    }
    getChartData() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.chartData);
    }
}


/***/ }),

/***/ "./src/app/@core/mock/mock-data.module.ts":
/*!************************************************!*\
  !*** ./src/app/@core/mock/mock-data.module.ts ***!
  \************************************************/
/*! exports provided: MockDataModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MockDataModule", function() { return MockDataModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _users_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./users.service */ "./src/app/@core/mock/users.service.ts");
/* harmony import */ var _electricity_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./electricity.service */ "./src/app/@core/mock/electricity.service.ts");
/* harmony import */ var _smart_table_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./smart-table.service */ "./src/app/@core/mock/smart-table.service.ts");
/* harmony import */ var _user_activity_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./user-activity.service */ "./src/app/@core/mock/user-activity.service.ts");
/* harmony import */ var _orders_chart_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./orders-chart.service */ "./src/app/@core/mock/orders-chart.service.ts");
/* harmony import */ var _profit_chart_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profit-chart.service */ "./src/app/@core/mock/profit-chart.service.ts");
/* harmony import */ var _traffic_list_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./traffic-list.service */ "./src/app/@core/mock/traffic-list.service.ts");
/* harmony import */ var _periods_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./periods.service */ "./src/app/@core/mock/periods.service.ts");
/* harmony import */ var _earning_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./earning.service */ "./src/app/@core/mock/earning.service.ts");
/* harmony import */ var _orders_profit_chart_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./orders-profit-chart.service */ "./src/app/@core/mock/orders-profit-chart.service.ts");
/* harmony import */ var _traffic_bar_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./traffic-bar.service */ "./src/app/@core/mock/traffic-bar.service.ts");
/* harmony import */ var _profit_bar_animation_chart_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./profit-bar-animation-chart.service */ "./src/app/@core/mock/profit-bar-animation-chart.service.ts");
/* harmony import */ var _temperature_humidity_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./temperature-humidity.service */ "./src/app/@core/mock/temperature-humidity.service.ts");
/* harmony import */ var _solar_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./solar.service */ "./src/app/@core/mock/solar.service.ts");
/* harmony import */ var _traffic_chart_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./traffic-chart.service */ "./src/app/@core/mock/traffic-chart.service.ts");
/* harmony import */ var _stats_bar_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./stats-bar.service */ "./src/app/@core/mock/stats-bar.service.ts");
/* harmony import */ var _country_order_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./country-order.service */ "./src/app/@core/mock/country-order.service.ts");
/* harmony import */ var _stats_progress_bar_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./stats-progress-bar.service */ "./src/app/@core/mock/stats-progress-bar.service.ts");
/* harmony import */ var _visitors_analytics_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./visitors-analytics.service */ "./src/app/@core/mock/visitors-analytics.service.ts");
/* harmony import */ var _security_cameras_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./security-cameras.service */ "./src/app/@core/mock/security-cameras.service.ts");





















const SERVICES = [
    _users_service__WEBPACK_IMPORTED_MODULE_1__["UserService"],
    _electricity_service__WEBPACK_IMPORTED_MODULE_2__["ElectricityService"],
    _smart_table_service__WEBPACK_IMPORTED_MODULE_3__["SmartTableService"],
    _user_activity_service__WEBPACK_IMPORTED_MODULE_4__["UserActivityService"],
    _orders_chart_service__WEBPACK_IMPORTED_MODULE_5__["OrdersChartService"],
    _profit_chart_service__WEBPACK_IMPORTED_MODULE_6__["ProfitChartService"],
    _traffic_list_service__WEBPACK_IMPORTED_MODULE_7__["TrafficListService"],
    _periods_service__WEBPACK_IMPORTED_MODULE_8__["PeriodsService"],
    _earning_service__WEBPACK_IMPORTED_MODULE_9__["EarningService"],
    _orders_profit_chart_service__WEBPACK_IMPORTED_MODULE_10__["OrdersProfitChartService"],
    _traffic_bar_service__WEBPACK_IMPORTED_MODULE_11__["TrafficBarService"],
    _profit_bar_animation_chart_service__WEBPACK_IMPORTED_MODULE_12__["ProfitBarAnimationChartService"],
    _temperature_humidity_service__WEBPACK_IMPORTED_MODULE_13__["TemperatureHumidityService"],
    _solar_service__WEBPACK_IMPORTED_MODULE_14__["SolarService"],
    _traffic_chart_service__WEBPACK_IMPORTED_MODULE_15__["TrafficChartService"],
    _stats_bar_service__WEBPACK_IMPORTED_MODULE_16__["StatsBarService"],
    _country_order_service__WEBPACK_IMPORTED_MODULE_17__["CountryOrderService"],
    _stats_progress_bar_service__WEBPACK_IMPORTED_MODULE_18__["StatsProgressBarService"],
    _visitors_analytics_service__WEBPACK_IMPORTED_MODULE_19__["VisitorsAnalyticsService"],
    _security_cameras_service__WEBPACK_IMPORTED_MODULE_20__["SecurityCamerasService"],
];
class MockDataModule {
    static forRoot() {
        return {
            ngModule: MockDataModule,
            providers: [
                ...SERVICES,
            ],
        };
    }
}


/***/ }),

/***/ "./src/app/@core/mock/orders-chart.service.ts":
/*!****************************************************!*\
  !*** ./src/app/@core/mock/orders-chart.service.ts ***!
  \****************************************************/
/*! exports provided: OrdersChartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersChartService", function() { return OrdersChartService; });
/* harmony import */ var _periods_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./periods.service */ "./src/app/@core/mock/periods.service.ts");
/* harmony import */ var _data_orders_chart__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data/orders-chart */ "./src/app/@core/data/orders-chart.ts");


class OrdersChartService extends _data_orders_chart__WEBPACK_IMPORTED_MODULE_1__["OrdersChartData"] {
    constructor(period) {
        super();
        this.period = period;
        this.year = [
            '2012',
            '2013',
            '2014',
            '2015',
            '2016',
            '2017',
            '2018',
        ];
        this.data = {};
        this.data = {
            week: this.getDataForWeekPeriod(),
            month: this.getDataForMonthPeriod(),
            year: this.getDataForYearPeriod(),
        };
    }
    getDataForWeekPeriod() {
        return {
            chartLabel: this.getDataLabels(42, this.period.getWeeks()),
            linesData: [
                [
                    184, 267, 326, 366, 389, 399,
                    392, 371, 340, 304, 265, 227,
                    191, 158, 130, 108, 95, 91, 97,
                    109, 125, 144, 166, 189, 212,
                    236, 259, 280, 300, 316, 329,
                    338, 342, 339, 329, 312, 288,
                    258, 221, 178, 128, 71,
                ],
                [
                    158, 178, 193, 205, 212, 213,
                    204, 190, 180, 173, 168, 164,
                    162, 160, 159, 158, 159, 166,
                    179, 195, 215, 236, 257, 276,
                    292, 301, 304, 303, 300, 293,
                    284, 273, 262, 251, 241, 234,
                    232, 232, 232, 232, 232, 232,
                ],
                [
                    58, 137, 202, 251, 288, 312,
                    323, 324, 311, 288, 257, 222,
                    187, 154, 124, 100, 81, 68, 61,
                    58, 61, 69, 80, 96, 115, 137,
                    161, 186, 210, 233, 254, 271,
                    284, 293, 297, 297, 297, 297,
                    297, 297, 297, 297, 297,
                ],
            ],
        };
    }
    getDataForMonthPeriod() {
        return {
            chartLabel: this.getDataLabels(47, this.period.getMonths()),
            linesData: [
                [
                    5, 63, 113, 156, 194, 225,
                    250, 270, 283, 289, 290,
                    286, 277, 264, 244, 220,
                    194, 171, 157, 151, 150,
                    152, 155, 160, 166, 170,
                    167, 153, 135, 115, 97,
                    82, 71, 64, 63, 62, 61,
                    62, 65, 73, 84, 102,
                    127, 159, 203, 259, 333,
                ],
                [
                    6, 83, 148, 200, 240,
                    265, 273, 259, 211,
                    122, 55, 30, 28, 36,
                    50, 68, 88, 109, 129,
                    146, 158, 163, 165,
                    173, 187, 208, 236,
                    271, 310, 346, 375,
                    393, 400, 398, 387,
                    368, 341, 309, 275,
                    243, 220, 206, 202,
                    207, 222, 247, 286, 348,
                ],
                [
                    398, 348, 315, 292, 274,
                    261, 251, 243, 237, 231,
                    222, 209, 192, 172, 152,
                    132, 116, 102, 90, 80, 71,
                    64, 58, 53, 49, 48, 54, 66,
                    84, 104, 125, 142, 156, 166,
                    172, 174, 172, 167, 159, 149,
                    136, 121, 105, 86, 67, 45, 22,
                ],
            ],
        };
    }
    getDataForYearPeriod() {
        return {
            chartLabel: this.getDataLabels(42, this.year),
            linesData: [
                [
                    190, 269, 327, 366, 389, 398,
                    396, 387, 375, 359, 343, 327,
                    312, 298, 286, 276, 270, 268,
                    265, 258, 247, 234, 220, 204,
                    188, 172, 157, 142, 128, 116,
                    106, 99, 95, 94, 92, 89, 84,
                    77, 69, 60, 49, 36, 22,
                ],
                [
                    265, 307, 337, 359, 375, 386,
                    393, 397, 399, 397, 390, 379,
                    365, 347, 326, 305, 282, 261,
                    241, 223, 208, 197, 190, 187,
                    185, 181, 172, 160, 145, 126,
                    105, 82, 60, 40, 26, 19, 22,
                    43, 82, 141, 220, 321,
                ],
                [
                    9, 165, 236, 258, 244, 206,
                    186, 189, 209, 239, 273, 307,
                    339, 365, 385, 396, 398, 385,
                    351, 300, 255, 221, 197, 181,
                    170, 164, 162, 161, 159, 154,
                    146, 135, 122, 108, 96, 87,
                    83, 82, 82, 82, 82, 82, 82,
                ],
            ],
        };
    }
    getDataLabels(nPoints, labelsArray) {
        const labelsArrayLength = labelsArray.length;
        const step = Math.round(nPoints / labelsArrayLength);
        return Array.from(Array(nPoints)).map((item, index) => {
            const dataIndex = Math.round(index / step);
            return index % step === 0 ? labelsArray[dataIndex] : '';
        });
    }
    getOrdersChartData(period) {
        return this.data[period];
    }
}


/***/ }),

/***/ "./src/app/@core/mock/orders-profit-chart.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/@core/mock/orders-profit-chart.service.ts ***!
  \***********************************************************/
/*! exports provided: OrdersProfitChartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersProfitChartService", function() { return OrdersProfitChartService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _data_orders_chart__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data/orders-chart */ "./src/app/@core/data/orders-chart.ts");
/* harmony import */ var _data_orders_profit_chart__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../data/orders-profit-chart */ "./src/app/@core/data/orders-profit-chart.ts");
/* harmony import */ var _data_profit_chart__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../data/profit-chart */ "./src/app/@core/data/profit-chart.ts");




class OrdersProfitChartService extends _data_orders_profit_chart__WEBPACK_IMPORTED_MODULE_2__["OrdersProfitChartData"] {
    constructor(ordersChartService, profitChartService) {
        super();
        this.ordersChartService = ordersChartService;
        this.profitChartService = profitChartService;
        this.summary = [
            {
                title: 'Marketplace',
                value: 3654,
            },
            {
                title: 'Last Month',
                value: 946,
            },
            {
                title: 'Last Week',
                value: 654,
            },
            {
                title: 'Today',
                value: 230,
            },
        ];
    }
    getOrderProfitChartSummary() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.summary);
    }
    getOrdersChartData(period) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.ordersChartService.getOrdersChartData(period));
    }
    getProfitChartData(period) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.profitChartService.getProfitChartData(period));
    }
}


/***/ }),

/***/ "./src/app/@core/mock/periods.service.ts":
/*!***********************************************!*\
  !*** ./src/app/@core/mock/periods.service.ts ***!
  \***********************************************/
/*! exports provided: PeriodsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PeriodsService", function() { return PeriodsService; });
class PeriodsService {
    getYears() {
        return [
            '2010', '2011', '2012',
            '2013', '2014', '2015',
            '2016', '2017', '2018',
        ];
    }
    getMonths() {
        return [
            'Jan', 'Feb', 'Mar',
            'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep',
            'Oct', 'Nov', 'Dec',
        ];
    }
    getWeeks() {
        return [
            'Mon',
            'Tue',
            'Wed',
            'Thu',
            'Fri',
            'Sat',
            'Sun',
        ];
    }
}


/***/ }),

/***/ "./src/app/@core/mock/profit-bar-animation-chart.service.ts":
/*!******************************************************************!*\
  !*** ./src/app/@core/mock/profit-bar-animation-chart.service.ts ***!
  \******************************************************************/
/*! exports provided: ProfitBarAnimationChartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfitBarAnimationChartService", function() { return ProfitBarAnimationChartService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _data_profit_bar_animation_chart__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data/profit-bar-animation-chart */ "./src/app/@core/data/profit-bar-animation-chart.ts");


class ProfitBarAnimationChartService extends _data_profit_bar_animation_chart__WEBPACK_IMPORTED_MODULE_1__["ProfitBarAnimationChartData"] {
    constructor() {
        super();
        this.data = {
            firstLine: this.getDataForFirstLine(),
            secondLine: this.getDataForSecondLine(),
        };
    }
    getDataForFirstLine() {
        return this.createEmptyArray(100)
            .map((_, index) => {
            const oneFifth = index / 5;
            return (Math.sin(oneFifth) * (oneFifth - 10) + index / 6) * 5;
        });
    }
    getDataForSecondLine() {
        return this.createEmptyArray(100)
            .map((_, index) => {
            const oneFifth = index / 5;
            return (Math.cos(oneFifth) * (oneFifth - 10) + index / 6) * 5;
        });
    }
    createEmptyArray(nPoints) {
        return Array.from(Array(nPoints));
    }
    getChartData() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.data);
    }
}


/***/ }),

/***/ "./src/app/@core/mock/profit-chart.service.ts":
/*!****************************************************!*\
  !*** ./src/app/@core/mock/profit-chart.service.ts ***!
  \****************************************************/
/*! exports provided: ProfitChartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfitChartService", function() { return ProfitChartService; });
/* harmony import */ var _periods_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./periods.service */ "./src/app/@core/mock/periods.service.ts");
/* harmony import */ var _data_profit_chart__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data/profit-chart */ "./src/app/@core/data/profit-chart.ts");


class ProfitChartService extends _data_profit_chart__WEBPACK_IMPORTED_MODULE_1__["ProfitChartData"] {
    constructor(period) {
        super();
        this.period = period;
        this.year = [
            '2012',
            '2013',
            '2014',
            '2015',
            '2016',
            '2017',
            '2018',
        ];
        this.data = {};
        this.data = {
            week: this.getDataForWeekPeriod(),
            month: this.getDataForMonthPeriod(),
            year: this.getDataForYearPeriod(),
        };
    }
    getDataForWeekPeriod() {
        const nPoint = this.period.getWeeks().length;
        return {
            chartLabel: this.period.getWeeks(),
            data: [
                this.getRandomData(nPoint),
                this.getRandomData(nPoint),
                this.getRandomData(nPoint),
            ],
        };
    }
    getDataForMonthPeriod() {
        const nPoint = this.period.getMonths().length;
        return {
            chartLabel: this.period.getMonths(),
            data: [
                this.getRandomData(nPoint),
                this.getRandomData(nPoint),
                this.getRandomData(nPoint),
            ],
        };
    }
    getDataForYearPeriod() {
        const nPoint = this.year.length;
        return {
            chartLabel: this.year,
            data: [
                this.getRandomData(nPoint),
                this.getRandomData(nPoint),
                this.getRandomData(nPoint),
            ],
        };
    }
    getRandomData(nPoints) {
        return Array.from(Array(nPoints)).map(() => {
            return Math.round(Math.random() * 500);
        });
    }
    getProfitChartData(period) {
        return this.data[period];
    }
}


/***/ }),

/***/ "./src/app/@core/mock/security-cameras.service.ts":
/*!********************************************************!*\
  !*** ./src/app/@core/mock/security-cameras.service.ts ***!
  \********************************************************/
/*! exports provided: SecurityCamerasService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SecurityCamerasService", function() { return SecurityCamerasService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _data_security_cameras__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data/security-cameras */ "./src/app/@core/data/security-cameras.ts");


class SecurityCamerasService extends _data_security_cameras__WEBPACK_IMPORTED_MODULE_1__["SecurityCamerasData"] {
    constructor() {
        super(...arguments);
        this.cameras = [
            {
                title: 'Camera #1',
                source: 'assets/images/camera1.jpg',
            },
            {
                title: 'Camera #2',
                source: 'assets/images/camera2.jpg',
            },
            {
                title: 'Camera #3',
                source: 'assets/images/camera3.jpg',
            },
            {
                title: 'Camera #4',
                source: 'assets/images/camera4.jpg',
            },
        ];
    }
    getCamerasData() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.cameras);
    }
}


/***/ }),

/***/ "./src/app/@core/mock/smart-table.service.ts":
/*!***************************************************!*\
  !*** ./src/app/@core/mock/smart-table.service.ts ***!
  \***************************************************/
/*! exports provided: SmartTableService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SmartTableService", function() { return SmartTableService; });
/* harmony import */ var _data_smart_table__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../data/smart-table */ "./src/app/@core/data/smart-table.ts");

class SmartTableService extends _data_smart_table__WEBPACK_IMPORTED_MODULE_0__["SmartTableData"] {
    constructor() {
        super(...arguments);
        this.data = [{
                id: 1,
                firstName: 'Mark',
                lastName: 'Otto',
                username: '@mdo',
                email: 'mdo@gmail.com',
                age: '28',
            }, {
                id: 2,
                firstName: 'Jacob',
                lastName: 'Thornton',
                username: '@fat',
                email: 'fat@yandex.ru',
                age: '45',
            }, {
                id: 3,
                firstName: 'Larry',
                lastName: 'Bird',
                username: '@twitter',
                email: 'twitter@outlook.com',
                age: '18',
            }, {
                id: 4,
                firstName: 'John',
                lastName: 'Snow',
                username: '@snow',
                email: 'snow@gmail.com',
                age: '20',
            }, {
                id: 5,
                firstName: 'Jack',
                lastName: 'Sparrow',
                username: '@jack',
                email: 'jack@yandex.ru',
                age: '30',
            }, {
                id: 6,
                firstName: 'Ann',
                lastName: 'Smith',
                username: '@ann',
                email: 'ann@gmail.com',
                age: '21',
            }, {
                id: 7,
                firstName: 'Barbara',
                lastName: 'Black',
                username: '@barbara',
                email: 'barbara@yandex.ru',
                age: '43',
            }, {
                id: 8,
                firstName: 'Sevan',
                lastName: 'Bagrat',
                username: '@sevan',
                email: 'sevan@outlook.com',
                age: '13',
            }, {
                id: 9,
                firstName: 'Ruben',
                lastName: 'Vardan',
                username: '@ruben',
                email: 'ruben@gmail.com',
                age: '22',
            }, {
                id: 10,
                firstName: 'Karen',
                lastName: 'Sevan',
                username: '@karen',
                email: 'karen@yandex.ru',
                age: '33',
            }, {
                id: 11,
                firstName: 'Mark',
                lastName: 'Otto',
                username: '@mark',
                email: 'mark@gmail.com',
                age: '38',
            }, {
                id: 12,
                firstName: 'Jacob',
                lastName: 'Thornton',
                username: '@jacob',
                email: 'jacob@yandex.ru',
                age: '48',
            }, {
                id: 13,
                firstName: 'Haik',
                lastName: 'Hakob',
                username: '@haik',
                email: 'haik@outlook.com',
                age: '48',
            }, {
                id: 14,
                firstName: 'Garegin',
                lastName: 'Jirair',
                username: '@garegin',
                email: 'garegin@gmail.com',
                age: '40',
            }, {
                id: 15,
                firstName: 'Krikor',
                lastName: 'Bedros',
                username: '@krikor',
                email: 'krikor@yandex.ru',
                age: '32',
            }, {
                'id': 16,
                'firstName': 'Francisca',
                'lastName': 'Brady',
                'username': '@Gibson',
                'email': 'franciscagibson@comtours.com',
                'age': 11,
            }, {
                'id': 17,
                'firstName': 'Tillman',
                'lastName': 'Figueroa',
                'username': '@Snow',
                'email': 'tillmansnow@comtours.com',
                'age': 34,
            }, {
                'id': 18,
                'firstName': 'Jimenez',
                'lastName': 'Morris',
                'username': '@Bryant',
                'email': 'jimenezbryant@comtours.com',
                'age': 45,
            }, {
                'id': 19,
                'firstName': 'Sandoval',
                'lastName': 'Jacobson',
                'username': '@Mcbride',
                'email': 'sandovalmcbride@comtours.com',
                'age': 32,
            }, {
                'id': 20,
                'firstName': 'Griffin',
                'lastName': 'Torres',
                'username': '@Charles',
                'email': 'griffincharles@comtours.com',
                'age': 19,
            }, {
                'id': 21,
                'firstName': 'Cora',
                'lastName': 'Parker',
                'username': '@Caldwell',
                'email': 'coracaldwell@comtours.com',
                'age': 27,
            }, {
                'id': 22,
                'firstName': 'Cindy',
                'lastName': 'Bond',
                'username': '@Velez',
                'email': 'cindyvelez@comtours.com',
                'age': 24,
            }, {
                'id': 23,
                'firstName': 'Frieda',
                'lastName': 'Tyson',
                'username': '@Craig',
                'email': 'friedacraig@comtours.com',
                'age': 45,
            }, {
                'id': 24,
                'firstName': 'Cote',
                'lastName': 'Holcomb',
                'username': '@Rowe',
                'email': 'coterowe@comtours.com',
                'age': 20,
            }, {
                'id': 25,
                'firstName': 'Trujillo',
                'lastName': 'Mejia',
                'username': '@Valenzuela',
                'email': 'trujillovalenzuela@comtours.com',
                'age': 16,
            }, {
                'id': 26,
                'firstName': 'Pruitt',
                'lastName': 'Shepard',
                'username': '@Sloan',
                'email': 'pruittsloan@comtours.com',
                'age': 44,
            }, {
                'id': 27,
                'firstName': 'Sutton',
                'lastName': 'Ortega',
                'username': '@Black',
                'email': 'suttonblack@comtours.com',
                'age': 42,
            }, {
                'id': 28,
                'firstName': 'Marion',
                'lastName': 'Heath',
                'username': '@Espinoza',
                'email': 'marionespinoza@comtours.com',
                'age': 47,
            }, {
                'id': 29,
                'firstName': 'Newman',
                'lastName': 'Hicks',
                'username': '@Keith',
                'email': 'newmankeith@comtours.com',
                'age': 15,
            }, {
                'id': 30,
                'firstName': 'Boyle',
                'lastName': 'Larson',
                'username': '@Summers',
                'email': 'boylesummers@comtours.com',
                'age': 32,
            }, {
                'id': 31,
                'firstName': 'Haynes',
                'lastName': 'Vinson',
                'username': '@Mckenzie',
                'email': 'haynesmckenzie@comtours.com',
                'age': 15,
            }, {
                'id': 32,
                'firstName': 'Miller',
                'lastName': 'Acosta',
                'username': '@Young',
                'email': 'milleryoung@comtours.com',
                'age': 55,
            }, {
                'id': 33,
                'firstName': 'Johnston',
                'lastName': 'Brown',
                'username': '@Knight',
                'email': 'johnstonknight@comtours.com',
                'age': 29,
            }, {
                'id': 34,
                'firstName': 'Lena',
                'lastName': 'Pitts',
                'username': '@Forbes',
                'email': 'lenaforbes@comtours.com',
                'age': 25,
            }, {
                'id': 35,
                'firstName': 'Terrie',
                'lastName': 'Kennedy',
                'username': '@Branch',
                'email': 'terriebranch@comtours.com',
                'age': 37,
            }, {
                'id': 36,
                'firstName': 'Louise',
                'lastName': 'Aguirre',
                'username': '@Kirby',
                'email': 'louisekirby@comtours.com',
                'age': 44,
            }, {
                'id': 37,
                'firstName': 'David',
                'lastName': 'Patton',
                'username': '@Sanders',
                'email': 'davidsanders@comtours.com',
                'age': 26,
            }, {
                'id': 38,
                'firstName': 'Holden',
                'lastName': 'Barlow',
                'username': '@Mckinney',
                'email': 'holdenmckinney@comtours.com',
                'age': 11,
            }, {
                'id': 39,
                'firstName': 'Baker',
                'lastName': 'Rivera',
                'username': '@Montoya',
                'email': 'bakermontoya@comtours.com',
                'age': 47,
            }, {
                'id': 40,
                'firstName': 'Belinda',
                'lastName': 'Lloyd',
                'username': '@Calderon',
                'email': 'belindacalderon@comtours.com',
                'age': 21,
            }, {
                'id': 41,
                'firstName': 'Pearson',
                'lastName': 'Patrick',
                'username': '@Clements',
                'email': 'pearsonclements@comtours.com',
                'age': 42,
            }, {
                'id': 42,
                'firstName': 'Alyce',
                'lastName': 'Mckee',
                'username': '@Daugherty',
                'email': 'alycedaugherty@comtours.com',
                'age': 55,
            }, {
                'id': 43,
                'firstName': 'Valencia',
                'lastName': 'Spence',
                'username': '@Olsen',
                'email': 'valenciaolsen@comtours.com',
                'age': 20,
            }, {
                'id': 44,
                'firstName': 'Leach',
                'lastName': 'Holcomb',
                'username': '@Humphrey',
                'email': 'leachhumphrey@comtours.com',
                'age': 28,
            }, {
                'id': 45,
                'firstName': 'Moss',
                'lastName': 'Baxter',
                'username': '@Fitzpatrick',
                'email': 'mossfitzpatrick@comtours.com',
                'age': 51,
            }, {
                'id': 46,
                'firstName': 'Jeanne',
                'lastName': 'Cooke',
                'username': '@Ward',
                'email': 'jeanneward@comtours.com',
                'age': 59,
            }, {
                'id': 47,
                'firstName': 'Wilma',
                'lastName': 'Briggs',
                'username': '@Kidd',
                'email': 'wilmakidd@comtours.com',
                'age': 53,
            }, {
                'id': 48,
                'firstName': 'Beatrice',
                'lastName': 'Perry',
                'username': '@Gilbert',
                'email': 'beatricegilbert@comtours.com',
                'age': 39,
            }, {
                'id': 49,
                'firstName': 'Whitaker',
                'lastName': 'Hyde',
                'username': '@Mcdonald',
                'email': 'whitakermcdonald@comtours.com',
                'age': 35,
            }, {
                'id': 50,
                'firstName': 'Rebekah',
                'lastName': 'Duran',
                'username': '@Gross',
                'email': 'rebekahgross@comtours.com',
                'age': 40,
            }, {
                'id': 51,
                'firstName': 'Earline',
                'lastName': 'Mayer',
                'username': '@Woodward',
                'email': 'earlinewoodward@comtours.com',
                'age': 52,
            }, {
                'id': 52,
                'firstName': 'Moran',
                'lastName': 'Baxter',
                'username': '@Johns',
                'email': 'moranjohns@comtours.com',
                'age': 20,
            }, {
                'id': 53,
                'firstName': 'Nanette',
                'lastName': 'Hubbard',
                'username': '@Cooke',
                'email': 'nanettecooke@comtours.com',
                'age': 55,
            }, {
                'id': 54,
                'firstName': 'Dalton',
                'lastName': 'Walker',
                'username': '@Hendricks',
                'email': 'daltonhendricks@comtours.com',
                'age': 25,
            }, {
                'id': 55,
                'firstName': 'Bennett',
                'lastName': 'Blake',
                'username': '@Pena',
                'email': 'bennettpena@comtours.com',
                'age': 13,
            }, {
                'id': 56,
                'firstName': 'Kellie',
                'lastName': 'Horton',
                'username': '@Weiss',
                'email': 'kellieweiss@comtours.com',
                'age': 48,
            }, {
                'id': 57,
                'firstName': 'Hobbs',
                'lastName': 'Talley',
                'username': '@Sanford',
                'email': 'hobbssanford@comtours.com',
                'age': 28,
            }, {
                'id': 58,
                'firstName': 'Mcguire',
                'lastName': 'Donaldson',
                'username': '@Roman',
                'email': 'mcguireroman@comtours.com',
                'age': 38,
            }, {
                'id': 59,
                'firstName': 'Rodriquez',
                'lastName': 'Saunders',
                'username': '@Harper',
                'email': 'rodriquezharper@comtours.com',
                'age': 20,
            }, {
                'id': 60,
                'firstName': 'Lou',
                'lastName': 'Conner',
                'username': '@Sanchez',
                'email': 'lousanchez@comtours.com',
                'age': 16,
            }];
    }
    getData() {
        return this.data;
    }
}


/***/ }),

/***/ "./src/app/@core/mock/solar.service.ts":
/*!*********************************************!*\
  !*** ./src/app/@core/mock/solar.service.ts ***!
  \*********************************************/
/*! exports provided: SolarService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SolarService", function() { return SolarService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _data_solar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data/solar */ "./src/app/@core/data/solar.ts");


class SolarService extends _data_solar__WEBPACK_IMPORTED_MODULE_1__["SolarData"] {
    constructor() {
        super(...arguments);
        this.value = 42;
    }
    getSolarData() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.value);
    }
}


/***/ }),

/***/ "./src/app/@core/mock/stats-bar.service.ts":
/*!*************************************************!*\
  !*** ./src/app/@core/mock/stats-bar.service.ts ***!
  \*************************************************/
/*! exports provided: StatsBarService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatsBarService", function() { return StatsBarService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _data_stats_bar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data/stats-bar */ "./src/app/@core/data/stats-bar.ts");


class StatsBarService extends _data_stats_bar__WEBPACK_IMPORTED_MODULE_1__["StatsBarData"] {
    constructor() {
        super(...arguments);
        this.statsBarData = [
            300, 520, 435, 530,
            730, 620, 660, 860,
        ];
    }
    getStatsBarData() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.statsBarData);
    }
}


/***/ }),

/***/ "./src/app/@core/mock/stats-progress-bar.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/@core/mock/stats-progress-bar.service.ts ***!
  \**********************************************************/
/*! exports provided: StatsProgressBarService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatsProgressBarService", function() { return StatsProgressBarService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _data_stats_progress_bar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data/stats-progress-bar */ "./src/app/@core/data/stats-progress-bar.ts");


class StatsProgressBarService extends _data_stats_progress_bar__WEBPACK_IMPORTED_MODULE_1__["StatsProgressBarData"] {
    constructor() {
        super(...arguments);
        this.progressInfoData = [
            {
                title: 'Today’s Profit',
                value: 572900,
                activeProgress: 70,
                description: 'Better than last week (70%)',
            },
            {
                title: 'New Orders',
                value: 6378,
                activeProgress: 30,
                description: 'Better than last week (30%)',
            },
            {
                title: 'New Comments',
                value: 200,
                activeProgress: 55,
                description: 'Better than last week (55%)',
            },
        ];
    }
    getProgressInfoData() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.progressInfoData);
    }
}


/***/ }),

/***/ "./src/app/@core/mock/temperature-humidity.service.ts":
/*!************************************************************!*\
  !*** ./src/app/@core/mock/temperature-humidity.service.ts ***!
  \************************************************************/
/*! exports provided: TemperatureHumidityService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TemperatureHumidityService", function() { return TemperatureHumidityService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _data_temperature_humidity__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data/temperature-humidity */ "./src/app/@core/data/temperature-humidity.ts");


class TemperatureHumidityService extends _data_temperature_humidity__WEBPACK_IMPORTED_MODULE_1__["TemperatureHumidityData"] {
    constructor() {
        super(...arguments);
        this.temperatureDate = {
            value: 24,
            min: 12,
            max: 30,
        };
        this.humidityDate = {
            value: 87,
            min: 0,
            max: 100,
        };
    }
    getTemperatureData() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.temperatureDate);
    }
    getHumidityData() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.humidityDate);
    }
}


/***/ }),

/***/ "./src/app/@core/mock/traffic-bar.service.ts":
/*!***************************************************!*\
  !*** ./src/app/@core/mock/traffic-bar.service.ts ***!
  \***************************************************/
/*! exports provided: TrafficBarService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrafficBarService", function() { return TrafficBarService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _periods_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./periods.service */ "./src/app/@core/mock/periods.service.ts");
/* harmony import */ var _data_traffic_bar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../data/traffic-bar */ "./src/app/@core/data/traffic-bar.ts");



class TrafficBarService extends _data_traffic_bar__WEBPACK_IMPORTED_MODULE_2__["TrafficBarData"] {
    constructor(period) {
        super();
        this.period = period;
        this.data = {};
        this.data = {
            week: this.getDataForWeekPeriod(),
            month: this.getDataForMonthPeriod(),
            year: this.getDataForYearPeriod(),
        };
    }
    getDataForWeekPeriod() {
        return {
            data: [10, 15, 19, 7, 20, 13, 15],
            labels: this.period.getWeeks(),
            formatter: '{c0} MB',
        };
    }
    getDataForMonthPeriod() {
        return {
            data: [0.5, 0.3, 0.8, 0.2, 0.3, 0.7, 0.8, 1, 0.7, 0.8, 0.6, 0.7],
            labels: this.period.getMonths(),
            formatter: '{c0} GB',
        };
    }
    getDataForYearPeriod() {
        return {
            data: [10, 15, 19, 7, 20, 13, 15, 19, 11],
            labels: this.period.getYears(),
            formatter: '{c0} GB',
        };
    }
    getTrafficBarData(period) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.data[period]);
    }
}


/***/ }),

/***/ "./src/app/@core/mock/traffic-chart.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/@core/mock/traffic-chart.service.ts ***!
  \*****************************************************/
/*! exports provided: TrafficChartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrafficChartService", function() { return TrafficChartService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _data_traffic_chart__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data/traffic-chart */ "./src/app/@core/data/traffic-chart.ts");


class TrafficChartService extends _data_traffic_chart__WEBPACK_IMPORTED_MODULE_1__["TrafficChartData"] {
    constructor() {
        super(...arguments);
        this.data = [
            300, 520, 435, 530,
            730, 620, 660, 860,
        ];
    }
    getTrafficChartData() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.data);
    }
}


/***/ }),

/***/ "./src/app/@core/mock/traffic-list.service.ts":
/*!****************************************************!*\
  !*** ./src/app/@core/mock/traffic-list.service.ts ***!
  \****************************************************/
/*! exports provided: TrafficListService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrafficListService", function() { return TrafficListService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _periods_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./periods.service */ "./src/app/@core/mock/periods.service.ts");
/* harmony import */ var _data_traffic_list__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../data/traffic-list */ "./src/app/@core/data/traffic-list.ts");



class TrafficListService extends _data_traffic_list__WEBPACK_IMPORTED_MODULE_2__["TrafficListData"] {
    constructor(period) {
        super();
        this.period = period;
        this.getRandom = (roundTo) => Math.round(Math.random() * roundTo);
        this.data = {};
        this.data = {
            week: this.getDataWeek(),
            month: this.getDataMonth(),
            year: this.getDataYear(),
        };
    }
    getDataWeek() {
        const getFirstDateInPeriod = () => {
            const weeks = this.period.getWeeks();
            return weeks[weeks.length - 1];
        };
        return this.reduceData(this.period.getWeeks(), getFirstDateInPeriod);
    }
    getDataMonth() {
        const getFirstDateInPeriod = () => {
            const months = this.period.getMonths();
            return months[months.length - 1];
        };
        return this.reduceData(this.period.getMonths(), getFirstDateInPeriod);
    }
    getDataYear() {
        const getFirstDateInPeriod = () => {
            const years = this.period.getYears();
            return `${parseInt(years[0], 10) - 1}`;
        };
        return this.reduceData(this.period.getYears(), getFirstDateInPeriod);
    }
    reduceData(timePeriods, getFirstDateInPeriod) {
        return timePeriods.reduce((result, timePeriod, index) => {
            const hasResult = result[index - 1];
            const prevDate = hasResult ?
                result[index - 1].comparison.nextDate :
                getFirstDateInPeriod();
            const prevValue = hasResult ?
                result[index - 1].comparison.nextValue :
                this.getRandom(100);
            const nextValue = this.getRandom(100);
            const deltaValue = prevValue - nextValue;
            const item = {
                date: timePeriod,
                value: this.getRandom(1000),
                delta: {
                    up: deltaValue <= 0,
                    value: Math.abs(deltaValue),
                },
                comparison: {
                    prevDate,
                    prevValue,
                    nextDate: timePeriod,
                    nextValue,
                },
            };
            return [...result, item];
        }, []);
    }
    getTrafficListData(period) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.data[period]);
    }
}


/***/ }),

/***/ "./src/app/@core/mock/user-activity.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/@core/mock/user-activity.service.ts ***!
  \*****************************************************/
/*! exports provided: UserActivityService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserActivityService", function() { return UserActivityService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _periods_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./periods.service */ "./src/app/@core/mock/periods.service.ts");
/* harmony import */ var _data_user_activity__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../data/user-activity */ "./src/app/@core/data/user-activity.ts");



class UserActivityService extends _data_user_activity__WEBPACK_IMPORTED_MODULE_2__["UserActivityData"] {
    constructor(periods) {
        super();
        this.periods = periods;
        this.getRandom = (roundTo) => Math.round(Math.random() * roundTo);
        this.data = {};
        this.data = {
            week: this.getDataWeek(),
            month: this.getDataMonth(),
            year: this.getDataYear(),
        };
    }
    generateUserActivityRandomData(date) {
        return {
            date,
            pagesVisitCount: this.getRandom(1000),
            deltaUp: this.getRandom(1) % 2 === 0,
            newVisits: this.getRandom(100),
        };
    }
    getDataWeek() {
        return this.periods.getWeeks().map((week) => {
            return this.generateUserActivityRandomData(week);
        });
    }
    getDataMonth() {
        const currentDate = new Date();
        const days = currentDate.getDate();
        const month = this.periods.getMonths()[currentDate.getMonth()];
        return Array.from(Array(days)).map((_, index) => {
            const date = `${index + 1} ${month}`;
            return this.generateUserActivityRandomData(date);
        });
    }
    getDataYear() {
        return this.periods.getYears().map((year) => {
            return this.generateUserActivityRandomData(year);
        });
    }
    getUserActivityData(period) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.data[period]);
    }
}


/***/ }),

/***/ "./src/app/@core/mock/users.service.ts":
/*!*********************************************!*\
  !*** ./src/app/@core/mock/users.service.ts ***!
  \*********************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _data_users__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data/users */ "./src/app/@core/data/users.ts");


class UserService extends _data_users__WEBPACK_IMPORTED_MODULE_1__["UserData"] {
    constructor() {
        super(...arguments);
        this.time = new Date;
        this.users = {
            nick: { name: 'Nick Jones', picture: 'assets/images/nick.png' },
            eva: { name: 'Eva Moor', picture: 'assets/images/eva.png' },
            jack: { name: 'Jack Williams', picture: 'assets/images/jack.png' },
            lee: { name: 'Lee Wong', picture: 'assets/images/lee.png' },
            alan: { name: 'Alan Thompson', picture: 'assets/images/alan.png' },
            kate: { name: 'Kate Martinez', picture: 'assets/images/kate.png' },
        };
        this.types = {
            mobile: 'mobile',
            home: 'home',
            work: 'work',
        };
        this.contacts = [
            { user: this.users.nick, type: this.types.mobile },
            { user: this.users.eva, type: this.types.home },
            { user: this.users.jack, type: this.types.mobile },
            { user: this.users.lee, type: this.types.mobile },
            { user: this.users.alan, type: this.types.home },
            { user: this.users.kate, type: this.types.work },
        ];
        this.recentUsers = [
            { user: this.users.alan, type: this.types.home, time: this.time.setHours(21, 12) },
            { user: this.users.eva, type: this.types.home, time: this.time.setHours(17, 45) },
            { user: this.users.nick, type: this.types.mobile, time: this.time.setHours(5, 29) },
            { user: this.users.lee, type: this.types.mobile, time: this.time.setHours(11, 24) },
            { user: this.users.jack, type: this.types.mobile, time: this.time.setHours(10, 45) },
            { user: this.users.kate, type: this.types.work, time: this.time.setHours(9, 42) },
            { user: this.users.kate, type: this.types.work, time: this.time.setHours(9, 31) },
            { user: this.users.jack, type: this.types.mobile, time: this.time.setHours(8, 0) },
        ];
    }
    getUsers() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.users);
    }
    getContacts() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.contacts);
    }
    getRecentUsers() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.recentUsers);
    }
}


/***/ }),

/***/ "./src/app/@core/mock/visitors-analytics.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/@core/mock/visitors-analytics.service.ts ***!
  \**********************************************************/
/*! exports provided: VisitorsAnalyticsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitorsAnalyticsService", function() { return VisitorsAnalyticsService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _periods_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./periods.service */ "./src/app/@core/mock/periods.service.ts");
/* harmony import */ var _data_visitors_analytics__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../data/visitors-analytics */ "./src/app/@core/data/visitors-analytics.ts");



class VisitorsAnalyticsService extends _data_visitors_analytics__WEBPACK_IMPORTED_MODULE_2__["VisitorsAnalyticsData"] {
    constructor(periodService) {
        super();
        this.periodService = periodService;
        this.pieChartValue = 75;
        this.innerLinePoints = [
            94, 188, 225, 244, 253, 254, 249, 235, 208,
            173, 141, 118, 105, 97, 94, 96, 104, 121, 147,
            183, 224, 265, 302, 333, 358, 375, 388, 395,
            400, 400, 397, 390, 377, 360, 338, 310, 278,
            241, 204, 166, 130, 98, 71, 49, 32, 20, 13, 9,
        ];
        this.outerLinePoints = [
            85, 71, 59, 50, 45, 42, 41, 44, 58, 88,
            136, 199, 267, 326, 367, 391, 400, 397,
            376, 319, 200, 104, 60, 41, 36, 37, 44,
            55, 74, 100, 131, 159, 180, 193, 199, 200,
            195, 184, 164, 135, 103, 73, 50, 33, 22, 15, 11,
        ];
    }
    generateOutlineLineData() {
        const months = this.periodService.getMonths();
        const outerLinePointsLength = this.outerLinePoints.length;
        const monthsLength = months.length;
        return this.outerLinePoints.map((p, index) => {
            const monthIndex = Math.round(index / 4);
            const label = (index % Math.round(outerLinePointsLength / monthsLength) === 0)
                ? months[monthIndex]
                : '';
            return {
                label,
                value: p,
            };
        });
    }
    getInnerLineChartData() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.innerLinePoints);
    }
    getOutlineLineChartData() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.generateOutlineLineData());
    }
    getPieChartData() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(this.pieChartValue);
    }
}


/***/ }),

/***/ "./src/app/@core/module-import-guard.ts":
/*!**********************************************!*\
  !*** ./src/app/@core/module-import-guard.ts ***!
  \**********************************************/
/*! exports provided: throwIfAlreadyLoaded */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "throwIfAlreadyLoaded", function() { return throwIfAlreadyLoaded; });
function throwIfAlreadyLoaded(parentModule, moduleName) {
    if (parentModule) {
        throw new Error(`${moduleName} has already been loaded. Import Core modules in the AppModule only.`);
    }
}


/***/ }),

/***/ "./src/app/@core/utils/analytics.service.ts":
/*!**************************************************!*\
  !*** ./src/app/@core/utils/analytics.service.ts ***!
  \**************************************************/
/*! exports provided: AnalyticsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AnalyticsService", function() { return AnalyticsService; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");



class AnalyticsService {
    constructor(location, router) {
        this.location = location;
        this.router = router;
        this.enabled = false;
    }
    trackPageViews() {
        if (this.enabled) {
            this.router.events.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["filter"])((event) => event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_0__["NavigationEnd"]))
                .subscribe(() => {
                ga('send', { hitType: 'pageview', page: this.location.path() });
            });
        }
    }
    trackEvent(eventName) {
        if (this.enabled) {
            ga('send', 'event', eventName);
        }
    }
}


/***/ }),

/***/ "./src/app/@core/utils/index.ts":
/*!**************************************!*\
  !*** ./src/app/@core/utils/index.ts ***!
  \**************************************/
/*! exports provided: LayoutService, AnalyticsService, PlayerService, StateService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _layout_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./layout.service */ "./src/app/@core/utils/layout.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LayoutService", function() { return _layout_service__WEBPACK_IMPORTED_MODULE_0__["LayoutService"]; });

/* harmony import */ var _analytics_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./analytics.service */ "./src/app/@core/utils/analytics.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AnalyticsService", function() { return _analytics_service__WEBPACK_IMPORTED_MODULE_1__["AnalyticsService"]; });

/* harmony import */ var _player_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./player.service */ "./src/app/@core/utils/player.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PlayerService", function() { return _player_service__WEBPACK_IMPORTED_MODULE_2__["PlayerService"]; });

/* harmony import */ var _state_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./state.service */ "./src/app/@core/utils/state.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "StateService", function() { return _state_service__WEBPACK_IMPORTED_MODULE_3__["StateService"]; });








/***/ }),

/***/ "./src/app/@core/utils/layout.service.ts":
/*!***********************************************!*\
  !*** ./src/app/@core/utils/layout.service.ts ***!
  \***********************************************/
/*! exports provided: LayoutService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutService", function() { return LayoutService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");


class LayoutService {
    constructor() {
        this.layoutSize$ = new rxjs__WEBPACK_IMPORTED_MODULE_0__["Subject"]();
    }
    changeLayoutSize() {
        this.layoutSize$.next();
    }
    onChangeLayoutSize() {
        return this.layoutSize$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["share"])(), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["delay"])(1));
    }
}


/***/ }),

/***/ "./src/app/@core/utils/player.service.ts":
/*!***********************************************!*\
  !*** ./src/app/@core/utils/player.service.ts ***!
  \***********************************************/
/*! exports provided: Track, PlayerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Track", function() { return Track; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlayerService", function() { return PlayerService; });
class Track {
}
class PlayerService {
    constructor() {
        this.playlist = [
            {
                name: 'Don\'t Wanna Fight',
                artist: 'Alabama Shakes',
                url: 'https://p.scdn.co/mp3-preview/6156cdbca425a894972c02fca9d76c0b70e001af',
                cover: 'assets/images/cover1.jpg',
            },
            {
                name: 'Harder',
                artist: 'Daft Punk',
                url: 'https://p.scdn.co/mp3-preview/92a04c7c0e96bf93a1b1b1cae7dfff1921969a7b',
                cover: 'assets/images/cover2.jpg',
            },
            {
                name: 'Come Together',
                artist: 'Beatles',
                url: 'https://p.scdn.co/mp3-preview/83090a4db6899eaca689ae35f69126dbe65d94c9',
                cover: 'assets/images/cover3.jpg',
            },
        ];
    }
    random() {
        this.current = Math.floor(Math.random() * this.playlist.length);
        return this.playlist[this.current];
    }
    next() {
        return this.getNextTrack();
    }
    prev() {
        return this.getPrevTrack();
    }
    getNextTrack() {
        if (this.current === this.playlist.length - 1) {
            this.current = 0;
        }
        else {
            this.current++;
        }
        return this.playlist[this.current];
    }
    getPrevTrack() {
        if (this.current === 0) {
            this.current = this.playlist.length - 1;
        }
        else {
            this.current--;
        }
        return this.playlist[this.current];
    }
}


/***/ }),

/***/ "./src/app/@core/utils/state.service.ts":
/*!**********************************************!*\
  !*** ./src/app/@core/utils/state.service.ts ***!
  \**********************************************/
/*! exports provided: StateService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StateService", function() { return StateService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/fesm2015/index.js");




class StateService {
    constructor(directionService) {
        this.layouts = [
            {
                name: 'One Column',
                icon: 'nb-layout-default',
                id: 'one-column',
                selected: true,
            },
            {
                name: 'Two Column',
                icon: 'nb-layout-two-column',
                id: 'two-column',
            },
            {
                name: 'Center Column',
                icon: 'nb-layout-centre',
                id: 'center-column',
            },
        ];
        this.sidebars = [
            {
                name: 'Sidebar at layout start',
                icon: 'nb-layout-sidebar-left',
                id: 'start',
                selected: true,
            },
            {
                name: 'Sidebar at layout end',
                icon: 'nb-layout-sidebar-right',
                id: 'end',
            },
        ];
        this.layoutState$ = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"](this.layouts[0]);
        this.sidebarState$ = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"](this.sidebars[0]);
        this.alive = true;
        directionService.onDirectionChange()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["takeWhile"])(() => this.alive))
            .subscribe(direction => this.updateSidebarIcons(direction));
        this.updateSidebarIcons(directionService.getDirection());
    }
    ngOnDestroy() {
        this.alive = false;
    }
    updateSidebarIcons(direction) {
        const [startSidebar, endSidebar] = this.sidebars;
        const isLtr = direction === _nebular_theme__WEBPACK_IMPORTED_MODULE_3__["NbLayoutDirection"].LTR;
        const startIconClass = isLtr ? 'nb-layout-sidebar-left' : 'nb-layout-sidebar-right';
        const endIconClass = isLtr ? 'nb-layout-sidebar-right' : 'nb-layout-sidebar-left';
        startSidebar.icon = startIconClass;
        endSidebar.icon = endIconClass;
    }
    setLayoutState(state) {
        this.layoutState$.next(state);
    }
    getLayoutStates() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(this.layouts);
    }
    onLayoutState() {
        return this.layoutState$.asObservable();
    }
    setSidebarState(state) {
        this.sidebarState$.next(state);
    }
    getSidebarStates() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(this.sidebars);
    }
    onSidebarState() {
        return this.sidebarState$.asObservable();
    }
}


/***/ }),

/***/ "./src/app/@theme/components/footer/footer.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/@theme/components/footer/footer.component.ts ***!
  \**************************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
class FooterComponent {
}


/***/ }),

/***/ "./src/app/@theme/components/header/header.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/@theme/components/header/header.component.ts ***!
  \**************************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/fesm2015/index.js");
/* harmony import */ var _core_data_users__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../@core/data/users */ "./src/app/@core/data/users.ts");
/* harmony import */ var _core_utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../@core/utils */ "./src/app/@core/utils/index.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _service_message_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../service/message.service */ "./src/app/service/message.service.ts");
/* harmony import */ var _service_admin_service_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../service/admin-service.service */ "./src/app/service/admin-service.service.ts");
/* harmony import */ var _service_user_service_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../service/user-service.service */ "./src/app/service/user-service.service.ts");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var ng_push__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ng-push */ "./node_modules/ng-push/ng-push.umd.js");
/* harmony import */ var ng_push__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(ng_push__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _service_client_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../service/client.service */ "./src/app/service/client.service.ts");
/* harmony import */ var _service_notification_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../service/notification.service */ "./src/app/service/notification.service.ts");
/* harmony import */ var _service_command_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../../service/command.service */ "./src/app/service/command.service.ts");















class HeaderComponent {
    constructor(sidebarService, _auth, menuService, themeService, userService, service, clientService, layoutService, adminService, window, router, commandeService, notificationService, _pushNotifications, nbMenuService, breakpointService, messageService) {
        this.sidebarService = sidebarService;
        this._auth = _auth;
        this.menuService = menuService;
        this.themeService = themeService;
        this.userService = userService;
        this.service = service;
        this.clientService = clientService;
        this.layoutService = layoutService;
        this.adminService = adminService;
        this.window = window;
        this.router = router;
        this.commandeService = commandeService;
        this.notificationService = notificationService;
        this._pushNotifications = _pushNotifications;
        this.nbMenuService = nbMenuService;
        this.breakpointService = breakpointService;
        this.messageService = messageService;
        this.destroy$ = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
        this.userPictureOnly = false;
        this.picture = 'https://www.samaauto.sn/img/logos/admin.png';
        this.themes = [
            {
                value: 'default',
                name: 'Light',
            },
            {
                value: 'dark',
                name: 'Dark',
            },
            {
                value: 'cosmic',
                name: 'Cosmic',
            },
            {
                value: 'corporate',
                name: 'Corporate',
            },
        ];
        this.currentTheme = 'default';
        this.item = [{ title: 'Profile' }, { title: 'Log out' }];
        this.itemm = [];
        this.numberBag = "0";
        this.clientList = [];
        this.commandeList = [];
        this.userDetails = this.message = JSON.parse(localStorage.getItem("currentUser"));
        //----------------
    }
    ngOnInit() {
        this.clientService.getUserById(this.userDetails.id).subscribe((response) => {
            this.picture = response.photoUrl;
            console.log(this.picture);
            this.userDetails.username = response.username;
        });
        this.messageService.getProfile().subscribe(res => {
            if (!!res) {
                this.picture = res.user.photoUrl;
                this.userDetails.username = res.user.username;
                console.log(res.user.photoUrl);
            }
        });
        this.role = this.userDetails.roles[0];
        this.nbMenuService.onItemClick()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["filter"])(({ tag }) => tag === 'my-context-menu'), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(({ item: { title } }) => title))
            .subscribe(title => {
            switch (title) {
                case "Profile":
                    if (this.userDetails.roles[0] === "ROLE_GROSSISTE")
                        this.router.navigate(["/dashboard/compte"]);
                    else
                        this.router.navigate(["/dashboard-pharmacie/compte"]);
                    break;
                case "Log out":
                    this.logOut();
                    break;
            }
        });
        this.message = this.userDetails;
        this.currentTheme = this.themeService.currentTheme;
        this.userService.getUsers()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this.destroy$))
            .subscribe((users) => this.user = users.nick);
        const { xl } = this.breakpointService.getBreakpointsMap();
        this.themeService.onMediaQueryChange()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(([, currentBreakpoint]) => currentBreakpoint.width < xl), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this.destroy$))
            .subscribe((isLessThanXl) => this.userPictureOnly = isLessThanXl);
        this.themeService.onThemeChange()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(({ name }) => name), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this.destroy$))
            .subscribe(themeName => this.currentTheme = themeName);
        this.messageService.getMessage().subscribe((msg) => {
            this.message = msg.token;
            console.log(msg);
        });
    }
    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }
    changeTheme(themeName) {
        this.themeService.changeTheme(themeName);
    }
    toggleSidebar() {
        this.sidebarService.toggle(true, 'menu-sidebar');
        this.layoutService.changeLayoutSize();
        return false;
    }
    navigateHome() {
        this.menuService.navigateHome();
        return false;
    }
    logOut() {
        this._auth.logout();
        this.router.navigate(['/auth/login']);
    }
}


/***/ }),

/***/ "./src/app/@theme/components/index.ts":
/*!********************************************!*\
  !*** ./src/app/@theme/components/index.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent, FooterComponent, SearchInputComponent, TinyMCEComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./header/header.component */ "./src/app/@theme/components/header/header.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return _header_header_component__WEBPACK_IMPORTED_MODULE_0__["HeaderComponent"]; });

/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/@theme/components/footer/footer.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return _footer_footer_component__WEBPACK_IMPORTED_MODULE_1__["FooterComponent"]; });

/* harmony import */ var _search_input_search_input_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./search-input/search-input.component */ "./src/app/@theme/components/search-input/search-input.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SearchInputComponent", function() { return _search_input_search_input_component__WEBPACK_IMPORTED_MODULE_2__["SearchInputComponent"]; });

/* harmony import */ var _tiny_mce_tiny_mce_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tiny-mce/tiny-mce.component */ "./src/app/@theme/components/tiny-mce/tiny-mce.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TinyMCEComponent", function() { return _tiny_mce_tiny_mce_component__WEBPACK_IMPORTED_MODULE_3__["TinyMCEComponent"]; });







/***/ }),

/***/ "./src/app/@theme/components/search-input/search-input.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/@theme/components/search-input/search-input.component.ts ***!
  \**************************************************************************/
/*! exports provided: SearchInputComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchInputComponent", function() { return SearchInputComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");

class SearchInputComponent {
    constructor() {
        this.search = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.isInputShown = false;
    }
    showInput() {
        this.isInputShown = true;
        this.input.nativeElement.focus();
    }
    hideInput() {
        this.isInputShown = false;
    }
    onInput(val) {
        this.search.emit(val);
    }
}


/***/ }),

/***/ "./src/app/@theme/components/tiny-mce/tiny-mce.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/@theme/components/tiny-mce/tiny-mce.component.ts ***!
  \******************************************************************/
/*! exports provided: TinyMCEComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TinyMCEComponent", function() { return TinyMCEComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");

class TinyMCEComponent {
    constructor(host) {
        this.host = host;
        this.editorKeyup = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    ngAfterViewInit() {
        tinymce.init({
            target: this.host.nativeElement,
            plugins: ['link', 'paste', 'table'],
            skin_url: 'assets/skins/lightgray',
            setup: editor => {
                this.editor = editor;
                editor.on('keyup', () => {
                    this.editorKeyup.emit(editor.getContent());
                });
            },
            height: '320',
        });
    }
    ngOnDestroy() {
        tinymce.remove(this.editor);
    }
}


/***/ }),

/***/ "./src/app/@theme/layouts/index.ts":
/*!*****************************************!*\
  !*** ./src/app/@theme/layouts/index.ts ***!
  \*****************************************/
/*! exports provided: OneColumnLayoutComponent, TwoColumnsLayoutComponent, ThreeColumnsLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _one_column_one_column_layout__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./one-column/one-column.layout */ "./src/app/@theme/layouts/one-column/one-column.layout.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OneColumnLayoutComponent", function() { return _one_column_one_column_layout__WEBPACK_IMPORTED_MODULE_0__["OneColumnLayoutComponent"]; });

/* harmony import */ var _two_columns_two_columns_layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./two-columns/two-columns.layout */ "./src/app/@theme/layouts/two-columns/two-columns.layout.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TwoColumnsLayoutComponent", function() { return _two_columns_two_columns_layout__WEBPACK_IMPORTED_MODULE_1__["TwoColumnsLayoutComponent"]; });

/* harmony import */ var _three_columns_three_columns_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./three-columns/three-columns.layout */ "./src/app/@theme/layouts/three-columns/three-columns.layout.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ThreeColumnsLayoutComponent", function() { return _three_columns_three_columns_layout__WEBPACK_IMPORTED_MODULE_2__["ThreeColumnsLayoutComponent"]; });






/***/ }),

/***/ "./src/app/@theme/layouts/one-column/one-column.layout.ts":
/*!****************************************************************!*\
  !*** ./src/app/@theme/layouts/one-column/one-column.layout.ts ***!
  \****************************************************************/
/*! exports provided: OneColumnLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OneColumnLayoutComponent", function() { return OneColumnLayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/fesm2015/index.js");
/* harmony import */ var _services_window_mode_block_scroll_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/window-mode-block-scroll.service */ "./src/app/@theme/services/window-mode-block-scroll.service.ts");




class OneColumnLayoutComponent {
    constructor(platformId, windowModeBlockScrollService) {
        this.platformId = platformId;
        this.windowModeBlockScrollService = windowModeBlockScrollService;
    }
    ngAfterViewInit() {
        if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["isPlatformBrowser"])(this.platformId)) {
            this.windowModeBlockScrollService.register(this.layout);
        }
    }
}


/***/ }),

/***/ "./src/app/@theme/layouts/three-columns/three-columns.layout.ts":
/*!**********************************************************************!*\
  !*** ./src/app/@theme/layouts/three-columns/three-columns.layout.ts ***!
  \**********************************************************************/
/*! exports provided: ThreeColumnsLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ThreeColumnsLayoutComponent", function() { return ThreeColumnsLayoutComponent; });
class ThreeColumnsLayoutComponent {
}


/***/ }),

/***/ "./src/app/@theme/layouts/two-columns/two-columns.layout.ts":
/*!******************************************************************!*\
  !*** ./src/app/@theme/layouts/two-columns/two-columns.layout.ts ***!
  \******************************************************************/
/*! exports provided: TwoColumnsLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TwoColumnsLayoutComponent", function() { return TwoColumnsLayoutComponent; });
class TwoColumnsLayoutComponent {
}


/***/ }),

/***/ "./src/app/@theme/pipes/capitalize.pipe.ts":
/*!*************************************************!*\
  !*** ./src/app/@theme/pipes/capitalize.pipe.ts ***!
  \*************************************************/
/*! exports provided: CapitalizePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CapitalizePipe", function() { return CapitalizePipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");

class CapitalizePipe {
    transform(input) {
        return input && input.length
            ? (input.charAt(0).toUpperCase() + input.slice(1).toLowerCase())
            : input;
    }
}


/***/ }),

/***/ "./src/app/@theme/pipes/index.ts":
/*!***************************************!*\
  !*** ./src/app/@theme/pipes/index.ts ***!
  \***************************************/
/*! exports provided: CapitalizePipe, PluralPipe, RoundPipe, TimingPipe, NumberWithCommasPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _capitalize_pipe__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./capitalize.pipe */ "./src/app/@theme/pipes/capitalize.pipe.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "CapitalizePipe", function() { return _capitalize_pipe__WEBPACK_IMPORTED_MODULE_0__["CapitalizePipe"]; });

/* harmony import */ var _plural_pipe__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./plural.pipe */ "./src/app/@theme/pipes/plural.pipe.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PluralPipe", function() { return _plural_pipe__WEBPACK_IMPORTED_MODULE_1__["PluralPipe"]; });

/* harmony import */ var _round_pipe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./round.pipe */ "./src/app/@theme/pipes/round.pipe.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "RoundPipe", function() { return _round_pipe__WEBPACK_IMPORTED_MODULE_2__["RoundPipe"]; });

/* harmony import */ var _timing_pipe__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./timing.pipe */ "./src/app/@theme/pipes/timing.pipe.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TimingPipe", function() { return _timing_pipe__WEBPACK_IMPORTED_MODULE_3__["TimingPipe"]; });

/* harmony import */ var _number_with_commas_pipe__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./number-with-commas.pipe */ "./src/app/@theme/pipes/number-with-commas.pipe.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "NumberWithCommasPipe", function() { return _number_with_commas_pipe__WEBPACK_IMPORTED_MODULE_4__["NumberWithCommasPipe"]; });








/***/ }),

/***/ "./src/app/@theme/pipes/number-with-commas.pipe.ts":
/*!*********************************************************!*\
  !*** ./src/app/@theme/pipes/number-with-commas.pipe.ts ***!
  \*********************************************************/
/*! exports provided: NumberWithCommasPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NumberWithCommasPipe", function() { return NumberWithCommasPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");

class NumberWithCommasPipe {
    transform(input) {
        return new Intl.NumberFormat().format(input);
    }
}


/***/ }),

/***/ "./src/app/@theme/pipes/plural.pipe.ts":
/*!*********************************************!*\
  !*** ./src/app/@theme/pipes/plural.pipe.ts ***!
  \*********************************************/
/*! exports provided: PluralPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PluralPipe", function() { return PluralPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");

class PluralPipe {
    transform(input, label, pluralLabel = '') {
        input = input || 0;
        return input === 1
            ? `${input} ${label}`
            : pluralLabel
                ? `${input} ${pluralLabel}`
                : `${input} ${label}s`;
    }
}


/***/ }),

/***/ "./src/app/@theme/pipes/round.pipe.ts":
/*!********************************************!*\
  !*** ./src/app/@theme/pipes/round.pipe.ts ***!
  \********************************************/
/*! exports provided: RoundPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoundPipe", function() { return RoundPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");

class RoundPipe {
    transform(input) {
        return Math.round(input);
    }
}


/***/ }),

/***/ "./src/app/@theme/pipes/timing.pipe.ts":
/*!*********************************************!*\
  !*** ./src/app/@theme/pipes/timing.pipe.ts ***!
  \*********************************************/
/*! exports provided: TimingPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimingPipe", function() { return TimingPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");

class TimingPipe {
    transform(time) {
        if (time) {
            const minutes = Math.floor(time / 60);
            const seconds = Math.floor(time % 60);
            return `${this.initZero(minutes)}${minutes}:${this.initZero(seconds)}${seconds}`;
        }
        return '00:00';
    }
    initZero(time) {
        return time < 10 ? '0' : '';
    }
}


/***/ }),

/***/ "./src/app/@theme/services/window-mode-block-scroll.service.ts":
/*!*********************************************************************!*\
  !*** ./src/app/@theme/services/window-mode-block-scroll.service.ts ***!
  \*********************************************************************/
/*! exports provided: WindowModeBlockScrollService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WindowModeBlockScrollService", function() { return WindowModeBlockScrollService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/cdk/coercion */ "./node_modules/@angular/cdk/esm2015/coercion.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/fesm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");





class WindowModeBlockScrollService {
    constructor(scrollService, viewportRuler, layout, window) {
        this.scrollService = scrollService;
        this.viewportRuler = viewportRuler;
        this.layout = layout;
        this.window = window;
        this.destroy$ = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Subject"]();
        this.blockEnabled = false;
        this.unblock$ = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Subject"]();
    }
    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
        this.unblock$.complete();
    }
    register(layout) {
        this.container = layout.scrollableContainerRef.nativeElement;
        this.content = this.container.children[0];
        this.scrollService.onScrollableChange()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["filter"])(() => layout.withScrollValue), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])((scrollable) => !scrollable), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this.destroy$))
            .subscribe((shouldBlock) => {
            if (shouldBlock) {
                this.blockScroll();
            }
            else {
                this.unblockScroll();
            }
        });
    }
    blockScroll() {
        if (!this.canBeBlocked()) {
            return;
        }
        this.previousScrollPosition = this.viewportRuler.getViewportScrollPosition();
        this.backupStyles();
        this.container.style.overflowY = 'scroll';
        this.content.style.overflow = 'hidden';
        this.content.style.position = 'fixed';
        this.updateContentSizeAndPosition();
        Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["fromEvent"])(this.window, 'resize')
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["merge"])(this.destroy$, this.unblock$).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["take"])(1))))
            .subscribe(() => this.updateContentSizeAndPosition());
        this.blockEnabled = true;
    }
    unblockScroll() {
        if (this.blockEnabled) {
            this.restoreStyles();
            this.scrollService.scrollTo(this.previousScrollPosition.left, this.previousScrollPosition.top);
            this.unblock$.next();
            this.blockEnabled = false;
        }
    }
    canBeBlocked() {
        if (this.blockEnabled) {
            return false;
        }
        const { height: containerHeight } = this.viewportRuler.getViewportSize();
        return this.content.scrollHeight > containerHeight;
    }
    updateContentSizeAndPosition() {
        const { top, left } = this.container.getBoundingClientRect();
        this.content.style.left = Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_1__["coerceCssPixelValue"])(-this.previousScrollPosition.left + left);
        this.content.style.top = Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_1__["coerceCssPixelValue"])(-this.previousScrollPosition.top + top);
        this.layout.getDimensions()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(({ clientWidth }) => Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_1__["coerceCssPixelValue"])(clientWidth)), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["take"])(1))
            .subscribe((clientWidth) => this.content.style.width = clientWidth);
    }
    backupStyles() {
        this.previousContainerStyles = { overflowY: this.container.style.overflowY };
        this.previousContentStyles = {
            overflow: this.content.style.overflow,
            position: this.content.style.position,
            left: this.content.style.left,
            top: this.content.style.top,
            width: this.content.style.width,
        };
    }
    restoreStyles() {
        this.container.style.overflowY = this.previousContainerStyles.overflowY;
        this.content.style.overflow = this.previousContentStyles.overflow;
        this.content.style.position = this.previousContentStyles.position;
        this.content.style.left = this.previousContentStyles.left;
        this.content.style.top = this.previousContentStyles.top;
        this.content.style.width = this.previousContentStyles.width;
    }
}


/***/ }),

/***/ "./src/app/@theme/styles/theme.corporate.ts":
/*!**************************************************!*\
  !*** ./src/app/@theme/styles/theme.corporate.ts ***!
  \**************************************************/
/*! exports provided: CORPORATE_THEME */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CORPORATE_THEME", function() { return CORPORATE_THEME; });
const palette = {
    primary: '#73a1ff',
    success: '#5dcfe3',
    info: '#ba7fec',
    warning: '#ffa36b',
    danger: '#ff6b83',
};
const theme = {
    fontMain: 'Open Sans, sans-serif',
    fontSecondary: 'Raleway, sans-serif',
    bg: '#ffffff',
    bg2: '#f7f9fc',
    bg3: '#edf1f7',
    bg4: '#e4e9f2',
    border: '#ffffff',
    border2: '#f7f9fc',
    border3: '#edf1f7',
    border4: '#e4e9f2',
    border5: '#c5cee0',
    fg: '#8f9bb3',
    fgHeading: '#1a2138',
    fgText: '#1a2138',
    fgHighlight: palette.primary,
    layoutBg: '#f7f9fc',
    separator: '#edf1f7',
    primary: palette.primary,
    success: palette.success,
    info: palette.info,
    warning: palette.warning,
    danger: palette.danger,
    primaryLight: '#598bff',
    successLight: '#2ce69b',
    infoLight: '#42aaff',
    warningLight: '#ffc94d',
    dangerLight: '#ff708d',
};
const CORPORATE_THEME = {
    name: 'corporate',
    variables: Object.assign({}, theme, { temperature: {
            arcFill: ['#ffa36b', '#ffa36b', '#ff9e7a', '#ff9888', '#ff8ea0'],
            arcEmpty: theme.bg2,
            thumbBg: theme.bg2,
            thumbBorder: '#ffa36b',
        }, solar: {
            gradientLeft: theme.primary,
            gradientRight: theme.primary,
            shadowColor: 'rgba(0, 0, 0, 0)',
            secondSeriesFill: theme.bg2,
            radius: ['80%', '90%'],
        }, traffic: {
            tooltipBg: theme.bg,
            tooltipBorderColor: theme.border2,
            tooltipExtraCss: 'border-radius: 10px; padding: 4px 16px;',
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            yAxisSplitLine: 'rgba(0, 0, 0, 0)',
            lineBg: theme.primary,
            lineShadowBlur: '0',
            itemColor: theme.border4,
            itemBorderColor: theme.border4,
            itemEmphasisBorderColor: theme.primaryLight,
            shadowLineDarkBg: 'rgba(0, 0, 0, 0)',
            shadowLineShadow: 'rgba(0, 0, 0, 0)',
            gradFrom: theme.bg,
            gradTo: theme.bg,
        }, electricity: {
            tooltipBg: theme.bg,
            tooltipLineColor: theme.fgText,
            tooltipLineWidth: '0',
            tooltipBorderColor: theme.border2,
            tooltipExtraCss: 'border-radius: 10px; padding: 8px 24px;',
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            axisLineColor: theme.border3,
            xAxisTextColor: theme.fg,
            yAxisSplitLine: theme.separator,
            itemBorderColor: theme.primary,
            lineStyle: 'solid',
            lineWidth: '4',
            lineGradFrom: theme.primary,
            lineGradTo: theme.primary,
            lineShadow: 'rgba(0, 0, 0, 0)',
            areaGradFrom: 'rgba(0, 0, 0, 0)',
            areaGradTo: 'rgba(0, 0, 0, 0)',
            shadowLineDarkBg: 'rgba(0, 0, 0, 0)',
        }, bubbleMap: {
            titleColor: theme.fgText,
            areaColor: theme.bg4,
            areaHoverColor: theme.fgHighlight,
            areaBorderColor: theme.border5,
        }, profitBarAnimationEchart: {
            textColor: theme.fgText,
            firstAnimationBarColor: theme.primary,
            secondAnimationBarColor: theme.success,
            splitLineStyleOpacity: '1',
            splitLineStyleWidth: '1',
            splitLineStyleColor: theme.separator,
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            tooltipFontSize: '16',
            tooltipBg: theme.bg,
            tooltipBorderColor: theme.border2,
            tooltipBorderWidth: '1',
            tooltipExtraCss: 'border-radius: 10px; padding: 4px 16px;',
        }, trafficBarEchart: {
            gradientFrom: theme.warningLight,
            gradientTo: theme.warning,
            shadow: theme.warningLight,
            shadowBlur: '0',
            axisTextColor: theme.fgText,
            axisFontSize: '12',
            tooltipBg: theme.bg,
            tooltipBorderColor: theme.border2,
            tooltipExtraCss: 'border-radius: 10px; padding: 8px 24px;',
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
        }, countryOrders: {
            countryBorderColor: theme.border4,
            countryFillColor: theme.bg4,
            countryBorderWidth: '1',
            hoveredCountryBorderColor: theme.primary,
            hoveredCountryFillColor: theme.primaryLight,
            hoveredCountryBorderWidth: '1',
            chartAxisLineColor: theme.border4,
            chartAxisTextColor: theme.fg,
            chartAxisFontSize: '16',
            chartGradientTo: theme.primary,
            chartGradientFrom: theme.primaryLight,
            chartAxisSplitLine: theme.separator,
            chartShadowLineColor: theme.primaryLight,
            chartLineBottomShadowColor: theme.primary,
            chartInnerLineColor: theme.bg2,
        }, echarts: {
            bg: theme.bg,
            textColor: theme.fgText,
            axisLineColor: theme.fgText,
            splitLineColor: theme.separator,
            itemHoverShadowColor: 'rgba(0, 0, 0, 0.5)',
            tooltipBackgroundColor: theme.primary,
            areaOpacity: '0.7',
        }, chartjs: {
            axisLineColor: theme.separator,
            textColor: theme.fgText,
        }, orders: {
            tooltipBg: theme.bg,
            tooltipLineColor: 'rgba(0, 0, 0, 0)',
            tooltipLineWidth: '0',
            tooltipBorderColor: theme.border2,
            tooltipExtraCss: 'border-radius: 10px; padding: 8px 24px;',
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            tooltipFontSize: '20',
            axisLineColor: theme.border4,
            axisFontSize: '16',
            axisTextColor: theme.fg,
            yAxisSplitLine: theme.separator,
            itemBorderColor: theme.primary,
            lineStyle: 'solid',
            lineWidth: '4',
            // first line
            firstAreaGradFrom: theme.bg3,
            firstAreaGradTo: theme.bg3,
            firstShadowLineDarkBg: 'rgba(0, 0, 0, 0)',
            // second line
            secondLineGradFrom: theme.primary,
            secondLineGradTo: theme.primary,
            secondAreaGradFrom: 'rgba(0, 0, 0, 0)',
            secondAreaGradTo: 'rgba(0, 0, 0, 0)',
            secondShadowLineDarkBg: 'rgba(0, 0, 0, 0)',
            // third line
            thirdLineGradFrom: theme.success,
            thirdLineGradTo: theme.successLight,
            thirdAreaGradFrom: 'rgba(0, 0, 0, 0)',
            thirdAreaGradTo: 'rgba(0, 0, 0, 0)',
            thirdShadowLineDarkBg: 'rgba(0, 0, 0, 0)',
        }, profit: {
            bg: theme.bg,
            textColor: theme.fgText,
            axisLineColor: theme.border4,
            splitLineColor: theme.separator,
            areaOpacity: '1',
            axisFontSize: '16',
            axisTextColor: theme.fg,
            // first bar
            firstLineGradFrom: theme.bg3,
            firstLineGradTo: theme.bg3,
            firstLineShadow: 'rgba(0, 0, 0, 0)',
            // second bar
            secondLineGradFrom: theme.primary,
            secondLineGradTo: theme.primary,
            secondLineShadow: 'rgba(0, 0, 0, 0)',
            // third bar
            thirdLineGradFrom: theme.success,
            thirdLineGradTo: theme.success,
            thirdLineShadow: 'rgba(0, 0, 0, 0)',
        }, orderProfitLegend: {
            firstItem: theme.success,
            secondItem: theme.primary,
            thirdItem: theme.bg3,
        }, visitors: {
            tooltipBg: theme.bg,
            tooltipLineColor: 'rgba(0, 0, 0, 0)',
            tooltipLineWidth: '1',
            tooltipBorderColor: theme.border2,
            tooltipExtraCss: 'border-radius: 10px; padding: 8px 24px;',
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            tooltipFontSize: '20',
            axisLineColor: theme.border4,
            axisFontSize: '16',
            axisTextColor: theme.fg,
            yAxisSplitLine: theme.separator,
            itemBorderColor: theme.primary,
            lineStyle: 'dotted',
            lineWidth: '6',
            lineGradFrom: '#ffffff',
            lineGradTo: '#ffffff',
            lineShadow: 'rgba(0, 0, 0, 0)',
            areaGradFrom: theme.primary,
            areaGradTo: theme.primaryLight,
            innerLineStyle: 'solid',
            innerLineWidth: '1',
            innerAreaGradFrom: theme.success,
            innerAreaGradTo: theme.success,
        }, visitorsLegend: {
            firstIcon: theme.success,
            secondIcon: theme.primary,
        }, visitorsPie: {
            firstPieGradientLeft: theme.success,
            firstPieGradientRight: theme.success,
            firstPieShadowColor: 'rgba(0, 0, 0, 0)',
            firstPieRadius: ['65%', '90%'],
            secondPieGradientLeft: theme.warning,
            secondPieGradientRight: theme.warningLight,
            secondPieShadowColor: 'rgba(0, 0, 0, 0)',
            secondPieRadius: ['63%', '92%'],
            shadowOffsetX: '-4',
            shadowOffsetY: '-4',
        }, visitorsPieLegend: {
            firstSection: theme.warning,
            secondSection: theme.success,
        }, earningPie: {
            radius: ['65%', '100%'],
            center: ['50%', '50%'],
            fontSize: '22',
            firstPieGradientLeft: theme.success,
            firstPieGradientRight: theme.success,
            firstPieShadowColor: 'rgba(0, 0, 0, 0)',
            secondPieGradientLeft: theme.primary,
            secondPieGradientRight: theme.primary,
            secondPieShadowColor: 'rgba(0, 0, 0, 0)',
            thirdPieGradientLeft: theme.warning,
            thirdPieGradientRight: theme.warning,
            thirdPieShadowColor: 'rgba(0, 0, 0, 0)',
        }, earningLine: {
            gradFrom: theme.primary,
            gradTo: theme.primary,
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            tooltipFontSize: '16',
            tooltipBg: theme.bg,
            tooltipBorderColor: theme.border2,
            tooltipBorderWidth: '1',
            tooltipExtraCss: 'border-radius: 10px; padding: 4px 16px;',
        } }),
};


/***/ }),

/***/ "./src/app/@theme/styles/theme.cosmic.ts":
/*!***********************************************!*\
  !*** ./src/app/@theme/styles/theme.cosmic.ts ***!
  \***********************************************/
/*! exports provided: COSMIC_THEME */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "COSMIC_THEME", function() { return COSMIC_THEME; });
const palette = {
    primary: '#a16eff',
    success: '#00d68f',
    info: '#0095ff',
    warning: '#ffaa00',
    danger: '#ff3d71',
};
const theme = {
    fontMain: 'Open Sans, sans-serif',
    fontSecondary: 'Raleway, sans-serif',
    bg: '#323259',
    bg2: '#252547',
    bg3: '#1b1b38',
    bg4: '#13132b',
    border: '#323259',
    border2: '#252547',
    border3: '#1b1b38',
    border4: '#13132b',
    border5: '#13132b',
    fg: '#b4b4db',
    fgHeading: '#ffffff',
    fgText: '#ffffff',
    fgHighlight: palette.primary,
    layoutBg: '#151a30',
    separator: '#151a30',
    primary: palette.primary,
    success: palette.success,
    info: palette.info,
    warning: palette.warning,
    danger: palette.danger,
    primaryLight: '#b18aff',
    successLight: '#2ce69b',
    infoLight: '#42aaff',
    warningLight: '#ffc94d',
    dangerLight: '#ff708d',
};
const COSMIC_THEME = {
    name: 'cosmic',
    variables: Object.assign({}, theme, { temperature: {
            arcFill: ['#2ec7fe', '#31ffad', '#7bff24', '#fff024', '#f7bd59'],
            arcEmpty: theme.bg2,
            thumbBg: '#ffffff',
            thumbBorder: '#ffffff',
        }, solar: {
            gradientLeft: theme.primary,
            gradientRight: theme.primary,
            shadowColor: 'rgba(0, 0, 0, 0)',
            secondSeriesFill: theme.bg2,
            radius: ['70%', '90%'],
        }, traffic: {
            tooltipBg: theme.bg,
            tooltipBorderColor: theme.border2,
            tooltipExtraCss: 'box-shadow: 0px 2px 46px 0 rgba(50, 50, 89); border-radius: 10px; padding: 4px 16px;',
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            yAxisSplitLine: theme.separator,
            lineBg: theme.border2,
            lineShadowBlur: '14',
            itemColor: theme.border2,
            itemBorderColor: theme.border2,
            itemEmphasisBorderColor: theme.primary,
            shadowLineDarkBg: theme.border3,
            shadowLineShadow: theme.border3,
            gradFrom: theme.bg,
            gradTo: theme.bg2,
        }, electricity: {
            tooltipBg: theme.bg,
            tooltipLineColor: theme.fgText,
            tooltipLineWidth: '0',
            tooltipBorderColor: theme.border2,
            tooltipExtraCss: 'box-shadow: 0px 2px 46px 0 rgba(0, 255, 170, 0.35); border-radius: 10px; padding: 8px 24px;',
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            axisLineColor: theme.border3,
            xAxisTextColor: theme.fg,
            yAxisSplitLine: theme.separator,
            itemBorderColor: theme.border2,
            lineStyle: 'dotted',
            lineWidth: '6',
            lineGradFrom: theme.success,
            lineGradTo: theme.warning,
            lineShadow: theme.bg4,
            areaGradFrom: theme.bg2,
            areaGradTo: theme.bg3,
            shadowLineDarkBg: theme.bg3,
        }, bubbleMap: {
            titleColor: theme.fgText,
            areaColor: theme.bg4,
            areaHoverColor: theme.fgHighlight,
            areaBorderColor: theme.border5,
        }, profitBarAnimationEchart: {
            textColor: theme.fgText,
            firstAnimationBarColor: theme.primary,
            secondAnimationBarColor: theme.success,
            splitLineStyleOpacity: '1',
            splitLineStyleWidth: '1',
            splitLineStyleColor: theme.border2,
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            tooltipFontSize: '16',
            tooltipBg: theme.bg,
            tooltipBorderColor: theme.border2,
            tooltipBorderWidth: '1',
            tooltipExtraCss: 'border-radius: 10px; padding: 4px 16px;',
        }, trafficBarEchart: {
            gradientFrom: theme.warningLight,
            gradientTo: theme.warning,
            shadow: theme.warningLight,
            shadowBlur: '5',
            axisTextColor: theme.fgText,
            axisFontSize: '12',
            tooltipBg: theme.bg,
            tooltipBorderColor: theme.border2,
            tooltipExtraCss: 'border-radius: 10px; padding: 4px 16px;',
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
        }, countryOrders: {
            countryBorderColor: theme.border4,
            countryFillColor: theme.bg3,
            countryBorderWidth: '1',
            hoveredCountryBorderColor: theme.primary,
            hoveredCountryFillColor: theme.primaryLight,
            hoveredCountryBorderWidth: '1',
            chartAxisLineColor: theme.border4,
            chartAxisTextColor: theme.fg,
            chartAxisFontSize: '16',
            chartGradientTo: theme.primary,
            chartGradientFrom: theme.primaryLight,
            chartAxisSplitLine: theme.separator,
            chartShadowLineColor: theme.primaryLight,
            chartLineBottomShadowColor: theme.primary,
            chartInnerLineColor: theme.bg2,
        }, echarts: {
            bg: theme.bg,
            textColor: theme.fgText,
            axisLineColor: theme.fgText,
            splitLineColor: theme.separator,
            itemHoverShadowColor: 'rgba(0, 0, 0, 0.5)',
            tooltipBackgroundColor: theme.primary,
            areaOpacity: '1',
        }, chartjs: {
            axisLineColor: theme.separator,
            textColor: theme.fgText,
        }, orders: {
            tooltipBg: theme.bg,
            tooltipLineColor: 'rgba(0, 0, 0, 0)',
            tooltipLineWidth: '0',
            tooltipBorderColor: theme.border2,
            tooltipExtraCss: 'border-radius: 10px; padding: 8px 24px;',
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            tooltipFontSize: '20',
            axisLineColor: theme.border4,
            axisFontSize: '16',
            axisTextColor: theme.fg,
            yAxisSplitLine: theme.separator,
            itemBorderColor: theme.primary,
            lineStyle: 'solid',
            lineWidth: '4',
            // first line
            firstAreaGradFrom: theme.bg2,
            firstAreaGradTo: theme.bg2,
            firstShadowLineDarkBg: theme.bg2,
            // second line
            secondLineGradFrom: theme.primary,
            secondLineGradTo: theme.primary,
            secondAreaGradFrom: 'rgba(161, 110, 255, 0.8)',
            secondAreaGradTo: 'rgba(161, 110, 255, 0.5)',
            secondShadowLineDarkBg: theme.primary,
            // third line
            thirdLineGradFrom: theme.success,
            thirdLineGradTo: theme.successLight,
            thirdAreaGradFrom: 'rgba(0, 214, 143, 0.7)',
            thirdAreaGradTo: 'rgba(0, 214, 143, 0.4)',
            thirdShadowLineDarkBg: theme.success,
        }, profit: {
            bg: theme.bg,
            textColor: theme.fgText,
            axisLineColor: theme.border4,
            splitLineColor: theme.separator,
            areaOpacity: '1',
            axisFontSize: '16',
            axisTextColor: theme.fg,
            // first bar
            firstLineGradFrom: theme.bg2,
            firstLineGradTo: theme.bg2,
            firstLineShadow: 'rgba(0, 0, 0, 0)',
            // second bar
            secondLineGradFrom: theme.primary,
            secondLineGradTo: theme.primary,
            secondLineShadow: 'rgba(0, 0, 0, 0)',
            // third bar
            thirdLineGradFrom: theme.success,
            thirdLineGradTo: theme.successLight,
            thirdLineShadow: 'rgba(0, 0, 0, 0)',
        }, orderProfitLegend: {
            firstItem: theme.success,
            secondItem: theme.primary,
            thirdItem: theme.bg2,
        }, visitors: {
            tooltipBg: theme.bg,
            tooltipLineColor: 'rgba(0, 0, 0, 0)',
            tooltipLineWidth: '1',
            tooltipBorderColor: theme.border2,
            tooltipExtraCss: 'border-radius: 10px; padding: 8px 24px;',
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            tooltipFontSize: '20',
            axisLineColor: theme.border4,
            axisFontSize: '16',
            axisTextColor: theme.fg,
            yAxisSplitLine: theme.separator,
            itemBorderColor: theme.primary,
            lineStyle: 'dotted',
            lineWidth: '6',
            lineGradFrom: '#ffffff',
            lineGradTo: '#ffffff',
            lineShadow: 'rgba(0, 0, 0, 0)',
            areaGradFrom: theme.primary,
            areaGradTo: theme.primaryLight,
            innerLineStyle: 'solid',
            innerLineWidth: '1',
            innerAreaGradFrom: theme.success,
            innerAreaGradTo: theme.success,
        }, visitorsLegend: {
            firstIcon: theme.success,
            secondIcon: theme.primary,
        }, visitorsPie: {
            firstPieGradientLeft: theme.success,
            firstPieGradientRight: theme.successLight,
            firstPieShadowColor: 'rgba(0, 0, 0, 0)',
            firstPieRadius: ['70%', '90%'],
            secondPieGradientLeft: theme.warning,
            secondPieGradientRight: theme.warningLight,
            secondPieShadowColor: 'rgba(0, 0, 0, 0)',
            secondPieRadius: ['60%', '95%'],
            shadowOffsetX: '0',
            shadowOffsetY: '3',
        }, visitorsPieLegend: {
            firstSection: theme.warning,
            secondSection: theme.success,
        }, earningPie: {
            radius: ['65%', '100%'],
            center: ['50%', '50%'],
            fontSize: '22',
            firstPieGradientLeft: theme.success,
            firstPieGradientRight: theme.success,
            firstPieShadowColor: 'rgba(0, 0, 0, 0)',
            secondPieGradientLeft: theme.primary,
            secondPieGradientRight: theme.primary,
            secondPieShadowColor: 'rgba(0, 0, 0, 0)',
            thirdPieGradientLeft: theme.warning,
            thirdPieGradientRight: theme.warning,
            thirdPieShadowColor: 'rgba(0, 0, 0, 0)',
        }, earningLine: {
            gradFrom: theme.primary,
            gradTo: theme.primary,
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            tooltipFontSize: '16',
            tooltipBg: theme.bg,
            tooltipBorderColor: theme.border2,
            tooltipBorderWidth: '1',
            tooltipExtraCss: 'border-radius: 10px; padding: 4px 16px;',
        } }),
};


/***/ }),

/***/ "./src/app/@theme/styles/theme.dark.ts":
/*!*********************************************!*\
  !*** ./src/app/@theme/styles/theme.dark.ts ***!
  \*********************************************/
/*! exports provided: DARK_THEME */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DARK_THEME", function() { return DARK_THEME; });
const palette = {
    primary: '#3366ff',
    success: '#00d68f',
    info: '#0095ff',
    warning: '#ffaa00',
    danger: '#ff3d71',
};
const theme = {
    fontMain: 'Open Sans, sans-serif',
    fontSecondary: 'Raleway, sans-serif',
    bg: '#222b45',
    bg2: '#1a2138',
    bg3: '#151a30',
    bg4: '#101426',
    border: '#222b45',
    border2: '#1a2138',
    border3: '#151a30',
    border4: '#101426',
    border5: '#101426',
    fg: '#8f9bb3',
    fgHeading: '#ffffff',
    fgText: '#ffffff',
    fgHighlight: palette.primary,
    layoutBg: '#1b1b38',
    separator: '#1b1b38',
    primary: palette.primary,
    success: palette.success,
    info: palette.info,
    warning: palette.warning,
    danger: palette.danger,
    primaryLight: '#598bff',
    successLight: '#2ce69b',
    infoLight: '#42aaff',
    warningLight: '#ffc94d',
    dangerLight: '#ff708d',
};
const DARK_THEME = {
    name: 'dark',
    variables: Object.assign({}, theme, { temperature: {
            arcFill: [theme.primary, theme.primary, theme.primary, theme.primary, theme.primary],
            arcEmpty: theme.bg2,
            thumbBg: theme.bg2,
            thumbBorder: theme.primary,
        }, solar: {
            gradientLeft: theme.primary,
            gradientRight: theme.primary,
            shadowColor: 'rgba(0, 0, 0, 0)',
            secondSeriesFill: theme.bg2,
            radius: ['80%', '90%'],
        }, traffic: {
            tooltipBg: theme.bg,
            tooltipBorderColor: theme.border2,
            tooltipExtraCss: 'border-radius: 10px; padding: 4px 16px;',
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            yAxisSplitLine: theme.separator,
            lineBg: theme.border4,
            lineShadowBlur: '1',
            itemColor: theme.border4,
            itemBorderColor: theme.border4,
            itemEmphasisBorderColor: theme.primary,
            shadowLineDarkBg: 'rgba(0, 0, 0, 0)',
            shadowLineShadow: 'rgba(0, 0, 0, 0)',
            gradFrom: theme.bg2,
            gradTo: theme.bg2,
        }, electricity: {
            tooltipBg: theme.bg,
            tooltipLineColor: theme.fgText,
            tooltipLineWidth: '0',
            tooltipBorderColor: theme.border2,
            tooltipExtraCss: 'border-radius: 10px; padding: 8px 24px;',
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            axisLineColor: theme.border3,
            xAxisTextColor: theme.fg,
            yAxisSplitLine: theme.separator,
            itemBorderColor: theme.primary,
            lineStyle: 'solid',
            lineWidth: '4',
            lineGradFrom: theme.primary,
            lineGradTo: theme.primary,
            lineShadow: 'rgba(0, 0, 0, 0)',
            areaGradFrom: theme.bg2,
            areaGradTo: theme.bg2,
            shadowLineDarkBg: 'rgba(0, 0, 0, 0)',
        }, bubbleMap: {
            titleColor: theme.fgText,
            areaColor: theme.bg4,
            areaHoverColor: theme.fgHighlight,
            areaBorderColor: theme.border5,
        }, profitBarAnimationEchart: {
            textColor: theme.fgText,
            firstAnimationBarColor: theme.primary,
            secondAnimationBarColor: theme.success,
            splitLineStyleOpacity: '1',
            splitLineStyleWidth: '1',
            splitLineStyleColor: theme.separator,
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            tooltipFontSize: '16',
            tooltipBg: theme.bg,
            tooltipBorderColor: theme.border2,
            tooltipBorderWidth: '1',
            tooltipExtraCss: 'border-radius: 10px; padding: 4px 16px;',
        }, trafficBarEchart: {
            gradientFrom: theme.warningLight,
            gradientTo: theme.warning,
            shadow: theme.warningLight,
            shadowBlur: '0',
            axisTextColor: theme.fgText,
            axisFontSize: '12',
            tooltipBg: theme.bg,
            tooltipBorderColor: theme.border2,
            tooltipExtraCss: 'border-radius: 10px; padding: 4px 16px;',
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
        }, countryOrders: {
            countryBorderColor: theme.border4,
            countryFillColor: theme.bg3,
            countryBorderWidth: '1',
            hoveredCountryBorderColor: theme.primary,
            hoveredCountryFillColor: theme.primaryLight,
            hoveredCountryBorderWidth: '1',
            chartAxisLineColor: theme.border4,
            chartAxisTextColor: theme.fg,
            chartAxisFontSize: '16',
            chartGradientTo: theme.primary,
            chartGradientFrom: theme.primaryLight,
            chartAxisSplitLine: theme.separator,
            chartShadowLineColor: theme.primaryLight,
            chartLineBottomShadowColor: theme.primary,
            chartInnerLineColor: theme.bg2,
        }, echarts: {
            bg: theme.bg,
            textColor: theme.fgText,
            axisLineColor: theme.fgText,
            splitLineColor: theme.separator,
            itemHoverShadowColor: 'rgba(0, 0, 0, 0.5)',
            tooltipBackgroundColor: theme.primary,
            areaOpacity: '0.7',
        }, chartjs: {
            axisLineColor: theme.separator,
            textColor: theme.fgText,
        }, orders: {
            tooltipBg: theme.bg,
            tooltipLineColor: 'rgba(0, 0, 0, 0)',
            tooltipLineWidth: '0',
            tooltipBorderColor: theme.border2,
            tooltipExtraCss: 'border-radius: 10px; padding: 8px 24px;',
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            tooltipFontSize: '20',
            axisLineColor: theme.border4,
            axisFontSize: '16',
            axisTextColor: theme.fg,
            yAxisSplitLine: theme.separator,
            itemBorderColor: theme.primary,
            lineStyle: 'solid',
            lineWidth: '4',
            // first line
            firstAreaGradFrom: theme.bg3,
            firstAreaGradTo: theme.bg3,
            firstShadowLineDarkBg: 'rgba(0, 0, 0, 0)',
            // second line
            secondLineGradFrom: theme.primary,
            secondLineGradTo: theme.primary,
            secondAreaGradFrom: 'rgba(51, 102, 255, 0.2)',
            secondAreaGradTo: 'rgba(51, 102, 255, 0)',
            secondShadowLineDarkBg: 'rgba(0, 0, 0, 0)',
            // third line
            thirdLineGradFrom: theme.success,
            thirdLineGradTo: theme.successLight,
            thirdAreaGradFrom: 'rgba(0, 214, 143, 0.2)',
            thirdAreaGradTo: 'rgba(0, 214, 143, 0)',
            thirdShadowLineDarkBg: 'rgba(0, 0, 0, 0)',
        }, profit: {
            bg: theme.bg,
            textColor: theme.fgText,
            axisLineColor: theme.border4,
            splitLineColor: theme.separator,
            areaOpacity: '1',
            axisFontSize: '16',
            axisTextColor: theme.fg,
            // first bar
            firstLineGradFrom: theme.bg3,
            firstLineGradTo: theme.bg3,
            firstLineShadow: 'rgba(0, 0, 0, 0)',
            // second bar
            secondLineGradFrom: theme.primary,
            secondLineGradTo: theme.primary,
            secondLineShadow: 'rgba(0, 0, 0, 0)',
            // third bar
            thirdLineGradFrom: theme.success,
            thirdLineGradTo: theme.successLight,
            thirdLineShadow: 'rgba(0, 0, 0, 0)',
        }, orderProfitLegend: {
            firstItem: theme.success,
            secondItem: theme.primary,
            thirdItem: theme.bg3,
        }, visitors: {
            tooltipBg: theme.bg,
            tooltipLineColor: 'rgba(0, 0, 0, 0)',
            tooltipLineWidth: '0',
            tooltipBorderColor: theme.border2,
            tooltipExtraCss: 'border-radius: 10px; padding: 8px 24px;',
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            tooltipFontSize: '20',
            axisLineColor: theme.border4,
            axisFontSize: '16',
            axisTextColor: theme.fg,
            yAxisSplitLine: theme.separator,
            itemBorderColor: theme.primary,
            lineStyle: 'dotted',
            lineWidth: '6',
            lineGradFrom: '#ffffff',
            lineGradTo: '#ffffff',
            lineShadow: 'rgba(0, 0, 0, 0)',
            areaGradFrom: theme.primary,
            areaGradTo: theme.primaryLight,
            innerLineStyle: 'solid',
            innerLineWidth: '1',
            innerAreaGradFrom: theme.success,
            innerAreaGradTo: theme.success,
        }, visitorsLegend: {
            firstIcon: theme.success,
            secondIcon: theme.primary,
        }, visitorsPie: {
            firstPieGradientLeft: theme.success,
            firstPieGradientRight: theme.success,
            firstPieShadowColor: 'rgba(0, 0, 0, 0)',
            firstPieRadius: ['70%', '90%'],
            secondPieGradientLeft: theme.warning,
            secondPieGradientRight: theme.warningLight,
            secondPieShadowColor: 'rgba(0, 0, 0, 0)',
            secondPieRadius: ['60%', '97%'],
            shadowOffsetX: '0',
            shadowOffsetY: '0',
        }, visitorsPieLegend: {
            firstSection: theme.warning,
            secondSection: theme.success,
        }, earningPie: {
            radius: ['65%', '100%'],
            center: ['50%', '50%'],
            fontSize: '22',
            firstPieGradientLeft: theme.success,
            firstPieGradientRight: theme.success,
            firstPieShadowColor: 'rgba(0, 0, 0, 0)',
            secondPieGradientLeft: theme.primary,
            secondPieGradientRight: theme.primary,
            secondPieShadowColor: 'rgba(0, 0, 0, 0)',
            thirdPieGradientLeft: theme.warning,
            thirdPieGradientRight: theme.warning,
            thirdPieShadowColor: 'rgba(0, 0, 0, 0)',
        }, earningLine: {
            gradFrom: theme.primary,
            gradTo: theme.primary,
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            tooltipFontSize: '16',
            tooltipBg: theme.bg,
            tooltipBorderColor: theme.border2,
            tooltipBorderWidth: '1',
            tooltipExtraCss: 'border-radius: 10px; padding: 4px 16px;',
        } }),
};


/***/ }),

/***/ "./src/app/@theme/styles/theme.default.ts":
/*!************************************************!*\
  !*** ./src/app/@theme/styles/theme.default.ts ***!
  \************************************************/
/*! exports provided: DEFAULT_THEME */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DEFAULT_THEME", function() { return DEFAULT_THEME; });
const palette = {
    primary: '#3366ff',
    success: '#00d68f',
    info: '#0095ff',
    warning: '#ffaa00',
    danger: '#ff3d71',
};
const theme = {
    fontMain: 'Open Sans, sans-serif',
    fontSecondary: 'Raleway, sans-serif',
    bg: '#ffffff',
    bg2: '#f7f9fc',
    bg3: '#edf1f7',
    bg4: '#e4e9f2',
    border: '#ffffff',
    border2: '#f7f9fc',
    border3: '#edf1f7',
    border4: '#e4e9f2',
    border5: '#c5cee0',
    fg: '#8f9bb3',
    fgHeading: '#1a2138',
    fgText: '#1a2138',
    fgHighlight: palette.primary,
    layoutBg: '#f7f9fc',
    separator: '#edf1f7',
    primary: palette.primary,
    success: palette.success,
    info: palette.info,
    warning: palette.warning,
    danger: palette.danger,
    primaryLight: '#598bff',
    successLight: '#2ce69b',
    infoLight: '#42aaff',
    warningLight: '#ffc94d',
    dangerLight: '#ff708d',
};
const DEFAULT_THEME = {
    name: 'default',
    variables: Object.assign({}, theme, { temperature: {
            arcFill: [theme.primary, theme.primary, theme.primary, theme.primary, theme.primary],
            arcEmpty: theme.bg2,
            thumbBg: theme.bg2,
            thumbBorder: theme.primary,
        }, solar: {
            gradientLeft: theme.primary,
            gradientRight: theme.primary,
            shadowColor: 'rgba(0, 0, 0, 0)',
            secondSeriesFill: theme.bg2,
            radius: ['80%', '90%'],
        }, traffic: {
            tooltipBg: theme.bg,
            tooltipBorderColor: theme.border2,
            tooltipExtraCss: 'border-radius: 10px; padding: 4px 16px;',
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            yAxisSplitLine: theme.separator,
            lineBg: theme.border4,
            lineShadowBlur: '1',
            itemColor: theme.border4,
            itemBorderColor: theme.border4,
            itemEmphasisBorderColor: theme.primary,
            shadowLineDarkBg: 'rgba(0, 0, 0, 0)',
            shadowLineShadow: 'rgba(0, 0, 0, 0)',
            gradFrom: theme.bg2,
            gradTo: theme.bg2,
        }, electricity: {
            tooltipBg: theme.bg,
            tooltipLineColor: theme.fgText,
            tooltipLineWidth: '0',
            tooltipBorderColor: theme.border2,
            tooltipExtraCss: 'border-radius: 10px; padding: 8px 24px;',
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            axisLineColor: theme.border3,
            xAxisTextColor: theme.fg,
            yAxisSplitLine: theme.separator,
            itemBorderColor: theme.primary,
            lineStyle: 'solid',
            lineWidth: '4',
            lineGradFrom: theme.primary,
            lineGradTo: theme.primary,
            lineShadow: 'rgba(0, 0, 0, 0)',
            areaGradFrom: theme.bg2,
            areaGradTo: theme.bg2,
            shadowLineDarkBg: 'rgba(0, 0, 0, 0)',
        }, bubbleMap: {
            titleColor: theme.fgText,
            areaColor: theme.bg4,
            areaHoverColor: theme.fgHighlight,
            areaBorderColor: theme.border5,
        }, profitBarAnimationEchart: {
            textColor: theme.fgText,
            firstAnimationBarColor: theme.primary,
            secondAnimationBarColor: theme.success,
            splitLineStyleOpacity: '1',
            splitLineStyleWidth: '1',
            splitLineStyleColor: theme.separator,
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            tooltipFontSize: '16',
            tooltipBg: theme.bg,
            tooltipBorderColor: theme.border2,
            tooltipBorderWidth: '1',
            tooltipExtraCss: 'border-radius: 10px; padding: 4px 16px;',
        }, trafficBarEchart: {
            gradientFrom: theme.warningLight,
            gradientTo: theme.warning,
            shadow: theme.warningLight,
            shadowBlur: '0',
            axisTextColor: theme.fgText,
            axisFontSize: '12',
            tooltipBg: theme.bg,
            tooltipBorderColor: theme.border2,
            tooltipExtraCss: 'border-radius: 10px; padding: 4px 16px;',
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
        }, countryOrders: {
            countryBorderColor: theme.border4,
            countryFillColor: theme.bg3,
            countryBorderWidth: '1',
            hoveredCountryBorderColor: theme.primary,
            hoveredCountryFillColor: theme.primaryLight,
            hoveredCountryBorderWidth: '1',
            chartAxisLineColor: theme.border4,
            chartAxisTextColor: theme.fg,
            chartAxisFontSize: '16',
            chartGradientTo: theme.primary,
            chartGradientFrom: theme.primaryLight,
            chartAxisSplitLine: theme.separator,
            chartShadowLineColor: theme.primaryLight,
            chartLineBottomShadowColor: theme.primary,
            chartInnerLineColor: theme.bg2,
        }, echarts: {
            bg: theme.bg,
            textColor: theme.fgText,
            axisLineColor: theme.fgText,
            splitLineColor: theme.separator,
            itemHoverShadowColor: 'rgba(0, 0, 0, 0.5)',
            tooltipBackgroundColor: theme.primary,
            areaOpacity: '0.7',
        }, chartjs: {
            axisLineColor: theme.separator,
            textColor: theme.fgText,
        }, orders: {
            tooltipBg: theme.bg,
            tooltipLineColor: 'rgba(0, 0, 0, 0)',
            tooltipLineWidth: '0',
            tooltipBorderColor: theme.border2,
            tooltipExtraCss: 'border-radius: 10px; padding: 8px 24px;',
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            tooltipFontSize: '20',
            axisLineColor: theme.border4,
            axisFontSize: '16',
            axisTextColor: theme.fg,
            yAxisSplitLine: theme.separator,
            itemBorderColor: theme.primary,
            lineStyle: 'solid',
            lineWidth: '4',
            // first line
            firstAreaGradFrom: theme.bg3,
            firstAreaGradTo: theme.bg3,
            firstShadowLineDarkBg: 'rgba(0, 0, 0, 0)',
            // second line
            secondLineGradFrom: theme.primary,
            secondLineGradTo: theme.primary,
            secondAreaGradFrom: 'rgba(51, 102, 255, 0.2)',
            secondAreaGradTo: 'rgba(51, 102, 255, 0)',
            secondShadowLineDarkBg: 'rgba(0, 0, 0, 0)',
            // third line
            thirdLineGradFrom: theme.success,
            thirdLineGradTo: theme.successLight,
            thirdAreaGradFrom: 'rgba(0, 214, 143, 0.2)',
            thirdAreaGradTo: 'rgba(0, 214, 143, 0)',
            thirdShadowLineDarkBg: 'rgba(0, 0, 0, 0)',
        }, profit: {
            bg: theme.bg,
            textColor: theme.fgText,
            axisLineColor: theme.border4,
            splitLineColor: theme.separator,
            areaOpacity: '1',
            axisFontSize: '16',
            axisTextColor: theme.fg,
            // first bar
            firstLineGradFrom: theme.bg3,
            firstLineGradTo: theme.bg3,
            firstLineShadow: 'rgba(0, 0, 0, 0)',
            // second bar
            secondLineGradFrom: theme.primary,
            secondLineGradTo: theme.primary,
            secondLineShadow: 'rgba(0, 0, 0, 0)',
            // third bar
            thirdLineGradFrom: theme.success,
            thirdLineGradTo: theme.successLight,
            thirdLineShadow: 'rgba(0, 0, 0, 0)',
        }, orderProfitLegend: {
            firstItem: theme.success,
            secondItem: theme.primary,
            thirdItem: theme.bg3,
        }, visitors: {
            tooltipBg: theme.bg,
            tooltipLineColor: 'rgba(0, 0, 0, 0)',
            tooltipLineWidth: '1',
            tooltipBorderColor: theme.border2,
            tooltipExtraCss: 'border-radius: 10px; padding: 8px 24px;',
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            tooltipFontSize: '20',
            axisLineColor: theme.border4,
            axisFontSize: '16',
            axisTextColor: theme.fg,
            yAxisSplitLine: theme.separator,
            itemBorderColor: theme.primary,
            lineStyle: 'dotted',
            lineWidth: '6',
            lineGradFrom: '#ffffff',
            lineGradTo: '#ffffff',
            lineShadow: 'rgba(0, 0, 0, 0)',
            areaGradFrom: theme.primary,
            areaGradTo: theme.primaryLight,
            innerLineStyle: 'solid',
            innerLineWidth: '1',
            innerAreaGradFrom: theme.success,
            innerAreaGradTo: theme.success,
        }, visitorsLegend: {
            firstIcon: theme.success,
            secondIcon: theme.primary,
        }, visitorsPie: {
            firstPieGradientLeft: theme.success,
            firstPieGradientRight: theme.success,
            firstPieShadowColor: 'rgba(0, 0, 0, 0)',
            firstPieRadius: ['70%', '90%'],
            secondPieGradientLeft: theme.warning,
            secondPieGradientRight: theme.warningLight,
            secondPieShadowColor: 'rgba(0, 0, 0, 0)',
            secondPieRadius: ['60%', '97%'],
            shadowOffsetX: '0',
            shadowOffsetY: '0',
        }, visitorsPieLegend: {
            firstSection: theme.warning,
            secondSection: theme.success,
        }, earningPie: {
            radius: ['65%', '100%'],
            center: ['50%', '50%'],
            fontSize: '22',
            firstPieGradientLeft: theme.success,
            firstPieGradientRight: theme.success,
            firstPieShadowColor: 'rgba(0, 0, 0, 0)',
            secondPieGradientLeft: theme.primary,
            secondPieGradientRight: theme.primary,
            secondPieShadowColor: 'rgba(0, 0, 0, 0)',
            thirdPieGradientLeft: theme.warning,
            thirdPieGradientRight: theme.warning,
            thirdPieShadowColor: 'rgba(0, 0, 0, 0)',
        }, earningLine: {
            gradFrom: theme.primary,
            gradTo: theme.primary,
            tooltipTextColor: theme.fgText,
            tooltipFontWeight: 'normal',
            tooltipFontSize: '16',
            tooltipBg: theme.bg,
            tooltipBorderColor: theme.border2,
            tooltipBorderWidth: '1',
            tooltipExtraCss: 'border-radius: 10px; padding: 4px 16px;',
        } }),
};


/***/ }),

/***/ "./src/app/@theme/theme.module.ts":
/*!****************************************!*\
  !*** ./src/app/@theme/theme.module.ts ***!
  \****************************************/
/*! exports provided: ThemeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ThemeModule", function() { return ThemeModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/fesm2015/index.js");
/* harmony import */ var _nebular_eva_icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @nebular/eva-icons */ "./node_modules/@nebular/eva-icons/fesm2015/index.js");
/* harmony import */ var _nebular_security__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @nebular/security */ "./node_modules/@nebular/security/fesm2015/index.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components */ "./src/app/@theme/components/index.ts");
/* harmony import */ var _pipes__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pipes */ "./src/app/@theme/pipes/index.ts");
/* harmony import */ var _layouts__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./layouts */ "./src/app/@theme/layouts/index.ts");
/* harmony import */ var _services_window_mode_block_scroll_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./services/window-mode-block-scroll.service */ "./src/app/@theme/services/window-mode-block-scroll.service.ts");
/* harmony import */ var _styles_theme_default__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./styles/theme.default */ "./src/app/@theme/styles/theme.default.ts");
/* harmony import */ var _styles_theme_cosmic__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./styles/theme.cosmic */ "./src/app/@theme/styles/theme.cosmic.ts");
/* harmony import */ var _styles_theme_corporate__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./styles/theme.corporate */ "./src/app/@theme/styles/theme.corporate.ts");
/* harmony import */ var _styles_theme_dark__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./styles/theme.dark */ "./src/app/@theme/styles/theme.dark.ts");












const NB_MODULES = [
    _nebular_theme__WEBPACK_IMPORTED_MODULE_1__["NbLayoutModule"],
    _nebular_theme__WEBPACK_IMPORTED_MODULE_1__["NbMenuModule"],
    _nebular_theme__WEBPACK_IMPORTED_MODULE_1__["NbUserModule"],
    _nebular_theme__WEBPACK_IMPORTED_MODULE_1__["NbActionsModule"],
    _nebular_theme__WEBPACK_IMPORTED_MODULE_1__["NbSearchModule"],
    _nebular_theme__WEBPACK_IMPORTED_MODULE_1__["NbSidebarModule"],
    _nebular_theme__WEBPACK_IMPORTED_MODULE_1__["NbContextMenuModule"],
    _nebular_security__WEBPACK_IMPORTED_MODULE_3__["NbSecurityModule"],
    _nebular_theme__WEBPACK_IMPORTED_MODULE_1__["NbButtonModule"],
    _nebular_theme__WEBPACK_IMPORTED_MODULE_1__["NbSelectModule"],
    _nebular_theme__WEBPACK_IMPORTED_MODULE_1__["NbIconModule"],
    _nebular_eva_icons__WEBPACK_IMPORTED_MODULE_2__["NbEvaIconsModule"],
];
const COMPONENTS = [
    _components__WEBPACK_IMPORTED_MODULE_4__["HeaderComponent"],
    _components__WEBPACK_IMPORTED_MODULE_4__["FooterComponent"],
    _components__WEBPACK_IMPORTED_MODULE_4__["SearchInputComponent"],
    _components__WEBPACK_IMPORTED_MODULE_4__["TinyMCEComponent"],
    _layouts__WEBPACK_IMPORTED_MODULE_6__["OneColumnLayoutComponent"],
    _layouts__WEBPACK_IMPORTED_MODULE_6__["ThreeColumnsLayoutComponent"],
    _layouts__WEBPACK_IMPORTED_MODULE_6__["TwoColumnsLayoutComponent"],
];
const PIPES = [
    _pipes__WEBPACK_IMPORTED_MODULE_5__["CapitalizePipe"],
    _pipes__WEBPACK_IMPORTED_MODULE_5__["PluralPipe"],
    _pipes__WEBPACK_IMPORTED_MODULE_5__["RoundPipe"],
    _pipes__WEBPACK_IMPORTED_MODULE_5__["TimingPipe"],
    _pipes__WEBPACK_IMPORTED_MODULE_5__["NumberWithCommasPipe"],
];
class ThemeModule {
    static forRoot() {
        return {
            ngModule: ThemeModule,
            providers: [
                ..._nebular_theme__WEBPACK_IMPORTED_MODULE_1__["NbThemeModule"].forRoot({
                    name: 'cosmic',
                }, [_styles_theme_default__WEBPACK_IMPORTED_MODULE_8__["DEFAULT_THEME"], _styles_theme_cosmic__WEBPACK_IMPORTED_MODULE_9__["COSMIC_THEME"], _styles_theme_corporate__WEBPACK_IMPORTED_MODULE_10__["CORPORATE_THEME"], _styles_theme_dark__WEBPACK_IMPORTED_MODULE_11__["DARK_THEME"]]).providers,
                _services_window_mode_block_scroll_service__WEBPACK_IMPORTED_MODULE_7__["WindowModeBlockScrollService"],
            ],
        };
    }
}


/***/ }),

/***/ "./src/app/_helpers/error.interceptor.ts":
/*!***********************************************!*\
  !*** ./src/app/_helpers/error.interceptor.ts ***!
  \***********************************************/
/*! exports provided: ErrorInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorInterceptor", function() { return ErrorInterceptor; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




class ErrorInterceptor {
    constructor(authenticationService, router) {
        this.authenticationService = authenticationService;
        this.router = router;
    }
    intercept(request, next) {
        return next.handle(request).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])(err => {
            if (err.status === 401) {
                // auto logout if 401 response returned from api
                this.authenticationService.logout();
                if (err.status === 401) {
                    this.router.navigate(["/auth/login"]);
                }
                location.reload(true);
            }
            const error = err.error.message || err.statusText;
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["throwError"])(error);
        }));
    }
}


/***/ }),

/***/ "./src/app/_helpers/jwt.interceptor.ts":
/*!*********************************************!*\
  !*** ./src/app/_helpers/jwt.interceptor.ts ***!
  \*********************************************/
/*! exports provided: JwtInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JwtInterceptor", function() { return JwtInterceptor; });
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../config */ "./src/app/config.ts");


class JwtInterceptor {
    constructor(authenticationService) {
        this.authenticationService = authenticationService;
    }
    intercept(request, next) {
        // add auth header with jwt if user is logged in and request is to api url
        const currentUser = this.authenticationService.currentUserValue;
        const isLoggedIn = currentUser && currentUser.accessToken;
        const isApiUrl = (request.url.startsWith(_config__WEBPACK_IMPORTED_MODULE_1__["config"].apiUrl1));
        if (isLoggedIn && isApiUrl) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${currentUser.accessToken}`
                }
            });
        }
        return next.handle(request);
    }
}


/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule, ɵ0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵ0", function() { return ɵ0; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _nebular_auth__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @nebular/auth */ "./node_modules/@nebular/auth/fesm2015/index.js");
/* harmony import */ var _auth_login_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./auth/login/login.component */ "./src/app/auth/login/login.component.ts");
/* harmony import */ var _auth_register_ngx_register_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./auth/register/ngx-register.component */ "./src/app/auth/register/ngx-register.component.ts");
/* harmony import */ var _gurads_auth_gurad__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./gurads/auth.gurad */ "./src/app/gurads/auth.gurad.ts");





const ɵ0 = () => __webpack_require__.e(/*! import() | dashboard-dashboard-module-ngfactory */ "dashboard-dashboard-module-ngfactory").then(__webpack_require__.bind(null, /*! ./dashboard/dashboard.module.ngfactory */ "./src/app/dashboard/dashboard.module.ngfactory.js")).then(m => m.DashboardModuleNgFactory);
const routes = [
    {
        path: 'dashboard',
        canActivate: [_gurads_auth_gurad__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]],
        loadChildren: ɵ0,
    },
    {
        path: 'auth',
        component: _nebular_auth__WEBPACK_IMPORTED_MODULE_1__["NbAuthComponent"],
        children: [
            {
                path: '',
                component: _auth_login_login_component__WEBPACK_IMPORTED_MODULE_2__["NgxLoginComponent"],
            },
            {
                path: 'login',
                component: _auth_login_login_component__WEBPACK_IMPORTED_MODULE_2__["NgxLoginComponent"],
            },
            {
                path: 'register',
                component: _auth_register_ngx_register_component__WEBPACK_IMPORTED_MODULE_3__["NgxRegisterComponent"],
            },
            {
                path: 'logout',
                component: _auth_login_login_component__WEBPACK_IMPORTED_MODULE_2__["NgxLoginComponent"],
            },
            {
                path: 'request-password',
                component: _auth_login_login_component__WEBPACK_IMPORTED_MODULE_2__["NgxLoginComponent"],
            },
            {
                path: 'reset-password',
                component: _nebular_auth__WEBPACK_IMPORTED_MODULE_1__["NbResetPasswordComponent"],
            },
        ],
    },
    { path: '', redirectTo: 'dashboard', pathMatch: 'full', canActivate: [_gurads_auth_gurad__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]], },
    { path: '**', redirectTo: 'dashboard', canActivate: [_gurads_auth_gurad__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]] },
];
const config = {
    useHash: false,
};
class AppRoutingModule {
}



/***/ }),

/***/ "./src/app/app.component.ngfactory.js":
/*!********************************************!*\
  !*** ./src/app/app.component.ngfactory.js ***!
  \********************************************/
/*! exports provided: RenderType_AppComponent, View_AppComponent_0, View_AppComponent_Host_0, AppComponentNgFactory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenderType_AppComponent", function() { return RenderType_AppComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_AppComponent_0", function() { return View_AppComponent_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_AppComponent_Host_0", function() { return View_AppComponent_Host_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponentNgFactory", function() { return AppComponentNgFactory; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _core_utils_analytics_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./@core/utils/analytics.service */ "./src/app/@core/utils/analytics.service.ts");
/* harmony import */ var _service_secure_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./service/secure-storage.service */ "./src/app/service/secure-storage.service.ts");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/fesm2015/index.js");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 






var styles_AppComponent = [];
var RenderType_AppComponent = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵcrt"]({ encapsulation: 2, styles: styles_AppComponent, data: {} });

function View_AppComponent_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 16777216, null, null, 1, "router-outlet", [], null, null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 212992, null, 0, _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"], [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ChildrenOutletContexts"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"], [8, null], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
function View_AppComponent_Host_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "ngx-app", [], null, null, null, View_AppComponent_0, RenderType_AppComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 114688, null, 0, _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"], [_core_utils_analytics_service__WEBPACK_IMPORTED_MODULE_3__["AnalyticsService"], _service_secure_storage_service__WEBPACK_IMPORTED_MODULE_4__["StorageService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_5__["NbThemeService"]], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
var AppComponentNgFactory = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵccf"]("ngx-app", _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"], View_AppComponent_Host_0, {}, {}, []);



/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _core_utils_analytics_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./@core/utils/analytics.service */ "./src/app/@core/utils/analytics.service.ts");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/fesm2015/index.js");
/* harmony import */ var _service_secure_storage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./service/secure-storage.service */ "./src/app/service/secure-storage.service.ts");
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../environments/environment.prod */ "./src/environments/environment.prod.ts");
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */





const { generateEncryptionKey, decrypt } = __webpack_require__(/*! symmetric-encrypt */ "../../../../../node_modules/symmetric-encrypt/build/index.js");
var aes256 = __webpack_require__(/*! aes256 */ "../../../../../node_modules/aes256/index.js");
const Cryptr = __webpack_require__(/*! cryptr */ "../../../../../node_modules/cryptr/index.js");
const cryptr = new Cryptr(_environments_environment_prod__WEBPACK_IMPORTED_MODULE_4__["environment"].SECRET_KEY);
var RSA = __webpack_require__(/*! node-crypto-js */ "../../../../../node_modules/node-crypto-js/lib/index.js").RSA;
var Crypt = __webpack_require__(/*! node-crypto-js */ "../../../../../node_modules/node-crypto-js/lib/index.js").Crypt;
class AppComponent {
    constructor(analytics, secureStorageService, theme) {
        this.analytics = analytics;
        this.secureStorageService = secureStorageService;
        this.theme = theme;
    }
    ngOnInit() {
        const userId = 'user001';
        this.theme.changeTheme("cosmic");
        this.analytics.trackPageViews();
    }
}


/***/ }),

/***/ "./src/app/app.module.ngfactory.js":
/*!*****************************************!*\
  !*** ./src/app/app.module.ngfactory.js ***!
  \*****************************************/
/*! exports provided: AppModuleNgFactory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModuleNgFactory", function() { return AppModuleNgFactory; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app.module */ "./src/app/app.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _node_modules_angular_material_tooltip_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../node_modules/@angular/material/tooltip/typings/index.ngfactory */ "./node_modules/@angular/material/tooltip/typings/index.ngfactory.js");
/* harmony import */ var _node_modules_angular_router_router_ngfactory__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../node_modules/@angular/router/router.ngfactory */ "./node_modules/@angular/router/router.ngfactory.js");
/* harmony import */ var _node_modules_nebular_auth_index_ngfactory__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../node_modules/@nebular/auth/index.ngfactory */ "./node_modules/@nebular/auth/index.ngfactory.js");
/* harmony import */ var _auth_login_login_component_ngfactory__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./auth/login/login.component.ngfactory */ "./src/app/auth/login/login.component.ngfactory.js");
/* harmony import */ var _auth_register_ngx_register_component_ngfactory__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./auth/register/ngx-register.component.ngfactory */ "./src/app/auth/register/ngx-register.component.ngfactory.js");
/* harmony import */ var _node_modules_nebular_theme_index_ngfactory__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../node_modules/@nebular/theme/index.ngfactory */ "./node_modules/@nebular/theme/index.ngfactory.js");
/* harmony import */ var _node_modules_swimlane_ngx_charts_release_common_tooltip_tooltip_component_ngfactory__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../node_modules/@swimlane/ngx-charts/release/common/tooltip/tooltip.component.ngfactory */ "./node_modules/@swimlane/ngx-charts/release/common/tooltip/tooltip.component.ngfactory.js");
/* harmony import */ var _app_component_ngfactory__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./app.component.ngfactory */ "./src/app/app.component.ngfactory.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/esm2015/core.js");
/* harmony import */ var _angular_animations_browser__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/animations/browser */ "./node_modules/@angular/animations/fesm2015/browser.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm2015/animations.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_cdk_observers__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/cdk/observers */ "./node_modules/@angular/cdk/esm2015/observers.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _helpers_jwt_interceptor__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./_helpers/jwt.interceptor */ "./src/app/_helpers/jwt.interceptor.ts");
/* harmony import */ var _helpers_error_interceptor__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./_helpers/error.interceptor */ "./src/app/_helpers/error.interceptor.ts");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/fesm2015/index.js");
/* harmony import */ var _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/cdk/scrolling */ "./node_modules/@angular/cdk/esm2015/scrolling.js");
/* harmony import */ var _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/cdk/overlay */ "./node_modules/@angular/cdk/esm2015/overlay.js");
/* harmony import */ var _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @angular/cdk/bidi */ "./node_modules/@angular/cdk/esm2015/bidi.js");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/esm2015/tooltip.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm2015/menu.js");
/* harmony import */ var ngx_avatar__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ngx-avatar */ "./node_modules/ngx-avatar/fesm2015/ngx-avatar.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm2015/select.js");
/* harmony import */ var ng_push__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ng-push */ "./node_modules/ng-push/ng-push.umd.js");
/* harmony import */ var ng_push__WEBPACK_IMPORTED_MODULE_32___default = /*#__PURE__*/__webpack_require__.n(ng_push__WEBPACK_IMPORTED_MODULE_32__);
/* harmony import */ var _swimlane_ngx_charts_release_common_tooltip_injection_service__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! @swimlane/ngx-charts/release/common/tooltip/injection.service */ "./node_modules/@swimlane/ngx-charts/release/common/tooltip/injection.service.js");
/* harmony import */ var _swimlane_ngx_charts_release_common_tooltip_tooltip_service__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! @swimlane/ngx-charts/release/common/tooltip/tooltip.service */ "./node_modules/@swimlane/ngx-charts/release/common/tooltip/tooltip.service.js");
/* harmony import */ var _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! @angular/cdk/platform */ "./node_modules/@angular/cdk/esm2015/platform.js");
/* harmony import */ var _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! @angular/cdk/a11y */ "./node_modules/@angular/cdk/esm2015/a11y.js");
/* harmony import */ var _theme_services_window_mode_block_scroll_service__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./@theme/services/window-mode-block-scroll.service */ "./src/app/@theme/services/window-mode-block-scroll.service.ts");
/* harmony import */ var _core_mock_users_service__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./@core/mock/users.service */ "./src/app/@core/mock/users.service.ts");
/* harmony import */ var _core_mock_electricity_service__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./@core/mock/electricity.service */ "./src/app/@core/mock/electricity.service.ts");
/* harmony import */ var _core_mock_smart_table_service__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./@core/mock/smart-table.service */ "./src/app/@core/mock/smart-table.service.ts");
/* harmony import */ var _core_mock_periods_service__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./@core/mock/periods.service */ "./src/app/@core/mock/periods.service.ts");
/* harmony import */ var _core_mock_user_activity_service__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./@core/mock/user-activity.service */ "./src/app/@core/mock/user-activity.service.ts");
/* harmony import */ var _core_mock_orders_chart_service__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./@core/mock/orders-chart.service */ "./src/app/@core/mock/orders-chart.service.ts");
/* harmony import */ var _core_mock_profit_chart_service__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./@core/mock/profit-chart.service */ "./src/app/@core/mock/profit-chart.service.ts");
/* harmony import */ var _core_mock_traffic_list_service__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./@core/mock/traffic-list.service */ "./src/app/@core/mock/traffic-list.service.ts");
/* harmony import */ var _core_mock_earning_service__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ./@core/mock/earning.service */ "./src/app/@core/mock/earning.service.ts");
/* harmony import */ var _core_data_orders_chart__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ./@core/data/orders-chart */ "./src/app/@core/data/orders-chart.ts");
/* harmony import */ var _core_data_profit_chart__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ./@core/data/profit-chart */ "./src/app/@core/data/profit-chart.ts");
/* harmony import */ var _core_mock_orders_profit_chart_service__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ./@core/mock/orders-profit-chart.service */ "./src/app/@core/mock/orders-profit-chart.service.ts");
/* harmony import */ var _core_mock_traffic_bar_service__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ./@core/mock/traffic-bar.service */ "./src/app/@core/mock/traffic-bar.service.ts");
/* harmony import */ var _core_mock_profit_bar_animation_chart_service__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! ./@core/mock/profit-bar-animation-chart.service */ "./src/app/@core/mock/profit-bar-animation-chart.service.ts");
/* harmony import */ var _core_mock_temperature_humidity_service__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! ./@core/mock/temperature-humidity.service */ "./src/app/@core/mock/temperature-humidity.service.ts");
/* harmony import */ var _core_mock_solar_service__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! ./@core/mock/solar.service */ "./src/app/@core/mock/solar.service.ts");
/* harmony import */ var _core_mock_traffic_chart_service__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(/*! ./@core/mock/traffic-chart.service */ "./src/app/@core/mock/traffic-chart.service.ts");
/* harmony import */ var _core_mock_stats_bar_service__WEBPACK_IMPORTED_MODULE_55__ = __webpack_require__(/*! ./@core/mock/stats-bar.service */ "./src/app/@core/mock/stats-bar.service.ts");
/* harmony import */ var _core_mock_country_order_service__WEBPACK_IMPORTED_MODULE_56__ = __webpack_require__(/*! ./@core/mock/country-order.service */ "./src/app/@core/mock/country-order.service.ts");
/* harmony import */ var _core_mock_stats_progress_bar_service__WEBPACK_IMPORTED_MODULE_57__ = __webpack_require__(/*! ./@core/mock/stats-progress-bar.service */ "./src/app/@core/mock/stats-progress-bar.service.ts");
/* harmony import */ var _core_mock_visitors_analytics_service__WEBPACK_IMPORTED_MODULE_58__ = __webpack_require__(/*! ./@core/mock/visitors-analytics.service */ "./src/app/@core/mock/visitors-analytics.service.ts");
/* harmony import */ var _core_mock_security_cameras_service__WEBPACK_IMPORTED_MODULE_59__ = __webpack_require__(/*! ./@core/mock/security-cameras.service */ "./src/app/@core/mock/security-cameras.service.ts");
/* harmony import */ var _core_data_users__WEBPACK_IMPORTED_MODULE_60__ = __webpack_require__(/*! ./@core/data/users */ "./src/app/@core/data/users.ts");
/* harmony import */ var _core_data_electricity__WEBPACK_IMPORTED_MODULE_61__ = __webpack_require__(/*! ./@core/data/electricity */ "./src/app/@core/data/electricity.ts");
/* harmony import */ var _core_data_smart_table__WEBPACK_IMPORTED_MODULE_62__ = __webpack_require__(/*! ./@core/data/smart-table */ "./src/app/@core/data/smart-table.ts");
/* harmony import */ var _core_data_user_activity__WEBPACK_IMPORTED_MODULE_63__ = __webpack_require__(/*! ./@core/data/user-activity */ "./src/app/@core/data/user-activity.ts");
/* harmony import */ var _core_data_traffic_list__WEBPACK_IMPORTED_MODULE_64__ = __webpack_require__(/*! ./@core/data/traffic-list */ "./src/app/@core/data/traffic-list.ts");
/* harmony import */ var _core_data_earning__WEBPACK_IMPORTED_MODULE_65__ = __webpack_require__(/*! ./@core/data/earning */ "./src/app/@core/data/earning.ts");
/* harmony import */ var _core_data_orders_profit_chart__WEBPACK_IMPORTED_MODULE_66__ = __webpack_require__(/*! ./@core/data/orders-profit-chart */ "./src/app/@core/data/orders-profit-chart.ts");
/* harmony import */ var _core_data_traffic_bar__WEBPACK_IMPORTED_MODULE_67__ = __webpack_require__(/*! ./@core/data/traffic-bar */ "./src/app/@core/data/traffic-bar.ts");
/* harmony import */ var _core_data_profit_bar_animation_chart__WEBPACK_IMPORTED_MODULE_68__ = __webpack_require__(/*! ./@core/data/profit-bar-animation-chart */ "./src/app/@core/data/profit-bar-animation-chart.ts");
/* harmony import */ var _core_data_temperature_humidity__WEBPACK_IMPORTED_MODULE_69__ = __webpack_require__(/*! ./@core/data/temperature-humidity */ "./src/app/@core/data/temperature-humidity.ts");
/* harmony import */ var _core_data_solar__WEBPACK_IMPORTED_MODULE_70__ = __webpack_require__(/*! ./@core/data/solar */ "./src/app/@core/data/solar.ts");
/* harmony import */ var _core_data_traffic_chart__WEBPACK_IMPORTED_MODULE_71__ = __webpack_require__(/*! ./@core/data/traffic-chart */ "./src/app/@core/data/traffic-chart.ts");
/* harmony import */ var _core_data_stats_bar__WEBPACK_IMPORTED_MODULE_72__ = __webpack_require__(/*! ./@core/data/stats-bar */ "./src/app/@core/data/stats-bar.ts");
/* harmony import */ var _core_data_country_order__WEBPACK_IMPORTED_MODULE_73__ = __webpack_require__(/*! ./@core/data/country-order */ "./src/app/@core/data/country-order.ts");
/* harmony import */ var _core_data_stats_progress_bar__WEBPACK_IMPORTED_MODULE_74__ = __webpack_require__(/*! ./@core/data/stats-progress-bar */ "./src/app/@core/data/stats-progress-bar.ts");
/* harmony import */ var _core_data_visitors_analytics__WEBPACK_IMPORTED_MODULE_75__ = __webpack_require__(/*! ./@core/data/visitors-analytics */ "./src/app/@core/data/visitors-analytics.ts");
/* harmony import */ var _core_data_security_cameras__WEBPACK_IMPORTED_MODULE_76__ = __webpack_require__(/*! ./@core/data/security-cameras */ "./src/app/@core/data/security-cameras.ts");
/* harmony import */ var _nebular_auth__WEBPACK_IMPORTED_MODULE_77__ = __webpack_require__(/*! @nebular/auth */ "./node_modules/@nebular/auth/fesm2015/index.js");
/* harmony import */ var _nebular_security__WEBPACK_IMPORTED_MODULE_78__ = __webpack_require__(/*! @nebular/security */ "./node_modules/@nebular/security/fesm2015/index.js");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_79__ = __webpack_require__(/*! ./@core/core.module */ "./src/app/@core/core.module.ts");
/* harmony import */ var _core_utils_analytics_service__WEBPACK_IMPORTED_MODULE_80__ = __webpack_require__(/*! ./@core/utils/analytics.service */ "./src/app/@core/utils/analytics.service.ts");
/* harmony import */ var _core_utils_layout_service__WEBPACK_IMPORTED_MODULE_81__ = __webpack_require__(/*! ./@core/utils/layout.service */ "./src/app/@core/utils/layout.service.ts");
/* harmony import */ var _core_utils_player_service__WEBPACK_IMPORTED_MODULE_82__ = __webpack_require__(/*! ./@core/utils/player.service */ "./src/app/@core/utils/player.service.ts");
/* harmony import */ var _core_utils_state_service__WEBPACK_IMPORTED_MODULE_83__ = __webpack_require__(/*! ./@core/utils/state.service */ "./src/app/@core/utils/state.service.ts");
/* harmony import */ var _auth_auth_service_service__WEBPACK_IMPORTED_MODULE_84__ = __webpack_require__(/*! ./auth/auth-service.service */ "./src/app/auth/auth-service.service.ts");
/* harmony import */ var _service_globals_service__WEBPACK_IMPORTED_MODULE_85__ = __webpack_require__(/*! ./service/globals.service */ "./src/app/service/globals.service.ts");
/* harmony import */ var _auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_86__ = __webpack_require__(/*! ./auth/auth-guard.service */ "./src/app/auth/auth-guard.service.ts");
/* harmony import */ var _service_localstorage_service__WEBPACK_IMPORTED_MODULE_87__ = __webpack_require__(/*! ./service/localstorage.service */ "./src/app/service/localstorage.service.ts");
/* harmony import */ var _gurads_client_auth_gurad__WEBPACK_IMPORTED_MODULE_88__ = __webpack_require__(/*! ./gurads-client/auth.gurad */ "./src/app/gurads-client/auth.gurad.ts");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_89__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm2015/ngx-spinner.js");
/* harmony import */ var _service_secure_storage_service__WEBPACK_IMPORTED_MODULE_90__ = __webpack_require__(/*! ./service/secure-storage.service */ "./src/app/service/secure-storage.service.ts");
/* harmony import */ var _ethereumProvider_ethereum__WEBPACK_IMPORTED_MODULE_91__ = __webpack_require__(/*! ./ethereumProvider/ethereum */ "./src/app/ethereumProvider/ethereum.ts");
/* harmony import */ var _auth_LoginActivate__WEBPACK_IMPORTED_MODULE_92__ = __webpack_require__(/*! ./auth/LoginActivate */ "./src/app/auth/LoginActivate.ts");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_93__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm2015/button.js");
/* harmony import */ var _angular_cdk_text_field__WEBPACK_IMPORTED_MODULE_94__ = __webpack_require__(/*! @angular/cdk/text-field */ "./node_modules/@angular/cdk/esm2015/text-field.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_95__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm2015/form-field.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_96__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm2015/input.js");
/* harmony import */ var ngx_copy_to_clipboard__WEBPACK_IMPORTED_MODULE_97__ = __webpack_require__(/*! ngx-copy-to-clipboard */ "./node_modules/ngx-copy-to-clipboard/fesm2015/ngx-copy-to-clipboard.js");
/* harmony import */ var _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_98__ = __webpack_require__(/*! @angular/cdk/portal */ "./node_modules/@angular/cdk/esm2015/portal.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_99__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/esm2015/card.js");
/* harmony import */ var _gurads_auth_gurad__WEBPACK_IMPORTED_MODULE_100__ = __webpack_require__(/*! ./gurads/auth.gurad */ "./src/app/gurads/auth.gurad.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_101__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _auth_login_login_component__WEBPACK_IMPORTED_MODULE_102__ = __webpack_require__(/*! ./auth/login/login.component */ "./src/app/auth/login/login.component.ts");
/* harmony import */ var _auth_register_ngx_register_component__WEBPACK_IMPORTED_MODULE_103__ = __webpack_require__(/*! ./auth/register/ngx-register.component */ "./src/app/auth/register/ngx-register.component.ts");
/* harmony import */ var _nebular_eva_icons__WEBPACK_IMPORTED_MODULE_104__ = __webpack_require__(/*! @nebular/eva-icons */ "./node_modules/@nebular/eva-icons/fesm2015/index.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_105__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm2015/icon.js");
/* harmony import */ var _angular_material_badge__WEBPACK_IMPORTED_MODULE_106__ = __webpack_require__(/*! @angular/material/badge */ "./node_modules/@angular/material/esm2015/badge.js");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_107__ = __webpack_require__(/*! ./@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var ngx_uploader__WEBPACK_IMPORTED_MODULE_108__ = __webpack_require__(/*! ngx-uploader */ "./node_modules/ngx-uploader/fesm2015/ngx-uploader.js");
/* harmony import */ var _swimlane_ngx_charts_release_common_axes_axes_module__WEBPACK_IMPORTED_MODULE_109__ = __webpack_require__(/*! @swimlane/ngx-charts/release/common/axes/axes.module */ "./node_modules/@swimlane/ngx-charts/release/common/axes/axes.module.js");
/* harmony import */ var _swimlane_ngx_charts_release_common_tooltip_tooltip_module__WEBPACK_IMPORTED_MODULE_110__ = __webpack_require__(/*! @swimlane/ngx-charts/release/common/tooltip/tooltip.module */ "./node_modules/@swimlane/ngx-charts/release/common/tooltip/tooltip.module.js");
/* harmony import */ var _swimlane_ngx_charts_release_common_chart_common_module__WEBPACK_IMPORTED_MODULE_111__ = __webpack_require__(/*! @swimlane/ngx-charts/release/common/chart-common.module */ "./node_modules/@swimlane/ngx-charts/release/common/chart-common.module.js");
/* harmony import */ var _swimlane_ngx_charts_release_area_chart_area_chart_module__WEBPACK_IMPORTED_MODULE_112__ = __webpack_require__(/*! @swimlane/ngx-charts/release/area-chart/area-chart.module */ "./node_modules/@swimlane/ngx-charts/release/area-chart/area-chart.module.js");
/* harmony import */ var _swimlane_ngx_charts_release_bar_chart_bar_chart_module__WEBPACK_IMPORTED_MODULE_113__ = __webpack_require__(/*! @swimlane/ngx-charts/release/bar-chart/bar-chart.module */ "./node_modules/@swimlane/ngx-charts/release/bar-chart/bar-chart.module.js");
/* harmony import */ var _swimlane_ngx_charts_release_bubble_chart_bubble_chart_module__WEBPACK_IMPORTED_MODULE_114__ = __webpack_require__(/*! @swimlane/ngx-charts/release/bubble-chart/bubble-chart.module */ "./node_modules/@swimlane/ngx-charts/release/bubble-chart/bubble-chart.module.js");
/* harmony import */ var _swimlane_ngx_charts_release_force_directed_graph_force_directed_graph_module__WEBPACK_IMPORTED_MODULE_115__ = __webpack_require__(/*! @swimlane/ngx-charts/release/force-directed-graph/force-directed-graph.module */ "./node_modules/@swimlane/ngx-charts/release/force-directed-graph/force-directed-graph.module.js");
/* harmony import */ var _swimlane_ngx_charts_release_heat_map_heat_map_module__WEBPACK_IMPORTED_MODULE_116__ = __webpack_require__(/*! @swimlane/ngx-charts/release/heat-map/heat-map.module */ "./node_modules/@swimlane/ngx-charts/release/heat-map/heat-map.module.js");
/* harmony import */ var _swimlane_ngx_charts_release_line_chart_line_chart_module__WEBPACK_IMPORTED_MODULE_117__ = __webpack_require__(/*! @swimlane/ngx-charts/release/line-chart/line-chart.module */ "./node_modules/@swimlane/ngx-charts/release/line-chart/line-chart.module.js");
/* harmony import */ var _swimlane_ngx_charts_release_pie_chart_pie_chart_module__WEBPACK_IMPORTED_MODULE_118__ = __webpack_require__(/*! @swimlane/ngx-charts/release/pie-chart/pie-chart.module */ "./node_modules/@swimlane/ngx-charts/release/pie-chart/pie-chart.module.js");
/* harmony import */ var _swimlane_ngx_charts_release_polar_chart_polar_chart_module__WEBPACK_IMPORTED_MODULE_119__ = __webpack_require__(/*! @swimlane/ngx-charts/release/polar-chart/polar-chart.module */ "./node_modules/@swimlane/ngx-charts/release/polar-chart/polar-chart.module.js");
/* harmony import */ var _swimlane_ngx_charts_release_number_card_number_card_module__WEBPACK_IMPORTED_MODULE_120__ = __webpack_require__(/*! @swimlane/ngx-charts/release/number-card/number-card.module */ "./node_modules/@swimlane/ngx-charts/release/number-card/number-card.module.js");
/* harmony import */ var _swimlane_ngx_charts_release_tree_map_tree_map_module__WEBPACK_IMPORTED_MODULE_121__ = __webpack_require__(/*! @swimlane/ngx-charts/release/tree-map/tree-map.module */ "./node_modules/@swimlane/ngx-charts/release/tree-map/tree-map.module.js");
/* harmony import */ var _swimlane_ngx_charts_release_gauge_gauge_module__WEBPACK_IMPORTED_MODULE_122__ = __webpack_require__(/*! @swimlane/ngx-charts/release/gauge/gauge.module */ "./node_modules/@swimlane/ngx-charts/release/gauge/gauge.module.js");
/* harmony import */ var _swimlane_ngx_charts_release_ngx_charts_module__WEBPACK_IMPORTED_MODULE_123__ = __webpack_require__(/*! @swimlane/ngx-charts/release/ngx-charts.module */ "./node_modules/@swimlane/ngx-charts/release/ngx-charts.module.js");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 




























































































































var AppModuleNgFactory = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵcmf"](_app_module__WEBPACK_IMPORTED_MODULE_1__["AppModule"], [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]], function (_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmod"]([_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](512, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵCodegenComponentFactoryResolver"], [[8, [_node_modules_angular_material_tooltip_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_3__["TooltipComponentNgFactory"], _node_modules_angular_router_router_ngfactory__WEBPACK_IMPORTED_MODULE_4__["ɵangular_packages_router_router_lNgFactory"], _node_modules_nebular_auth_index_ngfactory__WEBPACK_IMPORTED_MODULE_5__["NbAuthComponentNgFactory"], _auth_login_login_component_ngfactory__WEBPACK_IMPORTED_MODULE_6__["NgxLoginComponentNgFactory"], _auth_register_ngx_register_component_ngfactory__WEBPACK_IMPORTED_MODULE_7__["NgxRegisterComponentNgFactory"], _node_modules_nebular_auth_index_ngfactory__WEBPACK_IMPORTED_MODULE_5__["NbResetPasswordComponentNgFactory"], _node_modules_nebular_theme_index_ngfactory__WEBPACK_IMPORTED_MODULE_8__["NbSearchFieldComponentNgFactory"], _node_modules_nebular_theme_index_ngfactory__WEBPACK_IMPORTED_MODULE_8__["NbContextMenuComponentNgFactory"], _node_modules_nebular_theme_index_ngfactory__WEBPACK_IMPORTED_MODULE_8__["NbCalendarDayCellComponentNgFactory"], _node_modules_nebular_theme_index_ngfactory__WEBPACK_IMPORTED_MODULE_8__["NbCalendarMonthCellComponentNgFactory"], _node_modules_nebular_theme_index_ngfactory__WEBPACK_IMPORTED_MODULE_8__["NbCalendarYearCellComponentNgFactory"], _node_modules_nebular_theme_index_ngfactory__WEBPACK_IMPORTED_MODULE_8__["NbCalendarRangeDayCellComponentNgFactory"], _node_modules_nebular_theme_index_ngfactory__WEBPACK_IMPORTED_MODULE_8__["NbCalendarRangeYearCellComponentNgFactory"], _node_modules_nebular_theme_index_ngfactory__WEBPACK_IMPORTED_MODULE_8__["NbCalendarComponentNgFactory"], _node_modules_nebular_theme_index_ngfactory__WEBPACK_IMPORTED_MODULE_8__["NbCalendarRangeComponentNgFactory"], _node_modules_nebular_theme_index_ngfactory__WEBPACK_IMPORTED_MODULE_8__["NbDatepickerContainerComponentNgFactory"], _node_modules_nebular_theme_index_ngfactory__WEBPACK_IMPORTED_MODULE_8__["NbDialogContainerComponentNgFactory"], _node_modules_nebular_theme_index_ngfactory__WEBPACK_IMPORTED_MODULE_8__["NbWindowsContainerComponentNgFactory"], _node_modules_nebular_theme_index_ngfactory__WEBPACK_IMPORTED_MODULE_8__["NbWindowComponentNgFactory"], _node_modules_nebular_theme_index_ngfactory__WEBPACK_IMPORTED_MODULE_8__["NbToastrContainerComponentNgFactory"], _node_modules_nebular_theme_index_ngfactory__WEBPACK_IMPORTED_MODULE_8__["NbToastComponentNgFactory"], _node_modules_swimlane_ngx_charts_release_common_tooltip_tooltip_component_ngfactory__WEBPACK_IMPORTED_MODULE_9__["TooltipContentComponentNgFactory"], _app_component_ngfactory__WEBPACK_IMPORTED_MODULE_10__["AppComponentNgFactory"]]], [3, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"]], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModuleRef"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_core__WEBPACK_IMPORTED_MODULE_0__["LOCALE_ID"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵangular_packages_core_core_q"], [[3, _angular_core__WEBPACK_IMPORTED_MODULE_0__["LOCALE_ID"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_common__WEBPACK_IMPORTED_MODULE_11__["NgLocalization"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["NgLocaleLocalization"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["LOCALE_ID"], [2, _angular_common__WEBPACK_IMPORTED_MODULE_11__["ɵangular_packages_common_common_a"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵangular_packages_core_core_bb"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵangular_packages_core_core_s"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_core__WEBPACK_IMPORTED_MODULE_0__["APP_ID"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵangular_packages_core_core_f"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_core__WEBPACK_IMPORTED_MODULE_0__["IterableDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵangular_packages_core_core_o"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_core__WEBPACK_IMPORTED_MODULE_0__["KeyValueDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵangular_packages_core_core_p"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["DomSanitizer"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["ɵDomSanitizerImpl"], [_angular_common__WEBPACK_IMPORTED_MODULE_11__["DOCUMENT"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](6144, _angular_core__WEBPACK_IMPORTED_MODULE_0__["Sanitizer"], null, [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["DomSanitizer"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["HAMMER_GESTURE_CONFIG"], _angular_material_core__WEBPACK_IMPORTED_MODULE_13__["GestureConfig"], [[2, _angular_material_core__WEBPACK_IMPORTED_MODULE_13__["MAT_HAMMER_OPTIONS"]], [2, _angular_material_core__WEBPACK_IMPORTED_MODULE_13__["MatCommonModule"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["EVENT_MANAGER_PLUGINS"], function (p0_0, p0_1, p0_2, p1_0, p2_0, p2_1, p2_2, p2_3) { return [new _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["ɵDomEventsPlugin"](p0_0, p0_1, p0_2), new _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["ɵKeyEventsPlugin"](p1_0), new _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["ɵHammerGesturesPlugin"](p2_0, p2_1, p2_2, p2_3)]; }, [_angular_common__WEBPACK_IMPORTED_MODULE_11__["DOCUMENT"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["PLATFORM_ID"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["DOCUMENT"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["DOCUMENT"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["HAMMER_GESTURE_CONFIG"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵConsole"], [2, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["HAMMER_LOADER"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["EventManager"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["EventManager"], [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["EVENT_MANAGER_PLUGINS"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](135680, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["ɵDomSharedStylesHost"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["ɵDomSharedStylesHost"], [_angular_common__WEBPACK_IMPORTED_MODULE_11__["DOCUMENT"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["ɵDomRendererFactory2"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["ɵDomRendererFactory2"], [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["EventManager"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["ɵDomSharedStylesHost"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["APP_ID"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_animations_browser__WEBPACK_IMPORTED_MODULE_14__["AnimationDriver"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_15__["ɵangular_packages_platform_browser_animations_animations_a"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_animations_browser__WEBPACK_IMPORTED_MODULE_14__["ɵAnimationStyleNormalizer"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_15__["ɵangular_packages_platform_browser_animations_animations_b"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_animations_browser__WEBPACK_IMPORTED_MODULE_14__["ɵAnimationEngine"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_15__["ɵInjectableAnimationEngine"], [_angular_common__WEBPACK_IMPORTED_MODULE_11__["DOCUMENT"], _angular_animations_browser__WEBPACK_IMPORTED_MODULE_14__["AnimationDriver"], _angular_animations_browser__WEBPACK_IMPORTED_MODULE_14__["ɵAnimationStyleNormalizer"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_core__WEBPACK_IMPORTED_MODULE_0__["RendererFactory2"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_15__["ɵangular_packages_platform_browser_animations_animations_c"], [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["ɵDomRendererFactory2"], _angular_animations_browser__WEBPACK_IMPORTED_MODULE_14__["ɵAnimationEngine"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](6144, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["ɵSharedStylesHost"], null, [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["ɵDomSharedStylesHost"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_core__WEBPACK_IMPORTED_MODULE_0__["Testability"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Testability"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_animations__WEBPACK_IMPORTED_MODULE_16__["AnimationBuilder"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_15__["ɵBrowserAnimationBuilder"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["RendererFactory2"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["DOCUMENT"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_forms__WEBPACK_IMPORTED_MODULE_17__["FormBuilder"], _angular_forms__WEBPACK_IMPORTED_MODULE_17__["FormBuilder"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_forms__WEBPACK_IMPORTED_MODULE_17__["ɵangular_packages_forms_forms_o"], _angular_forms__WEBPACK_IMPORTED_MODULE_17__["ɵangular_packages_forms_forms_o"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_cdk_observers__WEBPACK_IMPORTED_MODULE_18__["MutationObserverFactory"], _angular_cdk_observers__WEBPACK_IMPORTED_MODULE_18__["MutationObserverFactory"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_material_core__WEBPACK_IMPORTED_MODULE_13__["ErrorStateMatcher"], _angular_material_core__WEBPACK_IMPORTED_MODULE_13__["ErrorStateMatcher"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpXsrfTokenExtractor"], _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["ɵangular_packages_common_http_http_g"], [_angular_common__WEBPACK_IMPORTED_MODULE_11__["DOCUMENT"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["PLATFORM_ID"], _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["ɵangular_packages_common_http_http_e"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["ɵangular_packages_common_http_http_h"], _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["ɵangular_packages_common_http_http_h"], [_angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpXsrfTokenExtractor"], _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["ɵangular_packages_common_http_http_f"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HTTP_INTERCEPTORS"], function (p0_0, p1_0, p2_0, p2_1) { return [p0_0, new _helpers_jwt_interceptor__WEBPACK_IMPORTED_MODULE_20__["JwtInterceptor"](p1_0), new _helpers_error_interceptor__WEBPACK_IMPORTED_MODULE_21__["ErrorInterceptor"](p2_0, p2_1)]; }, [_angular_common_http__WEBPACK_IMPORTED_MODULE_19__["ɵangular_packages_common_http_http_h"], _service_auth_service__WEBPACK_IMPORTED_MODULE_22__["AuthService"], _service_auth_service__WEBPACK_IMPORTED_MODULE_22__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["Router"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["ɵangular_packages_common_http_http_d"], _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["ɵangular_packages_common_http_http_d"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](6144, _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["XhrFactory"], null, [_angular_common_http__WEBPACK_IMPORTED_MODULE_19__["ɵangular_packages_common_http_http_d"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpXhrBackend"], _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpXhrBackend"], [_angular_common_http__WEBPACK_IMPORTED_MODULE_19__["XhrFactory"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](6144, _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpBackend"], null, [_angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpXhrBackend"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpHandler"], _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["ɵHttpInterceptingHandler"], [_angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpBackend"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpClient"], _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpClient"], [_angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpHandler"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbLayoutScrollService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbLayoutScrollService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbPlatform"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbPlatform"], [[2, _angular_core__WEBPACK_IMPORTED_MODULE_0__["PLATFORM_ID"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_25__["ScrollDispatcher"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbScrollDispatcherAdapter"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbPlatform"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbLayoutScrollService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbLayoutRulerService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbLayoutRulerService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](135680, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbViewportRulerAdapter"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbViewportRulerAdapter"], [_nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbPlatform"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbLayoutRulerService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbLayoutScrollService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](6144, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_DOCUMENT"], null, [_angular_common__WEBPACK_IMPORTED_MODULE_11__["DOCUMENT"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_26__["ScrollStrategyOptions"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["ɵb"], [_nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbLayoutScrollService"], _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_25__["ScrollDispatcher"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbViewportRulerAdapter"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_DOCUMENT"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](135680, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbOverlayContainerAdapter"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbOverlayContainerAdapter"], [_angular_common__WEBPACK_IMPORTED_MODULE_11__["DOCUMENT"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](6144, _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_26__["OverlayContainer"], null, [_nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbOverlayContainerAdapter"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_26__["Overlay"], _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_26__["Overlay"], [_angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_26__["ScrollStrategyOptions"], _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_26__["OverlayContainer"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"], _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_26__["OverlayPositionBuilder"], _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_26__["OverlayKeyboardDispatcher"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["DOCUMENT"], _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_27__["Directionality"], [2, _angular_common__WEBPACK_IMPORTED_MODULE_11__["Location"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_26__["ɵc"], _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_26__["ɵd"], [_angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_26__["Overlay"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_28__["MAT_TOOLTIP_SCROLL_STRATEGY"], _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_28__["MAT_TOOLTIP_SCROLL_STRATEGY_FACTORY"], [_angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_26__["Overlay"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_router__WEBPACK_IMPORTED_MODULE_23__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["ɵangular_packages_router_router_g"], [_angular_router__WEBPACK_IMPORTED_MODULE_23__["Router"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_router__WEBPACK_IMPORTED_MODULE_23__["NoPreloading"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["NoPreloading"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](6144, _angular_router__WEBPACK_IMPORTED_MODULE_23__["PreloadingStrategy"], null, [_angular_router__WEBPACK_IMPORTED_MODULE_23__["NoPreloading"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](135680, _angular_router__WEBPACK_IMPORTED_MODULE_23__["RouterPreloader"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["RouterPreloader"], [_angular_router__WEBPACK_IMPORTED_MODULE_23__["Router"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModuleFactoryLoader"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Compiler"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["PreloadingStrategy"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_router__WEBPACK_IMPORTED_MODULE_23__["PreloadAllModules"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["PreloadAllModules"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_router__WEBPACK_IMPORTED_MODULE_23__["ɵangular_packages_router_router_o"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["ɵangular_packages_router_router_c"], [_angular_router__WEBPACK_IMPORTED_MODULE_23__["Router"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["ViewportScroller"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["ROUTER_CONFIGURATION"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_router__WEBPACK_IMPORTED_MODULE_23__["ROUTER_INITIALIZER"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["ɵangular_packages_router_router_j"], [_angular_router__WEBPACK_IMPORTED_MODULE_23__["ɵangular_packages_router_router_h"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_core__WEBPACK_IMPORTED_MODULE_0__["APP_BOOTSTRAP_LISTENER"], function (p0_0) { return [p0_0]; }, [_angular_router__WEBPACK_IMPORTED_MODULE_23__["ROUTER_INITIALIZER"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbRestoreScrollTopHelper"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbRestoreScrollTopHelper"], [_angular_router__WEBPACK_IMPORTED_MODULE_23__["Router"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbSearchService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbSearchService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_material_menu__WEBPACK_IMPORTED_MODULE_29__["MAT_MENU_SCROLL_STRATEGY"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_29__["ɵb24"], [_angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_26__["Overlay"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, ngx_avatar__WEBPACK_IMPORTED_MODULE_30__["ɵa"], ngx_avatar__WEBPACK_IMPORTED_MODULE_30__["ɵa"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, ngx_avatar__WEBPACK_IMPORTED_MODULE_30__["ɵb"], ngx_avatar__WEBPACK_IMPORTED_MODULE_30__["ɵb"], [[2, ngx_avatar__WEBPACK_IMPORTED_MODULE_30__["ɵc"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, ngx_avatar__WEBPACK_IMPORTED_MODULE_30__["AvatarService"], ngx_avatar__WEBPACK_IMPORTED_MODULE_30__["AvatarService"], [_angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpClient"], ngx_avatar__WEBPACK_IMPORTED_MODULE_30__["ɵb"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbDateService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbNativeDateService"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["LOCALE_ID"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_common__WEBPACK_IMPORTED_MODULE_11__["DatePipe"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["DatePipe"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["LOCALE_ID"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbCalendarMonthModelService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbCalendarMonthModelService"], [_nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbDateService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_material_select__WEBPACK_IMPORTED_MODULE_31__["MAT_SELECT_SCROLL_STRATEGY"], _angular_material_select__WEBPACK_IMPORTED_MODULE_31__["MAT_SELECT_SCROLL_STRATEGY_PROVIDER_FACTORY"], [_angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_26__["Overlay"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, ng_push__WEBPACK_IMPORTED_MODULE_32__["PushNotificationsService"], ng_push__WEBPACK_IMPORTED_MODULE_32__["PushNotificationsService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _swimlane_ngx_charts_release_common_tooltip_injection_service__WEBPACK_IMPORTED_MODULE_33__["InjectionService"], _swimlane_ngx_charts_release_common_tooltip_injection_service__WEBPACK_IMPORTED_MODULE_33__["InjectionService"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _swimlane_ngx_charts_release_common_tooltip_tooltip_service__WEBPACK_IMPORTED_MODULE_34__["TooltipService"], _swimlane_ngx_charts_release_common_tooltip_tooltip_service__WEBPACK_IMPORTED_MODULE_34__["TooltipService"], [_swimlane_ngx_charts_release_common_tooltip_injection_service__WEBPACK_IMPORTED_MODULE_33__["InjectionService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_WINDOW"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["nbWindowFactory"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbJSThemesRegistry"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbJSThemesRegistry"], [_nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_BUILT_IN_JS_THEMES"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_JS_THEMES"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbMediaBreakpointsService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbMediaBreakpointsService"], [_nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_MEDIA_BREAKPOINTS"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbThemeService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbThemeService"], [_nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_THEME_OPTIONS"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbMediaBreakpointsService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbJSThemesRegistry"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbSpinnerService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbSpinnerService"], [_nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_DOCUMENT"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbLayoutDirectionService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbLayoutDirectionService"], [[2, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_LAYOUT_DIRECTION"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbOverlayPositionBuilder"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbOverlayPositionBuilder"], [_angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_25__["ViewportRuler"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["DOCUMENT"], _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_35__["Platform"], _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_26__["OverlayContainer"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbPositionBuilderService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbPositionBuilderService"], [_nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_DOCUMENT"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbViewportRulerAdapter"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbPlatform"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbOverlayPositionBuilder"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbOverlayContainerAdapter"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbTriggerStrategyBuilderService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbTriggerStrategyBuilderService"], [_nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_DOCUMENT"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbOverlay"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbOverlay"], [_angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_26__["ScrollStrategyOptions"], _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_26__["OverlayContainer"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"], _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_26__["OverlayPositionBuilder"], _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_26__["OverlayKeyboardDispatcher"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["DOCUMENT"], _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_27__["Directionality"], [2, _angular_common__WEBPACK_IMPORTED_MODULE_11__["Location"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbOverlayService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbOverlayService"], [_nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbOverlay"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbLayoutDirectionService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbPositionHelper"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbPositionHelper"], [_nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbLayoutDirectionService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbBlockScrollStrategyAdapter"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbBlockScrollStrategyAdapter"], [_nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_DOCUMENT"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbViewportRulerAdapter"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbLayoutScrollService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbFocusTrapFactoryService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbFocusTrapFactoryService"], [_angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_36__["InteractivityChecker"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_DOCUMENT"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](135680, _theme_services_window_mode_block_scroll_service__WEBPACK_IMPORTED_MODULE_37__["WindowModeBlockScrollService"], _theme_services_window_mode_block_scroll_service__WEBPACK_IMPORTED_MODULE_37__["WindowModeBlockScrollService"], [_nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbLayoutScrollService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbViewportRulerAdapter"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbLayoutRulerService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_WINDOW"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbSidebarService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbSidebarService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbMenuService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbMenuService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["ɵa"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["ɵa"], [_angular_common__WEBPACK_IMPORTED_MODULE_11__["Location"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_DATE_ADAPTER"], function (p0_0, p1_0) { return [new _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbDateAdapterService"](p0_0), new _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbRangeAdapterService"](p1_0)]; }, [_nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbDateService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbDateService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbDialogService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbDialogService"], [_nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_DOCUMENT"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_DIALOG_CONFIG"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbPositionBuilderService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbOverlayService"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbWindowService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbWindowService"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbOverlayService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbOverlayPositionBuilder"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbBlockScrollStrategyAdapter"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_WINDOW_CONFIG"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbToastrContainerRegistry"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbToastrContainerRegistry"], [_nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbOverlayService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbPositionBuilderService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbPositionHelper"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_DOCUMENT"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbToastrService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbToastrService"], [_nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_TOASTR_CONFIG"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbToastrContainerRegistry"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_mock_users_service__WEBPACK_IMPORTED_MODULE_38__["UserService"], _core_mock_users_service__WEBPACK_IMPORTED_MODULE_38__["UserService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_mock_electricity_service__WEBPACK_IMPORTED_MODULE_39__["ElectricityService"], _core_mock_electricity_service__WEBPACK_IMPORTED_MODULE_39__["ElectricityService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_mock_smart_table_service__WEBPACK_IMPORTED_MODULE_40__["SmartTableService"], _core_mock_smart_table_service__WEBPACK_IMPORTED_MODULE_40__["SmartTableService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_mock_periods_service__WEBPACK_IMPORTED_MODULE_41__["PeriodsService"], _core_mock_periods_service__WEBPACK_IMPORTED_MODULE_41__["PeriodsService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_mock_user_activity_service__WEBPACK_IMPORTED_MODULE_42__["UserActivityService"], _core_mock_user_activity_service__WEBPACK_IMPORTED_MODULE_42__["UserActivityService"], [_core_mock_periods_service__WEBPACK_IMPORTED_MODULE_41__["PeriodsService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_mock_orders_chart_service__WEBPACK_IMPORTED_MODULE_43__["OrdersChartService"], _core_mock_orders_chart_service__WEBPACK_IMPORTED_MODULE_43__["OrdersChartService"], [_core_mock_periods_service__WEBPACK_IMPORTED_MODULE_41__["PeriodsService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_mock_profit_chart_service__WEBPACK_IMPORTED_MODULE_44__["ProfitChartService"], _core_mock_profit_chart_service__WEBPACK_IMPORTED_MODULE_44__["ProfitChartService"], [_core_mock_periods_service__WEBPACK_IMPORTED_MODULE_41__["PeriodsService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_mock_traffic_list_service__WEBPACK_IMPORTED_MODULE_45__["TrafficListService"], _core_mock_traffic_list_service__WEBPACK_IMPORTED_MODULE_45__["TrafficListService"], [_core_mock_periods_service__WEBPACK_IMPORTED_MODULE_41__["PeriodsService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_mock_earning_service__WEBPACK_IMPORTED_MODULE_46__["EarningService"], _core_mock_earning_service__WEBPACK_IMPORTED_MODULE_46__["EarningService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_data_orders_chart__WEBPACK_IMPORTED_MODULE_47__["OrdersChartData"], _core_mock_orders_chart_service__WEBPACK_IMPORTED_MODULE_43__["OrdersChartService"], [_core_mock_periods_service__WEBPACK_IMPORTED_MODULE_41__["PeriodsService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_data_profit_chart__WEBPACK_IMPORTED_MODULE_48__["ProfitChartData"], _core_mock_profit_chart_service__WEBPACK_IMPORTED_MODULE_44__["ProfitChartService"], [_core_mock_periods_service__WEBPACK_IMPORTED_MODULE_41__["PeriodsService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_mock_orders_profit_chart_service__WEBPACK_IMPORTED_MODULE_49__["OrdersProfitChartService"], _core_mock_orders_profit_chart_service__WEBPACK_IMPORTED_MODULE_49__["OrdersProfitChartService"], [_core_data_orders_chart__WEBPACK_IMPORTED_MODULE_47__["OrdersChartData"], _core_data_profit_chart__WEBPACK_IMPORTED_MODULE_48__["ProfitChartData"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_mock_traffic_bar_service__WEBPACK_IMPORTED_MODULE_50__["TrafficBarService"], _core_mock_traffic_bar_service__WEBPACK_IMPORTED_MODULE_50__["TrafficBarService"], [_core_mock_periods_service__WEBPACK_IMPORTED_MODULE_41__["PeriodsService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_mock_profit_bar_animation_chart_service__WEBPACK_IMPORTED_MODULE_51__["ProfitBarAnimationChartService"], _core_mock_profit_bar_animation_chart_service__WEBPACK_IMPORTED_MODULE_51__["ProfitBarAnimationChartService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_mock_temperature_humidity_service__WEBPACK_IMPORTED_MODULE_52__["TemperatureHumidityService"], _core_mock_temperature_humidity_service__WEBPACK_IMPORTED_MODULE_52__["TemperatureHumidityService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_mock_solar_service__WEBPACK_IMPORTED_MODULE_53__["SolarService"], _core_mock_solar_service__WEBPACK_IMPORTED_MODULE_53__["SolarService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_mock_traffic_chart_service__WEBPACK_IMPORTED_MODULE_54__["TrafficChartService"], _core_mock_traffic_chart_service__WEBPACK_IMPORTED_MODULE_54__["TrafficChartService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_mock_stats_bar_service__WEBPACK_IMPORTED_MODULE_55__["StatsBarService"], _core_mock_stats_bar_service__WEBPACK_IMPORTED_MODULE_55__["StatsBarService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_mock_country_order_service__WEBPACK_IMPORTED_MODULE_56__["CountryOrderService"], _core_mock_country_order_service__WEBPACK_IMPORTED_MODULE_56__["CountryOrderService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_mock_stats_progress_bar_service__WEBPACK_IMPORTED_MODULE_57__["StatsProgressBarService"], _core_mock_stats_progress_bar_service__WEBPACK_IMPORTED_MODULE_57__["StatsProgressBarService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_mock_visitors_analytics_service__WEBPACK_IMPORTED_MODULE_58__["VisitorsAnalyticsService"], _core_mock_visitors_analytics_service__WEBPACK_IMPORTED_MODULE_58__["VisitorsAnalyticsService"], [_core_mock_periods_service__WEBPACK_IMPORTED_MODULE_41__["PeriodsService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_mock_security_cameras_service__WEBPACK_IMPORTED_MODULE_59__["SecurityCamerasService"], _core_mock_security_cameras_service__WEBPACK_IMPORTED_MODULE_59__["SecurityCamerasService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_data_users__WEBPACK_IMPORTED_MODULE_60__["UserData"], _core_mock_users_service__WEBPACK_IMPORTED_MODULE_38__["UserService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_data_electricity__WEBPACK_IMPORTED_MODULE_61__["ElectricityData"], _core_mock_electricity_service__WEBPACK_IMPORTED_MODULE_39__["ElectricityService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_data_smart_table__WEBPACK_IMPORTED_MODULE_62__["SmartTableData"], _core_mock_smart_table_service__WEBPACK_IMPORTED_MODULE_40__["SmartTableService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_data_user_activity__WEBPACK_IMPORTED_MODULE_63__["UserActivityData"], _core_mock_user_activity_service__WEBPACK_IMPORTED_MODULE_42__["UserActivityService"], [_core_mock_periods_service__WEBPACK_IMPORTED_MODULE_41__["PeriodsService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_data_traffic_list__WEBPACK_IMPORTED_MODULE_64__["TrafficListData"], _core_mock_traffic_list_service__WEBPACK_IMPORTED_MODULE_45__["TrafficListService"], [_core_mock_periods_service__WEBPACK_IMPORTED_MODULE_41__["PeriodsService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_data_earning__WEBPACK_IMPORTED_MODULE_65__["EarningData"], _core_mock_earning_service__WEBPACK_IMPORTED_MODULE_46__["EarningService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_data_orders_profit_chart__WEBPACK_IMPORTED_MODULE_66__["OrdersProfitChartData"], _core_mock_orders_profit_chart_service__WEBPACK_IMPORTED_MODULE_49__["OrdersProfitChartService"], [_core_data_orders_chart__WEBPACK_IMPORTED_MODULE_47__["OrdersChartData"], _core_data_profit_chart__WEBPACK_IMPORTED_MODULE_48__["ProfitChartData"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_data_traffic_bar__WEBPACK_IMPORTED_MODULE_67__["TrafficBarData"], _core_mock_traffic_bar_service__WEBPACK_IMPORTED_MODULE_50__["TrafficBarService"], [_core_mock_periods_service__WEBPACK_IMPORTED_MODULE_41__["PeriodsService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_data_profit_bar_animation_chart__WEBPACK_IMPORTED_MODULE_68__["ProfitBarAnimationChartData"], _core_mock_profit_bar_animation_chart_service__WEBPACK_IMPORTED_MODULE_51__["ProfitBarAnimationChartService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_data_temperature_humidity__WEBPACK_IMPORTED_MODULE_69__["TemperatureHumidityData"], _core_mock_temperature_humidity_service__WEBPACK_IMPORTED_MODULE_52__["TemperatureHumidityService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_data_solar__WEBPACK_IMPORTED_MODULE_70__["SolarData"], _core_mock_solar_service__WEBPACK_IMPORTED_MODULE_53__["SolarService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_data_traffic_chart__WEBPACK_IMPORTED_MODULE_71__["TrafficChartData"], _core_mock_traffic_chart_service__WEBPACK_IMPORTED_MODULE_54__["TrafficChartService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_data_stats_bar__WEBPACK_IMPORTED_MODULE_72__["StatsBarData"], _core_mock_stats_bar_service__WEBPACK_IMPORTED_MODULE_55__["StatsBarService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_data_country_order__WEBPACK_IMPORTED_MODULE_73__["CountryOrderData"], _core_mock_country_order_service__WEBPACK_IMPORTED_MODULE_56__["CountryOrderService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_data_stats_progress_bar__WEBPACK_IMPORTED_MODULE_74__["StatsProgressBarData"], _core_mock_stats_progress_bar_service__WEBPACK_IMPORTED_MODULE_57__["StatsProgressBarService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_data_visitors_analytics__WEBPACK_IMPORTED_MODULE_75__["VisitorsAnalyticsData"], _core_mock_visitors_analytics_service__WEBPACK_IMPORTED_MODULE_58__["VisitorsAnalyticsService"], [_core_mock_periods_service__WEBPACK_IMPORTED_MODULE_41__["PeriodsService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_data_security_cameras__WEBPACK_IMPORTED_MODULE_76__["SecurityCamerasData"], _core_mock_security_cameras_service__WEBPACK_IMPORTED_MODULE_59__["SecurityCamerasService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NB_AUTH_OPTIONS"], _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["nbOptionsFactory"], [_nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NB_AUTH_USER_OPTIONS"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NB_AUTH_STRATEGIES"], _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["nbStrategiesFactory"], [_nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NB_AUTH_OPTIONS"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NB_AUTH_TOKENS"], _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["nbTokensFactory"], [_nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NB_AUTH_STRATEGIES"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NbAuthTokenParceler"], _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NbAuthTokenParceler"], [_nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NB_AUTH_FALLBACK_TOKEN"], _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NB_AUTH_TOKENS"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NbTokenStorage"], _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NbTokenLocalStorage"], [_nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NbAuthTokenParceler"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NbTokenService"], _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NbTokenService"], [_nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NbTokenStorage"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NbAuthService"], _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NbAuthService"], [_nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NbTokenService"], _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NB_AUTH_STRATEGIES"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NbDummyAuthStrategy"], _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NbDummyAuthStrategy"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NbPasswordAuthStrategy"], _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NbPasswordAuthStrategy"], [_angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["ActivatedRoute"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NbOAuth2AuthStrategy"], _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NbOAuth2AuthStrategy"], [_angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["ActivatedRoute"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_WINDOW"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_security__WEBPACK_IMPORTED_MODULE_78__["NbAclService"], _nebular_security__WEBPACK_IMPORTED_MODULE_78__["NbAclService"], [[2, _nebular_security__WEBPACK_IMPORTED_MODULE_78__["NB_SECURITY_OPTIONS_TOKEN"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_security__WEBPACK_IMPORTED_MODULE_78__["NbRoleProvider"], _core_core_module__WEBPACK_IMPORTED_MODULE_79__["NbSimpleRoleProvider"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _nebular_security__WEBPACK_IMPORTED_MODULE_78__["NbAccessChecker"], _nebular_security__WEBPACK_IMPORTED_MODULE_78__["NbAccessChecker"], [_nebular_security__WEBPACK_IMPORTED_MODULE_78__["NbRoleProvider"], _nebular_security__WEBPACK_IMPORTED_MODULE_78__["NbAclService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_utils_analytics_service__WEBPACK_IMPORTED_MODULE_80__["AnalyticsService"], _core_utils_analytics_service__WEBPACK_IMPORTED_MODULE_80__["AnalyticsService"], [_angular_common__WEBPACK_IMPORTED_MODULE_11__["Location"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["Router"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_utils_layout_service__WEBPACK_IMPORTED_MODULE_81__["LayoutService"], _core_utils_layout_service__WEBPACK_IMPORTED_MODULE_81__["LayoutService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _core_utils_player_service__WEBPACK_IMPORTED_MODULE_82__["PlayerService"], _core_utils_player_service__WEBPACK_IMPORTED_MODULE_82__["PlayerService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](135680, _core_utils_state_service__WEBPACK_IMPORTED_MODULE_83__["StateService"], _core_utils_state_service__WEBPACK_IMPORTED_MODULE_83__["StateService"], [_nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbLayoutDirectionService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _auth_auth_service_service__WEBPACK_IMPORTED_MODULE_84__["AuthService"], _auth_auth_service_service__WEBPACK_IMPORTED_MODULE_84__["AuthService"], [_angular_router__WEBPACK_IMPORTED_MODULE_23__["Router"], _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpClient"], _service_globals_service__WEBPACK_IMPORTED_MODULE_85__["GlobalsService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_86__["AuthGuard"], _auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_86__["AuthGuard"], [_auth_auth_service_service__WEBPACK_IMPORTED_MODULE_84__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["Router"], _service_localstorage_service__WEBPACK_IMPORTED_MODULE_87__["LocalstorageService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _gurads_client_auth_gurad__WEBPACK_IMPORTED_MODULE_88__["AuthGuardClient"], _gurads_client_auth_gurad__WEBPACK_IMPORTED_MODULE_88__["AuthGuardClient"], [_angular_router__WEBPACK_IMPORTED_MODULE_23__["Router"], _service_auth_service__WEBPACK_IMPORTED_MODULE_22__["AuthService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, ngx_spinner__WEBPACK_IMPORTED_MODULE_89__["NgxSpinnerService"], ngx_spinner__WEBPACK_IMPORTED_MODULE_89__["NgxSpinnerService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _service_secure_storage_service__WEBPACK_IMPORTED_MODULE_90__["StorageService"], _service_secure_storage_service__WEBPACK_IMPORTED_MODULE_90__["StorageService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _ethereumProvider_ethereum__WEBPACK_IMPORTED_MODULE_91__["EthereumProvider"], _ethereumProvider_ethereum__WEBPACK_IMPORTED_MODULE_91__["EthereumProvider"], [_service_secure_storage_service__WEBPACK_IMPORTED_MODULE_90__["StorageService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _auth_LoginActivate__WEBPACK_IMPORTED_MODULE_92__["LoginActivate"], _auth_LoginActivate__WEBPACK_IMPORTED_MODULE_92__["LoginActivate"], [_auth_auth_service_service__WEBPACK_IMPORTED_MODULE_84__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["Router"], _service_localstorage_service__WEBPACK_IMPORTED_MODULE_87__["LocalstorageService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_common__WEBPACK_IMPORTED_MODULE_11__["CommonModule"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["CommonModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_27__["BidiModule"], _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_27__["BidiModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_material_core__WEBPACK_IMPORTED_MODULE_13__["MatCommonModule"], _angular_material_core__WEBPACK_IMPORTED_MODULE_13__["MatCommonModule"], [[2, _angular_material_core__WEBPACK_IMPORTED_MODULE_13__["MATERIAL_SANITY_CHECKS"]], [2, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["HAMMER_LOADER"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_35__["PlatformModule"], _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_35__["PlatformModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_material_core__WEBPACK_IMPORTED_MODULE_13__["MatRippleModule"], _angular_material_core__WEBPACK_IMPORTED_MODULE_13__["MatRippleModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_material_button__WEBPACK_IMPORTED_MODULE_93__["MatButtonModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_93__["MatButtonModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1024, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ErrorHandler"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["ɵangular_packages_platform_browser_platform_browser_a"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1024, _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgProbeToken"], function () { return [_angular_router__WEBPACK_IMPORTED_MODULE_23__["ɵangular_packages_router_router_b"]()]; }, []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](512, _angular_router__WEBPACK_IMPORTED_MODULE_23__["ɵangular_packages_router_router_h"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["ɵangular_packages_router_router_h"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1024, _angular_core__WEBPACK_IMPORTED_MODULE_0__["APP_INITIALIZER"], function (p0_0, p1_0) { return [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["ɵangular_packages_platform_browser_platform_browser_j"](p0_0), _angular_router__WEBPACK_IMPORTED_MODULE_23__["ɵangular_packages_router_router_i"](p1_0)]; }, [[2, _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgProbeToken"]], _angular_router__WEBPACK_IMPORTED_MODULE_23__["ɵangular_packages_router_router_h"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](512, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationInitStatus"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationInitStatus"], [[2, _angular_core__WEBPACK_IMPORTED_MODULE_0__["APP_INITIALIZER"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](131584, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationRef"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵConsole"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ErrorHandler"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationInitStatus"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationModule"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationModule"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationRef"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["BrowserModule"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["BrowserModule"], [[3, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["BrowserModule"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_15__["BrowserAnimationsModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_15__["BrowserAnimationsModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_forms__WEBPACK_IMPORTED_MODULE_17__["ɵangular_packages_forms_forms_d"], _angular_forms__WEBPACK_IMPORTED_MODULE_17__["ɵangular_packages_forms_forms_d"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_forms__WEBPACK_IMPORTED_MODULE_17__["ReactiveFormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_17__["ReactiveFormsModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_cdk_text_field__WEBPACK_IMPORTED_MODULE_94__["TextFieldModule"], _angular_cdk_text_field__WEBPACK_IMPORTED_MODULE_94__["TextFieldModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_cdk_observers__WEBPACK_IMPORTED_MODULE_18__["ObserversModule"], _angular_cdk_observers__WEBPACK_IMPORTED_MODULE_18__["ObserversModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_95__["MatFormFieldModule"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_95__["MatFormFieldModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_material_input__WEBPACK_IMPORTED_MODULE_96__["MatInputModule"], _angular_material_input__WEBPACK_IMPORTED_MODULE_96__["MatInputModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, ngx_copy_to_clipboard__WEBPACK_IMPORTED_MODULE_97__["NgxCopyToClipboardModule"], ngx_copy_to_clipboard__WEBPACK_IMPORTED_MODULE_97__["NgxCopyToClipboardModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpClientXsrfModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpClientXsrfModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpClientModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpClientModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_36__["A11yModule"], _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_36__["A11yModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_98__["PortalModule"], _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_98__["PortalModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_25__["ScrollingModule"], _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_25__["ScrollingModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_26__["OverlayModule"], _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_26__["OverlayModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_28__["MatTooltipModule"], _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_28__["MatTooltipModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_forms__WEBPACK_IMPORTED_MODULE_17__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_17__["FormsModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_material_card__WEBPACK_IMPORTED_MODULE_99__["MatCardModule"], _angular_material_card__WEBPACK_IMPORTED_MODULE_99__["MatCardModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1024, _angular_router__WEBPACK_IMPORTED_MODULE_23__["ɵangular_packages_router_router_a"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["ɵangular_packages_router_router_e"], [[3, _angular_router__WEBPACK_IMPORTED_MODULE_23__["Router"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](512, _angular_router__WEBPACK_IMPORTED_MODULE_23__["UrlSerializer"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["DefaultUrlSerializer"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](512, _angular_router__WEBPACK_IMPORTED_MODULE_23__["ChildrenOutletContexts"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["ChildrenOutletContexts"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](512, _angular_common__WEBPACK_IMPORTED_MODULE_11__["LocationStrategy"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["PathLocationStrategy"], [_angular_common__WEBPACK_IMPORTED_MODULE_11__["PlatformLocation"], [2, _angular_common__WEBPACK_IMPORTED_MODULE_11__["APP_BASE_HREF"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](512, _angular_common__WEBPACK_IMPORTED_MODULE_11__["Location"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["Location"], [_angular_common__WEBPACK_IMPORTED_MODULE_11__["LocationStrategy"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["PlatformLocation"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](512, _angular_core__WEBPACK_IMPORTED_MODULE_0__["Compiler"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Compiler"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](512, _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModuleFactoryLoader"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["SystemJsNgModuleLoader"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["Compiler"], [2, _angular_core__WEBPACK_IMPORTED_MODULE_0__["SystemJsNgModuleLoaderConfig"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1024, _angular_router__WEBPACK_IMPORTED_MODULE_23__["ROUTES"], function () { return [[{ path: "dashboard", canActivate: [_gurads_auth_gurad__WEBPACK_IMPORTED_MODULE_100__["AuthGuard"]], loadChildren: _app_routing_module__WEBPACK_IMPORTED_MODULE_101__["ɵ0"] }, { path: "auth", component: _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NbAuthComponent"], children: [{ path: "", component: _auth_login_login_component__WEBPACK_IMPORTED_MODULE_102__["NgxLoginComponent"] }, { path: "login", component: _auth_login_login_component__WEBPACK_IMPORTED_MODULE_102__["NgxLoginComponent"] }, { path: "register", component: _auth_register_ngx_register_component__WEBPACK_IMPORTED_MODULE_103__["NgxRegisterComponent"] }, { path: "logout", component: _auth_login_login_component__WEBPACK_IMPORTED_MODULE_102__["NgxLoginComponent"] }, { path: "request-password", component: _auth_login_login_component__WEBPACK_IMPORTED_MODULE_102__["NgxLoginComponent"] }, { path: "reset-password", component: _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NbResetPasswordComponent"] }] }, { path: "", redirectTo: "dashboard", pathMatch: "full", canActivate: [_gurads_auth_gurad__WEBPACK_IMPORTED_MODULE_100__["AuthGuard"]] }, { path: "**", redirectTo: "dashboard", canActivate: [_gurads_auth_gurad__WEBPACK_IMPORTED_MODULE_100__["AuthGuard"]] }]]; }, []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](256, _angular_router__WEBPACK_IMPORTED_MODULE_23__["ROUTER_CONFIGURATION"], { useHash: false }, []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1024, _angular_router__WEBPACK_IMPORTED_MODULE_23__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["ɵangular_packages_router_router_f"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationRef"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["UrlSerializer"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["ChildrenOutletContexts"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["Location"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModuleFactoryLoader"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Compiler"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["ROUTES"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["ROUTER_CONFIGURATION"], [2, _angular_router__WEBPACK_IMPORTED_MODULE_23__["UrlHandlingStrategy"]], [2, _angular_router__WEBPACK_IMPORTED_MODULE_23__["RouteReuseStrategy"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_router__WEBPACK_IMPORTED_MODULE_23__["RouterModule"], _angular_router__WEBPACK_IMPORTED_MODULE_23__["RouterModule"], [[2, _angular_router__WEBPACK_IMPORTED_MODULE_23__["ɵangular_packages_router_router_a"]], [2, _angular_router__WEBPACK_IMPORTED_MODULE_23__["Router"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _app_routing_module__WEBPACK_IMPORTED_MODULE_101__["AppRoutingModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_101__["AppRoutingModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["ɵc"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["ɵc"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbLayoutModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbLayoutModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbIconModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbIconModule"], [_nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbIconLibraries"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbMenuModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbMenuModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbBadgeModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbBadgeModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbUserModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbUserModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbActionsModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbActionsModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbCdkMappingModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbCdkMappingModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbCdkAdapterModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbCdkAdapterModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbOverlayModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbOverlayModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbButtonModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbButtonModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbSearchModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbSearchModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbSidebarModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbSidebarModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbContextMenuModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbContextMenuModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_security__WEBPACK_IMPORTED_MODULE_78__["NbSecurityModule"], _nebular_security__WEBPACK_IMPORTED_MODULE_78__["NbSecurityModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbInputModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbInputModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbCardModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbCardModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbCheckboxModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbCheckboxModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbSelectModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbSelectModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_eva_icons__WEBPACK_IMPORTED_MODULE_104__["NbEvaIconsModule"], _nebular_eva_icons__WEBPACK_IMPORTED_MODULE_104__["NbEvaIconsModule"], [_nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbIconLibraries"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_material_icon__WEBPACK_IMPORTED_MODULE_105__["MatIconModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_105__["MatIconModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_material_badge__WEBPACK_IMPORTED_MODULE_106__["MatBadgeModule"], _angular_material_badge__WEBPACK_IMPORTED_MODULE_106__["MatBadgeModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_material_menu__WEBPACK_IMPORTED_MODULE_29__["_MatMenuDirectivesModule"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_29__["_MatMenuDirectivesModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_material_menu__WEBPACK_IMPORTED_MODULE_29__["MatMenuModule"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_29__["MatMenuModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, ngx_avatar__WEBPACK_IMPORTED_MODULE_30__["AvatarModule"], ngx_avatar__WEBPACK_IMPORTED_MODULE_30__["AvatarModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _theme_theme_module__WEBPACK_IMPORTED_MODULE_107__["ThemeModule"], _theme_theme_module__WEBPACK_IMPORTED_MODULE_107__["ThemeModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbCalendarKitModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbCalendarKitModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbBaseCalendarModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbBaseCalendarModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbCalendarModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbCalendarModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbCalendarRangeModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbCalendarRangeModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbDatepickerModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbDatepickerModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbDialogModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbDialogModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbWindowModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbWindowModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbToastrModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbToastrModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbChatModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbChatModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbAlertModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbAlertModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NbAuthModule"], _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NbAuthModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _core_core_module__WEBPACK_IMPORTED_MODULE_79__["CoreModule"], _core_core_module__WEBPACK_IMPORTED_MODULE_79__["CoreModule"], [[3, _core_core_module__WEBPACK_IMPORTED_MODULE_79__["CoreModule"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_material_core__WEBPACK_IMPORTED_MODULE_13__["MatPseudoCheckboxModule"], _angular_material_core__WEBPACK_IMPORTED_MODULE_13__["MatPseudoCheckboxModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_material_core__WEBPACK_IMPORTED_MODULE_13__["MatOptionModule"], _angular_material_core__WEBPACK_IMPORTED_MODULE_13__["MatOptionModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_material_select__WEBPACK_IMPORTED_MODULE_31__["MatSelectModule"], _angular_material_select__WEBPACK_IMPORTED_MODULE_31__["MatSelectModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, ngx_uploader__WEBPACK_IMPORTED_MODULE_108__["NgxUploaderModule"], ngx_uploader__WEBPACK_IMPORTED_MODULE_108__["NgxUploaderModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, ng_push__WEBPACK_IMPORTED_MODULE_32__["PushNotificationsModule"], ng_push__WEBPACK_IMPORTED_MODULE_32__["PushNotificationsModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _swimlane_ngx_charts_release_common_axes_axes_module__WEBPACK_IMPORTED_MODULE_109__["AxesModule"], _swimlane_ngx_charts_release_common_axes_axes_module__WEBPACK_IMPORTED_MODULE_109__["AxesModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _swimlane_ngx_charts_release_common_tooltip_tooltip_module__WEBPACK_IMPORTED_MODULE_110__["TooltipModule"], _swimlane_ngx_charts_release_common_tooltip_tooltip_module__WEBPACK_IMPORTED_MODULE_110__["TooltipModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _swimlane_ngx_charts_release_common_chart_common_module__WEBPACK_IMPORTED_MODULE_111__["ChartCommonModule"], _swimlane_ngx_charts_release_common_chart_common_module__WEBPACK_IMPORTED_MODULE_111__["ChartCommonModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _swimlane_ngx_charts_release_area_chart_area_chart_module__WEBPACK_IMPORTED_MODULE_112__["AreaChartModule"], _swimlane_ngx_charts_release_area_chart_area_chart_module__WEBPACK_IMPORTED_MODULE_112__["AreaChartModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _swimlane_ngx_charts_release_bar_chart_bar_chart_module__WEBPACK_IMPORTED_MODULE_113__["BarChartModule"], _swimlane_ngx_charts_release_bar_chart_bar_chart_module__WEBPACK_IMPORTED_MODULE_113__["BarChartModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _swimlane_ngx_charts_release_bubble_chart_bubble_chart_module__WEBPACK_IMPORTED_MODULE_114__["BubbleChartModule"], _swimlane_ngx_charts_release_bubble_chart_bubble_chart_module__WEBPACK_IMPORTED_MODULE_114__["BubbleChartModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _swimlane_ngx_charts_release_force_directed_graph_force_directed_graph_module__WEBPACK_IMPORTED_MODULE_115__["ForceDirectedGraphModule"], _swimlane_ngx_charts_release_force_directed_graph_force_directed_graph_module__WEBPACK_IMPORTED_MODULE_115__["ForceDirectedGraphModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _swimlane_ngx_charts_release_heat_map_heat_map_module__WEBPACK_IMPORTED_MODULE_116__["HeatMapModule"], _swimlane_ngx_charts_release_heat_map_heat_map_module__WEBPACK_IMPORTED_MODULE_116__["HeatMapModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _swimlane_ngx_charts_release_line_chart_line_chart_module__WEBPACK_IMPORTED_MODULE_117__["LineChartModule"], _swimlane_ngx_charts_release_line_chart_line_chart_module__WEBPACK_IMPORTED_MODULE_117__["LineChartModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _swimlane_ngx_charts_release_pie_chart_pie_chart_module__WEBPACK_IMPORTED_MODULE_118__["PieChartModule"], _swimlane_ngx_charts_release_pie_chart_pie_chart_module__WEBPACK_IMPORTED_MODULE_118__["PieChartModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _swimlane_ngx_charts_release_polar_chart_polar_chart_module__WEBPACK_IMPORTED_MODULE_119__["PolarChartModule"], _swimlane_ngx_charts_release_polar_chart_polar_chart_module__WEBPACK_IMPORTED_MODULE_119__["PolarChartModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _swimlane_ngx_charts_release_number_card_number_card_module__WEBPACK_IMPORTED_MODULE_120__["NumberCardModule"], _swimlane_ngx_charts_release_number_card_number_card_module__WEBPACK_IMPORTED_MODULE_120__["NumberCardModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _swimlane_ngx_charts_release_tree_map_tree_map_module__WEBPACK_IMPORTED_MODULE_121__["TreeMapModule"], _swimlane_ngx_charts_release_tree_map_tree_map_module__WEBPACK_IMPORTED_MODULE_121__["TreeMapModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _swimlane_ngx_charts_release_gauge_gauge_module__WEBPACK_IMPORTED_MODULE_122__["GaugeModule"], _swimlane_ngx_charts_release_gauge_gauge_module__WEBPACK_IMPORTED_MODULE_122__["GaugeModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _swimlane_ngx_charts_release_ngx_charts_module__WEBPACK_IMPORTED_MODULE_123__["NgxChartsModule"], _swimlane_ngx_charts_release_ngx_charts_module__WEBPACK_IMPORTED_MODULE_123__["NgxChartsModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, ngx_spinner__WEBPACK_IMPORTED_MODULE_89__["NgxSpinnerModule"], ngx_spinner__WEBPACK_IMPORTED_MODULE_89__["NgxSpinnerModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _app_module__WEBPACK_IMPORTED_MODULE_1__["AppModule"], _app_module__WEBPACK_IMPORTED_MODULE_1__["AppModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](256, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵAPP_ROOT"], true, []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](256, _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_15__["ANIMATION_MODULE_TYPE"], "BrowserAnimations", []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](256, _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["ɵangular_packages_common_http_http_e"], "XSRF-TOKEN", []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](256, _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["ɵangular_packages_common_http_http_f"], "X-XSRF-TOKEN", []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](256, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_THEME_OPTIONS"], { name: "cosmic" }, []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](256, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_BUILT_IN_JS_THEMES"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["BUILT_IN_THEMES"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](256, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_JS_THEMES"], [{ name: "default", variables: { temperature: { arcFill: ["#3366ff", "#3366ff", "#3366ff", "#3366ff", "#3366ff"], arcEmpty: "#f7f9fc", thumbBg: "#f7f9fc", thumbBorder: "#3366ff" }, solar: { gradientLeft: "#3366ff", gradientRight: "#3366ff", shadowColor: "rgba(0, 0, 0, 0)", secondSeriesFill: "#f7f9fc", radius: ["80%", "90%"] }, traffic: { tooltipBg: "#ffffff", tooltipBorderColor: "#f7f9fc", tooltipExtraCss: "border-radius: 10px; padding: 4px 16px;", tooltipTextColor: "#1a2138", tooltipFontWeight: "normal", yAxisSplitLine: "#edf1f7", lineBg: "#e4e9f2", lineShadowBlur: "1", itemColor: "#e4e9f2", itemBorderColor: "#e4e9f2", itemEmphasisBorderColor: "#3366ff", shadowLineDarkBg: "rgba(0, 0, 0, 0)", shadowLineShadow: "rgba(0, 0, 0, 0)", gradFrom: "#f7f9fc", gradTo: "#f7f9fc" }, electricity: { tooltipBg: "#ffffff", tooltipLineColor: "#1a2138", tooltipLineWidth: "0", tooltipBorderColor: "#f7f9fc", tooltipExtraCss: "border-radius: 10px; padding: 8px 24px;", tooltipTextColor: "#1a2138", tooltipFontWeight: "normal", axisLineColor: "#edf1f7", xAxisTextColor: "#8f9bb3", yAxisSplitLine: "#edf1f7", itemBorderColor: "#3366ff", lineStyle: "solid", lineWidth: "4", lineGradFrom: "#3366ff", lineGradTo: "#3366ff", lineShadow: "rgba(0, 0, 0, 0)", areaGradFrom: "#f7f9fc", areaGradTo: "#f7f9fc", shadowLineDarkBg: "rgba(0, 0, 0, 0)" }, bubbleMap: { titleColor: "#1a2138", areaColor: "#e4e9f2", areaHoverColor: "#3366ff", areaBorderColor: "#c5cee0" }, profitBarAnimationEchart: { textColor: "#1a2138", firstAnimationBarColor: "#3366ff", secondAnimationBarColor: "#00d68f", splitLineStyleOpacity: "1", splitLineStyleWidth: "1", splitLineStyleColor: "#edf1f7", tooltipTextColor: "#1a2138", tooltipFontWeight: "normal", tooltipFontSize: "16", tooltipBg: "#ffffff", tooltipBorderColor: "#f7f9fc", tooltipBorderWidth: "1", tooltipExtraCss: "border-radius: 10px; padding: 4px 16px;" }, trafficBarEchart: { gradientFrom: "#ffc94d", gradientTo: "#ffaa00", shadow: "#ffc94d", shadowBlur: "0", axisTextColor: "#1a2138", axisFontSize: "12", tooltipBg: "#ffffff", tooltipBorderColor: "#f7f9fc", tooltipExtraCss: "border-radius: 10px; padding: 4px 16px;", tooltipTextColor: "#1a2138", tooltipFontWeight: "normal" }, countryOrders: { countryBorderColor: "#e4e9f2", countryFillColor: "#edf1f7", countryBorderWidth: "1", hoveredCountryBorderColor: "#3366ff", hoveredCountryFillColor: "#598bff", hoveredCountryBorderWidth: "1", chartAxisLineColor: "#e4e9f2", chartAxisTextColor: "#8f9bb3", chartAxisFontSize: "16", chartGradientTo: "#3366ff", chartGradientFrom: "#598bff", chartAxisSplitLine: "#edf1f7", chartShadowLineColor: "#598bff", chartLineBottomShadowColor: "#3366ff", chartInnerLineColor: "#f7f9fc" }, echarts: { bg: "#ffffff", textColor: "#1a2138", axisLineColor: "#1a2138", splitLineColor: "#edf1f7", itemHoverShadowColor: "rgba(0, 0, 0, 0.5)", tooltipBackgroundColor: "#3366ff", areaOpacity: "0.7" }, chartjs: { axisLineColor: "#edf1f7", textColor: "#1a2138" }, orders: { tooltipBg: "#ffffff", tooltipLineColor: "rgba(0, 0, 0, 0)", tooltipLineWidth: "0", tooltipBorderColor: "#f7f9fc", tooltipExtraCss: "border-radius: 10px; padding: 8px 24px;", tooltipTextColor: "#1a2138", tooltipFontWeight: "normal", tooltipFontSize: "20", axisLineColor: "#e4e9f2", axisFontSize: "16", axisTextColor: "#8f9bb3", yAxisSplitLine: "#edf1f7", itemBorderColor: "#3366ff", lineStyle: "solid", lineWidth: "4", firstAreaGradFrom: "#edf1f7", firstAreaGradTo: "#edf1f7", firstShadowLineDarkBg: "rgba(0, 0, 0, 0)", secondLineGradFrom: "#3366ff", secondLineGradTo: "#3366ff", secondAreaGradFrom: "rgba(51, 102, 255, 0.2)", secondAreaGradTo: "rgba(51, 102, 255, 0)", secondShadowLineDarkBg: "rgba(0, 0, 0, 0)", thirdLineGradFrom: "#00d68f", thirdLineGradTo: "#2ce69b", thirdAreaGradFrom: "rgba(0, 214, 143, 0.2)", thirdAreaGradTo: "rgba(0, 214, 143, 0)", thirdShadowLineDarkBg: "rgba(0, 0, 0, 0)" }, profit: { bg: "#ffffff", textColor: "#1a2138", axisLineColor: "#e4e9f2", splitLineColor: "#edf1f7", areaOpacity: "1", axisFontSize: "16", axisTextColor: "#8f9bb3", firstLineGradFrom: "#edf1f7", firstLineGradTo: "#edf1f7", firstLineShadow: "rgba(0, 0, 0, 0)", secondLineGradFrom: "#3366ff", secondLineGradTo: "#3366ff", secondLineShadow: "rgba(0, 0, 0, 0)", thirdLineGradFrom: "#00d68f", thirdLineGradTo: "#2ce69b", thirdLineShadow: "rgba(0, 0, 0, 0)" }, orderProfitLegend: { firstItem: "#00d68f", secondItem: "#3366ff", thirdItem: "#edf1f7" }, visitors: { tooltipBg: "#ffffff", tooltipLineColor: "rgba(0, 0, 0, 0)", tooltipLineWidth: "1", tooltipBorderColor: "#f7f9fc", tooltipExtraCss: "border-radius: 10px; padding: 8px 24px;", tooltipTextColor: "#1a2138", tooltipFontWeight: "normal", tooltipFontSize: "20", axisLineColor: "#e4e9f2", axisFontSize: "16", axisTextColor: "#8f9bb3", yAxisSplitLine: "#edf1f7", itemBorderColor: "#3366ff", lineStyle: "dotted", lineWidth: "6", lineGradFrom: "#ffffff", lineGradTo: "#ffffff", lineShadow: "rgba(0, 0, 0, 0)", areaGradFrom: "#3366ff", areaGradTo: "#598bff", innerLineStyle: "solid", innerLineWidth: "1", innerAreaGradFrom: "#00d68f", innerAreaGradTo: "#00d68f" }, visitorsLegend: { firstIcon: "#00d68f", secondIcon: "#3366ff" }, visitorsPie: { firstPieGradientLeft: "#00d68f", firstPieGradientRight: "#00d68f", firstPieShadowColor: "rgba(0, 0, 0, 0)", firstPieRadius: ["70%", "90%"], secondPieGradientLeft: "#ffaa00", secondPieGradientRight: "#ffc94d", secondPieShadowColor: "rgba(0, 0, 0, 0)", secondPieRadius: ["60%", "97%"], shadowOffsetX: "0", shadowOffsetY: "0" }, visitorsPieLegend: { firstSection: "#ffaa00", secondSection: "#00d68f" }, earningPie: { radius: ["65%", "100%"], center: ["50%", "50%"], fontSize: "22", firstPieGradientLeft: "#00d68f", firstPieGradientRight: "#00d68f", firstPieShadowColor: "rgba(0, 0, 0, 0)", secondPieGradientLeft: "#3366ff", secondPieGradientRight: "#3366ff", secondPieShadowColor: "rgba(0, 0, 0, 0)", thirdPieGradientLeft: "#ffaa00", thirdPieGradientRight: "#ffaa00", thirdPieShadowColor: "rgba(0, 0, 0, 0)" }, earningLine: { gradFrom: "#3366ff", gradTo: "#3366ff", tooltipTextColor: "#1a2138", tooltipFontWeight: "normal", tooltipFontSize: "16", tooltipBg: "#ffffff", tooltipBorderColor: "#f7f9fc", tooltipBorderWidth: "1", tooltipExtraCss: "border-radius: 10px; padding: 4px 16px;" } } }, { name: "cosmic", variables: { temperature: { arcFill: ["#2ec7fe", "#31ffad", "#7bff24", "#fff024", "#f7bd59"], arcEmpty: "#252547", thumbBg: "#ffffff", thumbBorder: "#ffffff" }, solar: { gradientLeft: "#a16eff", gradientRight: "#a16eff", shadowColor: "rgba(0, 0, 0, 0)", secondSeriesFill: "#252547", radius: ["70%", "90%"] }, traffic: { tooltipBg: "#323259", tooltipBorderColor: "#252547", tooltipExtraCss: "box-shadow: 0px 2px 46px 0 rgba(50, 50, 89); border-radius: 10px; padding: 4px 16px;", tooltipTextColor: "#ffffff", tooltipFontWeight: "normal", yAxisSplitLine: "#151a30", lineBg: "#252547", lineShadowBlur: "14", itemColor: "#252547", itemBorderColor: "#252547", itemEmphasisBorderColor: "#a16eff", shadowLineDarkBg: "#1b1b38", shadowLineShadow: "#1b1b38", gradFrom: "#323259", gradTo: "#252547" }, electricity: { tooltipBg: "#323259", tooltipLineColor: "#ffffff", tooltipLineWidth: "0", tooltipBorderColor: "#252547", tooltipExtraCss: "box-shadow: 0px 2px 46px 0 rgba(0, 255, 170, 0.35); border-radius: 10px; padding: 8px 24px;", tooltipTextColor: "#ffffff", tooltipFontWeight: "normal", axisLineColor: "#1b1b38", xAxisTextColor: "#b4b4db", yAxisSplitLine: "#151a30", itemBorderColor: "#252547", lineStyle: "dotted", lineWidth: "6", lineGradFrom: "#00d68f", lineGradTo: "#ffaa00", lineShadow: "#13132b", areaGradFrom: "#252547", areaGradTo: "#1b1b38", shadowLineDarkBg: "#1b1b38" }, bubbleMap: { titleColor: "#ffffff", areaColor: "#13132b", areaHoverColor: "#a16eff", areaBorderColor: "#13132b" }, profitBarAnimationEchart: { textColor: "#ffffff", firstAnimationBarColor: "#a16eff", secondAnimationBarColor: "#00d68f", splitLineStyleOpacity: "1", splitLineStyleWidth: "1", splitLineStyleColor: "#252547", tooltipTextColor: "#ffffff", tooltipFontWeight: "normal", tooltipFontSize: "16", tooltipBg: "#323259", tooltipBorderColor: "#252547", tooltipBorderWidth: "1", tooltipExtraCss: "border-radius: 10px; padding: 4px 16px;" }, trafficBarEchart: { gradientFrom: "#ffc94d", gradientTo: "#ffaa00", shadow: "#ffc94d", shadowBlur: "5", axisTextColor: "#ffffff", axisFontSize: "12", tooltipBg: "#323259", tooltipBorderColor: "#252547", tooltipExtraCss: "border-radius: 10px; padding: 4px 16px;", tooltipTextColor: "#ffffff", tooltipFontWeight: "normal" }, countryOrders: { countryBorderColor: "#13132b", countryFillColor: "#1b1b38", countryBorderWidth: "1", hoveredCountryBorderColor: "#a16eff", hoveredCountryFillColor: "#b18aff", hoveredCountryBorderWidth: "1", chartAxisLineColor: "#13132b", chartAxisTextColor: "#b4b4db", chartAxisFontSize: "16", chartGradientTo: "#a16eff", chartGradientFrom: "#b18aff", chartAxisSplitLine: "#151a30", chartShadowLineColor: "#b18aff", chartLineBottomShadowColor: "#a16eff", chartInnerLineColor: "#252547" }, echarts: { bg: "#323259", textColor: "#ffffff", axisLineColor: "#ffffff", splitLineColor: "#151a30", itemHoverShadowColor: "rgba(0, 0, 0, 0.5)", tooltipBackgroundColor: "#a16eff", areaOpacity: "1" }, chartjs: { axisLineColor: "#151a30", textColor: "#ffffff" }, orders: { tooltipBg: "#323259", tooltipLineColor: "rgba(0, 0, 0, 0)", tooltipLineWidth: "0", tooltipBorderColor: "#252547", tooltipExtraCss: "border-radius: 10px; padding: 8px 24px;", tooltipTextColor: "#ffffff", tooltipFontWeight: "normal", tooltipFontSize: "20", axisLineColor: "#13132b", axisFontSize: "16", axisTextColor: "#b4b4db", yAxisSplitLine: "#151a30", itemBorderColor: "#a16eff", lineStyle: "solid", lineWidth: "4", firstAreaGradFrom: "#252547", firstAreaGradTo: "#252547", firstShadowLineDarkBg: "#252547", secondLineGradFrom: "#a16eff", secondLineGradTo: "#a16eff", secondAreaGradFrom: "rgba(161, 110, 255, 0.8)", secondAreaGradTo: "rgba(161, 110, 255, 0.5)", secondShadowLineDarkBg: "#a16eff", thirdLineGradFrom: "#00d68f", thirdLineGradTo: "#2ce69b", thirdAreaGradFrom: "rgba(0, 214, 143, 0.7)", thirdAreaGradTo: "rgba(0, 214, 143, 0.4)", thirdShadowLineDarkBg: "#00d68f" }, profit: { bg: "#323259", textColor: "#ffffff", axisLineColor: "#13132b", splitLineColor: "#151a30", areaOpacity: "1", axisFontSize: "16", axisTextColor: "#b4b4db", firstLineGradFrom: "#252547", firstLineGradTo: "#252547", firstLineShadow: "rgba(0, 0, 0, 0)", secondLineGradFrom: "#a16eff", secondLineGradTo: "#a16eff", secondLineShadow: "rgba(0, 0, 0, 0)", thirdLineGradFrom: "#00d68f", thirdLineGradTo: "#2ce69b", thirdLineShadow: "rgba(0, 0, 0, 0)" }, orderProfitLegend: { firstItem: "#00d68f", secondItem: "#a16eff", thirdItem: "#252547" }, visitors: { tooltipBg: "#323259", tooltipLineColor: "rgba(0, 0, 0, 0)", tooltipLineWidth: "1", tooltipBorderColor: "#252547", tooltipExtraCss: "border-radius: 10px; padding: 8px 24px;", tooltipTextColor: "#ffffff", tooltipFontWeight: "normal", tooltipFontSize: "20", axisLineColor: "#13132b", axisFontSize: "16", axisTextColor: "#b4b4db", yAxisSplitLine: "#151a30", itemBorderColor: "#a16eff", lineStyle: "dotted", lineWidth: "6", lineGradFrom: "#ffffff", lineGradTo: "#ffffff", lineShadow: "rgba(0, 0, 0, 0)", areaGradFrom: "#a16eff", areaGradTo: "#b18aff", innerLineStyle: "solid", innerLineWidth: "1", innerAreaGradFrom: "#00d68f", innerAreaGradTo: "#00d68f" }, visitorsLegend: { firstIcon: "#00d68f", secondIcon: "#a16eff" }, visitorsPie: { firstPieGradientLeft: "#00d68f", firstPieGradientRight: "#2ce69b", firstPieShadowColor: "rgba(0, 0, 0, 0)", firstPieRadius: ["70%", "90%"], secondPieGradientLeft: "#ffaa00", secondPieGradientRight: "#ffc94d", secondPieShadowColor: "rgba(0, 0, 0, 0)", secondPieRadius: ["60%", "95%"], shadowOffsetX: "0", shadowOffsetY: "3" }, visitorsPieLegend: { firstSection: "#ffaa00", secondSection: "#00d68f" }, earningPie: { radius: ["65%", "100%"], center: ["50%", "50%"], fontSize: "22", firstPieGradientLeft: "#00d68f", firstPieGradientRight: "#00d68f", firstPieShadowColor: "rgba(0, 0, 0, 0)", secondPieGradientLeft: "#a16eff", secondPieGradientRight: "#a16eff", secondPieShadowColor: "rgba(0, 0, 0, 0)", thirdPieGradientLeft: "#ffaa00", thirdPieGradientRight: "#ffaa00", thirdPieShadowColor: "rgba(0, 0, 0, 0)" }, earningLine: { gradFrom: "#a16eff", gradTo: "#a16eff", tooltipTextColor: "#ffffff", tooltipFontWeight: "normal", tooltipFontSize: "16", tooltipBg: "#323259", tooltipBorderColor: "#252547", tooltipBorderWidth: "1", tooltipExtraCss: "border-radius: 10px; padding: 4px 16px;" } } }, { name: "corporate", variables: { temperature: { arcFill: ["#ffa36b", "#ffa36b", "#ff9e7a", "#ff9888", "#ff8ea0"], arcEmpty: "#f7f9fc", thumbBg: "#f7f9fc", thumbBorder: "#ffa36b" }, solar: { gradientLeft: "#73a1ff", gradientRight: "#73a1ff", shadowColor: "rgba(0, 0, 0, 0)", secondSeriesFill: "#f7f9fc", radius: ["80%", "90%"] }, traffic: { tooltipBg: "#ffffff", tooltipBorderColor: "#f7f9fc", tooltipExtraCss: "border-radius: 10px; padding: 4px 16px;", tooltipTextColor: "#1a2138", tooltipFontWeight: "normal", yAxisSplitLine: "rgba(0, 0, 0, 0)", lineBg: "#73a1ff", lineShadowBlur: "0", itemColor: "#e4e9f2", itemBorderColor: "#e4e9f2", itemEmphasisBorderColor: "#598bff", shadowLineDarkBg: "rgba(0, 0, 0, 0)", shadowLineShadow: "rgba(0, 0, 0, 0)", gradFrom: "#ffffff", gradTo: "#ffffff" }, electricity: { tooltipBg: "#ffffff", tooltipLineColor: "#1a2138", tooltipLineWidth: "0", tooltipBorderColor: "#f7f9fc", tooltipExtraCss: "border-radius: 10px; padding: 8px 24px;", tooltipTextColor: "#1a2138", tooltipFontWeight: "normal", axisLineColor: "#edf1f7", xAxisTextColor: "#8f9bb3", yAxisSplitLine: "#edf1f7", itemBorderColor: "#73a1ff", lineStyle: "solid", lineWidth: "4", lineGradFrom: "#73a1ff", lineGradTo: "#73a1ff", lineShadow: "rgba(0, 0, 0, 0)", areaGradFrom: "rgba(0, 0, 0, 0)", areaGradTo: "rgba(0, 0, 0, 0)", shadowLineDarkBg: "rgba(0, 0, 0, 0)" }, bubbleMap: { titleColor: "#1a2138", areaColor: "#e4e9f2", areaHoverColor: "#73a1ff", areaBorderColor: "#c5cee0" }, profitBarAnimationEchart: { textColor: "#1a2138", firstAnimationBarColor: "#73a1ff", secondAnimationBarColor: "#5dcfe3", splitLineStyleOpacity: "1", splitLineStyleWidth: "1", splitLineStyleColor: "#edf1f7", tooltipTextColor: "#1a2138", tooltipFontWeight: "normal", tooltipFontSize: "16", tooltipBg: "#ffffff", tooltipBorderColor: "#f7f9fc", tooltipBorderWidth: "1", tooltipExtraCss: "border-radius: 10px; padding: 4px 16px;" }, trafficBarEchart: { gradientFrom: "#ffc94d", gradientTo: "#ffa36b", shadow: "#ffc94d", shadowBlur: "0", axisTextColor: "#1a2138", axisFontSize: "12", tooltipBg: "#ffffff", tooltipBorderColor: "#f7f9fc", tooltipExtraCss: "border-radius: 10px; padding: 8px 24px;", tooltipTextColor: "#1a2138", tooltipFontWeight: "normal" }, countryOrders: { countryBorderColor: "#e4e9f2", countryFillColor: "#e4e9f2", countryBorderWidth: "1", hoveredCountryBorderColor: "#73a1ff", hoveredCountryFillColor: "#598bff", hoveredCountryBorderWidth: "1", chartAxisLineColor: "#e4e9f2", chartAxisTextColor: "#8f9bb3", chartAxisFontSize: "16", chartGradientTo: "#73a1ff", chartGradientFrom: "#598bff", chartAxisSplitLine: "#edf1f7", chartShadowLineColor: "#598bff", chartLineBottomShadowColor: "#73a1ff", chartInnerLineColor: "#f7f9fc" }, echarts: { bg: "#ffffff", textColor: "#1a2138", axisLineColor: "#1a2138", splitLineColor: "#edf1f7", itemHoverShadowColor: "rgba(0, 0, 0, 0.5)", tooltipBackgroundColor: "#73a1ff", areaOpacity: "0.7" }, chartjs: { axisLineColor: "#edf1f7", textColor: "#1a2138" }, orders: { tooltipBg: "#ffffff", tooltipLineColor: "rgba(0, 0, 0, 0)", tooltipLineWidth: "0", tooltipBorderColor: "#f7f9fc", tooltipExtraCss: "border-radius: 10px; padding: 8px 24px;", tooltipTextColor: "#1a2138", tooltipFontWeight: "normal", tooltipFontSize: "20", axisLineColor: "#e4e9f2", axisFontSize: "16", axisTextColor: "#8f9bb3", yAxisSplitLine: "#edf1f7", itemBorderColor: "#73a1ff", lineStyle: "solid", lineWidth: "4", firstAreaGradFrom: "#edf1f7", firstAreaGradTo: "#edf1f7", firstShadowLineDarkBg: "rgba(0, 0, 0, 0)", secondLineGradFrom: "#73a1ff", secondLineGradTo: "#73a1ff", secondAreaGradFrom: "rgba(0, 0, 0, 0)", secondAreaGradTo: "rgba(0, 0, 0, 0)", secondShadowLineDarkBg: "rgba(0, 0, 0, 0)", thirdLineGradFrom: "#5dcfe3", thirdLineGradTo: "#2ce69b", thirdAreaGradFrom: "rgba(0, 0, 0, 0)", thirdAreaGradTo: "rgba(0, 0, 0, 0)", thirdShadowLineDarkBg: "rgba(0, 0, 0, 0)" }, profit: { bg: "#ffffff", textColor: "#1a2138", axisLineColor: "#e4e9f2", splitLineColor: "#edf1f7", areaOpacity: "1", axisFontSize: "16", axisTextColor: "#8f9bb3", firstLineGradFrom: "#edf1f7", firstLineGradTo: "#edf1f7", firstLineShadow: "rgba(0, 0, 0, 0)", secondLineGradFrom: "#73a1ff", secondLineGradTo: "#73a1ff", secondLineShadow: "rgba(0, 0, 0, 0)", thirdLineGradFrom: "#5dcfe3", thirdLineGradTo: "#5dcfe3", thirdLineShadow: "rgba(0, 0, 0, 0)" }, orderProfitLegend: { firstItem: "#5dcfe3", secondItem: "#73a1ff", thirdItem: "#edf1f7" }, visitors: { tooltipBg: "#ffffff", tooltipLineColor: "rgba(0, 0, 0, 0)", tooltipLineWidth: "1", tooltipBorderColor: "#f7f9fc", tooltipExtraCss: "border-radius: 10px; padding: 8px 24px;", tooltipTextColor: "#1a2138", tooltipFontWeight: "normal", tooltipFontSize: "20", axisLineColor: "#e4e9f2", axisFontSize: "16", axisTextColor: "#8f9bb3", yAxisSplitLine: "#edf1f7", itemBorderColor: "#73a1ff", lineStyle: "dotted", lineWidth: "6", lineGradFrom: "#ffffff", lineGradTo: "#ffffff", lineShadow: "rgba(0, 0, 0, 0)", areaGradFrom: "#73a1ff", areaGradTo: "#598bff", innerLineStyle: "solid", innerLineWidth: "1", innerAreaGradFrom: "#5dcfe3", innerAreaGradTo: "#5dcfe3" }, visitorsLegend: { firstIcon: "#5dcfe3", secondIcon: "#73a1ff" }, visitorsPie: { firstPieGradientLeft: "#5dcfe3", firstPieGradientRight: "#5dcfe3", firstPieShadowColor: "rgba(0, 0, 0, 0)", firstPieRadius: ["65%", "90%"], secondPieGradientLeft: "#ffa36b", secondPieGradientRight: "#ffc94d", secondPieShadowColor: "rgba(0, 0, 0, 0)", secondPieRadius: ["63%", "92%"], shadowOffsetX: "-4", shadowOffsetY: "-4" }, visitorsPieLegend: { firstSection: "#ffa36b", secondSection: "#5dcfe3" }, earningPie: { radius: ["65%", "100%"], center: ["50%", "50%"], fontSize: "22", firstPieGradientLeft: "#5dcfe3", firstPieGradientRight: "#5dcfe3", firstPieShadowColor: "rgba(0, 0, 0, 0)", secondPieGradientLeft: "#73a1ff", secondPieGradientRight: "#73a1ff", secondPieShadowColor: "rgba(0, 0, 0, 0)", thirdPieGradientLeft: "#ffa36b", thirdPieGradientRight: "#ffa36b", thirdPieShadowColor: "rgba(0, 0, 0, 0)" }, earningLine: { gradFrom: "#73a1ff", gradTo: "#73a1ff", tooltipTextColor: "#1a2138", tooltipFontWeight: "normal", tooltipFontSize: "16", tooltipBg: "#ffffff", tooltipBorderColor: "#f7f9fc", tooltipBorderWidth: "1", tooltipExtraCss: "border-radius: 10px; padding: 4px 16px;" } } }, { name: "dark", variables: { temperature: { arcFill: ["#3366ff", "#3366ff", "#3366ff", "#3366ff", "#3366ff"], arcEmpty: "#1a2138", thumbBg: "#1a2138", thumbBorder: "#3366ff" }, solar: { gradientLeft: "#3366ff", gradientRight: "#3366ff", shadowColor: "rgba(0, 0, 0, 0)", secondSeriesFill: "#1a2138", radius: ["80%", "90%"] }, traffic: { tooltipBg: "#222b45", tooltipBorderColor: "#1a2138", tooltipExtraCss: "border-radius: 10px; padding: 4px 16px;", tooltipTextColor: "#ffffff", tooltipFontWeight: "normal", yAxisSplitLine: "#1b1b38", lineBg: "#101426", lineShadowBlur: "1", itemColor: "#101426", itemBorderColor: "#101426", itemEmphasisBorderColor: "#3366ff", shadowLineDarkBg: "rgba(0, 0, 0, 0)", shadowLineShadow: "rgba(0, 0, 0, 0)", gradFrom: "#1a2138", gradTo: "#1a2138" }, electricity: { tooltipBg: "#222b45", tooltipLineColor: "#ffffff", tooltipLineWidth: "0", tooltipBorderColor: "#1a2138", tooltipExtraCss: "border-radius: 10px; padding: 8px 24px;", tooltipTextColor: "#ffffff", tooltipFontWeight: "normal", axisLineColor: "#151a30", xAxisTextColor: "#8f9bb3", yAxisSplitLine: "#1b1b38", itemBorderColor: "#3366ff", lineStyle: "solid", lineWidth: "4", lineGradFrom: "#3366ff", lineGradTo: "#3366ff", lineShadow: "rgba(0, 0, 0, 0)", areaGradFrom: "#1a2138", areaGradTo: "#1a2138", shadowLineDarkBg: "rgba(0, 0, 0, 0)" }, bubbleMap: { titleColor: "#ffffff", areaColor: "#101426", areaHoverColor: "#3366ff", areaBorderColor: "#101426" }, profitBarAnimationEchart: { textColor: "#ffffff", firstAnimationBarColor: "#3366ff", secondAnimationBarColor: "#00d68f", splitLineStyleOpacity: "1", splitLineStyleWidth: "1", splitLineStyleColor: "#1b1b38", tooltipTextColor: "#ffffff", tooltipFontWeight: "normal", tooltipFontSize: "16", tooltipBg: "#222b45", tooltipBorderColor: "#1a2138", tooltipBorderWidth: "1", tooltipExtraCss: "border-radius: 10px; padding: 4px 16px;" }, trafficBarEchart: { gradientFrom: "#ffc94d", gradientTo: "#ffaa00", shadow: "#ffc94d", shadowBlur: "0", axisTextColor: "#ffffff", axisFontSize: "12", tooltipBg: "#222b45", tooltipBorderColor: "#1a2138", tooltipExtraCss: "border-radius: 10px; padding: 4px 16px;", tooltipTextColor: "#ffffff", tooltipFontWeight: "normal" }, countryOrders: { countryBorderColor: "#101426", countryFillColor: "#151a30", countryBorderWidth: "1", hoveredCountryBorderColor: "#3366ff", hoveredCountryFillColor: "#598bff", hoveredCountryBorderWidth: "1", chartAxisLineColor: "#101426", chartAxisTextColor: "#8f9bb3", chartAxisFontSize: "16", chartGradientTo: "#3366ff", chartGradientFrom: "#598bff", chartAxisSplitLine: "#1b1b38", chartShadowLineColor: "#598bff", chartLineBottomShadowColor: "#3366ff", chartInnerLineColor: "#1a2138" }, echarts: { bg: "#222b45", textColor: "#ffffff", axisLineColor: "#ffffff", splitLineColor: "#1b1b38", itemHoverShadowColor: "rgba(0, 0, 0, 0.5)", tooltipBackgroundColor: "#3366ff", areaOpacity: "0.7" }, chartjs: { axisLineColor: "#1b1b38", textColor: "#ffffff" }, orders: { tooltipBg: "#222b45", tooltipLineColor: "rgba(0, 0, 0, 0)", tooltipLineWidth: "0", tooltipBorderColor: "#1a2138", tooltipExtraCss: "border-radius: 10px; padding: 8px 24px;", tooltipTextColor: "#ffffff", tooltipFontWeight: "normal", tooltipFontSize: "20", axisLineColor: "#101426", axisFontSize: "16", axisTextColor: "#8f9bb3", yAxisSplitLine: "#1b1b38", itemBorderColor: "#3366ff", lineStyle: "solid", lineWidth: "4", firstAreaGradFrom: "#151a30", firstAreaGradTo: "#151a30", firstShadowLineDarkBg: "rgba(0, 0, 0, 0)", secondLineGradFrom: "#3366ff", secondLineGradTo: "#3366ff", secondAreaGradFrom: "rgba(51, 102, 255, 0.2)", secondAreaGradTo: "rgba(51, 102, 255, 0)", secondShadowLineDarkBg: "rgba(0, 0, 0, 0)", thirdLineGradFrom: "#00d68f", thirdLineGradTo: "#2ce69b", thirdAreaGradFrom: "rgba(0, 214, 143, 0.2)", thirdAreaGradTo: "rgba(0, 214, 143, 0)", thirdShadowLineDarkBg: "rgba(0, 0, 0, 0)" }, profit: { bg: "#222b45", textColor: "#ffffff", axisLineColor: "#101426", splitLineColor: "#1b1b38", areaOpacity: "1", axisFontSize: "16", axisTextColor: "#8f9bb3", firstLineGradFrom: "#151a30", firstLineGradTo: "#151a30", firstLineShadow: "rgba(0, 0, 0, 0)", secondLineGradFrom: "#3366ff", secondLineGradTo: "#3366ff", secondLineShadow: "rgba(0, 0, 0, 0)", thirdLineGradFrom: "#00d68f", thirdLineGradTo: "#2ce69b", thirdLineShadow: "rgba(0, 0, 0, 0)" }, orderProfitLegend: { firstItem: "#00d68f", secondItem: "#3366ff", thirdItem: "#151a30" }, visitors: { tooltipBg: "#222b45", tooltipLineColor: "rgba(0, 0, 0, 0)", tooltipLineWidth: "0", tooltipBorderColor: "#1a2138", tooltipExtraCss: "border-radius: 10px; padding: 8px 24px;", tooltipTextColor: "#ffffff", tooltipFontWeight: "normal", tooltipFontSize: "20", axisLineColor: "#101426", axisFontSize: "16", axisTextColor: "#8f9bb3", yAxisSplitLine: "#1b1b38", itemBorderColor: "#3366ff", lineStyle: "dotted", lineWidth: "6", lineGradFrom: "#ffffff", lineGradTo: "#ffffff", lineShadow: "rgba(0, 0, 0, 0)", areaGradFrom: "#3366ff", areaGradTo: "#598bff", innerLineStyle: "solid", innerLineWidth: "1", innerAreaGradFrom: "#00d68f", innerAreaGradTo: "#00d68f" }, visitorsLegend: { firstIcon: "#00d68f", secondIcon: "#3366ff" }, visitorsPie: { firstPieGradientLeft: "#00d68f", firstPieGradientRight: "#00d68f", firstPieShadowColor: "rgba(0, 0, 0, 0)", firstPieRadius: ["70%", "90%"], secondPieGradientLeft: "#ffaa00", secondPieGradientRight: "#ffc94d", secondPieShadowColor: "rgba(0, 0, 0, 0)", secondPieRadius: ["60%", "97%"], shadowOffsetX: "0", shadowOffsetY: "0" }, visitorsPieLegend: { firstSection: "#ffaa00", secondSection: "#00d68f" }, earningPie: { radius: ["65%", "100%"], center: ["50%", "50%"], fontSize: "22", firstPieGradientLeft: "#00d68f", firstPieGradientRight: "#00d68f", firstPieShadowColor: "rgba(0, 0, 0, 0)", secondPieGradientLeft: "#3366ff", secondPieGradientRight: "#3366ff", secondPieShadowColor: "rgba(0, 0, 0, 0)", thirdPieGradientLeft: "#ffaa00", thirdPieGradientRight: "#ffaa00", thirdPieShadowColor: "rgba(0, 0, 0, 0)" }, earningLine: { gradFrom: "#3366ff", gradTo: "#3366ff", tooltipTextColor: "#ffffff", tooltipFontWeight: "normal", tooltipFontSize: "16", tooltipBg: "#222b45", tooltipBorderColor: "#1a2138", tooltipBorderWidth: "1", tooltipExtraCss: "border-radius: 10px; padding: 4px 16px;" } } }], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](256, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_MEDIA_BREAKPOINTS"], _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["DEFAULT_MEDIA_BREAKPOINTS"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](256, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_LAYOUT_DIRECTION"], "ltr", []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](256, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_DIALOG_CONFIG"], {}, []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](256, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_WINDOW_CONFIG"], undefined, []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](256, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NB_TOASTR_CONFIG"], {}, []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](256, _nebular_theme__WEBPACK_IMPORTED_MODULE_24__["NbChatOptions"], { messageGoogleMapKey: "AIzaSyA_wNuCzia92MAmdLRzmqitRGvCF7wCZPY" }, []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](256, _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NB_AUTH_USER_OPTIONS"], { strategies: [[_nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NbDummyAuthStrategy"], { name: "email", delay: 3000 }]], forms: { login: { socialLinks: [{ url: "https://github.com/akveo/nebular", target: "_blank", icon: "github" }, { url: "https://www.facebook.com/akveo/", target: "_blank", icon: "facebook" }, { url: "https://twitter.com/akveo_inc", target: "_blank", icon: "twitter" }] }, register: { socialLinks: [{ url: "https://github.com/akveo/nebular", target: "_blank", icon: "github" }, { url: "https://www.facebook.com/akveo/", target: "_blank", icon: "facebook" }, { url: "https://twitter.com/akveo_inc", target: "_blank", icon: "twitter" }] } } }, []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](256, _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NB_AUTH_FALLBACK_TOKEN"], _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NbAuthSimpleToken"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](256, _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NB_AUTH_INTERCEPTOR_HEADER"], "Authorization", []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](256, _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["NB_AUTH_TOKEN_INTERCEPTOR_FILTER"], _nebular_auth__WEBPACK_IMPORTED_MODULE_77__["nbNoOpInterceptorFilter"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](256, _nebular_security__WEBPACK_IMPORTED_MODULE_78__["NB_SECURITY_OPTIONS_TOKEN"], { accessControl: { guest: { view: "*" }, user: { parent: "guest", create: "*", edit: "*", remove: "*" } } }, [])]); });



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: logger, metaReducers, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "logger", function() { return logger; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "metaReducers", function() { return metaReducers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm2015/store.js");
/* harmony import */ var ngrx_store_logger__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngrx-store-logger */ "./node_modules/ngrx-store-logger/dist/index.js");
/* harmony import */ var ngrx_store_logger__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(ngrx_store_logger__WEBPACK_IMPORTED_MODULE_1__);
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */


function logger(reducer) {
    return Object(ngrx_store_logger__WEBPACK_IMPORTED_MODULE_1__["storeLogger"])()(reducer);
}
const metaReducers = [logger];
// @ts-ignore
class AppModule {
}


/***/ }),

/***/ "./src/app/auth/LoginActivate.ts":
/*!***************************************!*\
  !*** ./src/app/auth/LoginActivate.ts ***!
  \***************************************/
/*! exports provided: LoginActivate */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginActivate", function() { return LoginActivate; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _auth_service_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./auth-service.service */ "./src/app/auth/auth-service.service.ts");
/* harmony import */ var _service_localstorage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/localstorage.service */ "./src/app/service/localstorage.service.ts");



class LoginActivate {
    constructor(authService, router, localstorageService) {
        this.authService = authService;
        this.router = router;
        this.localstorageService = localstorageService;
    }
    canActivate(route, state) {
        let user = JSON.parse(this.localstorageService.getItem('currentUser'));
        if (!user) {
            this.router.navigate(['/auth-client/login']);
            return true;
        }
        if (user.roles[0] === "ROLE_GROSSISTE") {
            this.router.navigate(['/dashboard/client']);
            return false;
        }
        else if (user.roles[0] === "ROLE_PHARMACIE") {
            this.router.navigate(['/dashboard-grossiste/produit']);
            return false;
        }
    }
}


/***/ }),

/***/ "./src/app/auth/auth-guard.service.ts":
/*!********************************************!*\
  !*** ./src/app/auth/auth-guard.service.ts ***!
  \********************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _auth_service_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./auth-service.service */ "./src/app/auth/auth-service.service.ts");
/* harmony import */ var _service_localstorage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/localstorage.service */ "./src/app/service/localstorage.service.ts");



class AuthGuard {
    constructor(authService, router, localstorageService) {
        this.authService = authService;
        this.router = router;
        this.localstorageService = localstorageService;
    }
    canActivate() {
        let user = JSON.parse(this.localstorageService.getItem('currentUser'));
        console.log(user);
        if (!user) {
            this.router.navigate(['/auth/login']);
            return false;
        }
        else
            return true;
    }
}


/***/ }),

/***/ "./src/app/auth/auth-service.service.ts":
/*!**********************************************!*\
  !*** ./src/app/auth/auth-service.service.ts ***!
  \**********************************************/
/*! exports provided: User, AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs_compat_add_observable_of__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs-compat/add/observable/of */ "./node_modules/rxjs-compat/add/observable/of.js");
/* harmony import */ var rxjs_compat_add_observable_of__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(rxjs_compat_add_observable_of__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _service_globals_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/globals.service */ "./src/app/service/globals.service.ts");




const httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
        'Content-Type': 'application/json'
    })
};
class User {
}
class AuthService {
    constructor(router, httpClient, globalService) {
        this.router = router;
        this.httpClient = httpClient;
        this.globalService = globalService;
        let headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': 'application/json',
            "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT"
        });
    }
    login(email, password) {
        return this.httpClient.post("/api/admin/login", {
            "email": email,
            "password": password
        }).toPromise()
            .then(response => response)
            .catch(this.handleError);
    }
    isAuthenticated() {
        const user = this.globalService.getUser();
        // Check whether the token is expired and return
        // true or false
        if (user != null) {
            return true;
        }
        else {
            return false;
        }
    }
    logout() {
        (this.router.navigate(['/auth/login']));
    }
}


/***/ }),

/***/ "./src/app/auth/login/login.component.ngfactory.js":
/*!*********************************************************!*\
  !*** ./src/app/auth/login/login.component.ngfactory.js ***!
  \*********************************************************/
/*! exports provided: RenderType_NgxLoginComponent, View_NgxLoginComponent_0, View_NgxLoginComponent_Host_0, NgxLoginComponentNgFactory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenderType_NgxLoginComponent", function() { return RenderType_NgxLoginComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_NgxLoginComponent_0", function() { return View_NgxLoginComponent_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_NgxLoginComponent_Host_0", function() { return View_NgxLoginComponent_Host_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxLoginComponentNgFactory", function() { return NgxLoginComponentNgFactory; });
/* harmony import */ var _login_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login.component.scss.shim.ngstyle */ "./src/app/auth/login/login.component.scss.shim.ngstyle.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm2015/form-field.js");
/* harmony import */ var _node_modules_angular_material_form_field_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/@angular/material/form-field/typings/index.ngfactory */ "./node_modules/@angular/material/form-field/typings/index.ngfactory.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/esm2015/core.js");
/* harmony import */ var _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/bidi */ "./node_modules/@angular/cdk/esm2015/bidi.js");
/* harmony import */ var _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/cdk/platform */ "./node_modules/@angular/cdk/esm2015/platform.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm2015/input.js");
/* harmony import */ var _angular_cdk_text_field__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/cdk/text-field */ "./node_modules/@angular/cdk/esm2015/text-field.js");
/* harmony import */ var _node_modules_angular_material_icon_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../../node_modules/@angular/material/icon/typings/index.ngfactory */ "./node_modules/@angular/material/icon/typings/index.ngfactory.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm2015/icon.js");
/* harmony import */ var _node_modules_angular_material_card_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../../node_modules/@angular/material/card/typings/index.ngfactory */ "./node_modules/@angular/material/card/typings/index.ngfactory.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/esm2015/card.js");
/* harmony import */ var _node_modules_ngx_spinner_ngx_spinner_ngfactory__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../../../node_modules/ngx-spinner/ngx-spinner.ngfactory */ "./node_modules/ngx-spinner/ngx-spinner.ngfactory.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm2015/ngx-spinner.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var ngx_uploader__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ngx-uploader */ "./node_modules/ngx-uploader/fesm2015/ngx-uploader.js");
/* harmony import */ var _node_modules_angular_material_button_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../../../../node_modules/@angular/material/button/typings/index.ngfactory */ "./node_modules/@angular/material/button/typings/index.ngfactory.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm2015/button.js");
/* harmony import */ var _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/cdk/a11y */ "./node_modules/@angular/cdk/esm2015/a11y.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _login_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./login.component */ "./src/app/auth/login/login.component.ts");
/* harmony import */ var _ethereumProvider_ethereum__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ../../ethereumProvider/ethereum */ "./src/app/ethereumProvider/ethereum.ts");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ../../service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _service_secure_storage_service__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ../../service/secure-storage.service */ "./src/app/service/secure-storage.service.ts");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 



























var styles_NgxLoginComponent = [_login_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__["styles"]];
var RenderType_NgxLoginComponent = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵcrt"]({ encapsulation: 0, styles: styles_NgxLoginComponent, data: {} });

function View_NgxLoginComponent_1(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 1, "div", [["class", "message-box error  mb-4"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](1, null, [" ", " "]))], null, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.error; _ck(_v, 1, 0, currVal_0); }); }
function View_NgxLoginComponent_2(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 2, "mat-error", [["class", "mat-error"], ["role", "alert"]], [[1, "id", 0]], null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](1, 16384, [[6, 4]], 0, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MatError"], [], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, [" Email is required "]))], null, function (_ck, _v) { var currVal_0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1).id; _ck(_v, 0, 0, currVal_0); }); }
function View_NgxLoginComponent_3(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 2, "mat-error", [["class", "mat-error"], ["role", "alert"]], [[1, "id", 0]], null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](1, 16384, [[6, 4]], 0, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MatError"], [], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, [" Please enter a valid email address "]))], null, function (_ck, _v) { var currVal_0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1).id; _ck(_v, 0, 0, currVal_0); }); }
function View_NgxLoginComponent_4(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 28, "mat-form-field", [["appearance", "outline"], ["class", "w-100 mat-form-field"]], [[2, "mat-form-field-appearance-standard", null], [2, "mat-form-field-appearance-fill", null], [2, "mat-form-field-appearance-outline", null], [2, "mat-form-field-appearance-legacy", null], [2, "mat-form-field-invalid", null], [2, "mat-form-field-can-float", null], [2, "mat-form-field-should-float", null], [2, "mat-form-field-has-label", null], [2, "mat-form-field-hide-placeholder", null], [2, "mat-form-field-disabled", null], [2, "mat-form-field-autofilled", null], [2, "mat-focused", null], [2, "mat-accent", null], [2, "mat-warn", null], [2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null], [2, "_mat-animation-noopable", null]], null, null, _node_modules_angular_material_form_field_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_3__["View_MatFormField_0"], _node_modules_angular_material_form_field_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_3__["RenderType_MatFormField"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](1, 7520256, null, 9, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MatFormField"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], [2, _angular_material_core__WEBPACK_IMPORTED_MODULE_4__["MAT_LABEL_GLOBAL_OPTIONS"]], [2, _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_5__["Directionality"]], [2, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MAT_FORM_FIELD_DEFAULT_OPTIONS"]], _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_6__["Platform"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"], [2, _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__["ANIMATION_MODULE_TYPE"]]], { appearance: [0, "appearance"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 19, { _controlNonStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](335544320, 20, { _controlStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 21, { _labelChildNonStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](335544320, 22, { _labelChildStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 23, { _placeholderChild: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 24, { _errorChildren: 1 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 25, { _hintChildren: 1 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 26, { _prefixChildren: 1 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 27, { _suffixChildren: 1 }), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](11, 0, null, 3, 2, "mat-label", [], null, null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](12, 16384, [[21, 4], [22, 4]], 0, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MatLabel"], [], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Private key "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](14, 0, null, 1, 7, "input", [["class", "mat-input-element mat-form-field-autofill-control"], ["formControlName", "privateKey"], ["matInput", ""], ["type", "password"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null], [2, "mat-input-server", null], [1, "id", 0], [1, "placeholder", 0], [8, "disabled", 0], [8, "required", 0], [1, "readonly", 0], [1, "aria-describedby", 0], [1, "aria-invalid", 0], [1, "aria-required", 0]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"], [null, "focus"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 15)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 15).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 15)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 15)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } if (("blur" === en)) {
        var pd_4 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 20)._focusChanged(false) !== false);
        ad = (pd_4 && ad);
    } if (("focus" === en)) {
        var pd_5 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 20)._focusChanged(true) !== false);
        ad = (pd_5 && ad);
    } if (("input" === en)) {
        var pd_6 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 20)._onInput() !== false);
        ad = (pd_6 && ad);
    } return ad; }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](15, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["DefaultValueAccessor"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["COMPOSITION_BUFFER_MODE"]]], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](1024, null, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NG_VALUE_ACCESSOR"], function (p0_0) { return [p0_0]; }, [_angular_forms__WEBPACK_IMPORTED_MODULE_8__["DefaultValueAccessor"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](17, 671744, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormControlName"], [[3, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ControlContainer"]], [8, null], [8, null], [6, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NG_VALUE_ACCESSOR"]], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵangular_packages_forms_forms_q"]]], { name: [0, "name"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControl"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormControlName"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](19, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControlStatus"], [[4, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControl"]]], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](20, 999424, null, 0, _angular_material_input__WEBPACK_IMPORTED_MODULE_9__["MatInput"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_6__["Platform"], [6, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControl"]], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgForm"]], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormGroupDirective"]], _angular_material_core__WEBPACK_IMPORTED_MODULE_4__["ErrorStateMatcher"], [8, null], _angular_cdk_text_field__WEBPACK_IMPORTED_MODULE_10__["AutofillMonitor"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]], { type: [0, "type"], value: [1, "value"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, [[19, 4], [20, 4]], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldControl"], null, [_angular_material_input__WEBPACK_IMPORTED_MODULE_9__["MatInput"]]), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](22, 0, null, 4, 3, "mat-icon", [["class", "secondary-text mat-icon notranslate"], ["matSuffix", ""], ["role", "img"]], [[2, "mat-icon-inline", null], [2, "mat-icon-no-color", null]], null, null, _node_modules_angular_material_icon_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_11__["View_MatIcon_0"], _node_modules_angular_material_icon_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_11__["RenderType_MatIcon"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](23, 16384, [[27, 4]], 0, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MatSuffix"], [], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](24, 9158656, null, 0, _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__["MatIcon"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__["MatIconRegistry"], [8, null], [2, _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__["MAT_ICON_LOCATION"]]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, 0, ["fingerprint"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](26, 0, null, 5, 2, "mat-error", [["class", "mat-error"], ["role", "alert"]], [[1, "id", 0]], null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](27, 16384, [[24, 4]], 0, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MatError"], [], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, [" Private key is required "]))], function (_ck, _v) { var _co = _v.component; var currVal_22 = "outline"; _ck(_v, 1, 0, currVal_22); var currVal_39 = "privateKey"; _ck(_v, 17, 0, currVal_39); var currVal_40 = "password"; var currVal_41 = _co.loginForm.controls["privateKey"].value; _ck(_v, 20, 0, currVal_40, currVal_41); _ck(_v, 24, 0); }, function (_ck, _v) { var currVal_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1).appearance == "standard"); var currVal_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1).appearance == "fill"); var currVal_2 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1).appearance == "outline"); var currVal_3 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1).appearance == "legacy"); var currVal_4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1)._control.errorState; var currVal_5 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1)._canLabelFloat; var currVal_6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1)._shouldLabelFloat(); var currVal_7 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1)._hasFloatingLabel(); var currVal_8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1)._hideControlPlaceholder(); var currVal_9 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1)._control.disabled; var currVal_10 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1)._control.autofilled; var currVal_11 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1)._control.focused; var currVal_12 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1).color == "accent"); var currVal_13 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1).color == "warn"); var currVal_14 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1)._shouldForward("untouched"); var currVal_15 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1)._shouldForward("touched"); var currVal_16 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1)._shouldForward("pristine"); var currVal_17 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1)._shouldForward("dirty"); var currVal_18 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1)._shouldForward("valid"); var currVal_19 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1)._shouldForward("invalid"); var currVal_20 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1)._shouldForward("pending"); var currVal_21 = !_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1)._animationsEnabled; _ck(_v, 0, 1, [currVal_0, currVal_1, currVal_2, currVal_3, currVal_4, currVal_5, currVal_6, currVal_7, currVal_8, currVal_9, currVal_10, currVal_11, currVal_12, currVal_13, currVal_14, currVal_15, currVal_16, currVal_17, currVal_18, currVal_19, currVal_20, currVal_21]); var currVal_23 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19).ngClassUntouched; var currVal_24 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19).ngClassTouched; var currVal_25 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19).ngClassPristine; var currVal_26 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19).ngClassDirty; var currVal_27 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19).ngClassValid; var currVal_28 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19).ngClassInvalid; var currVal_29 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19).ngClassPending; var currVal_30 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 20)._isServer; var currVal_31 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 20).id; var currVal_32 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 20).placeholder; var currVal_33 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 20).disabled; var currVal_34 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 20).required; var currVal_35 = ((_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 20).readonly && !_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 20)._isNativeSelect) || null); var currVal_36 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 20)._ariaDescribedby || null); var currVal_37 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 20).errorState; var currVal_38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 20).required.toString(); _ck(_v, 14, 1, [currVal_23, currVal_24, currVal_25, currVal_26, currVal_27, currVal_28, currVal_29, currVal_30, currVal_31, currVal_32, currVal_33, currVal_34, currVal_35, currVal_36, currVal_37, currVal_38]); var currVal_42 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 24).inline; var currVal_43 = (((_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 24).color !== "primary") && (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 24).color !== "accent")) && (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 24).color !== "warn")); _ck(_v, 22, 0, currVal_42, currVal_43); var currVal_44 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 27).id; _ck(_v, 26, 0, currVal_44); }); }
function View_NgxLoginComponent_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 99, "mat-card", [["class", "mat-card"]], null, null, null, _node_modules_angular_material_card_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_13__["View_MatCard_0"], _node_modules_angular_material_card_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_13__["RenderType_MatCard"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](1, 49152, null, 0, _angular_material_card__WEBPACK_IMPORTED_MODULE_14__["MatCard"], [], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](2, 0, null, 0, 2, "div", [["class", "text-center font-header mb-2 font-weight-bold "], ["style", "color:  #4d3fbd !important;"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](3, 0, null, null, 1, "h6", [["class", "font-header"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, [" BIENVENUE SUR DRUGSCHAINPHE "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](5, 0, null, 0, 3, "ngx-spinner", [["bdColor", "rgba(0, 0, 0, 0.8)"], ["color", "#fff"], ["size", "medium"], ["type", "ball-clip-rotate-pulse"]], null, null, null, _node_modules_ngx_spinner_ngx_spinner_ngfactory__WEBPACK_IMPORTED_MODULE_15__["View_NgxSpinnerComponent_0"], _node_modules_ngx_spinner_ngx_spinner_ngfactory__WEBPACK_IMPORTED_MODULE_15__["RenderType_NgxSpinnerComponent"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](6, 770048, null, 0, ngx_spinner__WEBPACK_IMPORTED_MODULE_16__["NgxSpinnerComponent"], [ngx_spinner__WEBPACK_IMPORTED_MODULE_16__["NgxSpinnerService"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]], { bdColor: [0, "bdColor"], size: [1, "size"], color: [2, "color"], type: [3, "type"], fullScreen: [4, "fullScreen"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](7, 0, null, 0, 1, "p", [], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, [" Connexion en cours... "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](9, 0, null, 0, 84, "form", [["name", "loginForm"], ["novalidate", ""]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "ngSubmit"], [null, "submit"], [null, "reset"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("submit" === en)) {
        var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 11).onSubmit($event) !== false);
        ad = (pd_0 && ad);
    } if (("reset" === en)) {
        var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 11).onReset() !== false);
        ad = (pd_1 && ad);
    } if (("ngSubmit" === en)) {
        var pd_2 = (_co.onLogin() !== false);
        ad = (pd_2 && ad);
    } return ad; }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](10, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵangular_packages_forms_forms_z"], [], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](11, 540672, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormGroupDirective"], [[8, null], [8, null]], { form: [0, "form"] }, { ngSubmit: "ngSubmit" }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ControlContainer"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormGroupDirective"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](13, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControlStatusGroup"], [[4, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ControlContainer"]]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_NgxLoginComponent_1)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](15, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_17__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](16, 0, null, null, 29, "mat-form-field", [["appearance", "outline"], ["class", "w-100 mat-form-field"]], [[2, "mat-form-field-appearance-standard", null], [2, "mat-form-field-appearance-fill", null], [2, "mat-form-field-appearance-outline", null], [2, "mat-form-field-appearance-legacy", null], [2, "mat-form-field-invalid", null], [2, "mat-form-field-can-float", null], [2, "mat-form-field-should-float", null], [2, "mat-form-field-has-label", null], [2, "mat-form-field-hide-placeholder", null], [2, "mat-form-field-disabled", null], [2, "mat-form-field-autofilled", null], [2, "mat-focused", null], [2, "mat-accent", null], [2, "mat-warn", null], [2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null], [2, "_mat-animation-noopable", null]], null, null, _node_modules_angular_material_form_field_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_3__["View_MatFormField_0"], _node_modules_angular_material_form_field_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_3__["RenderType_MatFormField"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](17, 7520256, null, 9, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MatFormField"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], [2, _angular_material_core__WEBPACK_IMPORTED_MODULE_4__["MAT_LABEL_GLOBAL_OPTIONS"]], [2, _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_5__["Directionality"]], [2, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MAT_FORM_FIELD_DEFAULT_OPTIONS"]], _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_6__["Platform"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"], [2, _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__["ANIMATION_MODULE_TYPE"]]], { appearance: [0, "appearance"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 1, { _controlNonStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](335544320, 2, { _controlStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 3, { _labelChildNonStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](335544320, 4, { _labelChildStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 5, { _placeholderChild: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 6, { _errorChildren: 1 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 7, { _hintChildren: 1 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 8, { _prefixChildren: 1 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 9, { _suffixChildren: 1 }), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](27, 0, null, 3, 2, "mat-label", [], null, null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](28, 16384, [[3, 4], [4, 4]], 0, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MatLabel"], [], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Email"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](30, 0, null, 1, 7, "input", [["class", "mat-input-element mat-form-field-autofill-control"], ["formControlName", "email"], ["matInput", ""]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null], [2, "mat-input-server", null], [1, "id", 0], [1, "placeholder", 0], [8, "disabled", 0], [8, "required", 0], [1, "readonly", 0], [1, "aria-describedby", 0], [1, "aria-invalid", 0], [1, "aria-required", 0]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"], [null, "focus"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 31)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 31).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 31)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 31)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } if (("blur" === en)) {
        var pd_4 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 36)._focusChanged(false) !== false);
        ad = (pd_4 && ad);
    } if (("focus" === en)) {
        var pd_5 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 36)._focusChanged(true) !== false);
        ad = (pd_5 && ad);
    } if (("input" === en)) {
        var pd_6 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 36)._onInput() !== false);
        ad = (pd_6 && ad);
    } return ad; }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](31, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["DefaultValueAccessor"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["COMPOSITION_BUFFER_MODE"]]], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](1024, null, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NG_VALUE_ACCESSOR"], function (p0_0) { return [p0_0]; }, [_angular_forms__WEBPACK_IMPORTED_MODULE_8__["DefaultValueAccessor"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](33, 671744, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormControlName"], [[3, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ControlContainer"]], [8, null], [8, null], [6, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NG_VALUE_ACCESSOR"]], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵangular_packages_forms_forms_q"]]], { name: [0, "name"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControl"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormControlName"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](35, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControlStatus"], [[4, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControl"]]], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](36, 999424, null, 0, _angular_material_input__WEBPACK_IMPORTED_MODULE_9__["MatInput"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_6__["Platform"], [6, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControl"]], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgForm"]], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormGroupDirective"]], _angular_material_core__WEBPACK_IMPORTED_MODULE_4__["ErrorStateMatcher"], [8, null], _angular_cdk_text_field__WEBPACK_IMPORTED_MODULE_10__["AutofillMonitor"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, [[1, 4], [2, 4]], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldControl"], null, [_angular_material_input__WEBPACK_IMPORTED_MODULE_9__["MatInput"]]), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](38, 0, null, 4, 3, "mat-icon", [["class", "secondary-text mat-icon notranslate"], ["matSuffix", ""], ["role", "img"]], [[2, "mat-icon-inline", null], [2, "mat-icon-no-color", null]], null, null, _node_modules_angular_material_icon_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_11__["View_MatIcon_0"], _node_modules_angular_material_icon_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_11__["RenderType_MatIcon"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](39, 16384, [[9, 4]], 0, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MatSuffix"], [], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](40, 9158656, null, 0, _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__["MatIcon"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__["MatIconRegistry"], [8, null], [2, _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__["MAT_ICON_LOCATION"]]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, 0, ["mail"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, 5, 1, null, View_NgxLoginComponent_2)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](43, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_17__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, 5, 1, null, View_NgxLoginComponent_3)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](45, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_17__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](46, 0, null, null, 28, "mat-form-field", [["appearance", "outline"], ["class", "w-100 mat-form-field"]], [[2, "mat-form-field-appearance-standard", null], [2, "mat-form-field-appearance-fill", null], [2, "mat-form-field-appearance-outline", null], [2, "mat-form-field-appearance-legacy", null], [2, "mat-form-field-invalid", null], [2, "mat-form-field-can-float", null], [2, "mat-form-field-should-float", null], [2, "mat-form-field-has-label", null], [2, "mat-form-field-hide-placeholder", null], [2, "mat-form-field-disabled", null], [2, "mat-form-field-autofilled", null], [2, "mat-focused", null], [2, "mat-accent", null], [2, "mat-warn", null], [2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null], [2, "_mat-animation-noopable", null]], null, null, _node_modules_angular_material_form_field_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_3__["View_MatFormField_0"], _node_modules_angular_material_form_field_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_3__["RenderType_MatFormField"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](47, 7520256, null, 9, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MatFormField"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], [2, _angular_material_core__WEBPACK_IMPORTED_MODULE_4__["MAT_LABEL_GLOBAL_OPTIONS"]], [2, _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_5__["Directionality"]], [2, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MAT_FORM_FIELD_DEFAULT_OPTIONS"]], _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_6__["Platform"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"], [2, _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__["ANIMATION_MODULE_TYPE"]]], { appearance: [0, "appearance"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 10, { _controlNonStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](335544320, 11, { _controlStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 12, { _labelChildNonStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](335544320, 13, { _labelChildStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 14, { _placeholderChild: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 15, { _errorChildren: 1 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 16, { _hintChildren: 1 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 17, { _prefixChildren: 1 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 18, { _suffixChildren: 1 }), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](57, 0, null, 3, 2, "mat-label", [], null, null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](58, 16384, [[12, 4], [13, 4]], 0, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MatLabel"], [], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Password "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](60, 0, null, 1, 7, "input", [["class", "mat-input-element mat-form-field-autofill-control"], ["formControlName", "password"], ["matInput", ""], ["type", "password"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null], [2, "mat-input-server", null], [1, "id", 0], [1, "placeholder", 0], [8, "disabled", 0], [8, "required", 0], [1, "readonly", 0], [1, "aria-describedby", 0], [1, "aria-invalid", 0], [1, "aria-required", 0]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"], [null, "focus"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 61)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 61).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 61)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 61)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } if (("blur" === en)) {
        var pd_4 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 66)._focusChanged(false) !== false);
        ad = (pd_4 && ad);
    } if (("focus" === en)) {
        var pd_5 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 66)._focusChanged(true) !== false);
        ad = (pd_5 && ad);
    } if (("input" === en)) {
        var pd_6 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 66)._onInput() !== false);
        ad = (pd_6 && ad);
    } return ad; }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](61, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["DefaultValueAccessor"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["COMPOSITION_BUFFER_MODE"]]], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](1024, null, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NG_VALUE_ACCESSOR"], function (p0_0) { return [p0_0]; }, [_angular_forms__WEBPACK_IMPORTED_MODULE_8__["DefaultValueAccessor"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](63, 671744, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormControlName"], [[3, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ControlContainer"]], [8, null], [8, null], [6, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NG_VALUE_ACCESSOR"]], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵangular_packages_forms_forms_q"]]], { name: [0, "name"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControl"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormControlName"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](65, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControlStatus"], [[4, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControl"]]], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](66, 999424, null, 0, _angular_material_input__WEBPACK_IMPORTED_MODULE_9__["MatInput"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_6__["Platform"], [6, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControl"]], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgForm"]], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormGroupDirective"]], _angular_material_core__WEBPACK_IMPORTED_MODULE_4__["ErrorStateMatcher"], [8, null], _angular_cdk_text_field__WEBPACK_IMPORTED_MODULE_10__["AutofillMonitor"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]], { type: [0, "type"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, [[10, 4], [11, 4]], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldControl"], null, [_angular_material_input__WEBPACK_IMPORTED_MODULE_9__["MatInput"]]), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](68, 0, null, 4, 3, "mat-icon", [["class", "secondary-text mat-icon notranslate"], ["matSuffix", ""], ["role", "img"]], [[2, "mat-icon-inline", null], [2, "mat-icon-no-color", null]], null, null, _node_modules_angular_material_icon_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_11__["View_MatIcon_0"], _node_modules_angular_material_icon_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_11__["RenderType_MatIcon"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](69, 16384, [[18, 4]], 0, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MatSuffix"], [], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](70, 9158656, null, 0, _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__["MatIcon"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__["MatIconRegistry"], [8, null], [2, _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__["MAT_ICON_LOCATION"]]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, 0, ["vpn_key"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](72, 0, null, 5, 2, "mat-error", [["class", "mat-error"], ["role", "alert"]], [[1, "id", 0]], null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](73, 16384, [[15, 4]], 0, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MatError"], [], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, [" Private key is required "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_NgxLoginComponent_4)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](76, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_17__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](77, 0, null, null, 12, "div", [["class", "text-center"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](78, 0, null, null, 11, "label", [["class", "upload-button"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](79, 0, null, null, 1, "input", [["accept", "js"], ["id", "image"], ["ngFileSelect", ""], ["style", "display:none;"], ["type", "file"]], null, [[null, "change"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("change" === en)) {
        var pd_0 = (_co.openFile($event) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](80, 212992, null, 0, ngx_uploader__WEBPACK_IMPORTED_MODULE_18__["NgFileSelectDirective"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](81, 0, null, null, 8, "a", [["mat-raised-button", ""], ["style", "background-color: ghostwhite ;box-shadow: lightgray"]], [[1, "tabindex", 0], [1, "disabled", 0], [1, "aria-disabled", 0], [2, "_mat-animation-noopable", null]], [[null, "click"]], function (_v, en, $event) { var ad = true; if (("click" === en)) {
        var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 82)._haltDisabledEvents($event) !== false);
        ad = (pd_0 && ad);
    } return ad; }, _node_modules_angular_material_button_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_19__["View_MatAnchor_0"], _node_modules_angular_material_button_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_19__["RenderType_MatAnchor"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](82, 180224, null, 0, _angular_material_button__WEBPACK_IMPORTED_MODULE_20__["MatAnchor"], [_angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_21__["FocusMonitor"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], [2, _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__["ANIMATION_MODULE_TYPE"]]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](83, 0, null, 0, 3, "a", [["href", "javascript:void(0)"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](84, 0, null, null, 2, "mat-icon", [["class", "d-inline mat-icon notranslate"], ["color", "primary"], ["role", "img"], ["style", "cursor: pointer;    font-size: 30px;"]], [[2, "mat-icon-inline", null], [2, "mat-icon-no-color", null]], null, null, _node_modules_angular_material_icon_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_11__["View_MatIcon_0"], _node_modules_angular_material_icon_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_11__["RenderType_MatIcon"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](85, 9158656, null, 0, _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__["MatIcon"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__["MatIconRegistry"], [8, null], [2, _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__["MAT_ICON_LOCATION"]]], { color: [0, "color"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, 0, ["publish"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](87, 0, null, 0, 2, "span", [["class", "aligned-with-icon"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](88, 0, null, null, 1, "small", [], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Importer Cle priv\u00E9e "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](90, 0, null, null, 0, "div", [["class", "remember-forgot-password"], ["fxLayout", "row"], ["fxLayout.xs", "column"], ["fxLayoutAlign", "space-between center"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](91, 0, null, null, 2, "button", [["aria-label", "LOGIN"], ["class", "submit-button w-100"], ["color", "primary"], ["mat-raised-button", ""]], [[1, "disabled", 0], [2, "_mat-animation-noopable", null]], null, null, _node_modules_angular_material_button_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_19__["View_MatButton_0"], _node_modules_angular_material_button_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_19__["RenderType_MatButton"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](92, 180224, null, 0, _angular_material_button__WEBPACK_IMPORTED_MODULE_20__["MatButton"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_21__["FocusMonitor"], [2, _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__["ANIMATION_MODULE_TYPE"]]], { disabled: [0, "disabled"], color: [1, "color"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, 0, [" Connexion "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](94, 0, null, 0, 5, "div", [["class", "links"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](95, 0, null, null, 4, "section", [["aria-label", "Register"], ["class", "another-action  font-header"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, [" Vous n'avez pas de compte? "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](97, 0, null, null, 2, "a", [["class", "link"]], [[1, "target", 0], [8, "href", 4]], [[null, "click"]], function (_v, en, $event) { var ad = true; if (("click" === en)) {
        var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 98).onClick($event.button, $event.ctrlKey, $event.metaKey, $event.shiftKey) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](98, 671744, null, 0, _angular_router__WEBPACK_IMPORTED_MODULE_22__["RouterLinkWithHref"], [_angular_router__WEBPACK_IMPORTED_MODULE_22__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_22__["ActivatedRoute"], _angular_common__WEBPACK_IMPORTED_MODULE_17__["LocationStrategy"]], { routerLink: [0, "routerLink"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Cr\u00E9ez un compte"]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = "rgba(0, 0, 0, 0.8)"; var currVal_1 = "medium"; var currVal_2 = "#fff"; var currVal_3 = "ball-clip-rotate-pulse"; var currVal_4 = true; _ck(_v, 6, 0, currVal_0, currVal_1, currVal_2, currVal_3, currVal_4); var currVal_12 = _co.loginForm; _ck(_v, 11, 0, currVal_12); var currVal_13 = _co.error; _ck(_v, 15, 0, currVal_13); var currVal_36 = "outline"; _ck(_v, 17, 0, currVal_36); var currVal_53 = "email"; _ck(_v, 33, 0, currVal_53); _ck(_v, 36, 0); _ck(_v, 40, 0); var currVal_56 = _co.loginForm.get("email").hasError("required"); _ck(_v, 43, 0, currVal_56); var currVal_57 = (!_co.loginForm.get("email").hasError("required") && _co.loginForm.get("email").hasError("email")); _ck(_v, 45, 0, currVal_57); var currVal_80 = "outline"; _ck(_v, 47, 0, currVal_80); var currVal_97 = "password"; _ck(_v, 63, 0, currVal_97); var currVal_98 = "password"; _ck(_v, 66, 0, currVal_98); _ck(_v, 70, 0); var currVal_102 = _co.jsonFile; _ck(_v, 76, 0, currVal_102); _ck(_v, 80, 0); var currVal_109 = "primary"; _ck(_v, 85, 0, currVal_109); var currVal_112 = _co.loginForm.invalid; var currVal_113 = "primary"; _ck(_v, 92, 0, currVal_112, currVal_113); var currVal_116 = "/auth/register"; _ck(_v, 98, 0, currVal_116); }, function (_ck, _v) { var currVal_5 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 13).ngClassUntouched; var currVal_6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 13).ngClassTouched; var currVal_7 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 13).ngClassPristine; var currVal_8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 13).ngClassDirty; var currVal_9 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 13).ngClassValid; var currVal_10 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 13).ngClassInvalid; var currVal_11 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 13).ngClassPending; _ck(_v, 9, 0, currVal_5, currVal_6, currVal_7, currVal_8, currVal_9, currVal_10, currVal_11); var currVal_14 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).appearance == "standard"); var currVal_15 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).appearance == "fill"); var currVal_16 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).appearance == "outline"); var currVal_17 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).appearance == "legacy"); var currVal_18 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17)._control.errorState; var currVal_19 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17)._canLabelFloat; var currVal_20 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17)._shouldLabelFloat(); var currVal_21 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17)._hasFloatingLabel(); var currVal_22 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17)._hideControlPlaceholder(); var currVal_23 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17)._control.disabled; var currVal_24 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17)._control.autofilled; var currVal_25 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17)._control.focused; var currVal_26 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).color == "accent"); var currVal_27 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).color == "warn"); var currVal_28 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17)._shouldForward("untouched"); var currVal_29 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17)._shouldForward("touched"); var currVal_30 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17)._shouldForward("pristine"); var currVal_31 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17)._shouldForward("dirty"); var currVal_32 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17)._shouldForward("valid"); var currVal_33 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17)._shouldForward("invalid"); var currVal_34 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17)._shouldForward("pending"); var currVal_35 = !_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17)._animationsEnabled; _ck(_v, 16, 1, [currVal_14, currVal_15, currVal_16, currVal_17, currVal_18, currVal_19, currVal_20, currVal_21, currVal_22, currVal_23, currVal_24, currVal_25, currVal_26, currVal_27, currVal_28, currVal_29, currVal_30, currVal_31, currVal_32, currVal_33, currVal_34, currVal_35]); var currVal_37 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 35).ngClassUntouched; var currVal_38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 35).ngClassTouched; var currVal_39 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 35).ngClassPristine; var currVal_40 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 35).ngClassDirty; var currVal_41 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 35).ngClassValid; var currVal_42 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 35).ngClassInvalid; var currVal_43 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 35).ngClassPending; var currVal_44 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 36)._isServer; var currVal_45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 36).id; var currVal_46 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 36).placeholder; var currVal_47 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 36).disabled; var currVal_48 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 36).required; var currVal_49 = ((_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 36).readonly && !_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 36)._isNativeSelect) || null); var currVal_50 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 36)._ariaDescribedby || null); var currVal_51 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 36).errorState; var currVal_52 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 36).required.toString(); _ck(_v, 30, 1, [currVal_37, currVal_38, currVal_39, currVal_40, currVal_41, currVal_42, currVal_43, currVal_44, currVal_45, currVal_46, currVal_47, currVal_48, currVal_49, currVal_50, currVal_51, currVal_52]); var currVal_54 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 40).inline; var currVal_55 = (((_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 40).color !== "primary") && (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 40).color !== "accent")) && (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 40).color !== "warn")); _ck(_v, 38, 0, currVal_54, currVal_55); var currVal_58 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 47).appearance == "standard"); var currVal_59 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 47).appearance == "fill"); var currVal_60 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 47).appearance == "outline"); var currVal_61 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 47).appearance == "legacy"); var currVal_62 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 47)._control.errorState; var currVal_63 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 47)._canLabelFloat; var currVal_64 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 47)._shouldLabelFloat(); var currVal_65 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 47)._hasFloatingLabel(); var currVal_66 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 47)._hideControlPlaceholder(); var currVal_67 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 47)._control.disabled; var currVal_68 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 47)._control.autofilled; var currVal_69 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 47)._control.focused; var currVal_70 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 47).color == "accent"); var currVal_71 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 47).color == "warn"); var currVal_72 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 47)._shouldForward("untouched"); var currVal_73 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 47)._shouldForward("touched"); var currVal_74 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 47)._shouldForward("pristine"); var currVal_75 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 47)._shouldForward("dirty"); var currVal_76 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 47)._shouldForward("valid"); var currVal_77 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 47)._shouldForward("invalid"); var currVal_78 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 47)._shouldForward("pending"); var currVal_79 = !_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 47)._animationsEnabled; _ck(_v, 46, 1, [currVal_58, currVal_59, currVal_60, currVal_61, currVal_62, currVal_63, currVal_64, currVal_65, currVal_66, currVal_67, currVal_68, currVal_69, currVal_70, currVal_71, currVal_72, currVal_73, currVal_74, currVal_75, currVal_76, currVal_77, currVal_78, currVal_79]); var currVal_81 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 65).ngClassUntouched; var currVal_82 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 65).ngClassTouched; var currVal_83 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 65).ngClassPristine; var currVal_84 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 65).ngClassDirty; var currVal_85 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 65).ngClassValid; var currVal_86 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 65).ngClassInvalid; var currVal_87 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 65).ngClassPending; var currVal_88 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 66)._isServer; var currVal_89 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 66).id; var currVal_90 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 66).placeholder; var currVal_91 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 66).disabled; var currVal_92 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 66).required; var currVal_93 = ((_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 66).readonly && !_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 66)._isNativeSelect) || null); var currVal_94 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 66)._ariaDescribedby || null); var currVal_95 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 66).errorState; var currVal_96 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 66).required.toString(); _ck(_v, 60, 1, [currVal_81, currVal_82, currVal_83, currVal_84, currVal_85, currVal_86, currVal_87, currVal_88, currVal_89, currVal_90, currVal_91, currVal_92, currVal_93, currVal_94, currVal_95, currVal_96]); var currVal_99 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 70).inline; var currVal_100 = (((_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 70).color !== "primary") && (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 70).color !== "accent")) && (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 70).color !== "warn")); _ck(_v, 68, 0, currVal_99, currVal_100); var currVal_101 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 73).id; _ck(_v, 72, 0, currVal_101); var currVal_103 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 82).disabled ? (0 - 1) : (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 82).tabIndex || 0)); var currVal_104 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 82).disabled || null); var currVal_105 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 82).disabled.toString(); var currVal_106 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 82)._animationMode === "NoopAnimations"); _ck(_v, 81, 0, currVal_103, currVal_104, currVal_105, currVal_106); var currVal_107 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 85).inline; var currVal_108 = (((_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 85).color !== "primary") && (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 85).color !== "accent")) && (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 85).color !== "warn")); _ck(_v, 84, 0, currVal_107, currVal_108); var currVal_110 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 92).disabled || null); var currVal_111 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 92)._animationMode === "NoopAnimations"); _ck(_v, 91, 0, currVal_110, currVal_111); var currVal_114 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 98).target; var currVal_115 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 98).href; _ck(_v, 97, 0, currVal_114, currVal_115); }); }
function View_NgxLoginComponent_Host_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 1, "ngx-login", [], null, null, null, View_NgxLoginComponent_0, RenderType_NgxLoginComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](1, 114688, null, 0, _login_component__WEBPACK_IMPORTED_MODULE_23__["NgxLoginComponent"], [_angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_22__["Router"], _ethereumProvider_ethereum__WEBPACK_IMPORTED_MODULE_24__["EthereumProvider"], _service_auth_service__WEBPACK_IMPORTED_MODULE_25__["AuthService"], ngx_spinner__WEBPACK_IMPORTED_MODULE_16__["NgxSpinnerService"], _service_secure_storage_service__WEBPACK_IMPORTED_MODULE_26__["StorageService"]], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
var NgxLoginComponentNgFactory = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵccf"]("ngx-login", _login_component__WEBPACK_IMPORTED_MODULE_23__["NgxLoginComponent"], View_NgxLoginComponent_Host_0, {}, {}, []);



/***/ }),

/***/ "./src/app/auth/login/login.component.scss.shim.ngstyle.js":
/*!*****************************************************************!*\
  !*** ./src/app/auth/login/login.component.scss.shim.ngstyle.js ***!
  \*****************************************************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 
var styles = ["mat-card {\n  background: #ffffff91 !important;\n}\n\n  .nb-theme-default .btn.btn-hero-success {\n  background-image: linear-gradient(to right, #3F51B5, #3F51B5);\n  border: none;\n  box-shadow: 0 0 0 0 #5452b8, 0 0 0 0 #506899, 0 0 transparent;\n  height: 100%;\n}\n\n  nb-card {\n  background-image: url('coverPhe.png');\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 100% !important;\n}\n\n  nb-layout-column {\n  padding: 0px !important;\n}\n\n.font-header[_ngcontent-%COMP%] {\n  font-family: \"Anton\", sans-serif;\n  color: #4d3fbd;\n}\n\n  nb-card {\n  height: 100% !important;\n}\n\n  nb-card-header {\n  display: none !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXV0aC9sb2dpbi9DOlxcVXNlcnNcXFlPVVNTRUZcXERlc2t0b3BcXFBGRVxccGZlLWJsb2NrY2hhaW5cXFBoYXJtYWNpZU9mZmljaW5lXFx1aS13ZWIvc3JjXFxhcHBcXGF1dGhcXGxvZ2luXFxsb2dpbi5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvYXV0aC9sb2dpbi9sb2dpbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVFLGdDQUFBO0FDQUY7O0FER0E7RUFDRSw2REFBQTtFQUNBLFlBQUE7RUFDQSw2REFBQTtFQUNBLFlBQUE7QUNBRjs7QURFQTtFQUNFLHFDQUFBO0VBQ0YsNEJBQUE7RUFDQSxzQkFBQTtFQUNFLHVCQUFBO0FDQ0Y7O0FEQ0E7RUFDQSx1QkFBQTtBQ0VBOztBREVBO0VBQ0UsZ0NBQUE7RUFDQSxjQUFBO0FDQ0Y7O0FERUE7RUFDRSx1QkFBQTtBQ0NGOztBRENBO0VBQ0Usd0JBQUE7QUNFRiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvbG9naW4vbG9naW4uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6Om5nLWRlZXAgbWF0LWNhcmR7XHJcblxyXG4gIGJhY2tncm91bmQ6ICNmZmZmZmY5MSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm5iLXRoZW1lLWRlZmF1bHQgLmJ0bi5idG4taGVyby1zdWNjZXNzIHtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMzRjUxQjUsICMzRjUxQjUpO1xyXG4gIGJvcmRlcjogbm9uZTtcclxuICBib3gtc2hhZG93OiAwIDAgMCAwICM1NDUyYjgsIDAgMCAwIDAgIzUwNjg5OSwgMCAwIHRyYW5zcGFyZW50O1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG46Om5nLWRlZXAgbmItY2FyZHtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9jb3ZlclBoZS5wbmcnKTtcclxuYmFja2dyb3VuZC1yZXBlYXQ6bm8tcmVwZWF0O1xyXG5iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gIGhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xyXG59XHJcbjo6bmctZGVlcCBuYi1sYXlvdXQtY29sdW1ue1xyXG5wYWRkaW5nOiAgMHB4ICFpbXBvcnRhbnQ7XHJcblxyXG59XHJcblxyXG4uZm9udC1oZWFkZXJ7XHJcbiAgZm9udC1mYW1pbHk6ICdBbnRvbicsIHNhbnMtc2VyaWY7XHJcbiAgY29sb3I6ICM0ZDNmYmQ7XHJcbn1cclxuXHJcbjo6bmctZGVlcCBuYi1jYXJke1xyXG4gIGhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xyXG59XHJcbjo6bmctZGVlcCBuYi1jYXJkLWhlYWRlcntcclxuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbn1cclxuIiwiOjpuZy1kZWVwIG1hdC1jYXJkIHtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjkxICFpbXBvcnRhbnQ7XG59XG5cbjo6bmctZGVlcCAubmItdGhlbWUtZGVmYXVsdCAuYnRuLmJ0bi1oZXJvLXN1Y2Nlc3Mge1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMzRjUxQjUsICMzRjUxQjUpO1xuICBib3JkZXI6IG5vbmU7XG4gIGJveC1zaGFkb3c6IDAgMCAwIDAgIzU0NTJiOCwgMCAwIDAgMCAjNTA2ODk5LCAwIDAgdHJhbnNwYXJlbnQ7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuOjpuZy1kZWVwIG5iLWNhcmQge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi8uLi9hc3NldHMvY292ZXJQaGUucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbn1cblxuOjpuZy1kZWVwIG5iLWxheW91dC1jb2x1bW4ge1xuICBwYWRkaW5nOiAwcHggIWltcG9ydGFudDtcbn1cblxuLmZvbnQtaGVhZGVyIHtcbiAgZm9udC1mYW1pbHk6IFwiQW50b25cIiwgc2Fucy1zZXJpZjtcbiAgY29sb3I6ICM0ZDNmYmQ7XG59XG5cbjo6bmctZGVlcCBuYi1jYXJkIHtcbiAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG59XG5cbjo6bmctZGVlcCBuYi1jYXJkLWhlYWRlciB7XG4gIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcbn0iXX0= */"];



/***/ }),

/***/ "./src/app/auth/login/login.component.ts":
/*!***********************************************!*\
  !*** ./src/app/auth/login/login.component.ts ***!
  \***********************************************/
/*! exports provided: NgxLoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxLoginComponent", function() { return NgxLoginComponent; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ethereumProvider_ethereum__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../ethereumProvider/ethereum */ "./src/app/ethereumProvider/ethereum.ts");
/* harmony import */ var ngx_uploader__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-uploader */ "./node_modules/ngx-uploader/fesm2015/ngx-uploader.js");
/* harmony import */ var app_service_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _service_secure_storage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../service/secure-storage.service */ "./src/app/service/secure-storage.service.ts");
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../environments/environment.prod */ "./src/environments/environment.prod.ts");
/* harmony import */ var random_hash__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! random-hash */ "./node_modules/random-hash/dist/index.js");
/* harmony import */ var random_hash__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(random_hash__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm2015/ngx-spinner.js");








const Cryptr = __webpack_require__(/*! cryptr */ "../../../../../node_modules/cryptr/index.js");
const cryptr = new Cryptr(_environments_environment_prod__WEBPACK_IMPORTED_MODULE_7__["environment"].SECRET_KEY);


function readBase64(file) {
    var reader = new FileReader();
    var future = new Promise((resolve, reject) => {
        reader.addEventListener("load", function () {
            resolve(reader.result);
        }, false);
        reader.addEventListener("error", function (event) {
            reject(event);
        }, false);
        reader.readAsDataURL(file);
    });
}
var crypto = __webpack_require__(/*! crypto */ "./node_modules/crypto-browserify/index.js");
class NgxLoginComponent {
    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(_formBuilder, _router, _web3, _auth, spinner, secureStorageService) {
        this._formBuilder = _formBuilder;
        this._router = _router;
        this._web3 = _web3;
        this._auth = _auth;
        this.spinner = spinner;
        this.secureStorageService = secureStorageService;
        this.files = []; // local uploading files array
        this.uploadInput = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"](); // input events, we use this to emit data to ngx-uploader
        this.humanizeBytes = ngx_uploader__WEBPACK_IMPORTED_MODULE_4__["humanizeBytes"];
        // Configure the layout
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    ngOnInit() {
        this.loginForm = this._formBuilder.group({
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            privateKey: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
        });
    }
    openFile(event) {
        let input = event.target;
        for (var index = 0; index < input.files.length; index++) {
            let reader = new FileReader();
            reader.onload = () => {
                // this 'text' is the content of the file
                var text = reader.result;
                this.jsonFile = JSON.parse(text.toString());
                this.loginForm.controls["privateKey"].patchValue(this.jsonFile.privateKey);
            };
            reader.readAsText(input.files[index]);
        }
        ;
    }
    onLogin() {
        this.spinner.show().then(console.log);
        let pubKey = this._web3.getPublicKeyFromPrivateKey(this.jsonFile.privateKey);
        this._auth.getUser(this.loginForm.value).subscribe(res => {
            let hash = random_hash__WEBPACK_IMPORTED_MODULE_8___default()({ length: 100 });
            var mykey = crypto.createCipher('aes-128-cbc', hash);
            var mystr = mykey.update("prvKey", 'utf8', 'hex');
            mystr += mykey.final('hex');
            let privateKey = this.jsonFile.privateKey.substr(2, this.jsonFile.privateKey.length);
            var key = crypto.createCipher('aes-128-cbc', hash);
            var str = key.update(privateKey, 'utf8', 'hex');
            str += key.final('hex');
            localStorage.setItem("hash", hash);
            localStorage.setItem(mystr, str);
            this.spinner.hide().then(console.log);
            if (res.accountAddress === pubKey) {
                this.isvalid = true;
                this._auth.signin(this.loginForm.value).subscribe(response => {
                    this.spinner.hide().then(console.log);
                    localStorage.setItem('currentUser', JSON.stringify(response));
                    this._auth.currentUserSubject.next(response);
                    window.location.replace("/dashboard");
                }, error1 => {
                    this.spinner.hide().then(console.log);
                    this.error = [error1];
                });
            }
            else {
                this.loginForm.controls["privateKey"].reset();
                this.spinner.hide().then(console.log);
                this.isvalid = false;
            }
        });
        if (this.loginForm.valid) {
            let signedData = this._web3.verify(this.loginForm.controls["privateKey"].value);
            console.log(signedData);
            console.log(this.loginForm.value);
        }
    }
    onUploadOutput(output) {
        if (output.type === 'allAddedToQueue') { // when all files added in queue
        }
        else if (output.type === 'addedToQueue' && typeof output.file !== 'undefined') { // add file to array when added
            this.files.push(output.file);
        }
        else if (output.type === 'uploading' && typeof output.file !== 'undefined') {
            // update current data in files array for uploading file
            const index = this.files.findIndex(file => typeof output.file !== 'undefined' && file.id === output.file.id);
            this.files[index] = output.file;
        }
        else if (output.type === 'removed') {
            // remove file from array when removed
            this.files = this.files.filter((file) => file !== output.file);
        }
        else if (output.type === 'dragOver') {
            this.dragOver = true;
        }
        else if (output.type === 'dragOut') {
            this.dragOver = false;
        }
        else if (output.type === 'drop') {
            this.dragOver = false;
        }
        console.log(this.files);
        let reader = new FileReader();
        reader.onload = () => {
            // this 'text' is the content of the file
            var text = reader.result;
        };
        reader.readAsText(output.file[0]);
    }
    startUpload() {
        const event = {
            type: 'uploadAll',
            url: 'http://ngx-uploader.com/upload',
            method: 'POST',
            data: { foo: 'bar' }
        };
        this.uploadInput.emit(event);
    }
    cancelUpload(id) {
        this.uploadInput.emit({ type: 'cancel', id: id });
    }
    removeFile(id) {
        this.uploadInput.emit({ type: 'remove', id: id });
    }
    removeAllFiles() {
        this.uploadInput.emit({ type: 'removeAll' });
    }
}


/***/ }),

/***/ "./src/app/auth/register/login.component.scss.shim.ngstyle.js":
/*!********************************************************************!*\
  !*** ./src/app/auth/register/login.component.scss.shim.ngstyle.js ***!
  \********************************************************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 
var styles = ["@import url(\"https://fonts.googleapis.com/css2?family=Anton&display=swap\");\n.font-header[_ngcontent-%COMP%] {\n  font-family: \"Anton\", sans-serif;\n  color: #4d3fbd;\n}\n  .nb-theme-default .btn.btn-hero-success {\n  background-image: linear-gradient(to right, #3F51B5, #3F51B5);\n  border: none;\n  box-shadow: 0 0 0 0 #5452b8, 0 0 0 0 #506899, 0 0 transparent;\n  height: 100%;\n}\n  nb-card {\n  background-image: url('coverPhe.png');\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 100% !important;\n}\n  nb-layout-column {\n  padding: 0px !important;\n}\n  nb-card {\n  height: 100% !important;\n}\n  nb-card-header {\n  display: none !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXV0aC9yZWdpc3Rlci9DOlxcVXNlcnNcXFlPVVNTRUZcXERlc2t0b3BcXFBGRVxccGZlLWJsb2NrY2hhaW5cXFBoYXJtYWNpZU9mZmljaW5lXFx1aS13ZWIvc3JjXFxhcHBcXGF1dGhcXHJlZ2lzdGVyXFxsb2dpbi5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvYXV0aC9yZWdpc3Rlci9sb2dpbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDUSwwRUFBQTtBQUNSO0VBQ0UsZ0NBQUE7RUFDQSxjQUFBO0FDQUY7QURFQTtFQUNFLDZEQUFBO0VBQ0EsWUFBQTtFQUNBLDZEQUFBO0VBQ0EsWUFBQTtBQ0NGO0FERUE7RUFDRSxxQ0FBQTtFQUNGLDRCQUFBO0VBQ0Esc0JBQUE7RUFDRSx1QkFBQTtBQ0NGO0FERUE7RUFDQSx1QkFBQTtBQ0NBO0FERUE7RUFDRSx1QkFBQTtBQ0NGO0FERUE7RUFDRSx3QkFBQTtBQ0NGIiwiZmlsZSI6InNyYy9hcHAvYXV0aC9yZWdpc3Rlci9sb2dpbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5AaW1wb3J0IHVybCgnaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3MyP2ZhbWlseT1BbnRvbiZkaXNwbGF5PXN3YXAnKTtcclxuLmZvbnQtaGVhZGVye1xyXG4gIGZvbnQtZmFtaWx5OiAnQW50b24nLCBzYW5zLXNlcmlmO1xyXG4gIGNvbG9yOiAjNGQzZmJkO1xyXG59XHJcbjo6bmctZGVlcCAubmItdGhlbWUtZGVmYXVsdCAuYnRuLmJ0bi1oZXJvLXN1Y2Nlc3Mge1xyXG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzNGNTFCNSwgIzNGNTFCNSk7XHJcbiAgYm9yZGVyOiBub25lO1xyXG4gIGJveC1zaGFkb3c6IDAgMCAwIDAgIzU0NTJiOCwgMCAwIDAgMCAjNTA2ODk5LCAwIDAgdHJhbnNwYXJlbnQ7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG46Om5nLWRlZXAgbmItY2FyZHtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9jb3ZlclBoZS5wbmcnKTtcclxuYmFja2dyb3VuZC1yZXBlYXQ6bm8tcmVwZWF0O1xyXG5iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gIGhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG46Om5nLWRlZXAgbmItbGF5b3V0LWNvbHVtbntcclxucGFkZGluZzogIDBweCAhaW1wb3J0YW50O1xyXG5cclxufVxyXG46Om5nLWRlZXAgbmItY2FyZHtcclxuICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcclxufVxyXG5cclxuOjpuZy1kZWVwIG5iLWNhcmQtaGVhZGVye1xyXG4gIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG4iLCJAaW1wb3J0IHVybChcImh0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzMj9mYW1pbHk9QW50b24mZGlzcGxheT1zd2FwXCIpO1xuLmZvbnQtaGVhZGVyIHtcbiAgZm9udC1mYW1pbHk6IFwiQW50b25cIiwgc2Fucy1zZXJpZjtcbiAgY29sb3I6ICM0ZDNmYmQ7XG59XG5cbjo6bmctZGVlcCAubmItdGhlbWUtZGVmYXVsdCAuYnRuLmJ0bi1oZXJvLXN1Y2Nlc3Mge1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMzRjUxQjUsICMzRjUxQjUpO1xuICBib3JkZXI6IG5vbmU7XG4gIGJveC1zaGFkb3c6IDAgMCAwIDAgIzU0NTJiOCwgMCAwIDAgMCAjNTA2ODk5LCAwIDAgdHJhbnNwYXJlbnQ7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuOjpuZy1kZWVwIG5iLWNhcmQge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi8uLi9hc3NldHMvY292ZXJQaGUucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbn1cblxuOjpuZy1kZWVwIG5iLWxheW91dC1jb2x1bW4ge1xuICBwYWRkaW5nOiAwcHggIWltcG9ydGFudDtcbn1cblxuOjpuZy1kZWVwIG5iLWNhcmQge1xuICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbn1cblxuOjpuZy1kZWVwIG5iLWNhcmQtaGVhZGVyIHtcbiAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xufSJdfQ== */"];



/***/ }),

/***/ "./src/app/auth/register/ngx-register.component.ngfactory.js":
/*!*******************************************************************!*\
  !*** ./src/app/auth/register/ngx-register.component.ngfactory.js ***!
  \*******************************************************************/
/*! exports provided: RenderType_NgxRegisterComponent, View_NgxRegisterComponent_0, View_NgxRegisterComponent_Host_0, NgxRegisterComponentNgFactory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenderType_NgxRegisterComponent", function() { return RenderType_NgxRegisterComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_NgxRegisterComponent_0", function() { return View_NgxRegisterComponent_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_NgxRegisterComponent_Host_0", function() { return View_NgxRegisterComponent_Host_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxRegisterComponentNgFactory", function() { return NgxRegisterComponentNgFactory; });
/* harmony import */ var _login_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login.component.scss.shim.ngstyle */ "./src/app/auth/register/login.component.scss.shim.ngstyle.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm2015/form-field.js");
/* harmony import */ var _node_modules_ngx_spinner_ngx_spinner_ngfactory__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../node_modules/ngx-spinner/ngx-spinner.ngfactory */ "./node_modules/ngx-spinner/ngx-spinner.ngfactory.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm2015/ngx-spinner.js");
/* harmony import */ var _node_modules_angular_material_card_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../node_modules/@angular/material/card/typings/index.ngfactory */ "./node_modules/@angular/material/card/typings/index.ngfactory.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/esm2015/card.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _node_modules_angular_material_form_field_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../node_modules/@angular/material/form-field/typings/index.ngfactory */ "./node_modules/@angular/material/form-field/typings/index.ngfactory.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/esm2015/core.js");
/* harmony import */ var _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/cdk/bidi */ "./node_modules/@angular/cdk/esm2015/bidi.js");
/* harmony import */ var _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/cdk/platform */ "./node_modules/@angular/cdk/esm2015/platform.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm2015/input.js");
/* harmony import */ var _angular_cdk_text_field__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/cdk/text-field */ "./node_modules/@angular/cdk/esm2015/text-field.js");
/* harmony import */ var _node_modules_angular_material_icon_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../../../node_modules/@angular/material/icon/typings/index.ngfactory */ "./node_modules/@angular/material/icon/typings/index.ngfactory.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm2015/icon.js");
/* harmony import */ var _node_modules_angular_material_button_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../../../../node_modules/@angular/material/button/typings/index.ngfactory */ "./node_modules/@angular/material/button/typings/index.ngfactory.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm2015/button.js");
/* harmony import */ var _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/cdk/a11y */ "./node_modules/@angular/cdk/esm2015/a11y.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ngx_register_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./ngx-register.component */ "./src/app/auth/register/ngx-register.component.ts");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../../service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _ethereumProvider_ethereum__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ../../ethereumProvider/ethereum */ "./src/app/ethereumProvider/ethereum.ts");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 

























var styles_NgxRegisterComponent = [_login_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__["styles"]];
var RenderType_NgxRegisterComponent = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵcrt"]({ encapsulation: 0, styles: styles_NgxRegisterComponent, data: {} });

function View_NgxRegisterComponent_2(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 1, "div", [], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](1, null, ["", ""]))], null, function (_ck, _v) { var currVal_0 = _v.context.$implicit; _ck(_v, 1, 0, currVal_0); }); }
function View_NgxRegisterComponent_1(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 5, "div", [["class", "alert alert-danger"], ["role", "alert"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](1, 0, null, null, 2, "div", [], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](2, 0, null, null, 1, "strong", [], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Oh snap!"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_NgxRegisterComponent_2)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](5, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgForOf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["IterableDiffers"]], { ngForOf: [0, "ngForOf"] }, null)], function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.errors; _ck(_v, 5, 0, currVal_0); }, null); }
function View_NgxRegisterComponent_3(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 2, "mat-error", [["class", "mat-error"], ["role", "alert"]], [[1, "id", 0]], null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](1, 16384, [[15, 4]], 0, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MatError"], [], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, [" Email is required "]))], null, function (_ck, _v) { var currVal_0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1).id; _ck(_v, 0, 0, currVal_0); }); }
function View_NgxRegisterComponent_4(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 2, "mat-error", [["class", "mat-error"], ["role", "alert"]], [[1, "id", 0]], null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](1, 16384, [[15, 4]], 0, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MatError"], [], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, [" Please enter a valid email address "]))], null, function (_ck, _v) { var currVal_0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1).id; _ck(_v, 0, 0, currVal_0); }); }
function View_NgxRegisterComponent_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 3, "ngx-spinner", [["bdColor", "rgba(0, 0, 0, 0.8)"], ["color", "#fff"], ["size", "large"], ["type", "ball-clip-rotate-multiple"]], null, null, null, _node_modules_ngx_spinner_ngx_spinner_ngfactory__WEBPACK_IMPORTED_MODULE_4__["View_NgxSpinnerComponent_0"], _node_modules_ngx_spinner_ngx_spinner_ngfactory__WEBPACK_IMPORTED_MODULE_4__["RenderType_NgxSpinnerComponent"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](1, 770048, null, 0, ngx_spinner__WEBPACK_IMPORTED_MODULE_5__["NgxSpinnerComponent"], [ngx_spinner__WEBPACK_IMPORTED_MODULE_5__["NgxSpinnerService"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]], { bdColor: [0, "bdColor"], size: [1, "size"], color: [2, "color"], type: [3, "type"], fullScreen: [4, "fullScreen"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](2, 0, null, 0, 1, "p", [], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, [" Inscription encours... "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](4, 0, null, null, 139, "mat-card", [["class", "mat-card"]], null, null, null, _node_modules_angular_material_card_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_6__["View_MatCard_0"], _node_modules_angular_material_card_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_6__["RenderType_MatCard"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](5, 49152, null, 0, _angular_material_card__WEBPACK_IMPORTED_MODULE_7__["MatCard"], [], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](6, 0, null, 0, 2, "div", [["class", "text-center font-header mb-2 font-weight-bold "], ["style", "color:  #4d3fbd !important;"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](7, 0, null, null, 1, "h6", [["class", "font-header"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, [" BIENVENUE SUR DRUGSCHAINPHE "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](9, 0, null, 0, 128, "form", [["name", "registerForm"], ["novalidate", ""]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "ngSubmit"], [null, "submit"], [null, "reset"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("submit" === en)) {
        var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 11).onSubmit($event) !== false);
        ad = (pd_0 && ad);
    } if (("reset" === en)) {
        var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 11).onReset() !== false);
        ad = (pd_1 && ad);
    } if (("ngSubmit" === en)) {
        var pd_2 = (_co.createAccount() !== false);
        ad = (pd_2 && ad);
    } return ad; }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](10, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵangular_packages_forms_forms_z"], [], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](11, 540672, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormGroupDirective"], [[8, null], [8, null]], { form: [0, "form"] }, { ngSubmit: "ngSubmit" }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ControlContainer"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormGroupDirective"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](13, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControlStatusGroup"], [[4, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ControlContainer"]]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_NgxRegisterComponent_1)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](15, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](16, 0, null, null, 121, "div", [["class", "col-12"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](17, 0, null, null, 120, "div", [["class", "row"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](18, 0, null, null, 28, "mat-form-field", [["appearance", "outline"], ["class", "w-100 mat-form-field"]], [[2, "mat-form-field-appearance-standard", null], [2, "mat-form-field-appearance-fill", null], [2, "mat-form-field-appearance-outline", null], [2, "mat-form-field-appearance-legacy", null], [2, "mat-form-field-invalid", null], [2, "mat-form-field-can-float", null], [2, "mat-form-field-should-float", null], [2, "mat-form-field-has-label", null], [2, "mat-form-field-hide-placeholder", null], [2, "mat-form-field-disabled", null], [2, "mat-form-field-autofilled", null], [2, "mat-focused", null], [2, "mat-accent", null], [2, "mat-warn", null], [2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null], [2, "_mat-animation-noopable", null]], null, null, _node_modules_angular_material_form_field_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_9__["View_MatFormField_0"], _node_modules_angular_material_form_field_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_9__["RenderType_MatFormField"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](19, 7520256, null, 9, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MatFormField"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], [2, _angular_material_core__WEBPACK_IMPORTED_MODULE_10__["MAT_LABEL_GLOBAL_OPTIONS"]], [2, _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_11__["Directionality"]], [2, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MAT_FORM_FIELD_DEFAULT_OPTIONS"]], _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_12__["Platform"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"], [2, _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_13__["ANIMATION_MODULE_TYPE"]]], { appearance: [0, "appearance"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 1, { _controlNonStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](335544320, 2, { _controlStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 3, { _labelChildNonStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](335544320, 4, { _labelChildStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 5, { _placeholderChild: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 6, { _errorChildren: 1 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 7, { _hintChildren: 1 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 8, { _prefixChildren: 1 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 9, { _suffixChildren: 1 }), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](29, 0, null, 3, 2, "mat-label", [], null, null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](30, 16384, [[3, 4], [4, 4]], 0, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MatLabel"], [], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["User name"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](32, 0, null, 1, 7, "input", [["class", "mat-input-element mat-form-field-autofill-control"], ["formControlName", "username"], ["matInput", ""]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null], [2, "mat-input-server", null], [1, "id", 0], [1, "placeholder", 0], [8, "disabled", 0], [8, "required", 0], [1, "readonly", 0], [1, "aria-describedby", 0], [1, "aria-invalid", 0], [1, "aria-required", 0]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"], [null, "focus"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 33)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 33).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 33)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 33)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } if (("blur" === en)) {
        var pd_4 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 38)._focusChanged(false) !== false);
        ad = (pd_4 && ad);
    } if (("focus" === en)) {
        var pd_5 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 38)._focusChanged(true) !== false);
        ad = (pd_5 && ad);
    } if (("input" === en)) {
        var pd_6 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 38)._onInput() !== false);
        ad = (pd_6 && ad);
    } return ad; }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](33, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["DefaultValueAccessor"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["COMPOSITION_BUFFER_MODE"]]], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](1024, null, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NG_VALUE_ACCESSOR"], function (p0_0) { return [p0_0]; }, [_angular_forms__WEBPACK_IMPORTED_MODULE_8__["DefaultValueAccessor"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](35, 671744, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormControlName"], [[3, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ControlContainer"]], [8, null], [8, null], [6, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NG_VALUE_ACCESSOR"]], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵangular_packages_forms_forms_q"]]], { name: [0, "name"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControl"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormControlName"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](37, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControlStatus"], [[4, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControl"]]], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](38, 999424, null, 0, _angular_material_input__WEBPACK_IMPORTED_MODULE_14__["MatInput"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_12__["Platform"], [6, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControl"]], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgForm"]], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormGroupDirective"]], _angular_material_core__WEBPACK_IMPORTED_MODULE_10__["ErrorStateMatcher"], [8, null], _angular_cdk_text_field__WEBPACK_IMPORTED_MODULE_15__["AutofillMonitor"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, [[1, 4], [2, 4]], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MatFormFieldControl"], null, [_angular_material_input__WEBPACK_IMPORTED_MODULE_14__["MatInput"]]), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](40, 0, null, 4, 3, "mat-icon", [["class", "secondary-text mat-icon notranslate"], ["color", "primary"], ["matSuffix", ""], ["role", "img"]], [[2, "mat-icon-inline", null], [2, "mat-icon-no-color", null]], null, null, _node_modules_angular_material_icon_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_16__["View_MatIcon_0"], _node_modules_angular_material_icon_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_16__["RenderType_MatIcon"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](41, 16384, [[9, 4]], 0, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MatSuffix"], [], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](42, 9158656, null, 0, _angular_material_icon__WEBPACK_IMPORTED_MODULE_17__["MatIcon"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_17__["MatIconRegistry"], [8, null], [2, _angular_material_icon__WEBPACK_IMPORTED_MODULE_17__["MAT_ICON_LOCATION"]]], { color: [0, "color"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, 0, ["account_circle"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](44, 0, null, 5, 2, "mat-error", [["class", "mat-error"], ["role", "alert"]], [[1, "id", 0]], null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](45, 16384, [[6, 4]], 0, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MatError"], [], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, [" username is required "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](47, 0, null, null, 29, "mat-form-field", [["appearance", "outline"], ["class", "w-100 mat-form-field"]], [[2, "mat-form-field-appearance-standard", null], [2, "mat-form-field-appearance-fill", null], [2, "mat-form-field-appearance-outline", null], [2, "mat-form-field-appearance-legacy", null], [2, "mat-form-field-invalid", null], [2, "mat-form-field-can-float", null], [2, "mat-form-field-should-float", null], [2, "mat-form-field-has-label", null], [2, "mat-form-field-hide-placeholder", null], [2, "mat-form-field-disabled", null], [2, "mat-form-field-autofilled", null], [2, "mat-focused", null], [2, "mat-accent", null], [2, "mat-warn", null], [2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null], [2, "_mat-animation-noopable", null]], null, null, _node_modules_angular_material_form_field_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_9__["View_MatFormField_0"], _node_modules_angular_material_form_field_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_9__["RenderType_MatFormField"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](48, 7520256, null, 9, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MatFormField"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], [2, _angular_material_core__WEBPACK_IMPORTED_MODULE_10__["MAT_LABEL_GLOBAL_OPTIONS"]], [2, _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_11__["Directionality"]], [2, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MAT_FORM_FIELD_DEFAULT_OPTIONS"]], _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_12__["Platform"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"], [2, _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_13__["ANIMATION_MODULE_TYPE"]]], { appearance: [0, "appearance"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 10, { _controlNonStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](335544320, 11, { _controlStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 12, { _labelChildNonStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](335544320, 13, { _labelChildStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 14, { _placeholderChild: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 15, { _errorChildren: 1 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 16, { _hintChildren: 1 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 17, { _prefixChildren: 1 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 18, { _suffixChildren: 1 }), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](58, 0, null, 3, 2, "mat-label", [], null, null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](59, 16384, [[12, 4], [13, 4]], 0, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MatLabel"], [], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Email"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](61, 0, null, 1, 7, "input", [["class", "mat-input-element mat-form-field-autofill-control"], ["formControlName", "email"], ["matInput", ""]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null], [2, "mat-input-server", null], [1, "id", 0], [1, "placeholder", 0], [8, "disabled", 0], [8, "required", 0], [1, "readonly", 0], [1, "aria-describedby", 0], [1, "aria-invalid", 0], [1, "aria-required", 0]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"], [null, "focus"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 62)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 62).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 62)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 62)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } if (("blur" === en)) {
        var pd_4 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 67)._focusChanged(false) !== false);
        ad = (pd_4 && ad);
    } if (("focus" === en)) {
        var pd_5 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 67)._focusChanged(true) !== false);
        ad = (pd_5 && ad);
    } if (("input" === en)) {
        var pd_6 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 67)._onInput() !== false);
        ad = (pd_6 && ad);
    } return ad; }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](62, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["DefaultValueAccessor"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["COMPOSITION_BUFFER_MODE"]]], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](1024, null, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NG_VALUE_ACCESSOR"], function (p0_0) { return [p0_0]; }, [_angular_forms__WEBPACK_IMPORTED_MODULE_8__["DefaultValueAccessor"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](64, 671744, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormControlName"], [[3, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ControlContainer"]], [8, null], [8, null], [6, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NG_VALUE_ACCESSOR"]], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵangular_packages_forms_forms_q"]]], { name: [0, "name"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControl"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormControlName"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](66, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControlStatus"], [[4, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControl"]]], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](67, 999424, null, 0, _angular_material_input__WEBPACK_IMPORTED_MODULE_14__["MatInput"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_12__["Platform"], [6, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControl"]], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgForm"]], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormGroupDirective"]], _angular_material_core__WEBPACK_IMPORTED_MODULE_10__["ErrorStateMatcher"], [8, null], _angular_cdk_text_field__WEBPACK_IMPORTED_MODULE_15__["AutofillMonitor"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, [[10, 4], [11, 4]], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MatFormFieldControl"], null, [_angular_material_input__WEBPACK_IMPORTED_MODULE_14__["MatInput"]]), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](69, 0, null, 4, 3, "mat-icon", [["class", "secondary-text mat-icon notranslate"], ["color", "primary"], ["matSuffix", ""], ["role", "img"]], [[2, "mat-icon-inline", null], [2, "mat-icon-no-color", null]], null, null, _node_modules_angular_material_icon_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_16__["View_MatIcon_0"], _node_modules_angular_material_icon_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_16__["RenderType_MatIcon"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](70, 16384, [[18, 4]], 0, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MatSuffix"], [], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](71, 9158656, null, 0, _angular_material_icon__WEBPACK_IMPORTED_MODULE_17__["MatIcon"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_17__["MatIconRegistry"], [8, null], [2, _angular_material_icon__WEBPACK_IMPORTED_MODULE_17__["MAT_ICON_LOCATION"]]], { color: [0, "color"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, 0, ["mail"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, 5, 1, null, View_NgxRegisterComponent_3)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](74, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, 5, 1, null, View_NgxRegisterComponent_4)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](76, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](77, 0, null, null, 28, "mat-form-field", [["appearance", "outline"], ["class", "w-100 mat-form-field"]], [[2, "mat-form-field-appearance-standard", null], [2, "mat-form-field-appearance-fill", null], [2, "mat-form-field-appearance-outline", null], [2, "mat-form-field-appearance-legacy", null], [2, "mat-form-field-invalid", null], [2, "mat-form-field-can-float", null], [2, "mat-form-field-should-float", null], [2, "mat-form-field-has-label", null], [2, "mat-form-field-hide-placeholder", null], [2, "mat-form-field-disabled", null], [2, "mat-form-field-autofilled", null], [2, "mat-focused", null], [2, "mat-accent", null], [2, "mat-warn", null], [2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null], [2, "_mat-animation-noopable", null]], null, null, _node_modules_angular_material_form_field_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_9__["View_MatFormField_0"], _node_modules_angular_material_form_field_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_9__["RenderType_MatFormField"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](78, 7520256, null, 9, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MatFormField"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], [2, _angular_material_core__WEBPACK_IMPORTED_MODULE_10__["MAT_LABEL_GLOBAL_OPTIONS"]], [2, _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_11__["Directionality"]], [2, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MAT_FORM_FIELD_DEFAULT_OPTIONS"]], _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_12__["Platform"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"], [2, _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_13__["ANIMATION_MODULE_TYPE"]]], { appearance: [0, "appearance"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 19, { _controlNonStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](335544320, 20, { _controlStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 21, { _labelChildNonStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](335544320, 22, { _labelChildStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 23, { _placeholderChild: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 24, { _errorChildren: 1 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 25, { _hintChildren: 1 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 26, { _prefixChildren: 1 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 27, { _suffixChildren: 1 }), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](88, 0, null, 3, 2, "mat-label", [], null, null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](89, 16384, [[21, 4], [22, 4]], 0, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MatLabel"], [], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Password "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](91, 0, null, 1, 7, "input", [["class", "mat-input-element mat-form-field-autofill-control"], ["formControlName", "password"], ["matInput", ""], ["type", "password"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null], [2, "mat-input-server", null], [1, "id", 0], [1, "placeholder", 0], [8, "disabled", 0], [8, "required", 0], [1, "readonly", 0], [1, "aria-describedby", 0], [1, "aria-invalid", 0], [1, "aria-required", 0]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"], [null, "focus"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 92)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 92).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 92)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 92)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } if (("blur" === en)) {
        var pd_4 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 97)._focusChanged(false) !== false);
        ad = (pd_4 && ad);
    } if (("focus" === en)) {
        var pd_5 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 97)._focusChanged(true) !== false);
        ad = (pd_5 && ad);
    } if (("input" === en)) {
        var pd_6 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 97)._onInput() !== false);
        ad = (pd_6 && ad);
    } return ad; }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](92, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["DefaultValueAccessor"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["COMPOSITION_BUFFER_MODE"]]], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](1024, null, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NG_VALUE_ACCESSOR"], function (p0_0) { return [p0_0]; }, [_angular_forms__WEBPACK_IMPORTED_MODULE_8__["DefaultValueAccessor"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](94, 671744, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormControlName"], [[3, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ControlContainer"]], [8, null], [8, null], [6, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NG_VALUE_ACCESSOR"]], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵangular_packages_forms_forms_q"]]], { name: [0, "name"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControl"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormControlName"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](96, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControlStatus"], [[4, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControl"]]], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](97, 999424, null, 0, _angular_material_input__WEBPACK_IMPORTED_MODULE_14__["MatInput"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_12__["Platform"], [6, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControl"]], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgForm"]], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormGroupDirective"]], _angular_material_core__WEBPACK_IMPORTED_MODULE_10__["ErrorStateMatcher"], [8, null], _angular_cdk_text_field__WEBPACK_IMPORTED_MODULE_15__["AutofillMonitor"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]], { type: [0, "type"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, [[19, 4], [20, 4]], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MatFormFieldControl"], null, [_angular_material_input__WEBPACK_IMPORTED_MODULE_14__["MatInput"]]), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](99, 0, null, 4, 3, "mat-icon", [["class", "secondary-text mat-icon notranslate"], ["color", "primary"], ["matSuffix", ""], ["role", "img"]], [[2, "mat-icon-inline", null], [2, "mat-icon-no-color", null]], null, null, _node_modules_angular_material_icon_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_16__["View_MatIcon_0"], _node_modules_angular_material_icon_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_16__["RenderType_MatIcon"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](100, 16384, [[27, 4]], 0, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MatSuffix"], [], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](101, 9158656, null, 0, _angular_material_icon__WEBPACK_IMPORTED_MODULE_17__["MatIcon"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_17__["MatIconRegistry"], [8, null], [2, _angular_material_icon__WEBPACK_IMPORTED_MODULE_17__["MAT_ICON_LOCATION"]]], { color: [0, "color"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, 0, ["vpn_key"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](103, 0, null, 5, 2, "mat-error", [["class", "mat-error"], ["role", "alert"]], [[1, "id", 0]], null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](104, 16384, [[24, 4]], 0, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MatError"], [], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, [" Password is required "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](106, 0, null, null, 28, "mat-form-field", [["appearance", "outline"], ["class", "w-100 mat-form-field"]], [[2, "mat-form-field-appearance-standard", null], [2, "mat-form-field-appearance-fill", null], [2, "mat-form-field-appearance-outline", null], [2, "mat-form-field-appearance-legacy", null], [2, "mat-form-field-invalid", null], [2, "mat-form-field-can-float", null], [2, "mat-form-field-should-float", null], [2, "mat-form-field-has-label", null], [2, "mat-form-field-hide-placeholder", null], [2, "mat-form-field-disabled", null], [2, "mat-form-field-autofilled", null], [2, "mat-focused", null], [2, "mat-accent", null], [2, "mat-warn", null], [2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null], [2, "_mat-animation-noopable", null]], null, null, _node_modules_angular_material_form_field_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_9__["View_MatFormField_0"], _node_modules_angular_material_form_field_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_9__["RenderType_MatFormField"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](107, 7520256, null, 9, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MatFormField"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], [2, _angular_material_core__WEBPACK_IMPORTED_MODULE_10__["MAT_LABEL_GLOBAL_OPTIONS"]], [2, _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_11__["Directionality"]], [2, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MAT_FORM_FIELD_DEFAULT_OPTIONS"]], _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_12__["Platform"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"], [2, _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_13__["ANIMATION_MODULE_TYPE"]]], { appearance: [0, "appearance"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 28, { _controlNonStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](335544320, 29, { _controlStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 30, { _labelChildNonStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](335544320, 31, { _labelChildStatic: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 32, { _placeholderChild: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 33, { _errorChildren: 1 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 34, { _hintChildren: 1 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 35, { _prefixChildren: 1 }), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](603979776, 36, { _suffixChildren: 1 }), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](117, 0, null, 3, 2, "mat-label", [], null, null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](118, 16384, [[30, 4], [31, 4]], 0, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MatLabel"], [], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["passwordConfirm "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](120, 0, null, 1, 7, "input", [["class", "mat-input-element mat-form-field-autofill-control"], ["formControlName", "passwordConfirm"], ["matInput", ""], ["type", "password"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null], [2, "mat-input-server", null], [1, "id", 0], [1, "placeholder", 0], [8, "disabled", 0], [8, "required", 0], [1, "readonly", 0], [1, "aria-describedby", 0], [1, "aria-invalid", 0], [1, "aria-required", 0]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"], [null, "focus"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 121)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 121).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 121)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 121)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } if (("blur" === en)) {
        var pd_4 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 126)._focusChanged(false) !== false);
        ad = (pd_4 && ad);
    } if (("focus" === en)) {
        var pd_5 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 126)._focusChanged(true) !== false);
        ad = (pd_5 && ad);
    } if (("input" === en)) {
        var pd_6 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 126)._onInput() !== false);
        ad = (pd_6 && ad);
    } return ad; }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](121, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["DefaultValueAccessor"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["COMPOSITION_BUFFER_MODE"]]], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](1024, null, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NG_VALUE_ACCESSOR"], function (p0_0) { return [p0_0]; }, [_angular_forms__WEBPACK_IMPORTED_MODULE_8__["DefaultValueAccessor"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](123, 671744, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormControlName"], [[3, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ControlContainer"]], [8, null], [8, null], [6, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NG_VALUE_ACCESSOR"]], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵangular_packages_forms_forms_q"]]], { name: [0, "name"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControl"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormControlName"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](125, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControlStatus"], [[4, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControl"]]], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](126, 999424, null, 0, _angular_material_input__WEBPACK_IMPORTED_MODULE_14__["MatInput"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_12__["Platform"], [6, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControl"]], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgForm"]], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormGroupDirective"]], _angular_material_core__WEBPACK_IMPORTED_MODULE_10__["ErrorStateMatcher"], [8, null], _angular_cdk_text_field__WEBPACK_IMPORTED_MODULE_15__["AutofillMonitor"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]], { type: [0, "type"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, [[28, 4], [29, 4]], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MatFormFieldControl"], null, [_angular_material_input__WEBPACK_IMPORTED_MODULE_14__["MatInput"]]), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](128, 0, null, 4, 3, "mat-icon", [["class", "secondary-text mat-icon notranslate"], ["color", "primary"], ["matSuffix", ""], ["role", "img"]], [[2, "mat-icon-inline", null], [2, "mat-icon-no-color", null]], null, null, _node_modules_angular_material_icon_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_16__["View_MatIcon_0"], _node_modules_angular_material_icon_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_16__["RenderType_MatIcon"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](129, 16384, [[36, 4]], 0, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MatSuffix"], [], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](130, 9158656, null, 0, _angular_material_icon__WEBPACK_IMPORTED_MODULE_17__["MatIcon"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_17__["MatIconRegistry"], [8, null], [2, _angular_material_icon__WEBPACK_IMPORTED_MODULE_17__["MAT_ICON_LOCATION"]]], { color: [0, "color"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, 0, ["vpn_key"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](132, 0, null, 5, 2, "mat-error", [["class", "mat-error"], ["role", "alert"]], [[1, "id", 0]], null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](133, 16384, [[33, 4]], 0, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MatError"], [], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, [" Password key not valid "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](135, 0, null, null, 2, "button", [["aria-label", "CREATE AN ACCOUNT"], ["class", "submit-button w-100"], ["color", "primary"], ["mat-raised-button", ""]], [[1, "disabled", 0], [2, "_mat-animation-noopable", null]], null, null, _node_modules_angular_material_button_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_18__["View_MatButton_0"], _node_modules_angular_material_button_typings_index_ngfactory__WEBPACK_IMPORTED_MODULE_18__["RenderType_MatButton"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](136, 180224, null, 0, _angular_material_button__WEBPACK_IMPORTED_MODULE_19__["MatButton"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_20__["FocusMonitor"], [2, _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_13__["ANIMATION_MODULE_TYPE"]]], { disabled: [0, "disabled"], color: [1, "color"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, 0, [" Inscription "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](138, 0, null, 0, 5, "div", [["class", "links"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](139, 0, null, null, 4, "section", [["aria-label", "Register"], ["class", "another-action font-header"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, [" Vous avez d\u00E9j\u00E0 un compte? "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](141, 0, null, null, 2, "a", [["class", "link"]], [[1, "target", 0], [8, "href", 4]], [[null, "click"]], function (_v, en, $event) { var ad = true; if (("click" === en)) {
        var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 142).onClick($event.button, $event.ctrlKey, $event.metaKey, $event.shiftKey) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](142, 671744, null, 0, _angular_router__WEBPACK_IMPORTED_MODULE_21__["RouterLinkWithHref"], [_angular_router__WEBPACK_IMPORTED_MODULE_21__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_21__["ActivatedRoute"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["LocationStrategy"]], { routerLink: [0, "routerLink"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["S'identifier?"]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = "rgba(0, 0, 0, 0.8)"; var currVal_1 = "large"; var currVal_2 = "#fff"; var currVal_3 = "ball-clip-rotate-multiple"; var currVal_4 = true; _ck(_v, 1, 0, currVal_0, currVal_1, currVal_2, currVal_3, currVal_4); var currVal_12 = _co.registerForm; _ck(_v, 11, 0, currVal_12); var currVal_13 = (_co.errors && (_co.errors.length > 0)); _ck(_v, 15, 0, currVal_13); var currVal_36 = "outline"; _ck(_v, 19, 0, currVal_36); var currVal_53 = "username"; _ck(_v, 35, 0, currVal_53); _ck(_v, 38, 0); var currVal_56 = "primary"; _ck(_v, 42, 0, currVal_56); var currVal_80 = "outline"; _ck(_v, 48, 0, currVal_80); var currVal_97 = "email"; _ck(_v, 64, 0, currVal_97); _ck(_v, 67, 0); var currVal_100 = "primary"; _ck(_v, 71, 0, currVal_100); var currVal_101 = _co.registerForm.get("email").hasError("required"); _ck(_v, 74, 0, currVal_101); var currVal_102 = _co.registerForm.get("email").hasError("email"); _ck(_v, 76, 0, currVal_102); var currVal_125 = "outline"; _ck(_v, 78, 0, currVal_125); var currVal_142 = "password"; _ck(_v, 94, 0, currVal_142); var currVal_143 = "password"; _ck(_v, 97, 0, currVal_143); var currVal_146 = "primary"; _ck(_v, 101, 0, currVal_146); var currVal_170 = "outline"; _ck(_v, 107, 0, currVal_170); var currVal_187 = "passwordConfirm"; _ck(_v, 123, 0, currVal_187); var currVal_188 = "password"; _ck(_v, 126, 0, currVal_188); var currVal_191 = "primary"; _ck(_v, 130, 0, currVal_191); var currVal_195 = _co.registerForm.invalid; var currVal_196 = "primary"; _ck(_v, 136, 0, currVal_195, currVal_196); var currVal_199 = "/auth/login"; _ck(_v, 142, 0, currVal_199); }, function (_ck, _v) { var currVal_5 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 13).ngClassUntouched; var currVal_6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 13).ngClassTouched; var currVal_7 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 13).ngClassPristine; var currVal_8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 13).ngClassDirty; var currVal_9 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 13).ngClassValid; var currVal_10 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 13).ngClassInvalid; var currVal_11 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 13).ngClassPending; _ck(_v, 9, 0, currVal_5, currVal_6, currVal_7, currVal_8, currVal_9, currVal_10, currVal_11); var currVal_14 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19).appearance == "standard"); var currVal_15 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19).appearance == "fill"); var currVal_16 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19).appearance == "outline"); var currVal_17 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19).appearance == "legacy"); var currVal_18 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19)._control.errorState; var currVal_19 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19)._canLabelFloat; var currVal_20 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19)._shouldLabelFloat(); var currVal_21 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19)._hasFloatingLabel(); var currVal_22 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19)._hideControlPlaceholder(); var currVal_23 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19)._control.disabled; var currVal_24 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19)._control.autofilled; var currVal_25 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19)._control.focused; var currVal_26 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19).color == "accent"); var currVal_27 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19).color == "warn"); var currVal_28 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19)._shouldForward("untouched"); var currVal_29 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19)._shouldForward("touched"); var currVal_30 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19)._shouldForward("pristine"); var currVal_31 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19)._shouldForward("dirty"); var currVal_32 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19)._shouldForward("valid"); var currVal_33 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19)._shouldForward("invalid"); var currVal_34 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19)._shouldForward("pending"); var currVal_35 = !_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19)._animationsEnabled; _ck(_v, 18, 1, [currVal_14, currVal_15, currVal_16, currVal_17, currVal_18, currVal_19, currVal_20, currVal_21, currVal_22, currVal_23, currVal_24, currVal_25, currVal_26, currVal_27, currVal_28, currVal_29, currVal_30, currVal_31, currVal_32, currVal_33, currVal_34, currVal_35]); var currVal_37 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 37).ngClassUntouched; var currVal_38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 37).ngClassTouched; var currVal_39 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 37).ngClassPristine; var currVal_40 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 37).ngClassDirty; var currVal_41 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 37).ngClassValid; var currVal_42 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 37).ngClassInvalid; var currVal_43 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 37).ngClassPending; var currVal_44 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 38)._isServer; var currVal_45 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 38).id; var currVal_46 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 38).placeholder; var currVal_47 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 38).disabled; var currVal_48 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 38).required; var currVal_49 = ((_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 38).readonly && !_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 38)._isNativeSelect) || null); var currVal_50 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 38)._ariaDescribedby || null); var currVal_51 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 38).errorState; var currVal_52 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 38).required.toString(); _ck(_v, 32, 1, [currVal_37, currVal_38, currVal_39, currVal_40, currVal_41, currVal_42, currVal_43, currVal_44, currVal_45, currVal_46, currVal_47, currVal_48, currVal_49, currVal_50, currVal_51, currVal_52]); var currVal_54 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 42).inline; var currVal_55 = (((_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 42).color !== "primary") && (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 42).color !== "accent")) && (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 42).color !== "warn")); _ck(_v, 40, 0, currVal_54, currVal_55); var currVal_57 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 45).id; _ck(_v, 44, 0, currVal_57); var currVal_58 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 48).appearance == "standard"); var currVal_59 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 48).appearance == "fill"); var currVal_60 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 48).appearance == "outline"); var currVal_61 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 48).appearance == "legacy"); var currVal_62 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 48)._control.errorState; var currVal_63 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 48)._canLabelFloat; var currVal_64 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 48)._shouldLabelFloat(); var currVal_65 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 48)._hasFloatingLabel(); var currVal_66 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 48)._hideControlPlaceholder(); var currVal_67 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 48)._control.disabled; var currVal_68 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 48)._control.autofilled; var currVal_69 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 48)._control.focused; var currVal_70 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 48).color == "accent"); var currVal_71 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 48).color == "warn"); var currVal_72 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 48)._shouldForward("untouched"); var currVal_73 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 48)._shouldForward("touched"); var currVal_74 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 48)._shouldForward("pristine"); var currVal_75 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 48)._shouldForward("dirty"); var currVal_76 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 48)._shouldForward("valid"); var currVal_77 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 48)._shouldForward("invalid"); var currVal_78 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 48)._shouldForward("pending"); var currVal_79 = !_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 48)._animationsEnabled; _ck(_v, 47, 1, [currVal_58, currVal_59, currVal_60, currVal_61, currVal_62, currVal_63, currVal_64, currVal_65, currVal_66, currVal_67, currVal_68, currVal_69, currVal_70, currVal_71, currVal_72, currVal_73, currVal_74, currVal_75, currVal_76, currVal_77, currVal_78, currVal_79]); var currVal_81 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 66).ngClassUntouched; var currVal_82 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 66).ngClassTouched; var currVal_83 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 66).ngClassPristine; var currVal_84 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 66).ngClassDirty; var currVal_85 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 66).ngClassValid; var currVal_86 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 66).ngClassInvalid; var currVal_87 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 66).ngClassPending; var currVal_88 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 67)._isServer; var currVal_89 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 67).id; var currVal_90 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 67).placeholder; var currVal_91 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 67).disabled; var currVal_92 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 67).required; var currVal_93 = ((_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 67).readonly && !_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 67)._isNativeSelect) || null); var currVal_94 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 67)._ariaDescribedby || null); var currVal_95 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 67).errorState; var currVal_96 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 67).required.toString(); _ck(_v, 61, 1, [currVal_81, currVal_82, currVal_83, currVal_84, currVal_85, currVal_86, currVal_87, currVal_88, currVal_89, currVal_90, currVal_91, currVal_92, currVal_93, currVal_94, currVal_95, currVal_96]); var currVal_98 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 71).inline; var currVal_99 = (((_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 71).color !== "primary") && (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 71).color !== "accent")) && (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 71).color !== "warn")); _ck(_v, 69, 0, currVal_98, currVal_99); var currVal_103 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 78).appearance == "standard"); var currVal_104 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 78).appearance == "fill"); var currVal_105 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 78).appearance == "outline"); var currVal_106 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 78).appearance == "legacy"); var currVal_107 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 78)._control.errorState; var currVal_108 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 78)._canLabelFloat; var currVal_109 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 78)._shouldLabelFloat(); var currVal_110 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 78)._hasFloatingLabel(); var currVal_111 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 78)._hideControlPlaceholder(); var currVal_112 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 78)._control.disabled; var currVal_113 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 78)._control.autofilled; var currVal_114 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 78)._control.focused; var currVal_115 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 78).color == "accent"); var currVal_116 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 78).color == "warn"); var currVal_117 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 78)._shouldForward("untouched"); var currVal_118 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 78)._shouldForward("touched"); var currVal_119 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 78)._shouldForward("pristine"); var currVal_120 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 78)._shouldForward("dirty"); var currVal_121 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 78)._shouldForward("valid"); var currVal_122 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 78)._shouldForward("invalid"); var currVal_123 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 78)._shouldForward("pending"); var currVal_124 = !_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 78)._animationsEnabled; _ck(_v, 77, 1, [currVal_103, currVal_104, currVal_105, currVal_106, currVal_107, currVal_108, currVal_109, currVal_110, currVal_111, currVal_112, currVal_113, currVal_114, currVal_115, currVal_116, currVal_117, currVal_118, currVal_119, currVal_120, currVal_121, currVal_122, currVal_123, currVal_124]); var currVal_126 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 96).ngClassUntouched; var currVal_127 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 96).ngClassTouched; var currVal_128 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 96).ngClassPristine; var currVal_129 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 96).ngClassDirty; var currVal_130 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 96).ngClassValid; var currVal_131 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 96).ngClassInvalid; var currVal_132 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 96).ngClassPending; var currVal_133 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 97)._isServer; var currVal_134 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 97).id; var currVal_135 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 97).placeholder; var currVal_136 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 97).disabled; var currVal_137 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 97).required; var currVal_138 = ((_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 97).readonly && !_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 97)._isNativeSelect) || null); var currVal_139 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 97)._ariaDescribedby || null); var currVal_140 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 97).errorState; var currVal_141 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 97).required.toString(); _ck(_v, 91, 1, [currVal_126, currVal_127, currVal_128, currVal_129, currVal_130, currVal_131, currVal_132, currVal_133, currVal_134, currVal_135, currVal_136, currVal_137, currVal_138, currVal_139, currVal_140, currVal_141]); var currVal_144 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 101).inline; var currVal_145 = (((_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 101).color !== "primary") && (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 101).color !== "accent")) && (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 101).color !== "warn")); _ck(_v, 99, 0, currVal_144, currVal_145); var currVal_147 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 104).id; _ck(_v, 103, 0, currVal_147); var currVal_148 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 107).appearance == "standard"); var currVal_149 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 107).appearance == "fill"); var currVal_150 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 107).appearance == "outline"); var currVal_151 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 107).appearance == "legacy"); var currVal_152 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 107)._control.errorState; var currVal_153 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 107)._canLabelFloat; var currVal_154 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 107)._shouldLabelFloat(); var currVal_155 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 107)._hasFloatingLabel(); var currVal_156 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 107)._hideControlPlaceholder(); var currVal_157 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 107)._control.disabled; var currVal_158 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 107)._control.autofilled; var currVal_159 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 107)._control.focused; var currVal_160 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 107).color == "accent"); var currVal_161 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 107).color == "warn"); var currVal_162 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 107)._shouldForward("untouched"); var currVal_163 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 107)._shouldForward("touched"); var currVal_164 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 107)._shouldForward("pristine"); var currVal_165 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 107)._shouldForward("dirty"); var currVal_166 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 107)._shouldForward("valid"); var currVal_167 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 107)._shouldForward("invalid"); var currVal_168 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 107)._shouldForward("pending"); var currVal_169 = !_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 107)._animationsEnabled; _ck(_v, 106, 1, [currVal_148, currVal_149, currVal_150, currVal_151, currVal_152, currVal_153, currVal_154, currVal_155, currVal_156, currVal_157, currVal_158, currVal_159, currVal_160, currVal_161, currVal_162, currVal_163, currVal_164, currVal_165, currVal_166, currVal_167, currVal_168, currVal_169]); var currVal_171 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 125).ngClassUntouched; var currVal_172 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 125).ngClassTouched; var currVal_173 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 125).ngClassPristine; var currVal_174 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 125).ngClassDirty; var currVal_175 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 125).ngClassValid; var currVal_176 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 125).ngClassInvalid; var currVal_177 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 125).ngClassPending; var currVal_178 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 126)._isServer; var currVal_179 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 126).id; var currVal_180 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 126).placeholder; var currVal_181 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 126).disabled; var currVal_182 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 126).required; var currVal_183 = ((_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 126).readonly && !_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 126)._isNativeSelect) || null); var currVal_184 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 126)._ariaDescribedby || null); var currVal_185 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 126).errorState; var currVal_186 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 126).required.toString(); _ck(_v, 120, 1, [currVal_171, currVal_172, currVal_173, currVal_174, currVal_175, currVal_176, currVal_177, currVal_178, currVal_179, currVal_180, currVal_181, currVal_182, currVal_183, currVal_184, currVal_185, currVal_186]); var currVal_189 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 130).inline; var currVal_190 = (((_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 130).color !== "primary") && (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 130).color !== "accent")) && (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 130).color !== "warn")); _ck(_v, 128, 0, currVal_189, currVal_190); var currVal_192 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 133).id; _ck(_v, 132, 0, currVal_192); var currVal_193 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 136).disabled || null); var currVal_194 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 136)._animationMode === "NoopAnimations"); _ck(_v, 135, 0, currVal_193, currVal_194); var currVal_197 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 142).target; var currVal_198 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 142).href; _ck(_v, 141, 0, currVal_197, currVal_198); }); }
function View_NgxRegisterComponent_Host_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 1, "ngx-login", [], null, null, null, View_NgxRegisterComponent_0, RenderType_NgxRegisterComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](1, 245760, null, 0, _ngx_register_component__WEBPACK_IMPORTED_MODULE_22__["NgxRegisterComponent"], [_service_auth_service__WEBPACK_IMPORTED_MODULE_23__["AuthService"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormBuilder"], _ethereumProvider_ethereum__WEBPACK_IMPORTED_MODULE_24__["EthereumProvider"], _angular_router__WEBPACK_IMPORTED_MODULE_21__["Router"], ngx_spinner__WEBPACK_IMPORTED_MODULE_5__["NgxSpinnerService"]], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
var NgxRegisterComponentNgFactory = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵccf"]("ngx-login", _ngx_register_component__WEBPACK_IMPORTED_MODULE_22__["NgxRegisterComponent"], View_NgxRegisterComponent_Host_0, {}, {}, []);



/***/ }),

/***/ "./src/app/auth/register/ngx-register.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/auth/register/ngx-register.component.ts ***!
  \*********************************************************/
/*! exports provided: NgxRegisterComponent, confirmPasswordValidator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxRegisterComponent", function() { return NgxRegisterComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "confirmPasswordValidator", function() { return confirmPasswordValidator; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! file-saver */ "./node_modules/file-saver/dist/FileSaver.min.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ethereumProvider_ethereum__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../ethereumProvider/ethereum */ "./src/app/ethereumProvider/ethereum.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var app_service_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm2015/ngx-spinner.js");








class NgxRegisterComponent {
    constructor(_auth, _formBuilder, _web3, _router, _spinner) {
        this._auth = _auth;
        this._formBuilder = _formBuilder;
        this._web3 = _web3;
        this._router = _router;
        this._spinner = _spinner;
        this.errors = [];
        this._unsubscribeAll = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    createAccount() {
        this._web3.generateAccount();
        let data = JSON.stringify({ address: this._web3.getAccount(), privateKey: this._web3.getPrivateKey() });
        let file = this.createFile(data);
        let timestamp = Math.round(+new Date() / 1000);
        this.registerForm.controls["roles"].patchValue([this.registerForm.controls["userrole"].value]);
        this.registerForm.controls["accountAddress"].patchValue(this._web3.getAccount());
        this._spinner.show().then(console.log);
        this._web3.createContract().then(signed => {
            console.log(signed);
            const data = Object.assign({}, this.registerForm.value, { transaction: signed.rawTransaction });
            this._auth.signup(data).subscribe(res => {
                this._spinner.hide();
                this._router.navigate(["/auth/login"]);
                file_saver__WEBPACK_IMPORTED_MODULE_1__(file, timestamp + 'key.json');
            }, error => {
                this._spinner.hide();
                this.errors = [error.message];
                console.log(error);
                this.errors = [error];
            });
        }, error1 => { this.errors = ['invalid Transaction']; this._spinner.hide(); });
    }
    createFile(data) {
        let file = new Blob([data], { type: 'text/text;charset=utf-8' });
        return file;
    }
    ngOnInit() {
        this.registerForm = this._formBuilder.group({
            username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]],
            accountAddress: [''],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            roles: ['pharmacie'],
            userrole: ['pharmacie'],
            passwordConfirm: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, confirmPasswordValidator]]
        });
        this._web3.generateAccount();
        // Update the validity of the 'passwordConfirm' field
        // when the 'password' field changes
        this.registerForm.get('password').valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this._unsubscribeAll))
            .subscribe(() => {
            this.registerForm.get('passwordConfirm').updateValueAndValidity();
        });
    }
    onRegister() {
    }
    ngOnDestroy() {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
const confirmPasswordValidator = (control) => {
    if (!control.parent || !control) {
        return null;
    }
    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');
    if (!password || !passwordConfirm) {
        return null;
    }
    if (passwordConfirm.value === '') {
        return null;
    }
    if (password.value === passwordConfirm.value) {
        return null;
    }
    return { passwordsNotMatching: true };
};


/***/ }),

/***/ "./src/app/config.ts":
/*!***************************!*\
  !*** ./src/app/config.ts ***!
  \***************************/
/*! exports provided: config */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "config", function() { return config; });
const config = {
    apiUrl1: 'http://51.178.143.238:8050/pharmacie-service',
    apiUrl2: 'http://51.178.143.238:8050/blockchain-service'
};


/***/ }),

/***/ "./src/app/ethereumProvider/contracts/Laboratoire.json":
/*!*************************************************************!*\
  !*** ./src/app/ethereumProvider/contracts/Laboratoire.json ***!
  \*************************************************************/
/*! exports provided: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, default */
/***/ (function(module) {

module.exports = [{"inputs":[],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"grossiste","type":"address"},{"indexed":false,"internalType":"string","name":"tagContainer","type":"string"}],"name":"addContainer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"grossiste","type":"address"},{"indexed":false,"internalType":"string","name":"numLot","type":"string"},{"indexed":false,"internalType":"uint256","name":"time","type":"uint256"}],"name":"addMedicament","type":"event"},{"inputs":[{"internalType":"string","name":"name","type":"string"},{"internalType":"string","name":"id","type":"string"},{"internalType":"address","name":"address_user","type":"address"}],"name":"AddPharmacie","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"users","type":"address"},{"indexed":false,"internalType":"enum SharedStruct.ROLE","name":"role","type":"uint8"}],"name":"addUser","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":false,"internalType":"string","name":"numLot","type":"string"},{"indexed":false,"internalType":"uint256","name":"time","type":"uint256"}],"name":"importMedicament","type":"event"},{"inputs":[{"internalType":"string","name":"name","type":"string"},{"internalType":"string","name":"numLot","type":"string"},{"internalType":"string","name":"serialNumber","type":"string"},{"internalType":"string","name":"dateExpiration","type":"string"},{"internalType":"address","name":"oldowner","type":"address"},{"internalType":"string","name":"tagContainer","type":"string"},{"internalType":"string","name":"tx","type":"string"},{"internalType":"string","name":"newtx","type":"string"},{"internalType":"string","name":"nameNewOwner","type":"string"}],"name":"ImportMedicament","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"string","name":"s1","type":"string"},{"internalType":"string","name":"s2","type":"string"}],"name":"compareStringsbyBytes","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"pure","type":"function"},{"inputs":[{"internalType":"string","name":"","type":"string"}],"name":"containers","outputs":[{"internalType":"string","name":"id","type":"string"},{"internalType":"string","name":"seuilTemperature","type":"string"},{"internalType":"string","name":"seuilHumidite","type":"string"},{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"grossiste","type":"address"},{"internalType":"enum SharedStruct.ContainerState","name":"state","type":"uint8"},{"internalType":"address","name":"transporteur","type":"address"},{"internalType":"bool","name":"exists","type":"bool"},{"internalType":"enum SharedStruct.ContainerType","name":"typeContainer","type":"uint8"},{"internalType":"string","name":"tx","type":"string"},{"internalType":"address","name":"oldowner","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"string","name":"tagContainer","type":"string"}],"name":"getContainer","outputs":[{"components":[{"internalType":"string","name":"id","type":"string"},{"internalType":"string[]","name":"temprature","type":"string[]"},{"internalType":"string[]","name":"humidite","type":"string[]"},{"internalType":"string","name":"seuilTemperature","type":"string"},{"internalType":"string","name":"seuilHumidite","type":"string"},{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"grossiste","type":"address"},{"components":[{"internalType":"string","name":"name","type":"string"},{"internalType":"string","name":"numLot","type":"string"},{"internalType":"string","name":"serialNumber","type":"string"},{"internalType":"string","name":"dateExpiration","type":"string"},{"internalType":"string","name":"tagContainer","type":"string"},{"internalType":"address","name":"newOwner","type":"address"},{"internalType":"string","name":"nameNewOwner","type":"string"},{"internalType":"address","name":"oldOwner","type":"address"},{"internalType":"string","name":"nameoldOnwer","type":"string"},{"internalType":"string","name":"transactionHash","type":"string"},{"internalType":"string","name":"newtransactionHash","type":"string"},{"internalType":"bool","name":"exists","type":"bool"}],"internalType":"struct SharedStruct.Medicament[]","name":"medicaments","type":"tuple[]"},{"internalType":"enum SharedStruct.ContainerState","name":"state","type":"uint8"},{"internalType":"address","name":"transporteur","type":"address"},{"internalType":"bool","name":"exists","type":"bool"},{"internalType":"enum SharedStruct.ContainerType","name":"typeContainer","type":"uint8"},{"internalType":"string","name":"tx","type":"string"},{"internalType":"address","name":"oldowner","type":"address"}],"internalType":"struct SharedStruct.Container","name":"data","type":"tuple"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"string","name":"numLot","type":"string"}],"name":"getMedicaments","outputs":[{"components":[{"internalType":"string","name":"name","type":"string"},{"internalType":"string","name":"numLot","type":"string"},{"internalType":"string","name":"serialNumber","type":"string"},{"internalType":"string","name":"dateExpiration","type":"string"},{"internalType":"string","name":"tagContainer","type":"string"},{"internalType":"address","name":"newOwner","type":"address"},{"internalType":"string","name":"nameNewOwner","type":"string"},{"internalType":"address","name":"oldOwner","type":"address"},{"internalType":"string","name":"nameoldOnwer","type":"string"},{"internalType":"string","name":"transactionHash","type":"string"},{"internalType":"string","name":"newtransactionHash","type":"string"},{"internalType":"bool","name":"exists","type":"bool"}],"internalType":"struct SharedStruct.Medicament","name":"data","type":"tuple"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"string","name":"","type":"string"}],"name":"medicaments","outputs":[{"internalType":"string","name":"name","type":"string"},{"internalType":"string","name":"numLot","type":"string"},{"internalType":"string","name":"serialNumber","type":"string"},{"internalType":"string","name":"dateExpiration","type":"string"},{"internalType":"string","name":"tagContainer","type":"string"},{"internalType":"address","name":"newOwner","type":"address"},{"internalType":"string","name":"nameNewOwner","type":"string"},{"internalType":"address","name":"oldOwner","type":"address"},{"internalType":"string","name":"nameoldOnwer","type":"string"},{"internalType":"string","name":"transactionHash","type":"string"},{"internalType":"string","name":"newtransactionHash","type":"string"},{"internalType":"bool","name":"exists","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"users","outputs":[{"internalType":"string","name":"name","type":"string"},{"internalType":"string","name":"id","type":"string"},{"internalType":"enum SharedStruct.ROLE","name":"role","type":"uint8"},{"internalType":"bool","name":"exists","type":"bool"}],"stateMutability":"view","type":"function"}];

/***/ }),

/***/ "./src/app/ethereumProvider/contracts/abi.json":
/*!*****************************************************!*\
  !*** ./src/app/ethereumProvider/contracts/abi.json ***!
  \*****************************************************/
/*! exports provided: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, default */
/***/ (function(module) {

module.exports = [{"inputs":[],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"grossiste","type":"address"},{"indexed":false,"internalType":"string","name":"tagContainer","type":"string"}],"name":"addContainer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"grossiste","type":"address"},{"indexed":false,"internalType":"string","name":"numLot","type":"string"},{"indexed":false,"internalType":"uint256","name":"time","type":"uint256"}],"name":"addMedicament","type":"event"},{"inputs":[{"internalType":"string","name":"name","type":"string"},{"internalType":"string","name":"id","type":"string"},{"internalType":"address","name":"address_user","type":"address"}],"name":"AddPharmacie","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"users","type":"address"},{"indexed":false,"internalType":"enum SharedStruct.ROLE","name":"role","type":"uint8"}],"name":"addUser","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":false,"internalType":"string","name":"numLot","type":"string"},{"indexed":false,"internalType":"uint256","name":"time","type":"uint256"}],"name":"importMedicament","type":"event"},{"inputs":[{"internalType":"string","name":"name","type":"string"},{"internalType":"string","name":"numLot","type":"string"},{"internalType":"string","name":"serialNumber","type":"string"},{"internalType":"string","name":"dateExpiration","type":"string"},{"internalType":"address","name":"oldowner","type":"address"},{"internalType":"string","name":"tagContainer","type":"string"},{"internalType":"string","name":"tx","type":"string"},{"internalType":"string","name":"newtx","type":"string"},{"internalType":"string","name":"nameNewOwner","type":"string"}],"name":"ImportMedicament","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"string","name":"s1","type":"string"},{"internalType":"string","name":"s2","type":"string"}],"name":"compareStringsbyBytes","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"pure","type":"function"},{"inputs":[{"internalType":"string","name":"","type":"string"}],"name":"containers","outputs":[{"internalType":"string","name":"id","type":"string"},{"internalType":"string","name":"seuilTemperature","type":"string"},{"internalType":"string","name":"seuilHumidite","type":"string"},{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"grossiste","type":"address"},{"internalType":"enum SharedStruct.ContainerState","name":"state","type":"uint8"},{"internalType":"address","name":"transporteur","type":"address"},{"internalType":"bool","name":"exists","type":"bool"},{"internalType":"enum SharedStruct.ContainerType","name":"typeContainer","type":"uint8"},{"internalType":"string","name":"tx","type":"string"},{"internalType":"address","name":"oldowner","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"string","name":"tagContainer","type":"string"}],"name":"getContainer","outputs":[{"components":[{"internalType":"string","name":"id","type":"string"},{"internalType":"string[]","name":"temprature","type":"string[]"},{"internalType":"string[]","name":"humidite","type":"string[]"},{"internalType":"string","name":"seuilTemperature","type":"string"},{"internalType":"string","name":"seuilHumidite","type":"string"},{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"grossiste","type":"address"},{"components":[{"internalType":"string","name":"name","type":"string"},{"internalType":"string","name":"numLot","type":"string"},{"internalType":"string","name":"serialNumber","type":"string"},{"internalType":"string","name":"dateExpiration","type":"string"},{"internalType":"string","name":"tagContainer","type":"string"},{"internalType":"address","name":"newOwner","type":"address"},{"internalType":"string","name":"nameNewOwner","type":"string"},{"internalType":"address","name":"oldOwner","type":"address"},{"internalType":"string","name":"nameoldOnwer","type":"string"},{"internalType":"string","name":"transactionHash","type":"string"},{"internalType":"string","name":"newtransactionHash","type":"string"},{"internalType":"bool","name":"exists","type":"bool"}],"internalType":"struct SharedStruct.Medicament[]","name":"medicaments","type":"tuple[]"},{"internalType":"enum SharedStruct.ContainerState","name":"state","type":"uint8"},{"internalType":"address","name":"transporteur","type":"address"},{"internalType":"bool","name":"exists","type":"bool"},{"internalType":"enum SharedStruct.ContainerType","name":"typeContainer","type":"uint8"},{"internalType":"string","name":"tx","type":"string"},{"internalType":"address","name":"oldowner","type":"address"}],"internalType":"struct SharedStruct.Container","name":"data","type":"tuple"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"string","name":"numLot","type":"string"}],"name":"getMedicaments","outputs":[{"components":[{"internalType":"string","name":"name","type":"string"},{"internalType":"string","name":"numLot","type":"string"},{"internalType":"string","name":"serialNumber","type":"string"},{"internalType":"string","name":"dateExpiration","type":"string"},{"internalType":"string","name":"tagContainer","type":"string"},{"internalType":"address","name":"newOwner","type":"address"},{"internalType":"string","name":"nameNewOwner","type":"string"},{"internalType":"address","name":"oldOwner","type":"address"},{"internalType":"string","name":"nameoldOnwer","type":"string"},{"internalType":"string","name":"transactionHash","type":"string"},{"internalType":"string","name":"newtransactionHash","type":"string"},{"internalType":"bool","name":"exists","type":"bool"}],"internalType":"struct SharedStruct.Medicament","name":"data","type":"tuple"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"string","name":"","type":"string"}],"name":"medicaments","outputs":[{"internalType":"string","name":"name","type":"string"},{"internalType":"string","name":"numLot","type":"string"},{"internalType":"string","name":"serialNumber","type":"string"},{"internalType":"string","name":"dateExpiration","type":"string"},{"internalType":"string","name":"tagContainer","type":"string"},{"internalType":"address","name":"newOwner","type":"address"},{"internalType":"string","name":"nameNewOwner","type":"string"},{"internalType":"address","name":"oldOwner","type":"address"},{"internalType":"string","name":"nameoldOnwer","type":"string"},{"internalType":"string","name":"transactionHash","type":"string"},{"internalType":"string","name":"newtransactionHash","type":"string"},{"internalType":"bool","name":"exists","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"users","outputs":[{"internalType":"string","name":"name","type":"string"},{"internalType":"string","name":"id","type":"string"},{"internalType":"enum SharedStruct.ROLE","name":"role","type":"uint8"},{"internalType":"bool","name":"exists","type":"bool"}],"stateMutability":"view","type":"function"}];

/***/ }),

/***/ "./src/app/ethereumProvider/ethereum.ts":
/*!**********************************************!*\
  !*** ./src/app/ethereumProvider/ethereum.ts ***!
  \**********************************************/
/*! exports provided: EthereumProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EthereumProvider", function() { return EthereumProvider; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _env__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../env */ "./env.ts");
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment.prod */ "./src/environments/environment.prod.ts");
/* harmony import */ var _service_secure_storage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/secure-storage.service */ "./src/app/service/secure-storage.service.ts");

const abi = __webpack_require__(/*! ./contracts/abi.json */ "./src/app/ethereumProvider/contracts/abi.json");
const laboratoire = __webpack_require__(/*! ./contracts/Laboratoire.json */ "./src/app/ethereumProvider/contracts/Laboratoire.json");
const privateKeyToAddress = __webpack_require__(/*! ethereum-private-key-to-address */ "./node_modules/ethereum-private-key-to-address/index.js");
const Tx = __webpack_require__(/*! ethereumjs-tx */ "./node_modules/ethereumjs-tx/dist/index.js");



const crypto = __webpack_require__(/*! crypto */ "./node_modules/crypto-browserify/index.js");
const eccrypto = __webpack_require__(/*! eccrypto */ "./node_modules/eccrypto/browser.js");
const Web3 = __webpack_require__(/*! web3 */ "./node_modules/web3/src/index.js");
class EthereumProvider {
    constructor(secureStorage) {
        this.secureStorage = secureStorage;
        this.interface = abi.interface;
        this.byteCode = "";
        this.web3 = new Web3(new Web3.providers.HttpProvider("http://5.135.52.74:8546"));
        this.web3.eth.net.getNetworkType(function (err, res) {
            console.log("Network Type: " + res);
        });
        this.accountAddress = _env__WEBPACK_IMPORTED_MODULE_1__["AppConfig"].ethereum.account;
        this.privateKey = _env__WEBPACK_IMPORTED_MODULE_1__["AppConfig"].ethereum.privateKey;
        this.userDetails = JSON.parse(localStorage.getItem('currentUser'));
        if (!!this.userDetails) {
            let data = JSON.parse(localStorage.getItem("currentUser")).smartContract;
            this.smartContractAddress = data;
            console.log(this.smartContractAddress);
            this.contract = new this.web3.eth.Contract(laboratoire, this.smartContractAddress);
            let key = this.secureStorage.encrypt("prvKey");
            let value = localStorage.getItem(key);
            this.key = "0x" + this.secureStorage.decrypt(value);
            console.log(this.key);
        }
    }
    encrypt(data) {
        return this.web3.eth.accounts.encrypt(data, _environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__["environment"].SECRET_KEY);
    }
    generateAccount() {
        const account = this.web3.eth.accounts.create();
        this.accountAddress = account.address;
        this.privateKey = account.privateKey;
    }
    getMedicamentData(numeroLot) {
        let contractTest = new this.web3.eth.Contract(laboratoire, this.smartContractAddress);
    }
    updateContainerSensorSeuil(data) {
        console.log(data);
        const contractTest = new this.web3.eth.Contract(laboratoire, this.smartContractAddress);
        let encoded = contractTest.methods.UpdateSensorSeuil(data.tagContainer, data.seuilTemperature.toString(), data.seuilHumidite.toString()).encodeABI();
        var tx = {
            to: this.userDetails.accountAddress,
            value: '0',
            gas: 3000000,
            gasPrice: '0',
            nonce: this.web3.eth.getTransactionCount(JSON.parse(localStorage.getItem("currentUser")).accountAddress, 'pending'),
            chainId: 1515,
            data: encoded
        };
        console.log(this.privateKey);
        return this.web3.eth.accounts.signTransaction(tx, this.privateKey);
    }
    createContainer(data) {
        console.log(data);
        console.log(this.smartContractAddress);
        let encoded = this.contract.methods.AddContainer(data.tagContainer, data.grossisteAddress, data.seuilTemperature.toString(), data.seuilHumidite.toString()).encodeABI();
        var tx = {
            to: this.smartContractAddress,
            value: '0',
            gas: 3000000,
            gasPrice: '0',
            nonce: this.web3.eth.getTransactionCount(JSON.parse(localStorage.getItem("currentUser")).accountAddress, 'pending'),
            chainId: 1515,
            data: encoded
        };
        return this.web3.eth.accounts.signTransaction(tx, this.key);
    }
    createContract() {
        let encoded = "0x608060405234801561001057600080fd5b5033600360006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550613d1c806100616000396000f3fe608060405234801561001057600080fd5b50600436106100935760003560e01c80638da5cb5b116100665780638da5cb5b1461016d578063a87430ba1461018b578063b6ae49b9146101be578063bd4985e8146101ee578063e9a734ff1461021e57610093565b806316d2edb014610098578063453a7184146100c85780634fb039ee146100f8578063763852e914610133575b600080fd5b6100b260048036038101906100ad9190612f8c565b61024e565b6040516100bf91906137de565b60405180910390f35b6100e260048036038101906100dd9190612edf565b61046d565b6040516100ef9190613a79565b60405180910390f35b610112600480360381019061010d9190612edf565b610b1f565b60405161012a9c9b9a9998979695949392919061392e565b60405180910390f35b61014d60048036038101906101489190612edf565b61113a565b6040516101649b9a99989796959493929190613867565b60405180910390f35b6101756114b1565b60405161018291906137c3565b60405180910390f35b6101a560048036038101906101a09190612eb6565b6114d7565b6040516101b59493929190613814565b60405180910390f35b6101d860048036038101906101d39190612edf565b611651565b6040516101e59190613a57565b60405180910390f35b6102086004803603810190610203919061300b565b61238a565b60405161021591906137de565b60405180910390f35b61023860048036038101906102339190612f20565b612b5a565b60405161024591906137de565b60405180910390f35b6000600360009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16146102aa57600080fd5b82600260008473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206001019080519060200190610300929190612bb3565b5083600260008473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206000019080519060200190610357929190612bb3565b506001600260008473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060020160016101000a81548160ff0219169083151502179055506000600260008473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060020160006101000a81548160ff0219169083600281111561041257fe5b02179055508173ffffffffffffffffffffffffffffffffffffffff167f9c3eead910e8aef9aab00191901ebb480f7b62df883268373956303a8a1cfc1f600060405161045e91906137f9565b60405180910390a29392505050565b610475612c33565b60008260405161048591906137ac565b908152602001604051809103902060405180610180016040529081600082018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156105385780601f1061050d57610100808354040283529160200191610538565b820191906000526020600020905b81548152906001019060200180831161051b57829003601f168201915b50505050508152602001600182018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156105da5780601f106105af576101008083540402835291602001916105da565b820191906000526020600020905b8154815290600101906020018083116105bd57829003601f168201915b50505050508152602001600282018054600181600116156101000203166002900480601f01602080910402602001604051908101604052809291908181526020018280546001816001161561010002031660029004801561067c5780601f106106515761010080835404028352916020019161067c565b820191906000526020600020905b81548152906001019060200180831161065f57829003601f168201915b50505050508152602001600382018054600181600116156101000203166002900480601f01602080910402602001604051908101604052809291908181526020018280546001816001161561010002031660029004801561071e5780601f106106f35761010080835404028352916020019161071e565b820191906000526020600020905b81548152906001019060200180831161070157829003601f168201915b50505050508152602001600482018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156107c05780601f10610795576101008083540402835291602001916107c0565b820191906000526020600020905b8154815290600101906020018083116107a357829003601f168201915b505050505081526020016005820160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001600682018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156108b85780601f1061088d576101008083540402835291602001916108b8565b820191906000526020600020905b81548152906001019060200180831161089b57829003601f168201915b505050505081526020016007820160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001600882018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156109b05780601f10610985576101008083540402835291602001916109b0565b820191906000526020600020905b81548152906001019060200180831161099357829003601f168201915b50505050508152602001600982018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015610a525780601f10610a2757610100808354040283529160200191610a52565b820191906000526020600020905b815481529060010190602001808311610a3557829003601f168201915b50505050508152602001600a82018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015610af45780601f10610ac957610100808354040283529160200191610af4565b820191906000526020600020905b815481529060010190602001808311610ad757829003601f168201915b50505050508152602001600b820160009054906101000a900460ff1615151515815250509050919050565b600081805160208101820180518482526020830160208501208183528095505050505050600091509050806000018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015610be15780601f10610bb657610100808354040283529160200191610be1565b820191906000526020600020905b815481529060010190602001808311610bc457829003601f168201915b505050505090806001018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015610c7f5780601f10610c5457610100808354040283529160200191610c7f565b820191906000526020600020905b815481529060010190602001808311610c6257829003601f168201915b505050505090806002018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015610d1d5780601f10610cf257610100808354040283529160200191610d1d565b820191906000526020600020905b815481529060010190602001808311610d0057829003601f168201915b505050505090806003018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015610dbb5780601f10610d9057610100808354040283529160200191610dbb565b820191906000526020600020905b815481529060010190602001808311610d9e57829003601f168201915b505050505090806004018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015610e595780601f10610e2e57610100808354040283529160200191610e59565b820191906000526020600020905b815481529060010190602001808311610e3c57829003601f168201915b5050505050908060050160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1690806006018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015610f1d5780601f10610ef257610100808354040283529160200191610f1d565b820191906000526020600020905b815481529060010190602001808311610f0057829003601f168201915b5050505050908060070160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1690806008018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015610fe15780601f10610fb657610100808354040283529160200191610fe1565b820191906000526020600020905b815481529060010190602001808311610fc457829003601f168201915b505050505090806009018054600181600116156101000203166002900480601f01602080910402602001604051908101604052809291908181526020018280546001816001161561010002031660029004801561107f5780601f106110545761010080835404028352916020019161107f565b820191906000526020600020905b81548152906001019060200180831161106257829003601f168201915b50505050509080600a018054600181600116156101000203166002900480601f01602080910402602001604051908101604052809291908181526020018280546001816001161561010002031660029004801561111d5780601f106110f25761010080835404028352916020019161111d565b820191906000526020600020905b81548152906001019060200180831161110057829003601f168201915b50505050509080600b0160009054906101000a900460ff1690508c565b600181805160208101820180518482526020830160208501208183528095505050505050600091509050806000018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156111fc5780601f106111d1576101008083540402835291602001916111fc565b820191906000526020600020905b8154815290600101906020018083116111df57829003601f168201915b505050505090806003018054600181600116156101000203166002900480601f01602080910402602001604051908101604052809291908181526020018280546001816001161561010002031660029004801561129a5780601f1061126f5761010080835404028352916020019161129a565b820191906000526020600020905b81548152906001019060200180831161127d57829003601f168201915b505050505090806004018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156113385780601f1061130d57610100808354040283529160200191611338565b820191906000526020600020905b81548152906001019060200180831161131b57829003601f168201915b5050505050908060050160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16908060060160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16908060080160009054906101000a900460ff16908060080160019054906101000a900473ffffffffffffffffffffffffffffffffffffffff16908060080160159054906101000a900460ff16908060080160169054906101000a900460ff1690806009018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156114815780601f1061145657610100808354040283529160200191611481565b820191906000526020600020905b81548152906001019060200180831161146457829003601f168201915b50505050509080600a0160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1690508b565b600360009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b6002602052806000526040600020600091509050806000018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156115835780601f1061155857610100808354040283529160200191611583565b820191906000526020600020905b81548152906001019060200180831161156657829003601f168201915b505050505090806001018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156116215780601f106115f657610100808354040283529160200191611621565b820191906000526020600020905b81548152906001019060200180831161160457829003601f168201915b5050505050908060020160009054906101000a900460ff16908060020160019054906101000a900460ff16905084565b611659612cc2565b60018260405161166991906137ac565b9081526020016040518091039020604051806101c0016040529081600082018054600181600116156101000203166002900480601f01602080910402602001604051908101604052809291908181526020018280546001816001161561010002031660029004801561171c5780601f106116f15761010080835404028352916020019161171c565b820191906000526020600020905b8154815290600101906020018083116116ff57829003601f168201915b5050505050815260200160018201805480602002602001604051908101604052809291908181526020016000905b82821015611806578382906000526020600020018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156117f25780601f106117c7576101008083540402835291602001916117f2565b820191906000526020600020905b8154815290600101906020018083116117d557829003601f168201915b50505050508152602001906001019061174a565b50505050815260200160028201805480602002602001604051908101604052809291908181526020016000905b828210156118ef578382906000526020600020018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156118db5780601f106118b0576101008083540402835291602001916118db565b820191906000526020600020905b8154815290600101906020018083116118be57829003601f168201915b505050505081526020019060010190611833565b505050508152602001600382018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156119905780601f1061196557610100808354040283529160200191611990565b820191906000526020600020905b81548152906001019060200180831161197357829003601f168201915b50505050508152602001600482018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015611a325780601f10611a0757610100808354040283529160200191611a32565b820191906000526020600020905b815481529060010190602001808311611a1557829003601f168201915b505050505081526020016005820160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020016006820160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200160078201805480602002602001604051908101604052809291908181526020016000905b828210156121b857838290600052602060002090600c020160405180610180016040529081600082018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015611bc95780601f10611b9e57610100808354040283529160200191611bc9565b820191906000526020600020905b815481529060010190602001808311611bac57829003601f168201915b50505050508152602001600182018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015611c6b5780601f10611c4057610100808354040283529160200191611c6b565b820191906000526020600020905b815481529060010190602001808311611c4e57829003601f168201915b50505050508152602001600282018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015611d0d5780601f10611ce257610100808354040283529160200191611d0d565b820191906000526020600020905b815481529060010190602001808311611cf057829003601f168201915b50505050508152602001600382018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015611daf5780601f10611d8457610100808354040283529160200191611daf565b820191906000526020600020905b815481529060010190602001808311611d9257829003601f168201915b50505050508152602001600482018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015611e515780601f10611e2657610100808354040283529160200191611e51565b820191906000526020600020905b815481529060010190602001808311611e3457829003601f168201915b505050505081526020016005820160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001600682018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015611f495780601f10611f1e57610100808354040283529160200191611f49565b820191906000526020600020905b815481529060010190602001808311611f2c57829003601f168201915b505050505081526020016007820160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001600882018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156120415780601f1061201657610100808354040283529160200191612041565b820191906000526020600020905b81548152906001019060200180831161202457829003601f168201915b50505050508152602001600982018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156120e35780601f106120b8576101008083540402835291602001916120e3565b820191906000526020600020905b8154815290600101906020018083116120c657829003601f168201915b50505050508152602001600a82018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156121855780601f1061215a57610100808354040283529160200191612185565b820191906000526020600020905b81548152906001019060200180831161216857829003601f168201915b50505050508152602001600b820160009054906101000a900460ff16151515158152505081526020019060010190611b0c565b5050505081526020016008820160009054906101000a900460ff1660028111156121de57fe5b60028111156121e957fe5b81526020016008820160019054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020016008820160159054906101000a900460ff161515151581526020016008820160169054906101000a900460ff16600181111561227c57fe5b600181111561228757fe5b8152602001600982018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156123245780601f106122f957610100808354040283529160200191612324565b820191906000526020600020905b81548152906001019060200180831161230757829003601f168201915b50505050508152602001600a820160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815250509050919050565b6000600360009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16146123e657600080fd5b6000151560008a6040516123fa91906137ac565b9081526020016040518091039020600b0160009054906101000a900460ff1615151461242557600080fd5b600015156124ec60008b60405161243c91906137ac565b90815260200160405180910390206009018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156124e15780601f106124b6576101008083540402835291602001916124e1565b820191906000526020600020905b8154815290600101906020018083116124c457829003601f168201915b505050505086612b5a565b1515146124f857600080fd5b8573ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161461253057600080fd5b6040518061018001604052808b81526020018a81526020018981526020018881526020018681526020013373ffffffffffffffffffffffffffffffffffffffff1681526020016040518060400160405280600481526020017f6e756c6c000000000000000000000000000000000000000000000000000000008152508152602001600360009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020016040518060400160405280600481526020017f6e756c6c0000000000000000000000000000000000000000000000000000000081525081526020018581526020018481526020016001151581525060008a60405161264d91906137ac565b90815260200160405180910390206000820151816000019080519060200190612677929190612bb3565b506020820151816001019080519060200190612694929190612bb3565b5060408201518160020190805190602001906126b1929190612bb3565b5060608201518160030190805190602001906126ce929190612bb3565b5060808201518160040190805190602001906126eb929190612bb3565b5060a08201518160050160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555060c082015181600601908051906020019061274f929190612bb3565b5060e08201518160070160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055506101008201518160080190805190602001906127b4929190612bb3565b506101208201518160090190805190602001906127d2929190612bb3565b5061014082015181600a0190805190602001906127f0929190612bb3565b5061016082015181600b0160006101000a81548160ff02191690831515021790555090505060018560405161282591906137ac565b908152602001604051809103902060070160008a60405161284691906137ac565b908152602001604051809103902090806001815401808255809150506001900390600052602060002090600c0201600090919091909150600082018160000190805460018160011615610100020316600290046128a4929190612da1565b50600182018160010190805460018160011615610100020316600290046128cc929190612da1565b50600282018160020190805460018160011615610100020316600290046128f4929190612da1565b506003820181600301908054600181600116156101000203166002900461291c929190612da1565b5060048201816004019080546001816001161561010002031660029004612944929190612da1565b506005820160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff168160050160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550600682018160060190805460018160011615610100020316600290046129d3929190612da1565b506007820160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff168160070160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555060088201816008019080546001816001161561010002031660029004612a62929190612da1565b5060098201816009019080546001816001161561010002031660029004612a8a929190612da1565b50600a820181600a019080546001816001161561010002031660029004612ab2929190612da1565b50600b820160009054906101000a900460ff1681600b0160006101000a81548160ff02191690831515021790555050508573ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff167f271db9f93dda5e21406a01862d32ef473f08d3eeb7bba3e66b80a8b05fc01f4d8b42604051612b41929190613a27565b60405180910390a3600190509998505050505050505050565b600081604051602001612b6d91906137ac565b6040516020818303038152906040528051906020012083604051602001612b9491906137ac565b6040516020818303038152906040528051906020012014905092915050565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f10612bf457805160ff1916838001178555612c22565b82800160010185558215612c22579182015b82811115612c21578251825591602001919060010190612c06565b5b509050612c2f9190612e28565b5090565b6040518061018001604052806060815260200160608152602001606081526020016060815260200160608152602001600073ffffffffffffffffffffffffffffffffffffffff16815260200160608152602001600073ffffffffffffffffffffffffffffffffffffffff1681526020016060815260200160608152602001606081526020016000151581525090565b604051806101c001604052806060815260200160608152602001606081526020016060815260200160608152602001600073ffffffffffffffffffffffffffffffffffffffff168152602001600073ffffffffffffffffffffffffffffffffffffffff1681526020016060815260200160006002811115612d3f57fe5b8152602001600073ffffffffffffffffffffffffffffffffffffffff16815260200160001515815260200160006001811115612d7757fe5b815260200160608152602001600073ffffffffffffffffffffffffffffffffffffffff1681525090565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f10612dda5780548555612e17565b82800160010185558215612e1757600052602060002091601f016020900482015b82811115612e16578254825591600101919060010190612dfb565b5b509050612e249190612e28565b5090565b612e4a91905b80821115612e46576000816000905550600101612e2e565b5090565b90565b600081359050612e5c81613ccf565b92915050565b600082601f830112612e7357600080fd5b8135612e86612e8182613ac8565b613a9b565b91508082526020830160208301858383011115612ea257600080fd5b612ead838284613c55565b50505092915050565b600060208284031215612ec857600080fd5b6000612ed684828501612e4d565b91505092915050565b600060208284031215612ef157600080fd5b600082013567ffffffffffffffff811115612f0b57600080fd5b612f1784828501612e62565b91505092915050565b60008060408385031215612f3357600080fd5b600083013567ffffffffffffffff811115612f4d57600080fd5b612f5985828601612e62565b925050602083013567ffffffffffffffff811115612f7657600080fd5b612f8285828601612e62565b9150509250929050565b600080600060608486031215612fa157600080fd5b600084013567ffffffffffffffff811115612fbb57600080fd5b612fc786828701612e62565b935050602084013567ffffffffffffffff811115612fe457600080fd5b612ff086828701612e62565b925050604061300186828701612e4d565b9150509250925092565b60008060008060008060008060006101208a8c03121561302a57600080fd5b60008a013567ffffffffffffffff81111561304457600080fd5b6130508c828d01612e62565b99505060208a013567ffffffffffffffff81111561306d57600080fd5b6130798c828d01612e62565b98505060408a013567ffffffffffffffff81111561309657600080fd5b6130a28c828d01612e62565b97505060608a013567ffffffffffffffff8111156130bf57600080fd5b6130cb8c828d01612e62565b96505060806130dc8c828d01612e4d565b95505060a08a013567ffffffffffffffff8111156130f957600080fd5b6131058c828d01612e62565b94505060c08a013567ffffffffffffffff81111561312257600080fd5b61312e8c828d01612e62565b93505060e08a013567ffffffffffffffff81111561314b57600080fd5b6131578c828d01612e62565b9250506101008a013567ffffffffffffffff81111561317557600080fd5b6131818c828d01612e62565b9150509295985092959850929598565b600061319d838361332a565b905092915050565b60006131b18383613525565b905092915050565b6131c281613b9e565b82525050565b6131d181613b9e565b82525050565b60006131e282613b14565b6131ec8185613b4f565b9350836020820285016131fe85613af4565b8060005b8581101561323a578484038952815161321b8582613191565b945061322683613b35565b925060208a01995050600181019050613202565b50829750879550505050505092915050565b600061325782613b1f565b6132618185613b60565b93508360208202850161327385613b04565b8060005b858110156132af578484038952815161329085826131a5565b945061329b83613b42565b925060208a01995050600181019050613277565b50829750879550505050505092915050565b6132ca81613bb0565b82525050565b6132d981613bb0565b82525050565b6132e881613c1f565b82525050565b6132f781613c1f565b82525050565b61330681613c31565b82525050565b61331581613c31565b82525050565b61332481613c43565b82525050565b600061333582613b2a565b61333f8185613b71565b935061334f818560208601613c64565b61335881613c97565b840191505092915050565b600061336e82613b2a565b6133788185613b82565b9350613388818560208601613c64565b61339181613c97565b840191505092915050565b60006133a782613b2a565b6133b18185613b93565b93506133c1818560208601613c64565b80840191505092915050565b60006101c08301600083015184820360008601526133eb828261332a565b9150506020830151848203602086015261340582826131d7565b9150506040830151848203604086015261341f82826131d7565b91505060608301518482036060860152613439828261332a565b91505060808301518482036080860152613453828261332a565b91505060a083015161346860a08601826131b9565b5060c083015161347b60c08601826131b9565b5060e083015184820360e0860152613493828261324c565b9150506101008301516134aa6101008601826132df565b506101208301516134bf6101208601826131b9565b506101408301516134d46101408601826132c1565b506101608301516134e96101608601826132fd565b50610180830151848203610180860152613503828261332a565b9150506101a083015161351a6101a08601826131b9565b508091505092915050565b6000610180830160008301518482036000860152613543828261332a565b9150506020830151848203602086015261355d828261332a565b91505060408301518482036040860152613577828261332a565b91505060608301518482036060860152613591828261332a565b915050608083015184820360808601526135ab828261332a565b91505060a08301516135c060a08601826131b9565b5060c083015184820360c08601526135d8828261332a565b91505060e08301516135ed60e08601826131b9565b50610100830151848203610100860152613607828261332a565b915050610120830151848203610120860152613623828261332a565b91505061014083015184820361014086015261363f828261332a565b9150506101608301516136566101608601826132c1565b508091505092915050565b600061018083016000830151848203600086015261367f828261332a565b91505060208301518482036020860152613699828261332a565b915050604083015184820360408601526136b3828261332a565b915050606083015184820360608601526136cd828261332a565b915050608083015184820360808601526136e7828261332a565b91505060a08301516136fc60a08601826131b9565b5060c083015184820360c0860152613714828261332a565b91505060e083015161372960e08601826131b9565b50610100830151848203610100860152613743828261332a565b91505061012083015184820361012086015261375f828261332a565b91505061014083015184820361014086015261377b828261332a565b9150506101608301516137926101608601826132c1565b508091505092915050565b6137a681613c15565b82525050565b60006137b8828461339c565b915081905092915050565b60006020820190506137d860008301846131c8565b92915050565b60006020820190506137f360008301846132d0565b92915050565b600060208201905061380e600083018461331b565b92915050565b6000608082019050818103600083015261382e8187613363565b905081810360208301526138428186613363565b9050613851604083018561331b565b61385e60608301846132d0565b95945050505050565b6000610160820190508181036000830152613882818e613363565b90508181036020830152613896818d613363565b905081810360408301526138aa818c613363565b90506138b9606083018b6131c8565b6138c6608083018a6131c8565b6138d360a08301896132ee565b6138e060c08301886131c8565b6138ed60e08301876132d0565b6138fb61010083018661330c565b81810361012083015261390e8185613363565b905061391e6101408301846131c8565b9c9b505050505050505050505050565b6000610180820190508181036000830152613949818f613363565b9050818103602083015261395d818e613363565b90508181036040830152613971818d613363565b90508181036060830152613985818c613363565b90508181036080830152613999818b613363565b90506139a860a083018a6131c8565b81810360c08301526139ba8189613363565b90506139c960e08301886131c8565b8181036101008301526139dc8187613363565b90508181036101208301526139f18186613363565b9050818103610140830152613a068185613363565b9050613a166101608301846132d0565b9d9c50505050505050505050505050565b60006040820190508181036000830152613a418185613363565b9050613a50602083018461379d565b9392505050565b60006020820190508181036000830152613a7181846133cd565b905092915050565b60006020820190508181036000830152613a938184613661565b905092915050565b6000604051905081810181811067ffffffffffffffff82111715613abe57600080fd5b8060405250919050565b600067ffffffffffffffff821115613adf57600080fd5b601f19601f8301169050602081019050919050565b6000819050602082019050919050565b6000819050602082019050919050565b600081519050919050565b600081519050919050565b600081519050919050565b6000602082019050919050565b6000602082019050919050565b600082825260208201905092915050565b600082825260208201905092915050565b600082825260208201905092915050565b600082825260208201905092915050565b600081905092915050565b6000613ba982613bf5565b9050919050565b60008115159050919050565b6000819050613bca82613ca8565b919050565b6000819050613bdd82613cb5565b919050565b6000819050613bf082613cc2565b919050565b600073ffffffffffffffffffffffffffffffffffffffff82169050919050565b6000819050919050565b6000613c2a82613bbc565b9050919050565b6000613c3c82613bcf565b9050919050565b6000613c4e82613be2565b9050919050565b82818337600083830152505050565b60005b83811015613c82578082015181840152602081019050613c67565b83811115613c91576000848401525b50505050565b6000601f19601f8301169050919050565b60038110613cb257fe5b50565b60028110613cbf57fe5b50565b60038110613ccc57fe5b50565b613cd881613b9e565b8114613ce357600080fd5b5056fea2646970667358221220214b3b15fd9c52eb0dbe33c6716d6610c7afcd24245a682e0e935ed6b2cefdcf64736f6c63430006060033";
        //s
        console.log(this.accountAddress);
        const tx = {
            from: this.accountAddress,
            value: '0',
            gas: 4700000,
            gasPrice: '0',
            nonce: this.web3.eth.getTransactionCount(this.accountAddress, 'pending'),
            chainId: 1515,
            data: encoded
        };
        return this.web3.eth.accounts.signTransaction(tx, this.privateKey);
    }
    get() {
        const contractTest = new this.web3.eth.Contract(laboratoire, "0x94Fc59c49a72621605Ff5b87613ba84a3073c1f6");
        let encoded = contractTest.methods.getContainer("1515").encodeABI();
        const tx = {
            to: "0x94Fc59c49a72621605Ff5b87613ba84a3073c1f6",
            value: '0',
            gas: 447940,
            gasPrice: '0',
            nonce: this.web3.eth.getTransactionCount(JSON.parse(localStorage.getItem("currentUser")).accountAddress, 'pending'),
            chainId: 1515,
            data: encoded
        };
        console.log(this.privateKey);
        return this.web3.eth.accounts.signTransaction(tx, this.privateKey);
    }
    addMedicament(data) {
        const contractTest = new this.web3.eth.Contract(laboratoire, this.smartContractAddress);
        let encoded = contractTest.methods.AddMedicament(data.numLot, data.newOwner, data.tagContainer, data.nameNewOwner).encodeABI();
        console.log(this.accountAddress);
        const tx = {
            to: this.smartContractAddress,
            value: '0',
            gas: 3000000,
            gasPrice: '0',
            nonce: this.web3.eth.getTransactionCount(JSON.parse(localStorage.getItem("currentUser")).accountAddress, 'pending'),
            chainId: 1515,
            data: encoded
        };
        return this.web3.eth.accounts.signTransaction(tx, this.key);
    }
    expandContainer(data) {
        let encoded = this.contract.methods.AssignmentContainer(data.addressTrasnporteur, data.tagContainer).encodeABI();
        var tx = {
            to: this.smartContractAddress,
            value: '0',
            gas: 4700000,
            gasPrice: '0',
            nonce: this.web3.eth.getTransactionCount(JSON.parse(localStorage.getItem("currentUser")).accountAddress, 'pending'),
            chainId: 1515,
            data: encoded
        };
        console.log(this.privateKey);
        return this.web3.eth.accounts.signTransaction(tx, this.key);
    }
    removeUser(data) {
        let encoded = this.contract.methods.removeUser(data.address).encodeABI();
        const tx = {
            to: this.smartContractAddress,
            value: '0',
            gas: 4000000,
            gasPrice: '0',
            nonce: this.web3.eth.getTransactionCount(JSON.parse(localStorage.getItem("currentUser")).accountAddress, 'pending'),
            chainId: 1515,
            data: encoded
        };
        console.log(this.privateKey);
        return this.web3.eth.accounts.signTransaction(tx, this.key);
    }
    data() {
        this.web3.eth.getTransactionReceipt('0x4f7dda82fcc24c84c35a7efff3c6b2bd4b21ef1577a793ed426e729b3a92bdfe').then(console.log);
    }
    deleteUser(data) {
        let encoded = this.contract.methods.removeUser(data.accountAddress).encodeABI();
        let tx = {
            to: this.smartContractAddress,
            value: '0',
            gas: 3000000,
            gasPrice: '0',
            nonce: this.web3.eth.getTransactionCount(JSON.parse(localStorage.getItem("currentUser")).accountAddress, 'pending'),
            chainId: 1515,
            data: encoded
        };
        return this.web3.eth.accounts.signTransaction(tx, this.key);
    }
    addGrossiste(data) {
        console.log(data);
        let encoded = this.contract.methods.AddPharmacie(data.name, data.id, data.accountAddress).encodeABI();
        console.log("smartContractAddress" + this.smartContractAddress);
        let tx = {
            to: this.smartContractAddress,
            value: '0',
            gas: 4000000,
            gasPrice: '0',
            nonce: this.web3.eth.getTransactionCount(JSON.parse(localStorage.getItem("currentUser")).accountAddress, 'pending'),
            chainId: 1515,
            data: encoded
        };
        return this.web3.eth.accounts.signTransaction(tx, this.key);
    }
    addTransporteur(data) {
        let encoded = this.contract.methods.AddTransporteur(data.name, data.id, data.accountAddress).encodeABI();
        const tx = {
            to: this.smartContractAddress,
            value: '0',
            gas: 4000000,
            gasPrice: '0',
            nonce: this.web3.eth.getTransactionCount(JSON.parse(localStorage.getItem("currentUser")).accountAddress, 'pending'),
            chainId: 1515,
            data: encoded
        };
        return this.web3.eth.accounts.signTransaction(tx, this.key);
    }
    addLaboratoire(data) {
        let encoded = this.contract.methods.AddLaboratoire(36, 35, data.email).encodeABI();
        var tx = {
            to: this.smartContractAddress,
            value: '0',
            gas: 4000000,
            gasPrice: '0',
            nonce: this.web3.eth.getTransactionCount(JSON.parse(localStorage.getItem("currentUser")).accountAddress, 'pending'),
            chainId: 1515,
            data: encoded
        };
        console.log(this.privateKey);
        return this.web3.eth.accounts.signTransaction(tx, this.privateKey);
    }
    getPrivateKey() {
        return this.privateKey;
    }
    getAccount() {
        return this.accountAddress;
    }
    getMnemonic() {
        return this.mnemonic;
    }
    // @ts-ignore
    getGasPrice() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return yield this.web3.eth.getGasPrice();
        });
    }
    verify(privateKey) {
        let data = this.web3.utils.utf8ToHex("hello");
        let signed_data = this.web3.eth.accounts.sign(data, privateKey);
        return signed_data; /*
        let v =signed_data.v;
          let r=signed_data.r;
          let s=signed_data.s;
          let l=this.web3.eth.accounts.recover({
            messageHash: signed_data.messageHash,
            v: v,
            r: r,
            s: s
          });
          console.log("result account"+l)
          console.log("account"+this.getAccount());
        
          console.log(JSON.stringify(signed_data))
          this.web3.eth.personal.ecRecover(data, signed_data.signature).then(console.log)
        */
    }
    getPublicKeyFromPrivateKey(privateKey) {
        return privateKeyToAddress(privateKey);
    }
    importMedicament(data) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let account = JSON.parse(localStorage.getItem("currentUser")).accountAddress;
            let change = false;
            let old = null;
            let encoded = this.contract.methods.ImportMedicament(data.name, data.numLot, data.serialNumber, data.dateExpiration, data.oldOwner, data.tagContainer, data.tx, data.oldTransactionHash, data.nameNewOwner).encodeABI();
            let tx = {
                to: this.smartContractAddress,
                value: '0',
                gas: 4000000,
                nonce: this.web3.eth.getTransactionCount(account, 'pending'),
                gasPrice: '0',
                chainId: 1515,
                data: encoded
            };
            return this.web3.eth.accounts.signTransaction(tx, this.key);
        });
    }
    send(transaction) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.generateAccount();
            let gas = yield transaction.estimateGas({ from: this.getAccount() });
            let options = {
                to: transaction._parent._address,
                data: transaction.encodeABI(),
                gas: gas
            };
            let signedTransaction = yield this.web3.eth.accounts.signTransaction(options, this.getPrivateKey());
            return yield this.web3.eth.sendSignedTransaction(signedTransaction.rawTransaction);
        });
    }
    checkAddress(address) {
        return this.web3.utils.isAddress(address);
    }
    getMedicaments() {
    }
}


/***/ }),

/***/ "./src/app/gurads-client/auth.gurad.ts":
/*!*********************************************!*\
  !*** ./src/app/gurads-client/auth.gurad.ts ***!
  \*********************************************/
/*! exports provided: AuthGuardClient */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuardClient", function() { return AuthGuardClient; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");





class AuthGuardClient {
    constructor(router, authenticationService) {
        this.router = router;
        this.authenticationService = authenticationService;
    }
    canActivate(route, state) {
        const currentUser = this.authenticationService.currentUserValue;
        if (currentUser && currentUser.roles[0] === "ROLE_PHARMACIE") {
            // logged in so return true
            return true;
        }
        else if (currentUser && currentUser.roles[0] === "ROLE_GROSSISTE") {
            this.router.navigate(['/dashboard/client']);
            return false;
        }
        console.log("root"); // not logged in so redirect to login page with the return url
        return false;
    }
}
AuthGuardClient.ngInjectableDef = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjectable"]({ factory: function AuthGuardClient_Factory() { return new AuthGuardClient(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵinject"](_service_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"])); }, token: AuthGuardClient, providedIn: "root" });


/***/ }),

/***/ "./src/app/gurads/auth.gurad.ts":
/*!**************************************!*\
  !*** ./src/app/gurads/auth.gurad.ts ***!
  \**************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");





class AuthGuard {
    constructor(router, authenticationService) {
        this.router = router;
        this.authenticationService = authenticationService;
    }
    canActivate(route, state) {
        const currentUser = this.authenticationService.currentUserValue;
        console.log(currentUser);
        if (!currentUser) {
            // logged in so return true
            this.router.navigate(['/auth/login']);
            return false;
        }
        // not logged in so redirect to login page with the return url
        return true;
    }
}
AuthGuard.ngInjectableDef = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjectable"]({ factory: function AuthGuard_Factory() { return new AuthGuard(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵinject"](_service_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"])); }, token: AuthGuard, providedIn: "root" });


/***/ }),

/***/ "./src/app/service/admin-service.service.ts":
/*!**************************************************!*\
  !*** ./src/app/service/admin-service.service.ts ***!
  \**************************************************/
/*! exports provided: AdminServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminServiceService", function() { return AdminServiceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");

class AdminServiceService {
}
AdminServiceService.ngInjectableDef = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ factory: function AdminServiceService_Factory() { return new AdminServiceService(); }, token: AdminServiceService, providedIn: "root" });


/***/ }),

/***/ "./src/app/service/auth.service.ts":
/*!*****************************************!*\
  !*** ./src/app/service/auth.service.ts ***!
  \*****************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../config */ "./src/app/config.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");





class AuthService {
    constructor(_http) {
        this._http = _http;
        this.currentUserSubject = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"](JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }
    get currentUserValue() {
        return this.currentUserSubject.value;
    }
    signin(data) {
        console.log(data);
        return this._http.post(_config__WEBPACK_IMPORTED_MODULE_2__["config"].apiUrl1 + "/api/auth/signin", data);
    }
    signup(data) {
        return this._http.post(_config__WEBPACK_IMPORTED_MODULE_2__["config"].apiUrl1 + "/api/auth/signup", data);
    }
    signup2(data, tx) {
        return this._http.post(_config__WEBPACK_IMPORTED_MODULE_2__["config"].apiUrl1 + "/api/auth/signup", data);
    }
    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.clear();
        this.currentUserSubject.next(null);
    }
    getUser(data) {
        return this._http.post(_config__WEBPACK_IMPORTED_MODULE_2__["config"].apiUrl1 + "/api/auth/user", data);
    }
}
AuthService.ngInjectableDef = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjectable"]({ factory: function AuthService_Factory() { return new AuthService(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"])); }, token: AuthService, providedIn: "root" });


/***/ }),

/***/ "./src/app/service/client.service.ts":
/*!*******************************************!*\
  !*** ./src/app/service/client.service.ts ***!
  \*******************************************/
/*! exports provided: ClientService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientService", function() { return ClientService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _ethereumProvider_ethereum__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../ethereumProvider/ethereum */ "./src/app/ethereumProvider/ethereum.ts");
/* harmony import */ var _message_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./message.service */ "./src/app/service/message.service.ts");
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../config */ "./src/app/config.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");








class ClientService {
    constructor(httpClient, web3js, msg) {
        this.httpClient = httpClient;
        this.web3js = web3js;
        this.msg = msg;
    }
    getAllDemand() {
        return this.httpClient.get(_config__WEBPACK_IMPORTED_MODULE_3__["config"].apiUrl1 + "/api/user/client");
    }
    activateAccount(data) {
        return this.httpClient.post(_config__WEBPACK_IMPORTED_MODULE_3__["config"].apiUrl1 + "/api/user/activateAccount", data);
    }
    getClient() {
        return this.httpClient.get(_config__WEBPACK_IMPORTED_MODULE_3__["config"].apiUrl1 + "/api/user/unblocked");
    }
    updateUser(id, user) {
        return this.httpClient.post(_config__WEBPACK_IMPORTED_MODULE_3__["config"].apiUrl1 + "/api/user/" + id, user);
    }
    getUserById(id) {
        return this.httpClient.get(_config__WEBPACK_IMPORTED_MODULE_3__["config"].apiUrl1 + "/api/user/" + id);
    }
    BlockedClient() {
        return this.httpClient.get(_config__WEBPACK_IMPORTED_MODULE_3__["config"].apiUrl1 + "/api/user/blocked");
    }
    blockClient(data) {
        return this.httpClient.post(_config__WEBPACK_IMPORTED_MODULE_3__["config"].apiUrl1 + "/api/user/blocked", data);
    }
    delete(id) {
        return this.httpClient.delete(_config__WEBPACK_IMPORTED_MODULE_3__["config"].apiUrl1 + "/api/user/client" + "/" + id.id);
    }
    removeUserFromBlockChian(data) {
        return this.httpClient.post(_config__WEBPACK_IMPORTED_MODULE_3__["config"].apiUrl1 + "/api/user/delete", data);
    }
}
ClientService.ngInjectableDef = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({ factory: function ClientService_Factory() { return new ClientService(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_ethereumProvider_ethereum__WEBPACK_IMPORTED_MODULE_1__["EthereumProvider"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_message_service__WEBPACK_IMPORTED_MODULE_2__["MessageService"])); }, token: ClientService, providedIn: "root" });


/***/ }),

/***/ "./src/app/service/command.service.ts":
/*!********************************************!*\
  !*** ./src/app/service/command.service.ts ***!
  \********************************************/
/*! exports provided: CommandService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommandService", function() { return CommandService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var app_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/config */ "./src/app/config.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");






class CommandService {
    constructor(_http) {
        this._http = _http;
    }
    // Open connection with the back-end socket
    addMedicament(data) {
        return this._http.post(app_config__WEBPACK_IMPORTED_MODULE_2__["config"].apiUrl1 + "/api/container/addMedicament", data);
    }
    addCommand(data) {
        console.log(data);
        return this._http.post(app_config__WEBPACK_IMPORTED_MODULE_2__["config"].apiUrl1 + "/api/command", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])((error) => Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(error.message)));
    }
    getContainerById(id) {
        return this._http.get(app_config__WEBPACK_IMPORTED_MODULE_2__["config"].apiUrl1 + "/api/container/" + id);
    }
    getCommandeById(id) {
        return this._http.get(app_config__WEBPACK_IMPORTED_MODULE_2__["config"].apiUrl1 + '/api/command/' + id);
    }
    createContainer(data) {
        return this._http.post(app_config__WEBPACK_IMPORTED_MODULE_2__["config"].apiUrl1 + "/api/container", data);
    }
    getCommand() {
        return this._http.get(app_config__WEBPACK_IMPORTED_MODULE_2__["config"].apiUrl1 + "/api/command/" + JSON.parse(localStorage.getItem("currentUser")).id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])((error) => Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(error.message)));
    }
    getAllCommand() {
        return this._http.get(app_config__WEBPACK_IMPORTED_MODULE_2__["config"].apiUrl1 + "/api/command/")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])((error) => Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(error.message)));
    }
    getAllContainer() {
        return this._http.get(app_config__WEBPACK_IMPORTED_MODULE_2__["config"].apiUrl1 + '/api/container')
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])((error) => Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(error.message)));
    }
    updateSensorContainer(data) {
        console.log(data);
        return this._http.post(app_config__WEBPACK_IMPORTED_MODULE_2__["config"].apiUrl1 + "/api/container/updateSeuil", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])((error) => Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(error.message)));
    }
    getTransactionDetails(tx) {
        return this._http.post(app_config__WEBPACK_IMPORTED_MODULE_2__["config"].apiUrl2 + "/api/getTransactionDetails", {
            transactionHash: tx
        });
    }
    getTransactionOriginDetails(tx) {
        return this._http.post(app_config__WEBPACK_IMPORTED_MODULE_2__["config"].apiUrl2 + "/api/getTransactionLaboDetails", {
            transactionHash: tx
        });
    }
    getContainerDetails(tx) {
        return this._http.post(app_config__WEBPACK_IMPORTED_MODULE_2__["config"].apiUrl2 + "/api/getContainerDetails", {
            transactionHash: tx
        });
    }
    getMedicament(numLot) {
        return this._http.post(app_config__WEBPACK_IMPORTED_MODULE_2__["config"].apiUrl2 + "/api/getMedicament", {
            numLot: numLot,
            contractAddress: JSON.parse(localStorage.getItem("currentUser")).smartContract
        });
    }
    getAllContainerExpand() {
        return this._http.get(app_config__WEBPACK_IMPORTED_MODULE_2__["config"].apiUrl1 + "/api/container/expand");
    }
    getCommandByPublicAdress() {
        return this._http.get(app_config__WEBPACK_IMPORTED_MODULE_2__["config"].apiUrl1 + "/api/command/client/" + JSON.parse(localStorage.getItem("currentUser")).accountAddress)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])((error) => Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(error.message)));
    }
    disableCommande(id) {
        return this._http.post(app_config__WEBPACK_IMPORTED_MODULE_2__["config"].apiUrl1 + "/api/command/disable", { id: id });
    }
    submitContract(data) {
        return this._http.post(app_config__WEBPACK_IMPORTED_MODULE_2__["config"].apiUrl1 + "/api/addCustomer", { transaction: data });
    }
    delete(deleteRequest) {
    }
}
CommandService.ngInjectableDef = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({ factory: function CommandService_Factory() { return new CommandService(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"])); }, token: CommandService, providedIn: "root" });


/***/ }),

/***/ "./src/app/service/globals.service.ts":
/*!********************************************!*\
  !*** ./src/app/service/globals.service.ts ***!
  \********************************************/
/*! exports provided: GlobalsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GlobalsService", function() { return GlobalsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");

class GlobalsService {
    constructor() {
    }
    getUser() {
        return this.user;
    }
    setUser(user) {
        this.user = user;
    }
}
GlobalsService.ngInjectableDef = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ factory: function GlobalsService_Factory() { return new GlobalsService(); }, token: GlobalsService, providedIn: "root" });


/***/ }),

/***/ "./src/app/service/localstorage.service.ts":
/*!*************************************************!*\
  !*** ./src/app/service/localstorage.service.ts ***!
  \*************************************************/
/*! exports provided: LocalstorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalstorageService", function() { return LocalstorageService; });
/* harmony import */ var _secure_storage_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./secure-storage.service */ "./src/app/service/secure-storage.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");



class LocalstorageService {
    constructor(storageService) {
        this.storageService = storageService;
    }
    // Set the json data to local storage
    setIem(key, data) {
        let key_encoded = this.storageService.encrypt(key);
        let value_encoded = this.storageService.encrypt(data);
        localStorage.setItem(key_encoded, value_encoded);
    }
    //sm
    getItem(key) {
        let val = this.storageService.encrypt(key);
        return this.storageService.decrypt(localStorage.getItem(val));
    }
    // Clear the local storage
    clearStorage() {
        localStorage.clear();
    }
}
LocalstorageService.ngInjectableDef = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ factory: function LocalstorageService_Factory() { return new LocalstorageService(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_secure_storage_service__WEBPACK_IMPORTED_MODULE_0__["StorageService"])); }, token: LocalstorageService, providedIn: "root" });


/***/ }),

/***/ "./src/app/service/message.service.ts":
/*!********************************************!*\
  !*** ./src/app/service/message.service.ts ***!
  \********************************************/
/*! exports provided: MessageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MessageService", function() { return MessageService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


class MessageService {
    constructor() {
        this.subject = new rxjs__WEBPACK_IMPORTED_MODULE_0__["Subject"]();
        this.removeSubject = new rxjs__WEBPACK_IMPORTED_MODULE_0__["Subject"]();
        this.commandeSubject = new rxjs__WEBPACK_IMPORTED_MODULE_0__["Subject"]();
        this.removeCommandeSubject = new rxjs__WEBPACK_IMPORTED_MODULE_0__["Subject"]();
        this.subjectProfile = new rxjs__WEBPACK_IMPORTED_MODULE_0__["Subject"]();
    }
    sendProfile(user) {
        this.subjectProfile.next({ user: user });
    }
    getProfile() {
        return this.subjectProfile.asObservable();
    }
    sendMessage(user) {
        this.subject.next({ user: user });
    }
    removeUser(id) {
        this.removeSubject.next({ id: id });
    }
    sendCommandeMessage(commande) {
        this.commandeSubject.next({ commande: commande });
    }
    removeCommande(id) {
        this.removeCommandeSubject.next({ id: id });
    }
    getCommandeToRemove() {
        return this.removeCommandeSubject.asObservable();
    }
    clearMessage() {
        this.subject.next();
    }
    getUserToRemove() {
        return this.removeSubject.asObservable();
    }
    getMessage() {
        return this.subject.asObservable();
    }
    getMessageCommande() {
        return this.commandeSubject.asObservable();
    }
}
MessageService.ngInjectableDef = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ factory: function MessageService_Factory() { return new MessageService(); }, token: MessageService, providedIn: "root" });


/***/ }),

/***/ "./src/app/service/notification.service.ts":
/*!*************************************************!*\
  !*** ./src/app/service/notification.service.ts ***!
  \*************************************************/
/*! exports provided: NotificationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationService", function() { return NotificationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");



class NotificationService {
    constructor(zone) {
        this.zone = zone;
    }
    /**
     * Subscribe to the teams update Server Sent Event stream
     */
    getUsers() {
        return new rxjs__WEBPACK_IMPORTED_MODULE_1__["Observable"]((observer) => {
            let url = 'http://localhost:8000/api/auth/demande';
            let eventSource = new EventSource(url);
            eventSource.onmessage = (event) => {
                let json = JSON.parse(event.data);
                if (json !== undefined && json !== '') {
                    this.zone.run(() => observer.next(json));
                }
            };
            eventSource.onerror = (error) => {
                if (eventSource.readyState === 0) {
                    console.log('The stream has been closed by the server.');
                    eventSource.close();
                    observer.complete();
                }
                else {
                    observer.error('EventSource error: ' + error);
                }
            };
        });
    }
    getCommande() {
        return new rxjs__WEBPACK_IMPORTED_MODULE_1__["Observable"]((observer) => {
            let url = 'http://localhost:8000/api/command/notification';
            let eventSource = new EventSource(url);
            eventSource.onmessage = (event) => {
                let json = JSON.parse(event.data);
                if (json !== undefined && json !== '') {
                    this.zone.run(() => observer.next(json));
                }
            };
            eventSource.onerror = (error) => {
                if (eventSource.readyState === 0) {
                    console.log('The stream has been closed by the server.');
                    eventSource.close();
                    observer.complete();
                }
                else {
                    observer.error('EventSource error: ' + error);
                }
            };
        });
    }
    getContainers() {
        return new rxjs__WEBPACK_IMPORTED_MODULE_1__["Observable"]((observer) => {
            let url = 'http://localhost:8000/api/container/notification';
            let eventSource = new EventSource(url);
            eventSource.onmessage = (event) => {
                let json = JSON.parse(event.data);
                if (json !== undefined && json !== '') {
                    this.zone.run(() => observer.next(json));
                }
            };
            eventSource.onerror = (error) => {
                if (eventSource.readyState === 0) {
                    console.log('The stream has been closed by the server.');
                    eventSource.close();
                    observer.complete();
                }
                else {
                    observer.error('EventSource error: ' + error);
                }
            };
        });
    }
}
NotificationService.ngInjectableDef = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ factory: function NotificationService_Factory() { return new NotificationService(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"])); }, token: NotificationService, providedIn: "root" });


/***/ }),

/***/ "./src/app/service/secure-storage.service.ts":
/*!***************************************************!*\
  !*** ./src/app/service/secure-storage.service.ts ***!
  \***************************************************/
/*! exports provided: StorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StorageService", function() { return StorageService; });
const SecureStorage = __webpack_require__(/*! secure-web-storage */ "./node_modules/secure-web-storage/secure-storage.js");
var crypto = __webpack_require__(/*! crypto */ "./node_modules/crypto-browserify/index.js");
class StorageService {
    constructor() { }
    encrypt(data) {
        const SECRET_KEY = localStorage.getItem("hash");
        let mykey = crypto.createCipher('aes-128-cbc', SECRET_KEY);
        let str = mykey.update(data, 'utf8', 'hex');
        str += mykey.final('hex');
        return str;
    }
    decrypt(data) {
        const SECRET_KEY = localStorage.getItem("hash");
        let mykey = crypto.createDecipher('aes-128-cbc', SECRET_KEY);
        return mykey.update(data, 'hex', 'utf8');
    }
}


/***/ }),

/***/ "./src/app/service/user-service.service.ts":
/*!*************************************************!*\
  !*** ./src/app/service/user-service.service.ts ***!
  \*************************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _ethereumProvider_ethereum__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../ethereumProvider/ethereum */ "./src/app/ethereumProvider/ethereum.ts");
/* harmony import */ var _message_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./message.service */ "./src/app/service/message.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");







class UserService {
    constructor(httpClient, web3js, msg) {
        this.httpClient = httpClient;
        this.web3js = web3js;
        this.msg = msg;
        let headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]({
            'Content-Type': 'application/json',
            "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT"
        });
    }
    addUser(email, address) {
        return this.httpClient.post("http://5.135.52.74:3000/register", ({ username: 'ddm', email: email, address: address, photo: "", password: "25" }));
    }
    getAllschoolarship() {
        return this.httpClient.get("http://5.135.52.74:3000/getAllSchoolarShip");
    }
    getAllFoyerType() {
        return this.httpClient.get("http://5.135.52.74:3000/getAllFoyerType");
    }
    getAllAbonnementType() {
        return this.httpClient.get("http://5.135.52.74:3000/getAllAbonnement");
    }
    login(email, signed_data) {
        return this.httpClient.post("http://5.135.52.74:3000/login", ({ username: 'ddm', email: email, signed_data: signed_data }));
    }
    uploadImg(data) {
        return this.httpClient.post("/api/users/uploadFile", data);
    }
}
UserService.ngInjectableDef = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjectable"]({ factory: function UserService_Factory() { return new UserService(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_ethereumProvider_ethereum__WEBPACK_IMPORTED_MODULE_1__["EthereumProvider"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_message_service__WEBPACK_IMPORTED_MODULE_2__["MessageService"])); }, token: UserService, providedIn: "root" });


/***/ }),

/***/ "./src/environments/environment.prod.ts":
/*!**********************************************!*\
  !*** ./src/environments/environment.prod.ts ***!
  \**********************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
const environment = {
    production: true,
    SECRET_KEY: "See License.txt in the project root for license information",
    NOTIFICATION_API: 'http//localhost:8000',
    firebase: {
        apiKey: "AIzaSyC-4kVJqPEkLMbntCA18Fh-gbgGyM8_rxE",
        authDomain: "supplychainlabo.firebaseapp.com",
        databaseURL: "https://supplychainlabo.firebaseio.com",
        projectId: "supplychainlabo",
        storageBucket: "supplychainlabo.appspot.com",
        messagingSenderId: "838218019602",
        appId: "1:838218019602:web:30684d430ceb3e65bab9cb",
        measurementId: "G-QP99M2L1MG"
    },
};


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
const environment = {
    production: true,
    firebase: {
        apiKey: "AIzaSyC-4kVJqPEkLMbntCA18Fh-gbgGyM8_rxE",
        authDomain: "supplychainlabo.firebaseapp.com",
        databaseURL: "https://supplychainlabo.firebaseio.com",
        projectId: "supplychainlabo",
        storageBucket: "supplychainlabo.appspot.com",
        messagingSenderId: "838218019602",
        appId: "1:838218019602:web:30684d430ceb3e65bab9cb",
        measurementId: "G-QP99M2L1MG"
    },
    NOTIFICATION_API: 'http://localhost:8000/api'
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _app_app_module_ngfactory__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module.ngfactory */ "./src/app/app.module.ngfactory.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */





if (_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["platformBrowser"]().bootstrapModuleFactory(_app_app_module_ngfactory__WEBPACK_IMPORTED_MODULE_3__["AppModuleNgFactory"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\YOUSSEF\Desktop\PFE\pfe-blockchain\PharmacieOfficine\ui-web\src\main.ts */"./src/main.ts");


/***/ }),

/***/ 1:
/*!**********************!*\
  !*** util (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 2:
/*!**********************!*\
  !*** util (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 3:
/*!************************!*\
  !*** buffer (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 4:
/*!************************!*\
  !*** crypto (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 5:
/*!********************!*\
  !*** fs (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 6:
/*!************************!*\
  !*** crypto (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 7:
/*!**********************!*\
  !*** util (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 8:
/*!**********************!*\
  !*** util (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map