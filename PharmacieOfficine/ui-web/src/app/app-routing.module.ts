import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {
  NbAuthComponent,
  NbResetPasswordComponent,
} from '@nebular/auth';
import {NgxLoginComponent} from './auth/login/login.component';
import {NgxRegisterComponent} from "./auth/register/ngx-register.component";
import {AuthGuard} from './gurads/auth.gurad';

const routes: Routes = [
  {
    path: 'dashboard',
    canActivate: [AuthGuard], // here we tell Angular to check the access with our AuthGuard
    loadChildren:()=>import("./dashboard/dashboard.module").then(m=>m.DashboardModule),
  },
  {

    path: 'auth',
    component: NbAuthComponent,
    children: [
      {
        path: '',
        component: NgxLoginComponent,
      },
      {
        path: 'login',
        component: NgxLoginComponent,
      },
      {
        path: 'register',
        component: NgxRegisterComponent,
      },
      {
        path: 'logout',
        component: NgxLoginComponent,
      },
      {
        path: 'request-password',
        component: NgxLoginComponent,
      },
      {
        path: 'reset-password',
        component: NbResetPasswordComponent,
      },
    ],
  },

  { path: '', redirectTo: 'dashboard', pathMatch: 'full' , canActivate: [AuthGuard],},
  { path: '**', redirectTo: 'dashboard' ,  canActivate: [AuthGuard] },

];






const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
