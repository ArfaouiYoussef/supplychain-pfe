
import { Component, OnInit } from '@angular/core';
import {LocalDataSource} from "ng2-smart-table";
import { ImageUploaderOptions, FileQueueObject } from 'ngx-image-uploader';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MedicamentService} from "../../../service/medicament.service";
import {ClassTh} from 'app/model/ClassTh';
import {Observable} from "rxjs";
import Swal from 'sweetalert2';
import {Router} from "@angular/router";
import {container_validation_messages} from 'app/dashboard/Validators/container_validation_message';
import {NgxSpinnerService} from "ngx-spinner";
import {CommandService} from "../../../service/command.service";
import {Medicament} from "../../../model/Medicament";
import * as moment from "moment";
import {EthereumProvider} from "../../../ethereumProvider/ethereum"; // add this 1 of 4
import { __await } from 'tslib';

@Component({
  selector: 'ngx-import-container',
  templateUrl: './import-container.component.html',
  styleUrls: ['./import-container.component.scss']
})
export class ImportContainerComponent implements OnInit {

  filteredItems:any;

  addContainer: FormGroup;
  class: ClassTh[];
  validation_messages: any;
  selectedFile: any;
  picture: any;
  isSuccess:boolean=false;
  errors: string[];
  valid: boolean=true;
  decodeAffectationTx: Object;
  decodeTx: Object;

  invalid:boolean=false;
  medicament:any[];
  containerDetails: { owner:any,tagContainer: any; seuilTemperature: any; seuilHumidite: any; stateTemperature: any; stateHumidite: any; };
  numberImportMedicament: number;
  hide:boolean=false;
  validContainer: boolean;
  constructor(private _formBuilder:FormBuilder,private _serviceMedicament:MedicamentService,
              private _route:Router,
              private _commandeService:CommandService,
              private _web3:EthereumProvider,
              private spinner:NgxSpinnerService){
    this.addContainer = this._formBuilder.group({
      transactionHash: ['', Validators.required],


    });

    this._serviceMedicament.getAllClass().subscribe(res=>{
      this.class=res;
    })



  }



  options: ImageUploaderOptions = {
    thumbnailHeight: 150,

    thumbnailWidth: 150,
    autoUpload:false,
    uploadUrl: 'http://some-server.com/upload',
    allowedImageTypes: ['image/png', 'image/jpeg'],
    maxImageSize: 3,
    fieldName:"image medicament",


  };





  ngOnInit(): void {
    this.validation_messages=container_validation_messages;

  }

  onSave() {




  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0]
    console.log(this.selectedFile);
    const uploadData = new FormData();
    uploadData.append('file', this.selectedFile, this.selectedFile.name);
    console.log(event);
    if (event.target.files.length > 0) {
      const file = event.target.files[0];


      const formData = new FormData();
      formData.append('file', file);
      this._serviceMedicament.upload(formData).subscribe((res:any)=>{
        this.picture=res.fileDownloadUri;
        this.addContainer.controls["photoUrl"].setValue(this.picture);


      })
    }


  }

  onReset() {

  }
  validate_txhash(event){
    this.valid= /^0x([A-Fa-f0-9]{64})$/.test(event.target.value);
    console.log(this.valid)
    if(event.target.value===''){
      this.errors=[]
    }
    if(!this.valid){
      this.errors=['invalide transaction hash']
    }
    else
      this.errors=[]
  }






  addContainers() {

  }
  tx:any;
  data:any;
  importContainer() {
    this.validContainer=true;
    this.invalid=false;
    this.hide=false;
    this.spinner.show().then(console.log)
    this.numberImportMedicament=0

    this._commandeService.getTransactionDetails(this.addContainer.controls["transactionHash"].value).subscribe((res:any)=>{
      this.spinner.hide();
      this.decodeTx=res.medicaments;

      this.containerDetails={
        tagContainer: res.events[0].args.tagContainer,
        seuilTemperature:res.seuilTemperature,
        seuilHumidite:res.seuilHumidite   ,
        stateTemperature:res.temprature,
        stateHumidite:res.humidite,
        owner:res.grossiste,


      }

      if(this.containerDetails.owner!=JSON.parse(localStorage.getItem("currentUser")).accountAddress){
        this.invalid=true;

        return;

      }
      this.tx={tx:this.addContainer.controls['transactionHash'].value}
      this.data={...this.containerDetails,...this.tx};

      this.isSuccess=true;

      this.containerDetails.stateTemperature.map(item=>{

        if( parseInt(item)>parseInt(this.containerDetails.seuilTemperature))
        {
          this.isSuccess=false;
          this.validContainer=false;
          Swal.fire(
            '<span style="color:black">Echec Importation!</span>',
            'Température  de conteneur  dépasse la valeur maximale !',
            'error'
          )
          return false;
        }


      });
      this.medicament=[];
      res.medicaments.map(item=>{
        let medicament= {
          name: item[0],
          amm:item[2],
          numLot: item[1],
          serialNumber: item[3],
          transactionHash:this.addContainer.controls["transactionHash"].value,
          dateExpiration: item[4],
          oldOwner: item[6],
          tagContainer: item[5],
          owner: item[7],
          oldTransactionHash:item[10],

          nameNewOwner:JSON.parse(localStorage.getItem('currentUser')).username
        }

        this.medicament.push(medicament);
        console.log(this.medicament)
      });
      console.log(this.medicament);
      let i=0;

      console.log(this.medicament.length)
      this.addToBlockChain(this.medicament[0],0);


    },error=>{
      this.errors=error;
      this.invalid=true;
      this.spinner.hide().then(console.log)

    });
  };



  checkDate(date){
    if(!!date)
    {let now= moment(new Date()).format("DD-MM-YYYY");
      let expireDate = moment(date, 'DD-MM-YYYY').toDate();


      return   moment(expireDate,'DD-MM-YYYY').isAfter(moment(now,"DD-MM-YYYY"));
    }
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );

  }
  addToBlockChain(item,indice){
    console.log("block"+indice)
    console.log(this.medicament)
    console.log(item)
    let i=indice;
    if(indice>this.medicament.length){
      this.hide=true;
      console.log("block")

      return;
    }
    if( this.checkDate(item.dateExpiration)){

      let data={...item,...this.tx};
      console.log(data)
      console.log("ssss")


      this._web3.importMedicament(data).then(  signed=>{
        console.log(this.numberImportMedicament);
        let rawtransaction={transaction:signed.rawTransaction}
        let request= {...item,...rawtransaction}
        this._serviceMedicament.addMedicaments(request).subscribe(  done=>{
          this.numberImportMedicament++;

          this.addToBlockChain(this.medicament[indice++],indice++)

        },error => {

          this.addToBlockChain(this.medicament[indice++],indice++)

        });
      });
    }
    else{
      this.addToBlockChain(this.medicament[++indice],indice++)

    }
  }

  onClose(){
    this.hide=!this.hide;


  }
}


