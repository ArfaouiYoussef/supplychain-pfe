import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContainerRoutingModule } from './container-routing.module';
import { ImportContainerComponent } from './import-container/import-container.component';
import { ListContainerComponent } from './list-container/list-container.component';
import {ContainerComponent} from "./container.component";
import {
  NbAccordionModule,
  NbActionsModule,
  NbAlertModule,
  NbButtonModule,
  NbCardModule,
  NbIconModule, NbInputModule, NbListModule,
  NbUserModule,
  NbProgressBarModule
} from "@nebular/theme";
import {NgxSpinnerModule} from "ngx-spinner";
import {ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";

@NgModule({
  declarations: [ImportContainerComponent,ContainerComponent, ListContainerComponent],
  imports: [
    CommonModule,
    ContainerRoutingModule,
    NbCardModule,
    NgxSpinnerModule,
    NbActionsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    NbAccordionModule,
    NbButtonModule,
    NbIconModule,
    NbUserModule,
    NbProgressBarModule,
    MatIconModule,
    NbListModule,
    NbAlertModule,
    NbInputModule
  ]
})
export class ContainerModule { }
