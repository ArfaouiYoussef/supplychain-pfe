import { Component, OnInit } from '@angular/core';
import {LocalDataSource} from "ng2-smart-table";
import { ImageUploaderOptions, FileQueueObject } from 'ngx-image-uploader';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MedicamentService} from "../../../service/medicament.service";
import {ClassTh} from 'app/model/ClassTh';
import {Observable} from "rxjs";
import {medicament_validation_messages} from 'app/dashboard/Validators/medicament_validation_messages';
import Swal from 'sweetalert2';
import {Router} from "@angular/router";

@Component({
  selector: 'ngx-add-medicament',
  templateUrl: './add-medicament.component.html',
  styleUrls: ['./add-medicament.component.scss']
})
export class AddMedicamentComponent implements OnInit {
  addMedicament: FormGroup;
  class: ClassTh[];
  validation_messages: any;
  selectedFile: any;
  picture: any;


  constructor(private _formBuilder:FormBuilder,private _serviceMedicament:MedicamentService,private _route:Router){
    this.addMedicament = this._formBuilder.group({
      name: ['', [Validators.required]],
      indication : ['', [Validators.required]],
      photoUrl:[''  ],
      amm:['',Validators.required],
      dosage:['', [Validators.required]],
      formegalenique:['', [Validators.required]],
      composition:['', [Validators.required]],
      classeTherapeutique:['', [Validators.required]],
      presentation:['', [Validators.required]],
      stock:['', [Validators.required]],

      marque:['', [Validators.required]],

      medicamentPrice:['', [Validators.required]],
photo:[''],
      type:['model'  ],
  });

this._serviceMedicament.getAllClass().subscribe(res=>{
  this.class=res;
})
  }



  options: ImageUploaderOptions = {
    thumbnailHeight: 150,

    thumbnailWidth: 150,
    autoUpload:false,
    uploadUrl: 'http://some-server.com/upload',
    allowedImageTypes: ['image/png', 'image/jpeg'],
    maxImageSize: 3,
    fieldName:"image medicament",


  };





  ngOnInit(): void {
    this.validation_messages=medicament_validation_messages;

  }

  onSave() {

    if (this.addMedicament.invalid) {
      console.log('invalid');
      Object.keys(this.addMedicament.controls).forEach(field => {
        const control = this.addMedicament.get(field);
        control.markAsTouched({onlySelf: true});
      });
    }else{

      this._serviceMedicament.addMedicament(this.addMedicament.value).subscribe(response=>{
       Swal.fire(
         'Success!',
         'Medicament Ajouté avec succès!',
         'success'
       )
this._route.navigate(["/dashboard/medicament/"])
      });
   return;
    }

      console.log(this.addMedicament.value) }

  onSelect(event) {
this.addMedicament.controls["photoUrl"].patchValue(event);
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0]
    console.log(this.selectedFile);
    const uploadData = new FormData();
   uploadData.append('file', this.selectedFile, this.selectedFile.name);
console.log(event);
if (event.target.files.length > 0) {
      const file = event.target.files[0];


      const formData = new FormData();
      formData.append('file', file);
      this._serviceMedicament.upload(formData).subscribe((res:any)=>{
        this.picture=res.fileDownloadUri;
        this.addMedicament.controls["photoUrl"].setValue(this.picture);


      })
    }


  }

  onReset() {

  }
}

