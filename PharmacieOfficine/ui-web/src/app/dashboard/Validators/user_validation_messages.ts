export const user_validation_messages = {
  'name': [
    {type: 'required', message: 'name is required'}
  ],
  'telephone': [
    {type: 'required', message: 'telephone is required'}],
  'email': [
    {type: 'required', message: 'email is required'}],
  'adresse': [
    {type: 'required', message: 'adresse is required'}]

};

