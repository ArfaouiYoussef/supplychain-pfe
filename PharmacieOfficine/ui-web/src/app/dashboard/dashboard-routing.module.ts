import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from "./dashboard.component";
import {HomeComponent} from "./home/home.component";

const routes: Routes = [

  {
  path: '',
  component: DashboardComponent,
  children: [

    {path:'import-container',
    loadChildren:()=>import('./container/container.module').then(m=>m.ContainerModule)},


      {path:'medicament',
      loadChildren:()=>import("./medicament/medicament.module").then(m=>m.MedicamentModule)
    },

    {path:"compte",
      loadChildren:()=>import ("./parametres/parametres.module").then(m=>m.ParametresModule)},
    {path:"",
    redirectTo:"import-container"}
  ],

}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
