import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ClassTh} from "../../../model/ClassTh";
import {MedicamentService} from "../../../service/medicament.service";
import {Router} from "@angular/router";
import {ImageUploaderOptions} from "ngx-image-uploader";
import {user_validation_messages} from "../../Validators/user_validation_messages";
import Swal from "sweetalert2";
import {ClientService} from "../../../service/client.service";
import {User} from "../../../model/user";
import {MessageService} from "../../../service/message.service";
import {NbToastrService} from "@nebular/theme";

@Component({
  selector: 'ngx-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  profileForm: FormGroup;
  class: ClassTh[];
  validation_messages: any;
  selectedFile: any;
  picture: any;
  publicKey: any;
  userDetails: any;
  oldValue: any;
  disable: boolean=true;


  constructor(private _formBuilder:FormBuilder,
              private _sharedService:MessageService,
              private _serviceMedicament:MedicamentService,
              private  _userService:ClientService,
              private toastr: NbToastrService,
              private _route:Router){
    this.userDetails=  JSON.parse(localStorage.getItem('currentUser'));

  this.publicKey=  JSON.parse(localStorage.getItem('currentUser')).accountAddress;
    this.profileForm = this._formBuilder.group({
      username: ['', [Validators.required]],
      photoUrl:[''  ],
      email:[''  ],
      telephone:[''],
      address:['']
    });

    this._serviceMedicament.getAllClass().subscribe(res=>{
      this.class=res;
    })
  }



  options: ImageUploaderOptions = {
    thumbnailHeight: 150,

    thumbnailWidth: 150,
    autoUpload:false,
    uploadUrl: 'http://some-server.com/upload',
    allowedImageTypes: ['image/png', 'image/jpeg'],
    maxImageSize: 3,
    fieldName:"image medicament",


  };


  Onchange(event){
this.disable=(JSON.stringify(this.profileForm.value)===JSON.stringify(this.oldValue));

    console.log()
  }



  ngOnInit(): void {
    this._userService.getUserById(this.userDetails.id).subscribe((res:any)=> {

      this.picture=!!res.photoUrl?res.photoUrl:'';

      this.profileForm.patchValue({
      username:!!res.username?res.username:'',
      email:!!res.email?res.email:'',
      address:!!res.address?res.address :'',
      telephone:!!res.telephone?res.telephone:'',
    })
    this.oldValue=  this.profileForm.value;

    })
    this.validation_messages=user_validation_messages;
  }

  onSave() {

this._userService.updateUser(this.userDetails.id,this.profileForm.value).subscribe(res=> {
  this._sharedService.sendProfile(this.profileForm.value);
  console.log(this.userDetails)

  this.oldValue=this.profileForm.value;
  this.userDetails.username=this.profileForm.value.username;
  this.userDetails.email=this.profileForm.value.email;
  this.userDetails.address=this.profileForm.value.address;
  this.userDetails.telephone=this.profileForm.value.telephone;
  localStorage.setItem("currentUser",JSON.stringify(this.userDetails));
console.log(this.userDetails)
  this.disable=true;
  Swal.fire(
    'Success!',
    'Mise a jour effectuee avec succes!',
    'success'
  )

})


      return;
    }



  onSelect(event) {
    this.profileForm.controls["photoUrl"].patchValue(event);
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0]
    console.log(this.selectedFile);
    const uploadData = new FormData();
    uploadData.append('file', this.selectedFile, this.selectedFile.name);
    console.log(event);
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      const formData = new FormData();
      formData.append('file', file);
      this._serviceMedicament.upload(formData).subscribe((res:any)=>{
        this.picture=res.fileDownloadUri;
        this.profileForm.controls["photoUrl"].setValue(this.picture);
        this.disable=false;
console.log(this
  .picture);

      })
    }


  }
  name:string;

  copyInputMessage(inputElement){
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
  }

copyText(val: string){
  let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  this.toastr.success('Copier dans le presse-papiers', '');

}
  onReset() {

  }
}

