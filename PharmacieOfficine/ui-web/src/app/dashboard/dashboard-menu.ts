import {NbMenuItem} from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [

  {
    title: 'Tableau de bord\n',
  },


  {
    title: 'Gestion Conteneurs ',
    icon: 'archive-outline',
    children: [
      {
        title: 'importation',
        link: '/dashboard/import-container',
      },

    ],
  }
  ,{
    title: 'Gestion Medicament',
    icon: 'clipboard-outline',
    children: [
      {
        title: 'Medicaments',
        link: '/dashboard/medicament',
      }
    ],
  },





  {
    title: 'Compte ',
    icon: 'settings-2-outline',
    children: [
      {
        title: 'Profile',
        link: '/dashboard/compte',
      }
    ],
  }

];
