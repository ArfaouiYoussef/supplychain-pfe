import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import {AuthService} from "./auth-service.service";
import {LocalstorageService} from "../service/localstorage.service";

@Injectable()
export class AuthGuard implements CanActivate {
test:any;
  constructor(private authService: AuthService, private router: Router,
              private localstorageService:LocalstorageService) {
  }

  canActivate(): boolean {
   let user= JSON.parse(this.localstorageService.getItem('currentUser'));
   console.log(user)
    if ( !user ) {

      this.router.navigate(['/auth/login']);
    return false;
    }
   else
    return true;

  }
}
