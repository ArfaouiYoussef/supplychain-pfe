import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject} from "rxjs";
import {User} from "../model/User";
import {config} from "../config";
import transaction from "ethereumjs-tx/dist/transaction";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
    currentUserSubject: any;
    currentUser:any;
  constructor(private _http:HttpClient) {
      this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
      this.currentUser = this.currentUserSubject.asObservable();

  }
    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }



signin(data){
      console.log(data)
    return this._http.post(config.apiUrl1+"/api/auth/signin",data);

}
  signup(data){

      return this._http.post<any>(config.apiUrl1+"/api/auth/signup",data);

}
  signup2(data,tx){

    return this._http.post<any>(config.apiUrl1+"/api/auth/signup",data);

  }
    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.clear();

        this.currentUserSubject.next(null);
    }
getUser(data){
    return this._http.post<any>(config.apiUrl1+"/api/auth/user",data);

}
}
