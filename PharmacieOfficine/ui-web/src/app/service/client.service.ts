import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from "@angular/common/http";
import {EthereumProvider} from "../ethereumProvider/ethereum";
import {MessageService} from "./message.service";
import {config} from "../config";
import {User} from "../model/User";
import {Observable} from 'rxjs';
import {map} from "rxjs/operators";
import {ActivateRequest} from "../dashboard/interface/ActivateRequest";
import {DeleteRequest} from "../dashboard/interface/DeleteRequest";

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  constructor(private  httpClient: HttpClient,
              private web3js: EthereumProvider,
              private msg: MessageService) {

  }

getAllDemand(): Observable<User[]>{
    return this.httpClient.get<User[]>(config.apiUrl1+"/api/user/client");
}

  activateAccount(data):Observable<Object>{

    return this.httpClient.post(config.apiUrl1+"/api/user/activateAccount",data)
  }
  getClient(): Observable<User[]>{
    return this.httpClient.get<User[]>(config.apiUrl1+"/api/user/unblocked");
  }
  updateUser(id,user):Observable<any>{

    return this.httpClient.post(config.apiUrl1+"/api/user/"+id,user);
  }

  getUserById(id): Observable<User[]>{
    return this.httpClient.get<User[]>(config.apiUrl1+"/api/user/"+id);
  }
  BlockedClient(): Observable<User[]>{
  return this.httpClient.get<User[]>(config.apiUrl1+"/api/user/blocked");
}


  blockClient(data){
    return this.httpClient.post(config.apiUrl1+"/api/user/blocked",data)
  }
  delete(id:DeleteRequest):Observable<Object>{

    return this.httpClient.delete<User>(config.apiUrl1+"/api/user/client"+ "/"+ id.id);
  }
removeUserFromBlockChian(data):Observable<any>
{
  return this.httpClient.post(config.apiUrl1+"/api/user/delete",data);
}

}

