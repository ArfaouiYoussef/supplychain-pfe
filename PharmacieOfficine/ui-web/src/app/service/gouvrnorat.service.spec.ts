import { TestBed } from '@angular/core/testing';

import { GouvrnoratService } from './gouvrnorat.service';

describe('GouvrnoratService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GouvrnoratService = TestBed.get(GouvrnoratService);
    expect(service).toBeTruthy();
  });
});
