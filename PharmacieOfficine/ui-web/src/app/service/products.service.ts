
import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private  httpClient: HttpClient) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT"
    });

  }
  updateProductAlbumById(productAlbum){
    return this.httpClient.post("/api/product_album/update_product_album",

    productAlbum)
  }

getAlbumById(id){
  return this.httpClient.post("/api/product_album/get_product_album_byId",

    {productAlbumId:id})
}
deleteAlbumById(id:any){
    return this.httpClient.post("/api/product_album/delete_product_album",

      {productAlbumId:id})

}
updateAlbumById(album){

  return this.httpClient.post("/api/product_album/update_product_albumById",

    album)

}




  addProduct(product){
    return this.httpClient.post("/api/products/add_product",
      product)
  }
  updateProductByStoreId(product){
    return  this.httpClient.post("/api/products/get_by_store_id",product)}

getProductByStore(storeId){
   return  this.httpClient.post("/api/products/get_by_store_id",{
      "store_id":storeId
    })}
updateProduct(product){

    return this.httpClient.post("/api/products/update_product_ById",product)

}



  getProductById(id){
    return  this.httpClient.post("/api/products/get_product_ById",{
      "productId":id
    })}


  uploadImg(data){


    return  this.httpClient.post<any>("/api/products/uploadProductImage", data
    );
  }
addAlbum(album){
    return this.httpClient.post("/api/product_album/add_product_album",album);

}
getAlbumsByProductId(id:any){
 return    this.httpClient.post("/api/product_album/get_product_album_by_productId",{"productId":id});

}
  uploadAlbumImg(data){


    return  this.httpClient.post<any>("/api/product_album/uploadProductAlbumImage", data
    );
  }
  deleteProduct(id: any) {
    return this.httpClient.post("/api/products/delete_product_ById",{
      "productId":id
    })

  }
}
