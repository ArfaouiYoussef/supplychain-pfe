import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {EthereumProvider} from "../ethereumProvider/ethereum";
import {MessageService} from "./message.service";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private  httpClient: HttpClient,
              private web3js:EthereumProvider,
              private msg:MessageService) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT"
    });

  }

  addUser(email,address){
    return    this.httpClient.post("http://5.135.52.74:3000/register",
      ({ username: 'ddm', email: email,address:address,photo:"", password: "25" }));}

getAllschoolarship(){
    return this.httpClient.get("http://5.135.52.74:3000/getAllSchoolarShip");


  }
  getAllFoyerType(){
    return this.httpClient.get("http://5.135.52.74:3000/getAllFoyerType");


  }
  getAllAbonnementType(){
    return this.httpClient.get("http://5.135.52.74:3000/getAllAbonnement");

  }


login(email,signed_data){
  return    this.httpClient.post("http://5.135.52.74:3000/login",
    ({ username: 'ddm', email: email,signed_data:signed_data }));}

  uploadImg(data){


    return  this.httpClient.post<any>("/api/users/uploadFile", data
   );
  }
}
