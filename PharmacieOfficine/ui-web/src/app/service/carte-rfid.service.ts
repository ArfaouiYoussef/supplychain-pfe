import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {config} from "../config";

@Injectable({
  providedIn: 'root'
})
export class CarteRfidService {

  constructor(private httpClient:HttpClient) { }
 getAllCard()
 {
return    this.httpClient.get(config.apiUrl1+"/api/tagRfid");

 }
getCardById(id){
    return this.httpClient.get(config.apiUrl1+"/api/tagRfid/"+id);
}
addTagCard(data){
    return  this.httpClient.post(config.apiUrl1+"/api/tagRfid",{tag:data})
}

  disableTagCard(data){
    return this.httpClient.post(config.apiUrl1+"/api/tagRfid/disable",{tag:data})
  }
removeTagCard(data){
    return this.httpClient.post(config.apiUrl1+"/api/tagRfid/delete", {id:data});
}

}
