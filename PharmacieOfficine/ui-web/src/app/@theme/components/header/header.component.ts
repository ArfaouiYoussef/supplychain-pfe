import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import { NbMediaBreakpointsService, NbMenuService, NbSidebarService, NbThemeService } from '@nebular/theme';
import { NB_WINDOW } from '@nebular/theme';

import { UserData } from '../../../@core/data/users';
import { LayoutService } from '../../../@core/utils';
import {filter, map, takeUntil} from 'rxjs/operators';
import {Subject, Subscription} from 'rxjs';
import {Router} from "@angular/router";
import {MessageService} from "../../../service/message.service";
import {AdminServiceService} from "../../../service/admin-service.service";
import {UserService} from "../../../service/user-service.service";

import {AuthService} from "../../../service/auth.service";
import {PushNotificationsService} from 'ng-push';
import {ClientService} from "../../../service/client.service";
import {NotificationService} from "../../../service/notification.service";
import {CommandService} from "../../../service/command.service";

export interface DialogData {
  animal: string;
  name: string;
}


@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {
role:boolean;
  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: any;
picture= 'https://www.samaauto.sn/img/logos/admin.png'
  themes = [
    {
      value: 'default',
      name: 'Light',
    },
    {
      value: 'dark',
      name: 'Dark',
    },
    {
      value: 'cosmic',
      name: 'Cosmic',
    },
    {
      value: 'corporate',
      name: 'Corporate',
    },
  ];

  currentTheme = 'default';
  item = [ { title: 'Profile' }, { title: 'Log out' } ];
  itemm=[];

  numberBag:string="0";
  message: any;
  subscription: Subscription;
  userDetails: any;
  list: any;
  users: any;
  clientNumber: any;
clientList:any=[];
  commandeNumber: any;
  commandeList: any=[];
  constructor(private sidebarService: NbSidebarService,
              private _auth:AuthService,
              private menuService: NbMenuService,
              private themeService: NbThemeService,
              private userService: UserData,
              private service:UserService,
              private clientService:ClientService,
              private layoutService: LayoutService,
              private adminService:AdminServiceService,@Inject(NB_WINDOW) private window,
              private router:Router,
              private commandeService:CommandService,
              private notificationService:NotificationService,
              private _pushNotifications: PushNotificationsService,
              private nbMenuService: NbMenuService,
              private breakpointService: NbMediaBreakpointsService,private messageService:MessageService) {
    this.userDetails = this.message = JSON.parse(localStorage.getItem("currentUser"));


//----------------


  }

  ngOnInit() {

    this.clientService.getUserById(this.userDetails.id).subscribe((response:any)=>{
      this.picture=response.photoUrl;
      console.log(this.picture)
      this.userDetails.username=response.username;
    })
    this.messageService.getProfile().subscribe(res=>{

      if(!!res )
{this.picture=res.user.photoUrl;
      this.userDetails.username=res.user.username;
console.log(res.user.photoUrl)}
    })
    this.role=this.userDetails.roles[0];




    this.nbMenuService.onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'my-context-menu'),
        map(({ item: { title } }) => title),
      )
      .subscribe(title => {
      switch(title){
        case"Profile":
        if(this.userDetails.roles[0]==="ROLE_GROSSISTE")  this.router.navigate(["/dashboard/compte"]);
        else this.router.navigate(["/dashboard-pharmacie/compte"]);
        break;
        case"Log out":this.logOut();break;

      }
      });

    this.message=this.userDetails;
    this.currentTheme = this.themeService.currentTheme;

    this.userService.getUsers()
      .pipe(takeUntil(this.destroy$))
      .subscribe((users: any) => this.user = users.nick);

    const { xl } = this.breakpointService.getBreakpointsMap();
    this.themeService.onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$),
      )
      .subscribe((isLessThanXl: boolean) => this.userPictureOnly = isLessThanXl);

    this.themeService.onThemeChange()
      .pipe(
        map(({ name }) => name),
        takeUntil(this.destroy$),
      )
      .subscribe(themeName => this.currentTheme = themeName);

  this.messageService.getMessage().subscribe((msg:any)=>{
 this.message=msg.token;
  console.log(msg)})
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  changeTheme(themeName: string) {
    this.themeService.changeTheme(themeName);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  navigateHome() {
    this.menuService.navigateHome();
    return false;
  }
  logOut(){
    this._auth.logout();
   this.router.navigate(['/auth/login']);
  }


}
