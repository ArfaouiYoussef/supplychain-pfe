/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from './@core/utils/analytics.service';
import { NbThemeService } from '@nebular/theme';
import { StorageService } from './service/secure-storage.service';
import {environment} from "../environments/environment.prod";
const { generateEncryptionKey, decrypt } = require('symmetric-encrypt')

var aes256 = require('aes256');
const Cryptr = require('cryptr');
const cryptr = new Cryptr(environment.SECRET_KEY);
var RSA = require('node-crypto-js').RSA;
var Crypt = require('node-crypto-js').Crypt;

@Component({
  selector: 'ngx-app',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit {

  constructor(private analytics: AnalyticsService,
              private secureStorageService:StorageService,
              private theme:NbThemeService) {



  }
  message;





  ngOnInit(): void {
    const userId = 'user001';
this.theme.changeTheme("cosmic");
    this.analytics.trackPageViews();

  }
}
