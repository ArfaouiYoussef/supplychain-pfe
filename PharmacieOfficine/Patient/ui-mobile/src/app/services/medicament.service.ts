import { Injectable } from '@angular/core';
import {config} from '../../config.js'
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MedicamentService {

  constructor(private _http:HttpClient) { }
apiUrl='http://51.178.143.238:8050/blockchain-service'
  getcontainers(data){
      return this._http.post("http://51.178.143.238:8050/blockchain-service/api/getMedicament",
      {
        contractAddress:data.smartContract,
numLot: data.numLot

      });
  }


    getTransactionDetails(tx){
        return this._http.post(this.apiUrl+"/api/getTransactionDetails",{
            transactionHash:tx

        })
    }


    getTransactionOriginDetails(tx){
        return this._http.post(this.apiUrl+"/api/getTransactionLaboDetails",{
            transactionHash:tx

        })
    }







    getContainerDetails(tx){
        return this._http.post(this.apiUrl+"/api/getContainerDetails",{
            transactionHash:tx

        })
    }

    getMedicament(numLot){
        return this._http.post(this.apiUrl+"/api/getMedicament",{
            numLot:numLot,
            contractAddress:JSON.parse(localStorage.getItem("currentUser")).smartContract
        })
    }

}
