import { MedicamentService } from './../../services/medicament.service';
import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {FCM} from '@ionic-native/fcm/ngx';
import {ToastController} from '@ionic/angular';
import generateHash from 'random-hash';
import {NgxSpinnerService} from 'ngx-spinner';
// @ts-ignore
var crypto = require('crypto');

@Component({
  selector: 'app-login',
  templateUrl: './check.medicament.component.html',
  styleUrls: ['./check.medicament.component.scss'],
})
export class CheckMedicamentComponent implements OnInit {

  medicamentData:FormGroup;

  loginForm: FormGroup;
  formData: FormData;
  humanizeBytes: Function;
  dragOver: boolean;
  jsonFile: any;
  isvalid: boolean;
  error: any;
    temperatureOrigine: string;
    humiditeOrigine: string;
    fabricants: string;
    transporteurFabricants: string;
    humiditeGr: string;
    temperatureGr: string;
    fabricantsGr: string;
    transporteurGr: string;
    commandeService: any;
    grossiste: any;
    dateExpiration: any;
    pharmacieAddress: any;
    serialNumber: any;
    PharmacieTx: any;
    lastTransactionHash: any;
    grossisteTxHash: any;

  /**
   * Constructor
   *
   * @param {FormBuilder} _formBuilder
   * @param _router
   * @param _web3
   * @param _auth
   */
   constructor(
      private _formBuilder: FormBuilder,
      private _router: Router,
      private medicamentService: MedicamentService,
      private _spinner:NgxSpinnerService,
      private toastController: ToastController,
  ) {

this.medicamentData=this._formBuilder.group({
  smartContract:['',Validators.required],
  numLot:['',Validators.required]
  
})


      // Configure the layout
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void
  {
  }

checkMedicament(){

  this.medicamentService.getcontainers(this.medicamentData.value).subscribe((res:any)=>{
    console.log(res);

    this.name=res.name;
this.dateExpiration=res.dateExpiration;
this.pharmacieAddress=res.newOwner;
this.serialNumber=res.serialNumber;
this.PharmacieTx=res.transactionHash;
this.lastTransactionHash=res.lastTransactionHash;
this.getMedicament(res);

  })




}


    getMedicament(res){
        this.humiditeOrigine='';
        this.temperatureOrigine='';
        this.fabricants='';
        this.transporteurFabricants='';
        this.humiditeGr='';
        this.temperatureGr='';
        this.fabricantsGr='';
        this.transporteurGr='';

            if(res.dateExpiration){
                this.grossisteTxHash=res.transactionHash;
                this.medicamentService.getTransactionDetails(res.transactionHash).subscribe((res:any)=>{
                    console.log("---------------grossiste - pharma--------------------")
                    this.humiditeGr=res.humidite;
                    this.temperatureGr=res.temprature;
                    this.transporteurGr=res.transporteur;
                    console.log(res)

                })

                this.medicamentService.getTransactionOriginDetails(res.lastTransactionHash).subscribe((res:any)=>{
                    this.humiditeOrigine=res.humidite;
                    this.temperatureOrigine=res.temprature;
                    this.fabricants=res.owner;
                    this.transporteurFabricants=res.transporteur;
                    this.grossiste=res.grossiste
                    console.log(res);
                })

            }

    }


}
