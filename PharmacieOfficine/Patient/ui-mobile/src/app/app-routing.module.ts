import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import {CheckComponent} from './patient/check.component';
import {CheckMedicamentComponent} from './patient/check/check.medicament.component';

const routes: Routes = [
 // { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
    {
        path: '',
        component: CheckComponent,
        children: [
            {
                path: '',
                component: CheckMedicamentComponent,
            },
            {
                path: 'login',
                component: CheckMedicamentComponent,
            }
        ]},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
