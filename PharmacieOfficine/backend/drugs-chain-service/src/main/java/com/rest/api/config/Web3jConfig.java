package com.rest.api.config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.http.HttpService;

import java.io.IOException;

@Configuration
public class Web3jConfig {
    @Value("${ethereum.node.address}")
    private String networkAdress;

    @Bean
    Web3j web3j() throws Exception {
        Web3j web3j = Web3j.build(new HttpService(networkAdress));
        System.out.println(TestConnection(web3j));

        return  web3j ;
    }

    String  TestConnection(Web3j web3j) throws IOException {
        return web3j.web3ClientVersion().send().getWeb3ClientVersion();
    }




}
