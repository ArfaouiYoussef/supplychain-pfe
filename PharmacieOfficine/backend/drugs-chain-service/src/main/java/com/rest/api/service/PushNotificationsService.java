package com.rest.api.service;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;

import com.rest.api.security.services.HeaderRequestInterceptor;
import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class  PushNotificationsService {

        private static final String FIREBASE_SERVER_KEY = "AAAAwymv8xI:APA91bHyzMlPdwlDxr8Ark7D4W46isn7gclvvbCEYDBJN-20SSl-LnZ9Pei1OPLThMgRxD0tX_LTKrGHoQQqkG7OirreUqyT2EMseytNrC88BhKLgo6HXbQryQk20_FSbIBA3y1VNuc0";
        private static final String FIREBASE_API_URL = "https://fcm.googleapis.com/fcm/send";

        @Async
        public CompletableFuture<String> send(HttpEntity<String> entity) {

                RestTemplate restTemplate = new RestTemplate();

                ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
                interceptors.add(new HeaderRequestInterceptor("Authorization", "key=" + FIREBASE_SERVER_KEY));
                interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json"));
                restTemplate.setInterceptors(interceptors);

                String firebaseResponse = restTemplate.postForObject(FIREBASE_API_URL, entity, String.class);

                return CompletableFuture.completedFuture(firebaseResponse);
        }
}
