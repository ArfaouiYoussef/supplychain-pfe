package com.rest.api.repository;

import com.rest.api.models.MedicamentInBasket;

import java.util.List;

public interface MongoTemplateRepository {
    // You will write your queries which will use mongoTemplate here.
    List<MedicamentInBasket> findByMedicamentIdAndUserId(String id,String userId);
}
