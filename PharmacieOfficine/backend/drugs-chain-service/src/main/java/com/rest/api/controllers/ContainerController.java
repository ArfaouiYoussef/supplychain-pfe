package com.rest.api.controllers;

import com.rest.api.models.*;
import com.rest.api.payload.request.ContainerRequest;
import com.rest.api.payload.response.MessageResponse;
import com.rest.api.rabbitMq.Beans.MessageListener;
import com.rest.api.repository.CommandeRepository;
import com.rest.api.repository.ContainerRepository;
import com.rest.api.repository.MedicamentRepository;
import com.rest.api.service.BlockChainService;
import com.rest.api.service.PushNotificationsService;
import com.rest.api.service.SendTransactionService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.EthGetTransactionReceipt;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;
import sun.security.util.Resources_zh_CN;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@RestController

@RequestMapping("/api/container")
public class ContainerController {
@Autowired
    CommandeRepository commandeRepository;
@Autowired
    ContainerRepository containerRepository;
    SendTransactionService sendTransactionService;
@Autowired
    MedicamentRepository medicamentRepository;
    @Autowired
    PushNotificationsService PushNotificationsService;
@Autowired
    BlockChainService  blockChainService;
    @Autowired(required = false)
    private JavaMailSender emailSender;



@PostMapping()
    public ResponseEntity addContainer(@Valid @RequestBody ContainerRequest containerRequest ) throws ExecutionException, InterruptedException, IOException {

if(commandeRepository.findById(containerRequest.getCommandeId()).isPresent() && !containerRepository.existsByTagContainer(containerRequest
.getTagContainer())){
    TransactionReceipt transactionReceipt=blockChainService.sendTransaction(containerRequest.getTransaction());
    if (!transactionReceipt.isStatusOK()) {
        return ResponseEntity.badRequest().body(new MessageResponse("Transaction Failed"));

    }
    if (!transactionReceipt.isStatusOK()) {
        return ResponseEntity.badRequest().body(new MessageResponse(transactionReceipt.toString()));

    }
    if (transactionReceipt.isStatusOK()) {
        Commande commande = commandeRepository.findById(containerRequest.getCommandeId()).get();

        Container container = new Container();
        container.setTagContainer(containerRequest.getTagContainer());
        container.setSeuilHumidite(containerRequest.getSeuilHumidite());
        container.setState(Status.DESACTIVER);
        container.setSeuilTemperature(containerRequest.getSeuilTemperature());
        container.setGrossisteAddress(containerRequest.getGrossisteAddress());
        container.setDestinationAddress(containerRequest.getDestinationAddress());
        container.setLaboAddress(containerRequest.getLaboAddress());
        container.setCommandeRef(containerRequest.getCommandeId());
        containerRepository.save(container);
        commande.getContainers().put(containerRequest.getTagContainer(),containerRequest.getCommandeId());
        return ResponseEntity.ok(new MessageResponse(transactionReceipt.getTransactionHash()));

    }
}
   return      ResponseEntity.badRequest().body(new MessageResponse("Conteneur existe "));


}
@PostMapping("/updateSeuil")
public ResponseEntity<?> updateSeuil(@RequestBody ContainerRequest containerRequest) throws InterruptedException, IOException, ExecutionException {
    if (containerRepository.findByTagContainer(containerRequest.getTagContainer()).isPresent()) {
        TransactionReceipt transactionReceipt=blockChainService.sendTransaction(containerRequest.getTransaction());
        if (!transactionReceipt.isStatusOK()) {
            return ResponseEntity.badRequest().body(new MessageResponse("Transaction Failed"));

        }

        if (!transactionReceipt.isStatusOK()) {
            return ResponseEntity.badRequest().body(new MessageResponse(transactionReceipt.toString()));

        }
        if (transactionReceipt.isStatusOK() ) {

            Container container = containerRepository.findByTagContainer(containerRequest.getTagContainer()).get();
            container.setSeuilTemperature(containerRequest.getSeuilTemperature());
            container.setSeuilHumidite(containerRequest.getSeuilHumidite());

            containerRepository.save(container);
            return ResponseEntity.ok(new MessageResponse(transactionReceipt.getTransactionHash()));
        }
        return ResponseEntity.badRequest().body(new MessageResponse("Transaction Failed"));
    }
    return ResponseEntity.badRequest().body(new MessageResponse("Container not found"));

}





@PostMapping("/addMedicament")
public  ResponseEntity<?> addMedicament(@RequestBody ContainerRequest containerRequest) throws ExecutionException, InterruptedException, IOException {
System.out.println(containerRequest);
  if(  containerRepository.findByCommandeRef(containerRequest.getCommandeId()).isPresent())
  {
      TransactionReceipt transactionReceipt=blockChainService.sendTransaction(containerRequest.getTransaction());
      if (!transactionReceipt.isStatusOK()) {
          return ResponseEntity.badRequest().body(new MessageResponse("Transaction Failed"));

      }

      if (!transactionReceipt.isStatusOK()) {
          return ResponseEntity.badRequest().body(new MessageResponse(transactionReceipt.toString()));

      }


      Container container = containerRepository.findByCommandeRef(containerRequest.getCommandeId()).get();
      if(container.getMedicaments().size()==0){
          Commande commande=commandeRepository.findById(container.getCommandeRef()).get();

          commande.setStatus(Status.Encours);
          commandeRepository.save(commande);


      }

      Medicament medicament1=containerRequest.getMedicament();
      Medicament medicament=new Medicament();
      medicament.setType("vente");
      medicament.setName(medicament1.getName());
      medicament.setDateExpiration(medicament1.getDateExpiration());
      medicament.setAmm(medicament1.getAmm());
      medicament.setSerialNumber(medicament1.getSerialNumber());
      medicament.setNumeroLot(containerRequest.getNumLot());
      medicament.setOriginTransaction(containerRequest.getOriginTransaction());
      Medicament model= medicamentRepository.findById(containerRequest.getMedicament().getId()).get();
      model.getNumeroLots().remove(containerRequest.getNumLot());
    System.out.println( model.getNumeroLots().indexOf(containerRequest.getNumLot()));
      medicamentRepository.save(model);
      medicamentRepository.save(medicament);
      container.getMedicaments().add(medicament);
      containerRepository.save(container);
      return ResponseEntity.ok(medicament);


  }

  else {
   return    ResponseEntity.badRequest().body(new MessageResponse("container Not Found"));
  }

}

    @PostMapping("/received")
    public ResponseEntity<?> receivedContainer(@RequestBody ContainerRequest  containerRequest) throws InterruptedException, IOException, ExecutionException, JSONException {
        if( containerRepository.findByTagContainer(containerRequest.getTagContainer()).isPresent()){

            TransactionReceipt transactionReceipt=blockChainService.sendTransaction(containerRequest.getTransaction());
            if (!transactionReceipt.isStatusOK()) {
                return ResponseEntity.badRequest().body(new MessageResponse("Transaction Failed"));

            }

            if (!transactionReceipt.isStatusOK()) {
                return ResponseEntity.badRequest().body(new MessageResponse("Transaction Failed"));

            }


            Container container=containerRepository.findByTagContainer(containerRequest.getTagContainer()).get();
            container.setTransactionInTransit(transactionReceipt.getTransactionHash());
            if(commandeRepository.findById(containerRequest.getCommandeId()).isPresent()){
                Commande commande=  commandeRepository.findById(containerRequest.getCommandeId()).get();
                commande.setStatus(Status.Received);
                commandeRepository.save(commande);
                SimpleMailMessage message = new SimpleMailMessage();

                message.setTo(commande.getEmail());
                message.setSubject("votre commande a été livrée avec Transaction hash: ");
                message.setText("Transaction Hash:  "+transactionReceipt.getTransactionHash());
                // Send Message
                this.emailSender.send(message);

            }

            container.setState(Status.Received);
            container.setTransporteurAddress(containerRequest.getAddressTrasnporteur());
            this.sendNotification(containerRequest.getAddressTrasnporteur(),containerRepository.save(container));

            return ResponseEntity.ok(new MessageResponse("success"));
        }
        return ResponseEntity.badRequest().body(new MessageResponse("failed"));

    }
@GetMapping("/notification")
SseEmitter Notification() {
    SseEmitter.SseEventBuilder eventBuilder = SseEmitter.event();
    SseEmitter emitter = new SseEmitter();

    SseEmitter sseEmitter = new SseEmitter(6 *0L);

    MessageListener.sses.put("containers", sseEmitter);

    return sseEmitter;

}

@GetMapping("/test")
public void test(){
Container container=new Container();
    try {
        this.sendNotification( "0x174A6c7457AceB5348CF9f0C95E53cF96beEAc2e",container);
    } catch (JSONException e) {
        e.printStackTrace();
    }

}
@PostMapping("/expand")
public ResponseEntity<?> expandContainer(@RequestBody ContainerRequest  containerRequest) throws InterruptedException, IOException, ExecutionException, JSONException {
   if( containerRepository.findByTagContainer(containerRequest.getTagContainer()).isPresent()){

       TransactionReceipt transactionReceipt=blockChainService.sendTransaction(containerRequest.getTransaction());
       if (!transactionReceipt.isStatusOK()) {
           return ResponseEntity.badRequest().body(new MessageResponse("Transaction Failed"));

       }

       if (!transactionReceipt.isStatusOK()) {
           return ResponseEntity.badRequest().body(new MessageResponse("Transaction Failed"));

       }
       if(commandeRepository.findById(containerRequest.getCommandeId()).isPresent()){
           Commande commande=  commandeRepository.findById(containerRequest.getCommandeId()).get();
           commande.setStatus(Status.Expedie);
           commande.setAffectationTransaction(transactionReceipt.getTransactionHash());
           SimpleMailMessage message = new SimpleMailMessage();

           message.setTo(commande.getEmail());
           message.setSubject("votre commande  affecter au transporteur hash transaction:");
           message.setText("Transaction Hash:  "+transactionReceipt.getTransactionHash());
           // Send Message
           this.emailSender.send(message);

           commandeRepository.save(commande);
       }

       Container container=containerRepository.findByTagContainer(containerRequest.getTagContainer()).get();

       container.setAffectationTransaction(transactionReceipt.getTransactionHash());


            container.setState(Status.Expedie);
this.sendNotification(containerRequest.getAddressTrasnporteur(),container);
container.setTransporteurAddress(containerRequest.getAddressTrasnporteur());
       containerRepository.save(container);
       return ResponseEntity.ok(new MessageResponse("success"));
   }
    return ResponseEntity.badRequest().body(new MessageResponse("failed"));


}
@GetMapping("/{id}")
    public ResponseEntity<?> getContainerByCommandeId(@PathVariable String id){
return ResponseEntity.ok(containerRepository.findByCommandeRef(id));

}
@PostMapping("/start")
public ResponseEntity<?> start(@RequestBody ContainerRequest containerRequest) throws InterruptedException, ExecutionException, IOException {

    if (containerRepository.findByTagContainer(containerRequest.getTagContainer()).isPresent()) {
        Container container = containerRepository.findByTagContainer(containerRequest.getTagContainer()).get();
        if (container.getTemperature().size() > 0 && container.getHumidity().size() > 0) {
            TransactionReceipt transactionReceipt=blockChainService.sendTransaction(containerRequest.getTransaction());
            if (!transactionReceipt.isStatusOK()) {
                return ResponseEntity.badRequest().body(new MessageResponse("Transaction Failed"));

            }




            // Send Message
     Commande commande=commandeRepository.findById(container.getCommandeRef()).get();
            SimpleMailMessage message = new SimpleMailMessage();

            message.setTo(commande.getEmail());
            message.setSubject("votre commande est en route hash transaction ");
            message.setText("Transaction Hash:  "+transactionReceipt.getTransactionHash());
            commande.setStatus(Status.InTransit);
          commande.setTransactionInTransit(transactionReceipt.getTransactionHash());
            commandeRepository.save(commande);

            container.setState(Status.InTransit);
            container.setTransactionInTransit(transactionReceipt.getTransactionHash());
            containerRepository.save(container);
            this.emailSender.send(message);
            return ResponseEntity.ok(container);
        }
        return ResponseEntity.badRequest().body(new MessageResponse("Temperature or Humidity not  found "));
    }
    return ResponseEntity.badRequest().body(new MessageResponse("Container  found "));
}

    @PostMapping("/end")
    public ResponseEntity<?> endDelivery(@RequestBody ContainerRequest containerRequest) throws InterruptedException, ExecutionException, IOException {

        if (containerRepository.findByTagContainer(containerRequest.getTagContainer()).isPresent()) {
            Container container = containerRepository.findByTagContainer(containerRequest.getTagContainer()).get();
            if (container.getTemperature().size() > 0 && container.getHumidity().size() > 0) {
                TransactionReceipt transactionReceipt=blockChainService.sendTransaction(containerRequest.getTransaction());
                if (!transactionReceipt.isStatusOK()) {
                    return ResponseEntity.badRequest().body(new MessageResponse("Transaction Failed"));

                }

                if (!transactionReceipt.isStatusOK()) {
                    return ResponseEntity.badRequest().body(new MessageResponse("Transaction Failed"));

                }

                // Create a Simple MailMessage.
                SimpleMailMessage message = new SimpleMailMessage();
                Commande commande=commandeRepository.findById(container.getCommandeRef()).get();

                message.setTo(commande.getEmail());
                message.setSubject("Votre commande est livrée transaction Hash");
                message.setText("Transaction Hash:  "+transactionReceipt.getTransactionHash());
                // Send Message

                commande.setStatus(Status.Received);
                commande.setTransactionReceived(transactionReceipt.getTransactionHash());
                commandeRepository.save(commande);
                container.setState(Status.Received);
                container.setTransactionReceived(transactionReceipt.getTransactionHash());
                containerRepository.save(container);
                this.emailSender.send(message);
                return ResponseEntity.ok(container);
           }
            return ResponseEntity.badRequest().body(new MessageResponse("Temperature or Humidity not  found "));
        }
        return ResponseEntity.badRequest().body(new MessageResponse("Container  found "));
    }



    @GetMapping("/transporteur/{id}")
    public ResponseEntity<?>getContainerTransporteur(@PathVariable String id){
    return  ResponseEntity.ok(containerRepository.findAllByTransporteurAddress(id));

}




    public boolean sendNotification(String topic,Container container) throws JSONException {

        JSONObject body = new JSONObject();
        body.put("to", "/topics/" + topic);
        body.put("priority", "high");

        JSONObject notification = new JSONObject();
        notification.put("title", "Affectation");
        notification.put("body", "nouveau conteneur disponible Tag:"+container.getTagContainer());

        JSONObject data = new JSONObject();
        data.put("tagContainer", container.getTagContainer() );
        data.put("container", container);
        data.put("alert", false);

        body.put("notification", notification);
        body.put("data", data);

        HttpEntity<String> request = new HttpEntity<>(body.toString());

        CompletableFuture<String> pushNotification = PushNotificationsService.send(request);
        CompletableFuture.allOf(pushNotification).join();

        try {
            String firebaseResponse = pushNotification.get();

            return true;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return false;
    }


@GetMapping("")
    public ResponseEntity<?>  GetAllData(){
    return  ResponseEntity.ok(containerRepository.findAll());
}
@GetMapping("/expand")
    public  ResponseEntity<? >GetExpandContainer(){
   return ResponseEntity.ok( containerRepository.findAllByState("Expedie"));
}

}



