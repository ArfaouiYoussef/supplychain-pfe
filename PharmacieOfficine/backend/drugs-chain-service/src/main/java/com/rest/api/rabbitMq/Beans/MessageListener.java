package com.rest.api.rabbitMq.Beans;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.rest.api.models.Container;
import com.rest.api.payload.response.ContainerResponse;
import com.rest.api.rabbitMq.ApplicationConfigReader;
import com.rest.api.repository.ContainerRepository;
import com.rest.api.service.ContainerNotificationService;
import com.rest.api.service.MessageSenderService;
import com.rest.api.service.PushNotificationsService;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

/**
 * Message Listener for RabbitMQ
 */
@Component
public class MessageListener {
    private static final Logger log = LoggerFactory.getLogger(MessageListener.class);
    @Autowired
    ApplicationConfigReader applicationConfigReader;
    @Autowired
    PushNotificationsService PushNotificationsService;
    public static final Map<String, SseEmitter> sses = new ConcurrentHashMap<>();

    class Messages {
        public String tagcontainer;
        private String sensorTemp;
        private String sensorHumidite;
        private String device;
        public String getTagcontainer() {
            return tagcontainer;
        }

        public void setTagcontainer(String tagcontainer) {
            this.tagcontainer = tagcontainer;
        }

        public String getSensorTemp() {
            return sensorTemp;
        }

        public void setSensorTemp(String sensorTemp) {
            this.sensorTemp = sensorTemp;
        }

        public String getSensorHumidite() {
            return sensorHumidite;
        }

        public void setSensorHumidite(String sensorHumidite) {
            this.sensorHumidite = sensorHumidite;
        }
    }
@Autowired
ContainerNotificationService containerNotificationService;
    @Autowired
    ContainerRepository containerRepository;
    @RabbitListener(queues = "${app1.queue.name}")
    public void recievedMessage(Message incomingMessage) throws JsonProcessingException, JSONException {
        byte[] body = incomingMessage.getBody();
        System.out.println("Recieved Message : " + new String(body));
        System.out.println("Event received");
        JsonObject jsonObject = new JsonParser().parse(new String(body)).getAsJsonObject();
        String tagContainer=  jsonObject.get("tagcontainer").toString().replaceAll("\"", "");
       tagContainer= tagContainer.substring(1,tagContainer.length()).toUpperCase();
        String sensorTemp=  jsonObject.get("sensorTemp").toString();
        String sensorHumidite=  jsonObject.get("sensorHumidite").toString();
        System.out.println("EventType: " +tagContainer.trim().length());
        if(!sensorTemp.equals("NaN") && !sensorHumidite.equals("NaN")) {
            ContainerResponse containerResponse = new ContainerResponse();
            containerResponse.setTagContainer(tagContainer.trim());
            containerResponse.setHumidite(Float.parseFloat(sensorHumidite));
            containerResponse.setTemperature(Float.parseFloat(sensorTemp));

            if (containerRepository.findByTagContainer(tagContainer.trim()).isPresent()) {
                Container container = containerRepository.findByTagContainer(tagContainer.trim()).get();
                if (!sensorTemp.equals("NaN") || !sensorHumidite.equals("NaN"))
                    if (Float.parseFloat(container.getSeuilHumidite()) < Float.parseFloat(sensorTemp) || Float.parseFloat(container.getSeuilHumidite()) < Float.parseFloat(sensorHumidite)) {
                        this.sendNotification(container.getTransporteurAddress(), container, sensorTemp, sensorHumidite);
                    }
                if (!container.getTemperature().contains(sensorTemp))
                { container.getTemperature().add(sensorTemp);}
                if (!container.getTemperature().contains(sensorHumidite))
                {  container.getHumidity().add(sensorHumidite);}
                containerRepository.save(container);
                System.out.println("Success");

            }
            containerNotificationService.sendMessage(containerResponse);

            System.out.println("EventType: " + tagContainer);
        }
    }

    public boolean sendNotification(String topic,Container container,String temperature,
                                   String humidite) throws JSONException {

        JSONObject body = new JSONObject();
        body.put("to", "/topics/" + topic);
        body.put("priority", "high");

        JSONObject notification = new JSONObject();
        notification.put("title", "Alert");

        notification.put("body", "Conteneur depasse le seuil maximum! Tag:"+container.getTagContainer()+" avec  Temperature"+temperature +" C"+"et Humidite  "+humidite+"%");

        JSONObject data = new JSONObject();
        data.put("tagContainer", container.getTagContainer() );
        data.put("container", container);
        data.put("temperature", temperature);
        data.put("humidite", humidite);
        data.put("alert", true);


        body.put("notification", notification);
        body.put("data", data);

        HttpEntity<String> request = new HttpEntity<>(body.toString());

        CompletableFuture<String> pushNotification = PushNotificationsService.send(request);
        CompletableFuture.allOf(pushNotification).join();

        try {
            String firebaseResponse = pushNotification.get();

            return true;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return false;
    }

}

