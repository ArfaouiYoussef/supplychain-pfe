package com.rest.api.payload.request;

import com.rest.api.models.Panier;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class CommandeRequest {

    private String userId;
    private String id;
private Panier panier;
private String addressCommande;
private String lat;
private String lng;
    private String publicAddress;
    private String telephone;
    private String name;
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPublicAddress() {
        return publicAddress;
    }

    public void setPublicAddress(String publicAddress) {
        this.publicAddress = publicAddress;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CommandeRequest{" +
                "userId='" + userId + '\'' +
                ", panier=" + panier +
                ", addressCommande='" + addressCommande + '\'' +
                ", lat='" + lat + '\'' +
                ", lng='" + lng + '\'' +
                '}';
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Panier getPanier() {
        return panier;
    }

    public void setPanier(Panier panier) {
        this.panier = panier;
    }

    public String getAddressCommande() {
        return addressCommande;
    }

    public void setAddressCommande(String addressCommande) {
        this.addressCommande = addressCommande;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
