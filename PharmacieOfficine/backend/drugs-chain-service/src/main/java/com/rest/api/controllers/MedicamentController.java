package com.rest.api.controllers;

import com.rest.api.models.ClasseTherapeutique;
import com.rest.api.models.Medicament;
import com.rest.api.payload.request.MedicamentRequest;
import com.rest.api.payload.response.MessageResponse;
import com.rest.api.repository.ClassTherapeutiqueRepository;
import com.rest.api.repository.MedicamentRepository;
import com.rest.api.service.BlockChainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.EthGetTransactionReceipt;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api/medicament")
public class MedicamentController {
    @Autowired
    MedicamentRepository medicamentRepository;
    @Autowired
    ClassTherapeutiqueRepository therapeutiqueRepository;
    FileController fileController;

    @Autowired
    BlockChainService blockChainService;


    @GetMapping()
    public ResponseEntity<?> getAll(){
       List<Medicament> medicamentList=medicamentRepository.findByType("model");
        medicamentList.addAll(medicamentRepository.findByType("importer"));
        return ResponseEntity.ok(medicamentList);
    }

    @GetMapping("/{id}")
    public  ResponseEntity<?> getById(@PathVariable String id){

        if(medicamentRepository.findById(id).isPresent())
        {
            Medicament medicament= medicamentRepository.findById(id).get();
            return ResponseEntity.ok(medicament);

        }
        else
            return ResponseEntity.badRequest().body(new MessageResponse("class not found"));
    }

    @PostMapping()
    public  ResponseEntity<?> addMedicament( @RequestBody MedicamentRequest medicamentRequest
    )  {
        TransactionReceipt   transactionReceipt = null;

        try {
            transactionReceipt = blockChainService.sendTransaction(medicamentRequest.getTransaction());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        if (!transactionReceipt.isStatusOK()) {
            return ResponseEntity.badRequest().body(new MessageResponse("Transaction failed"));

        }


        if (!medicamentRepository.findByName(medicamentRequest.getName()).isPresent()) {
            Medicament medicament = new Medicament();

            medicament.getNumeroLots().add(medicamentRequest.getNumLot());
            medicament.setName(medicamentRequest.getName());
            medicament.setStock(1);
            medicament.setType("importer");
            medicamentRepository.save(medicament);

            return ResponseEntity.ok(new MessageResponse("success"));

        } else {
            Medicament medicament = medicamentRepository.findByName(medicamentRequest.getName()).get();
            medicament.getNumeroLots().add(medicamentRequest.getNumLot());

            medicament.setStock(medicament.getStock() + 1);
            medicamentRepository.save(medicament);
            return ResponseEntity.ok(new MessageResponse("success"));
        }
    }

    @PutMapping()
    public  ResponseEntity<?> updateMedicament( @RequestBody MedicamentRequest medicamentRequest){

        if(therapeutiqueRepository.findById(medicamentRequest.getClasseTherapeutique()).isPresent()&&medicamentRepository.findById(medicamentRequest.getId()).isPresent())
        {
            ClasseTherapeutique classe= therapeutiqueRepository.findById(medicamentRequest.getClasseTherapeutique()).get();
            Medicament medicament=medicamentRepository.findById(medicamentRequest.getId()).get();
            medicament.setDosage(medicamentRequest.getDosage());
            medicament.setFormegalenique(medicamentRequest.getFormegalenique());
            medicament.setIndication(medicamentRequest.getIndication());
            medicament.setMarque(medicamentRequest.getMarque());
            medicament.setName(medicamentRequest.getName());
            medicament.setMedicamentPrice(medicamentRequest.getMedicamentPrice());
            medicament.setQuantity(medicamentRequest.getQuantity());
            medicament.setStock(medicamentRequest.getStock());
            medicament.setType(medicamentRequest.getType());
            medicament.setPresentation(medicamentRequest.getPresentation());
            medicament.setClasseTherapeutique(classe);
            medicament.setPhotoUrl(medicamentRequest.getPhotoUrl());

            medicament.setComposition(medicamentRequest.getComposition());
            medicamentRepository.save(medicament);
            return ResponseEntity.ok(new MessageResponse("update success"));

        }
        else
            return ResponseEntity.badRequest().body(new MessageResponse("class not found"));
    }






    @GetMapping("class/{id}/")
    public ResponseEntity<?> getByClassId(@PathVariable String id)
    {
        if( ! medicamentRepository.findByClasseTherapeutiqueId(id).isEmpty())
        {
            return ResponseEntity.ok(medicamentRepository.findByClasseTherapeutiqueId(id));
        }
        else {
            return ResponseEntity.badRequest().body(new MessageResponse("Medicament not found"));

        }



    }
    @DeleteMapping("/{id}")
    public  ResponseEntity<?> delete(@PathVariable String id){

        if(medicamentRepository.findById(id).isPresent())
        {
            medicamentRepository.deleteById(id);
            return ResponseEntity.ok(new MessageResponse("ok"));

        }
        else
            return ResponseEntity.badRequest().body(new MessageResponse("failed"));
    }


}




