package com.rest.api.payload.response;

public class PanierResponse {
	private String message;

	public PanierResponse(String message) {
	    this.message = message;
	  }

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
