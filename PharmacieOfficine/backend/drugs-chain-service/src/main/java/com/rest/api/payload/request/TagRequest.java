package com.rest.api.payload.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class TagRequest {

    @NotBlank
    private String tag;

    private  String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
