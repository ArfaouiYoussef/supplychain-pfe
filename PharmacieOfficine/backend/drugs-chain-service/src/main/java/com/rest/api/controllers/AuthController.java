package com.rest.api.controllers;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import javax.validation.Valid;

import com.rest.api.models.*;
import com.rest.api.payload.request.*;
import com.rest.api.payload.response.UserResponse;
import com.rest.api.repository.PanierRepository;
import com.rest.api.service.BlockChainService;
import com.rest.api.service.MessageSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import com.rest.api.payload.response.JwtResponse;
import com.rest.api.payload.response.MessageResponse;
import com.rest.api.repository.RoleRepository;
import com.rest.api.repository.UserRepository;
import com.rest.api.security.jwt.JwtUtils;
import com.rest.api.security.services.UserDetailsImpl;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.EthGetTransactionReceipt;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;
@RestController
@RequestMapping("/api/auth")

public class AuthController {

	@Autowired
	AuthenticationManager authenticationManager;
	@Autowired
	private  PasswordEncoder passwordEncoder;
@Autowired
	BlockChainService blockChainService;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;
@Autowired
	PanierRepository panierRepository;
	@Autowired
	MessageSenderService messageSenderService;



	public static final Map<String, SseEmitter> sses = new ConcurrentHashMap<>();
	private EthSendTransaction ethSendTransaction=null;
@PostMapping("/editPassword")
public ResponseEntity<?> resetPassword(@RequestBody  ResetPasswordRequest resetPasswordRequest){
	if(userRepository.findById(resetPasswordRequest.getId()).isPresent())
	{
	User user=	userRepository.findById(resetPasswordRequest.getId()).get();
		boolean result = passwordEncoder.matches(resetPasswordRequest.getOldPassword(), user.getPassword());

		if(result){
			user.setPassword(resetPasswordRequest.getNewPassword());
		return ResponseEntity.ok(new MessageResponse("success"));
		}
		return ResponseEntity.badRequest().body(new MessageResponse("bad password"));
	}

return ResponseEntity.badRequest().body(new MessageResponse("user not Found"));

}
	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
if(!userRepository.findByEmail(loginRequest.getEmail()).isPresent()){
	return  ResponseEntity.badRequest().body(new MessageResponse("Aucun compte trouvé pour cette adresse e-mail "));
}
		if(!userRepository.findByEmail(loginRequest.getEmail()).get().isAccountActive())
{
	return  ResponseEntity.badRequest().body(new MessageResponse("votre compte est désactiver contacter l'administration"));
}
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
System.out.println(jwt);
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		List<String> roles = userDetails.getAuthorities().stream()
				.map(item -> item.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok(new JwtResponse(jwt,
				userDetails.getId(),
				userDetails.getUsername(),
				userDetails.getEmail(),
userDetails.getSmartContract(),
				userDetails.getAccountAddress(),
				roles,userDetails.getPanier()));
	}


	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) throws InterruptedException, IOException, ExecutionException {
		AtomicBoolean contractExist= new AtomicBoolean(false);

		if (userRepository.existsByUsername(signUpRequest.getUsername())||userRepository.existsByAccountAddress(signUpRequest.getAccountAddress())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Username/accountAddress is already taken!"));
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Email is already in use!"));


		}

		// Create new user's account
		User
				user = new User();
		user.setUsername(signUpRequest.getUsername());
		user.setEmail(signUpRequest.getEmail());
		user.setAccountAddress(signUpRequest.getAccountAddress());
		user.setPassword(encoder.encode(signUpRequest.getPassword()));
		Set<String> strRoles = signUpRequest.getRoles();
		Set<Role> roles = new HashSet<>();

		if (strRoles == null) {
			Role userRole = roleRepository.findByName(ERole.ROLE_PHARMACIE)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			user.setAccountActive(false);
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {

					case "pharmacie":
						Role lab = roleRepository.findByName(ERole.ROLE_PHARMACIE)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(lab);
						user.setAccountActive(true);
						Panier paniers=	panierRepository.save(new Panier());
						user.setPanier(paniers);
						contractExist.set(true);





				}
			});
		}
		user.setRoles(roles);
		if(contractExist.get()) {


			TransactionReceipt transactionReceipt=blockChainService.sendTransaction(signUpRequest.getTransaction());
			if (!transactionReceipt.isStatusOK()) {
				return ResponseEntity.badRequest().body(new MessageResponse("Transaction Failed"));

			}

	if (!transactionReceipt.isStatusOK()) {
		return ResponseEntity.badRequest().body(new MessageResponse("Transaction Failed"));

	}
	user.setSmartContract(transactionReceipt.getContractAddress());
	user.setCreatedDate(new Date());
	userRepository.save(user);

	return ResponseEntity.ok(transactionReceipt);
}

messageSenderService.sendMessage(userRepository.save(user));


		return ResponseEntity.ok(user);

	}
	@CrossOrigin(value = "*",maxAge = 30000)
	@GetMapping("/demande")
	SseEmitter messages() {
		SseEmitter.SseEventBuilder eventBuilder = SseEmitter.event();
		SseEmitter emitter = new SseEmitter();

		SseEmitter sseEmitter = new SseEmitter(6 *0L);

		sses.put("4", sseEmitter);

		return sseEmitter;

	}



	@PostMapping("/user")
	public ResponseEntity<?> registerUser(@Valid @RequestBody GetUserRequest getUserRequest) {

		User user = userRepository.findByEmail(getUserRequest.getEmail()).
				orElseThrow(() -> new UsernameNotFoundException("User Not Found "));


		return ResponseEntity.ok(new UserResponse(user.getId(), user.getUsername(), user.getEmail(),
				user.getAccountAddress(),user.getPanier()
		));


	}
	@GetMapping()
	public ResponseEntity<?> getclientAll() {

		List<User> users = userRepository.findAll();

		return ResponseEntity.ok(users);
	}
	@GetMapping("/activeClient")
	public ResponseEntity<?> getclientActive() {
		Role role = roleRepository.findByName(ERole.ROLE_PHARMACIE)
				.orElseThrow(() -> new RuntimeException("Error: Role is not found."));

		List<User> users = userRepository.findByisAccountActiveAndRoles(true,role);

		return ResponseEntity.ok(users);
	}
	@GetMapping("/client")
	public ResponseEntity<?> getClient() {
		Role role = roleRepository.findByName(ERole.ROLE_PHARMACIE)
				.orElseThrow(() -> new RuntimeException("Error: Role is not found."));

		List<User> users = userRepository.findByisAccountActiveAndRoles(false,role);

		return ResponseEntity.ok(users);
	}
@GetMapping("/{id}")
public  ResponseEntity<?> getUserById(@PathVariable String id){

		if(userRepository.findById(id).isPresent())
		return 	ResponseEntity.ok(userRepository.findById(id).get());
else
	return  ResponseEntity.badRequest().body(new MessageResponse("user not found"));
	}

	@PostMapping("/{id}")
	public ResponseEntity<?> updateUser(@RequestBody UserRequest userRequest,@PathVariable String id){
		if(userRepository.findById(id).isPresent()){
			User user=userRepository.findById(id).get();
			user.setPhotoUrl(userRequest.getPhotoUrl());
			user.setAddress(userRequest.getAddress());
			user.setEmail(userRequest.getEmail());
			user.setUsername(userRequest.getUsername());
			user.setTelephone(userRequest.getTelephone());
			userRepository.save(user);
			return ResponseEntity.ok(new MessageResponse("success"));
		}
		else
			return ResponseEntity.badRequest().body(new MessageResponse("failed"));



	}
	@PostMapping("/delete")
	public ResponseEntity<?> deleteFromBlockChain(@RequestBody DeleteRequest deleteRequest ) throws InterruptedException, IOException, ExecutionException {
		if(userRepository.existsById(deleteRequest.getId())){



			TransactionReceipt transactionReceipt=blockChainService.sendTransaction(deleteRequest.getTransaction());
			if (!transactionReceipt.isStatusOK()) {
				return ResponseEntity.badRequest().body(new MessageResponse("Transaction Failed"));

			}



			userRepository.deleteById(deleteRequest.getId());
			return ResponseEntity.ok().body(new MessageResponse("success"));

		};

		return 	 	ResponseEntity.badRequest().body(new MessageResponse("user not found"));

	}
@DeleteMapping("/client/{id}")
		public ResponseEntity<?> delete(@PathVariable String id ){
			if(userRepository.existsById(id)){
						userRepository.deleteById(id);
					return ResponseEntity.ok().body(new MessageResponse("success"));

			};

			return 	 	ResponseEntity.badRequest().body(new MessageResponse("user not found"));

		}


	@GetMapping("/unblocked")
	public ResponseEntity<?> getClientUnBlocked(){
		Role grossisteRole = roleRepository.findByName(ERole.ROLE_PHARMACIE)
				.orElseThrow(() -> new RuntimeException("Error: Role is not found."));

		List<User> users=userRepository.findByisAccountActiveAndRoles(true,grossisteRole);

		return 	 	ResponseEntity.ok(users);

	}
	@GetMapping("/blocked")
	public ResponseEntity<?> getClientBlocked(){
		Role role = roleRepository.findByName(ERole.ROLE_PHARMACIE)
				.orElseThrow(() -> new RuntimeException("Error: Role is not found."));

		List<User> users=userRepository.findByisBlockedAndRoles(true,role);

		return 	 	ResponseEntity.ok(users);

	}
	@PostMapping("/blocked")
	public ResponseEntity<?> Block(@Valid @RequestBody BlockRequest blockRequest){
		if(userRepository.existsByAccountAddress(blockRequest.getAccountAddress())){
		User user=userRepository.findByAccountAddress(blockRequest.getAccountAddress()).get();
		user.setAccountActive(false);
		user.setBlocked(blockRequest.isBlock());
		userRepository.save(user);
			return 	 	ResponseEntity.ok().body(new MessageResponse("success"));

		};

		return 	 	ResponseEntity.badRequest().body(new MessageResponse("user not found"));

	}

	@PostMapping("/activateAccount")
	public ResponseEntity<?> activateAccount(@Valid @RequestBody ActivateRequest activateRequest) throws InterruptedException, IOException, ExecutionException {


		if(userRepository.existsByisBlockedAndId(false,activateRequest.getId())){
			if(userRepository.findById(activateRequest.getId()).isPresent()) {
				User user = userRepository.findById(activateRequest.getId()).get();
				user.setAccountActive(activateRequest.isActive());
				user.setBlocked(false);


				TransactionReceipt transactionReceipt=blockChainService.sendTransaction(activateRequest.getTransaction());
				if (!transactionReceipt.isStatusOK()) {
					return ResponseEntity.badRequest().body(new MessageResponse("Transaction Failed"));

				}
				user.setSmartContract(activateRequest.getSmartContract());

				userRepository.save(user);
				return ResponseEntity.ok(transactionReceipt);

			}

			else {
				return ResponseEntity.badRequest().body(new MessageResponse("user not found"));
			}

		}
		else if(userRepository.existsById(activateRequest.getId()))
		{
			User user= userRepository.findById(activateRequest.getId()).get();
			user.setBlocked(false);
			user.setAccountActive(true);
			userRepository.save(user);
			return ResponseEntity.ok(new MessageResponse("success"));
		}

		return 	 	ResponseEntity.badRequest().body(new MessageResponse("user not found"));

	}

}