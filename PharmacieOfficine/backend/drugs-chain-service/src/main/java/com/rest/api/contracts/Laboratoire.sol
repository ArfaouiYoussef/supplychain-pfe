pragma solidity >=0.4.21 <0.7.0;
pragma experimental ABIEncoderV2;
library SharedStruct{
enum ContainerState {
    ENCOURS,
    INTRANSIT,
    RECEIVED
  }
  struct Medicament {
    string id;
    string name;
    string numLot;
     string amm;
     string serialNumber;
     string dateExpiration;
     address grossiste;
     string tagContainer;
     address owner;

  }
  enum ROLE { LABORATOIRE, GROSSISTE ,TRANSPORTEUR,DEVICE

  }

struct User{
    string name;
    string id;
    ROLE role;
    bool exists;

}



   struct Container {
        string id;
        string name;
        uint256 dateImport;
        uint256 dateExport;
        string[] temprature;
        string[] humidite;
        address owner;
        address saler;
 Medicament[] medicaments;
 ContainerState state;
    address transporteur;
    address device;
    }

}
contract Laboratoire {


    mapping(string => SharedStruct.Medicament) public medicaments;
    mapping(string => SharedStruct.Container) public containers;
    mapping(address=>SharedStruct.User) public users;
event addMedicament(address indexed owner,address indexed saler,string numLot,uint256 time);
event addContainer(address indexed owner,address indexed saler,string tagContainer);
event assignmentContainer(address indexed owner,address indexed transporteur,string tagContainer);
event stateContainer(string tagContainer,string temprature,string humidite);
event addUser(address indexed users ,SharedStruct.ROLE role);


  address public owner;


  constructor() public {
    owner = msg.sender;
  }

  modifier restrictedTransporteur() {
    require (users[msg.sender].exists) ;
    _;
  }


  modifier restricted() {
    require (msg.sender == owner);
    _;
  }


function AddTransporteur(string memory name, string memory id,address address_user) public  restricted returns (bool){
users[address_user].id=id;
users[address_user].name=name;
users[address_user].role=SharedStruct.ROLE.TRANSPORTEUR;
emit addUser(address_user,SharedStruct.ROLE.TRANSPORTEUR);

}
function AddGrossiste(string memory name, string memory id,address address_user) public  restricted returns (bool){
users[address_user].id=id;
users[address_user].name=name;
users[address_user].role=SharedStruct.ROLE.GROSSISTE;
emit addUser(address_user,SharedStruct.ROLE.GROSSISTE);
return true;
}
function removeUser(address address_user) external restricted returns(bool){
    users[address_user].exists=false;
    return true;
}
function AddDevice(string memory name, string memory id,address address_user) public  restrictedTransporteur returns (bool){
users[address_user].id=id;
users[address_user].name=name;
users[address_user].role=SharedStruct.ROLE.DEVICE;
emit addUser(address_user,SharedStruct.ROLE.DEVICE);
return true;
}
function AddMedicament(string memory id,string  memory name ,string memory numLot,string memory amm,string memory serialNumber
,string memory dateExpiration,address grossiste,string memory tagContainer) public  returns(bool){
medicaments[numLot]=SharedStruct.Medicament(id,name,numLot,amm,serialNumber,dateExpiration,grossiste,tagContainer,msg.sender);
containers[tagContainer].medicaments.push(medicaments[numLot]);
 emit addMedicament( msg.sender,  grossiste, numLot, now);
     return true;
}
function AddContainer(string  calldata tagContainer,address saler) restricted external returns(bool){
containers[tagContainer].dateImport=now;
containers[tagContainer].saler=saler;
containers[tagContainer].state=SharedStruct.ContainerState.ENCOURS;
emit addContainer(msg.sender, saler, tagContainer);


}
function AssignmentContainer(address transporteur,string calldata tagContainer)restricted external returns(bool){
containers[tagContainer].state=SharedStruct.ContainerState.INTRANSIT;
containers[tagContainer].transporteur=transporteur;
emit assignmentContainer( owner, transporteur, tagContainer);
return true;
}
function StateContainer(string calldata tagContainer,string calldata temprature,string calldata humidite)external restrictedTransporteur returns(bool){
  containers[tagContainer].temprature.push(temprature);
  containers[tagContainer].humidite.push(humidite);
  containers[tagContainer].transporteur=msg.sender;
  containers[tagContainer].device=msg.sender;
  emit stateContainer(tagContainer,temprature,humidite);
}
 function getContainer(string memory tagContainer) public view returns(SharedStruct.Container memory record){
     return containers[tagContainer];
 }

}
