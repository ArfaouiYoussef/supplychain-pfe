package com.rest.api.payload.response;

import java.util.ArrayList;

public class ContainerResponse {

    private String tagContainer;
    private double temperature;
    private double humidite;

    public String getTagContainer() {
        return tagContainer;
    }

    public void setTagContainer(String tagContainer) {
        this.tagContainer = tagContainer;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getHumidite() {
        return humidite;
    }

    public void setHumidite(double humidite) {
        this.humidite = humidite;
    }
}
