package com.rest.api.payload.request;

import javax.validation.constraints.NotBlank;

public class DeleteRequest {
    private String id;
    @NotBlank
    private String transaction;

    private String accountAddress;

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public String getAccountAddress() {
        return accountAddress;
    }

    public void setAccountAddress(String accountAddress) {
        this.accountAddress = accountAddress;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
