package com.rest.api.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Document(collection = "medicamentInBasket")
public class MedicamentInBasket {
@Id
private String id;
    private String medicamentId;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
private String userId;
    private String name;
    private Number medicamentPrice;
    private Number totalMedicamentPrice;
    private Number quantity =1;

    public Number getDispo() {
        return dispo;
    }

    public void setDispo(Number dispo) {
        this.dispo = dispo;
    }

    private Number dispo=0;
    @NotBlank
    private String  indication;
    @NotBlank
    private String  composition;
    @NotBlank
    @Size(max = 20)
    private String dosage;
    @NotBlank
    @Size(max = 120)
    private String formegalenique;
    @DBRef
    private ClasseTherapeutique classeTherapeutique;
    private String presentation;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    private String marque;

    private String photoUrl;

    public String getMedicamentId() {
        return medicamentId;
    }

    public void setMedicamentId(String medicamentId) {
        this.medicamentId = medicamentId;
    }


    public Number getMedicamentPrice() {
        return medicamentPrice;
    }

    public void setMedicamentPrice(Number medicamentPrice) {
        this.medicamentPrice = medicamentPrice;
    }

    public Number getTotalMedicamentPrice() {
        return totalMedicamentPrice;
    }

    public void setTotalMedicamentPrice(Number totalMedicamentPrice) {
        this.totalMedicamentPrice = totalMedicamentPrice;
    }

    public Number getQuantity() {
        return quantity;
    }

    public void setQuantity(Number quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndication() {
        return indication;
    }

    public void setIndication(String indication) {
        this.indication = indication;
    }

    public String getComposition() {
        return composition;
    }

    public void setComposition(String composition) {
        this.composition = composition;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getFormegalenique() {
        return formegalenique;
    }

    public void setFormegalenique(String formegalenique) {
        this.formegalenique = formegalenique;
    }

    public ClasseTherapeutique getClasseTherapeutique() {
        return classeTherapeutique;
    }

    public void setClasseTherapeutique(ClasseTherapeutique classeTherapeutique) {
        this.classeTherapeutique = classeTherapeutique;
    }

    public String getPresentation() {
        return presentation;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    @Override
    public String toString() {
        return "MedicamentInBasket{" +
                "id='" + id + '\'' +
                ", medicamentId='" + medicamentId + '\'' +
                ", userId='" + userId + '\'' +
                ", name='" + name + '\'' +
                ", medicamentPrice=" + medicamentPrice +
                ", totalMedicamentPrice=" + totalMedicamentPrice +
                ", quantity=" + quantity +
                ", dispo=" + dispo +
                ", indication='" + indication + '\'' +
                ", composition='" + composition + '\'' +
                ", dosage='" + dosage + '\'' +
                ", formegalenique='" + formegalenique + '\'' +
                ", classeTherapeutique=" + classeTherapeutique +
                ", presentation='" + presentation + '\'' +
                ", marque='" + marque + '\'' +
                ", photoUrl='" + photoUrl + '\'' +
                '}';
    }
}