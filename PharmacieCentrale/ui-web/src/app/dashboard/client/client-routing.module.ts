import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ClientComponent} from "./client.component";
import {AddClientComponent} from "./add-client/add-client.component";
import {ListClientComponent} from "./list-client/list-client.component";
import {UpdateClientComponent} from "./update-client/update-client.component";
import {AuthGuard} from "../../gurads/auth.gurad";
import {NotFoundComponent} from "../../not-found/not-found.component";
import {ListApprobationComponent} from "./list-approbation/list-approbation.component";
import {BlocageComponent} from "./blocage/blocage.component";

const routes: Routes = [
  {
    path:'',
    component:ClientComponent,
    children:[
      {
        path:'',
        component:ListClientComponent,
      },
      {
        path:'ajouter',
        component:AddClientComponent,
      },
      {
        path:":id/modifier",
        component:UpdateClientComponent
      },{
        path: 'approbation',
        component: ListApprobationComponent,
      }
      ,{
        path: 'blocage',
        component: BlocageComponent,
      }

      ]



},
  { path: 'notfound', component: NotFoundComponent  },
  { path: '**', redirectTo: 'notfound'  }



];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule { }
