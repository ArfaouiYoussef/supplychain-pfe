import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import {ThemeModule} from "../@theme/theme.module";
import {
    NbButtonModule,
    NbCardModule,
    NbIconModule,
    NbMenuModule,
    NbProgressBarModule,
    NbTabsetModule
} from "@nebular/theme";
import {HomeComponent} from "./home/home.component";
import {MatIconModule} from "@angular/material/icon";
import {NgxChartsModule} from "@swimlane/ngx-charts";

@NgModule({
  declarations: [DashboardComponent,HomeComponent],
    imports: [
        CommonModule,
        DashboardRoutingModule,
        ThemeModule,
        NbMenuModule,
        NbCardModule,
        NbButtonModule,
        NbProgressBarModule,
        MatIconModule,
        NbIconModule,
        NgxChartsModule,
        NbTabsetModule,

    ]
})
export class DashboardModule { }
