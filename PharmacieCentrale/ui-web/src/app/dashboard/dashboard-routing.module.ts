import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from "./dashboard.component";
import {AddClientComponent} from "./client/add-client/add-client.component";
import {LivraisonComponent} from "./commande/livraison/livraison.component";
import {HomeComponent} from "./home/home.component";

const routes: Routes = [

  {
  path: '',
  component: DashboardComponent,
  children: [

    {path:'',
    component:HomeComponent},
    {path:"carte-rfid",
      loadChildren:()=>import ("./carte-rfid/carte-rfid.module").then(m=>m.CarteRfidModule)},
    {path:'transporteur',
      loadChildren:()=>import("./transporteur/transporteur.module").then(m=>m.TransporteurModule)
    },
    {path:'client',
loadChildren:()=>import("./client/client.module").then(m=>m.ClientModule)
    },
      {path:'medicament',
      loadChildren:()=>import("./medicament/medicament.module").then(m=>m.MedicamentModule)
    },
    {path:'commande',
    loadChildren:()=>import ("./commande/commande.module").then(m=>m.CommandeModule)},
    {path:'suivi',
      loadChildren:()=>import ("./suivi/suivi.module").then(m=>m.SuiviModule)},
    {path:"compte",
      loadChildren:()=>import ("./parametres/parametres.module").then(m=>m.ParametresModule)},
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
