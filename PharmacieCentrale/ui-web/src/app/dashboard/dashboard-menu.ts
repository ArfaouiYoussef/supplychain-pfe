import {NbMenuItem} from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [

  {
    title: 'Tableau de bord\n',
  },
  {
    title: 'Gestion Carte RFID',
    icon: 'credit-card-outline',
  children:[
    {
      title: 'Cartes RFID',
      link: '/dashboard/carte-rfid',
    }
  ]
  },
  {
    title: 'Gestion Transporteur',
    icon: 'car-outline',
    children: [

      {
        title: 'Transporteur',
        link: '/dashboard/transporteur',

      },
      {
        title: 'Demande Approbation',
        link: '/dashboard/transporteur/approbation',

      },
      {
        title: 'Liste  Blocage',
        link: '/dashboard/transporteur/blocage',

      },

    ],
  },
  {
    title: 'Gestion client',
    icon: 'people-outline',
    children: [

      {
        title: 'Client',
        link: '/dashboard/client',

      },
      {
        title: 'Demande Approbation',
        link: '/dashboard/client/approbation',

      },
      {
        title: 'Liste  Blocage',
        link: '/dashboard/client/blocage',

      },

    ],
  }
  ,
  {
    title: 'Gestion Medicament',
    icon: 'clipboard-outline',
    children: [
      {
        title: 'Medicaments',
        link: '/dashboard/medicament',
      },
      {
        title: 'Classe Therapeutique',
        link: '/dashboard/medicament/classe-therapeutique',
      }
    ],
  },

  {
    title: 'Gestion commande',
    icon: 'shopping-cart-outline',
    children: [
      {
        title: 'commande',
        link: '/dashboard/commande',
      }
    ],
  }
  ,{
    title: 'Suivi',
    icon: 'navigation-2-outline',
    children: [
      {
        title: 'Conteneur',
        link: '/dashboard/suivi',
      }
    ],
  },
  {
    title: 'Compte ',
    icon: 'settings-2-outline',
    children: [
      {
        title: 'Profile',
        link: '/dashboard/compte',
      }
    ],
  }

];
