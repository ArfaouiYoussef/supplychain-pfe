import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MedicamentComponent} from "./medicament.component";
import {AddMedicamentComponent} from "./add-medicament/add-medicament.component";
import {ListMedicamentComponent} from "./list-medicament/list-medicament.component";
import {UpdateMedicamentComponent} from "./update-medicament/update-medicament.component";
import {ClasseTherapeutiqueComponent} from "./classe-therapeutique/classe-therapeutique.component";

const routes: Routes = [
  {
    path:'',
    component:MedicamentComponent,
    children:[
      {
        path:'',
        component:ListMedicamentComponent
      },
      {
        path:"ajouter",
        component:AddMedicamentComponent
      }
,
      {
        path:":id/modifier",
        component:UpdateMedicamentComponent
      },
      {
        path:'classe-therapeutique',
        component:ClasseTherapeutiqueComponent
      },


    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MedicamentRoutingModule { }
