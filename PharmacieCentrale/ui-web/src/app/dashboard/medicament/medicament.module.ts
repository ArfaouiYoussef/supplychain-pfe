import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MedicamentRoutingModule } from './medicament-routing.module';
import { MedicamentComponent } from './medicament.component';
import { AddMedicamentComponent } from './add-medicament/add-medicament.component';
import {
  NbAccordionModule,
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbInputModule, NbSelectModule,
  NbUserModule
} from "@nebular/theme";
import {Ng2SmartTableModule} from "ng2-smart-table";
import { ListMedicamentComponent } from './list-medicament/list-medicament.component';
import {ImageUploaderModule} from "ngx-image-uploader";
import {
  MatButtonModule,
  MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatTooltipModule
} from "@angular/material";
import {AvatarModule} from "ngx-avatar";
import { UpdateMedicamentComponent } from './update-medicament/update-medicament.component';
import { ClasseTherapeutiqueComponent } from './classe-therapeutique/classe-therapeutique.component';
import {ReactiveFormsModule} from "@angular/forms";
import {NgxImgModule} from "ngx-img";

@NgModule({
  declarations: [MedicamentComponent, AddMedicamentComponent, ListMedicamentComponent, UpdateMedicamentComponent, ClasseTherapeutiqueComponent ],
  imports: [
    CommonModule,
    MedicamentRoutingModule,
    NbCardModule,
    Ng2SmartTableModule,
    NbInputModule,
    NbButtonModule,
    NbUserModule,
    ImageUploaderModule,
    NbActionsModule,
    MatIconModule,
    NbAccordionModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatTooltipModule,
    MatDividerModule,
    AvatarModule,
    NbSelectModule,
    ReactiveFormsModule,
    NgxImgModule,

  ]
})
export class MedicamentModule { }
