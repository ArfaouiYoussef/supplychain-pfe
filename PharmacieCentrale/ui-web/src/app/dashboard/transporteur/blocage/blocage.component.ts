import {Component, OnInit, TemplateRef} from '@angular/core';
import {User} from "../../../model/User";
import {ActivateRequest} from "../../interface/ActivateRequest";
import {DeleteRequest} from "../../interface/DeleteRequest";
import {ClientService} from "../../../service/client.service";
import {NbDialogService} from "@nebular/theme";
import {Observable, of} from "rxjs";
import {map} from "rxjs/operators";
import Swal from "sweetalert2";
import {TransporteurService} from "../../../service/transporteur.service";
import {EthereumProvider} from "../../../ethereumProvider/ethereum";
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'ngx-blocage',
  templateUrl: './blocage.component.html',
  styleUrls: ['./blocage.component.scss']
})
export class BlocageComponent implements OnInit {

  private users:User[];
  private  request:ActivateRequest;
  private deleteRequest:DeleteRequest;
  filteredItems: any[] & User[];

  constructor(private _transporteurService:TransporteurService,
              private dialogService: NbDialogService,
              private spinner:NgxSpinnerService,
              private web3:EthereumProvider) {
this.users=[];
  }

  ngOnInit() {
    this.options = ['Option 1', 'Option 2', 'Option 3'];
    this.filteredOptions$ = of(this.options);

    this._transporteurService.BlockedClient().subscribe(res=>{
      this.users=res;
      this.assignCopy();
      },error1 => this.assignCopy())



  }

  options: string[];
  filteredOptions$: Observable<string[]>;



  private filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(optionValue => optionValue.toLowerCase().includes(filterValue));
  }

  getFilteredOptions(value: string): Observable<string[]> {
    return of(value).pipe(
      map(filterString => this.filter(filterString)),
    );
  }
  assignCopy(){
    this.filteredItems = Object.assign([], this.users);
  }
  filterItem(value){

  }


  onChange(event) {
    let value=event.target.value;

    if(!value){
      this.assignCopy();
    }
    this.filteredItems = Object.assign([], this.users).filter(
      item => item.username.toLowerCase().indexOf(value.toLowerCase()) > -1
    )


  }

  onSelectionChange($event) {
    this.filteredOptions$ = this.getFilteredOptions($event);
  }




  open(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog);
  }


  debloquer(user: any) {
    this.spinner.show().then(console.log) ;
    let data = { id: user.id ,active:true,accountAddress: user.accountAddress,name:user.username};
    this.web3.addGrossiste(data).then(signed=>{
      let request={...data,transaction: signed.rawTransaction}
      this._transporteurService.activateAccount(request).subscribe(response=>{
        this.spinner.hide()
        Swal.fire('Success', 'Compte activé!', 'success')
        this.filteredItems=this.users.filter(user=> user.id.toString()!==data.id);

      },error => {
        this.spinner.hide();
        Swal.fire('Echec', '', 'error');


      })
    });


  }

  delete(id: any,accountAddress:string) {
    this.deleteRequest = {id: id,accountAddress:accountAddress};

    this._transporteurService.delete(this.deleteRequest).subscribe(response=>{
      Swal.fire('success...', 'Compte supprimer!', 'success')
      this.users=this.users.filter(user=> user.id.toString()!==id);

    })

  }
}
