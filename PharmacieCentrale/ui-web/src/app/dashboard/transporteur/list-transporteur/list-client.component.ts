import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {User} from "../../../model/User";
import {ActivateRequest} from "../../interface/ActivateRequest";
import {DeleteRequest} from "../../interface/DeleteRequest";
import {ClientService} from "../../../service/client.service";
import {NbDialogService} from "@nebular/theme";
import {Observable, of} from "rxjs";
import {map} from "rxjs/operators";
import Swal from "sweetalert2";
import {TransporteurService} from 'app/service/transporteur.service';
import {NgxSpinnerService} from "ngx-spinner";
import {EthereumProvider} from "../../../ethereumProvider/ethereum";

@Component({
  selector: 'ngx-list-client',
  templateUrl: './list-client.component.html',
  styleUrls: ['./list-client.component.scss']
})
export class ListTransporteurComponent implements OnInit {


  @ViewChild('item', { static: true }) accordion;

  toggle() {
    this.accordion.toggle();
  }

  private users:User[];
  private  request:ActivateRequest;
  private deleteRequest:DeleteRequest;
  filteredItems: any[] & User[];

  constructor(private _transporteurService:TransporteurService,
              private dialogService: NbDialogService,
              private _spinner:NgxSpinnerService,
              private _web3:EthereumProvider) {
this.filteredItems=[];
  }

  ngOnInit() {
    this.options = ['Option 1', 'Option 2', 'Option 3'];
    this.filteredOptions$ = of(this.options);

    this._transporteurService.getAllTransporteur().subscribe(res=>{
      this.users=res.filter(item=>item.blocked==false);
      console.log(res)
      this.assignCopy();
    },error1 => this.assignCopy())


  }

  options: string[];
  filteredOptions$: Observable<string[]>;



  private filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(optionValue => optionValue.toLowerCase().includes(filterValue));
  }

  getFilteredOptions(value: string): Observable<string[]> {
    return of(value).pipe(
      map(filterString => this.filter(filterString)),
    );
  }
  assignCopy(){
    this.filteredItems = Object.assign([], this.users);
  }
  filterItem(value){

  }


  onChange(event) {
    let value=event.target.value;

    if(!value){
      this.assignCopy();
    }
    this.filteredItems = Object.assign([], this.users).filter(
      item => item.username.toLowerCase().indexOf(value.toLowerCase()) > -1
    )


  }

  onSelectionChange($event) {
    this.filteredOptions$ = this.getFilteredOptions($event);
  }




  open(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog);
  }


  confirm(id: any) {
    this.request = { accountAddress: id ,active:false};
    // @ts-ignore
    this._clientService.activateAccount(this.request).subscribe(response=>{
      Swal.fire('Oops...', 'Compte activé!', 'success')
      this.users=this.users.filter(user=> user.id.toString()!==id);

    })

  }

  delete(user) {
    this._spinner.show().then(console.log);

    this.deleteRequest = {id: user.id,accountAddress : user.accountAddress};
    this._web3.deleteUser(this.deleteRequest).then(signed=>{
      let data={...this.deleteRequest,transaction:signed.rawTransaction}
      this._transporteurService.removeUserFromBlockChian(data).subscribe(response=>{
        this._spinner.hide();
        Swal.fire('success...', 'Compte supprimer!', 'success')

        this.filteredItems=this.filteredItems.filter( item => (item.id)!==user.id);

      },error => {
        this._spinner.hide().then(console.log)
        Swal.fire('Echec ...', '', 'error')

      })


    })

  }


  block(data: any) {
    console.log(data)
    this._spinner.show().then(console.log)
    const  del ={ accountAddress :data.accountAddress};
    this._web3.deleteUser(del).then(signed=>{
      var request = {id:data.id, accountAddress: data.accountAddress ,block:true,transaction: signed.rawTransaction};

      this._transporteurService.blockClient(request).subscribe(response=>{
        Swal.fire('success...', 'Compte bloquer!', 'success')
        this.filteredItems=this.filteredItems.filter( item => (item.accountAddress)!==data.accountAddress);
        this._spinner.hide();
      },error => { this._spinner.hide();     Swal.fire('Echec', '', 'error');
      })

    });

  }


}
