import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarteRfidComponent } from './carte-rfid.component';

describe('CarteRfidComponent', () => {
  let component: CarteRfidComponent;
  let fixture: ComponentFixture<CarteRfidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarteRfidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarteRfidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
