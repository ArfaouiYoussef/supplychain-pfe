import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SuiviComponent} from "./suivi.component";
import {ListCommandeComponent} from "./list-commande/list-commande.component";

const routes: Routes = [{
  path:"",
  component:SuiviComponent,
  children:[{
    path:"",
    component:ListCommandeComponent,
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuiviRoutingModule { }
