import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SuiviRoutingModule } from './suivi-routing.module';
import { SuiviComponent } from './suivi.component';
import { ListCommandeComponent } from './list-commande/list-commande.component';
import {MatIconModule} from "@angular/material/icon";
import {
    NbAccordionModule,
    NbActionsModule, NbButtonModule,
    NbCardModule,
    NbIconModule,
    NbInputModule, NbSelectModule,
    NbStepperModule, NbTabsetModule, NbUserModule
} from "@nebular/theme";
import {MatDividerModule} from "@angular/material/divider";
import {MatFormFieldModule} from "@angular/material/form-field";
import {AvatarModule} from "ngx-avatar";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatStepperModule} from "@angular/material/stepper";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgxGaugeModule} from "ngx-gauge";
import {NgxSpinnerModule} from "ngx-spinner";
import {BarChartModule, LineChartModule, PieChartModule} from "@swimlane/ngx-charts";

@NgModule({
  declarations: [SuiviComponent, ListCommandeComponent],
  imports: [
    CommonModule,
    SuiviRoutingModule,
    MatIconModule,
    NbActionsModule,
    MatDividerModule,
    MatFormFieldModule,
    NbAccordionModule,
    AvatarModule,
    NbCardModule,
    NbIconModule,
    MatInputModule,
    MatButtonModule,
    NbInputModule,
    MatTooltipModule,
    NbStepperModule,
    NgxGaugeModule,

    MatStepperModule,
    ReactiveFormsModule,
    NbUserModule,
    NbButtonModule,
    NgxGaugeModule,
    NbTabsetModule,
    FormsModule,
    NgxSpinnerModule,
    NbSelectModule,
    PieChartModule,
    LineChartModule,
    BarChartModule
  ]
})
export class SuiviModule { }
