import { Component, OnInit } from '@angular/core';
import {MedicamentService} from "../../../service/medicament.service";
import {Router, ActivatedRoute} from '@angular/router';
import {EthereumProvider} from "../../../ethereumProvider/ethereum";
import {TransactionService} from "../../../service/transaction.service";
import {CommandService} from "../../../service/command.service";
import {Medicament} from 'app/dashboard-grossiste/redux/common/models/medicament';
import {TransporteurService} from 'app/service/transporteur.service';
import {NgxSpinnerService} from "ngx-spinner";
import Swal from "sweetalert2";
import { SmartTableDatePickerComponent } from './smart-table-date-picker/smart-table-date-picker.component';
import * as moment from "moment";
import {MessageService} from "../../../service/message.service";

@Component({
  selector: 'ngx-medicament-commande',
  templateUrl: './medicament-commande.component.html',
  styleUrls: ['./medicament-commande.component.scss']
})
export class MedicamentCommandeComponent implements OnInit {
  containerTag;
  medicamentId: any;
  medicament: any;
  commandeId: string;
  commande: any;
  medicamentList: any;
  qte: any;
  qteDemande: any;
  transporteurList: any[];
  selectedItem:any;
  qteTotal: any;
  total: any;
  disable: boolean=true;
  newOwnerName: any;
  newOwner: any;
  transporteurAddress: any;

  constructor(private _serviceMedicament:MedicamentService,
              private route:ActivatedRoute,private _web3:EthereumProvider,
              private commandService:CommandService,
private router: Router,
              private msg:MessageService,
              private spinner:NgxSpinnerService,
              private transporteurService:TransporteurService) {

  }

  ngOnInit(): void {
    this._web3.generateAccount();
  console.log(this.route.snapshot);
    this.route.params
      .subscribe(params =>{
        console.log(params);
this.medicamentId=params.id
      });
    this.route.queryParams
      .subscribe(params =>{
console.log(params)
        console.log(params.tag); // {order: "popular"}
        this.containerTag=params.tag;
this.commandeId=params.commandeId;
      });
    console.log(this.commandeId)
this.commandService.getContainerById(this.commandeId).subscribe((response:any)=>{
  this.medicamentList=response.medicaments
  this.transporteurAddress=response.transporteurAddress;
  this.total=this.medicamentList.length;
  this.medicamentList=this.medicamentList.filter(item=>item.id===this.medicamentId)
  console.log(this.medicamentList)
  this.qte=!!this.medicamentList.length?this.medicamentList.length:0;
  console.log(this.medicamentList);
});
    this.transporteurService.getAllTransporteur().subscribe(res=>{
      console.log(this.transporteurAddress)
      this.transporteurList=res.filter(item=>item.accountAddress!==this.transporteurAddress)}
    )

    this.commandService.getCommandeById(this.commandeId).subscribe((res:any)=>{
this.newOwnerName=res.name;
this.newOwner=res.publicAddress;
console.log(this.newOwner);
console.log(this.newOwnerName)
      this.commande=res;
      this.qteDemande=0;
      this.qteTotal=0;
      console.log(res.panier.productQuantities)
      Object.keys(res.panier.products).forEach((key) => {
        this.qteDemande=res.panier.productQuantities[key];
        this.qteTotal+=res.panier.productQuantities[key];
        this.total;
        console.log(this.qteTotal)
      });

    });

    this._serviceMedicament.getMedicamentById(this.medicamentId).subscribe(res=>{
this.medicament=res;
    });
    this._serviceMedicament.getAllClass().subscribe(response=>{
      this.data=response;
    })

  }
  settings = {
    delete: {

      deleteButtonContent:'<i class="nb-trash"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmDelete: true,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate:true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',

      confirmSave: true,
    },
    columns: {

      numeroLot:{
        title:"NumeroLot"
      },  serialNumber:{
        title:'Numero de  Serie',
        type:'date'},
      dateExpiration:{
        title:'Date Expiration',
        type: 'html', editor:{ type: 'custom', component: SmartTableDatePickerComponent, } }






    },
  };

  data = [
    {
      name: "classe 1 ",
    },

  ];

  onDeleteConfirm(event) {
    console.log("Delete Event In Console")
    console.log(event);
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
expandContainer(){
    this.spinner.show().then(console.log)
    this.disable=true;

    console.log(this.selectedItem)
  let data={addressTrasnporteur:this.selectedItem,tagContainer:this.containerTag,commandeId:this.commandeId };
    this._web3.expandContainer(data).then(signed=>{
      console.log(signed)
      let request={...data,transaction:signed.rawTransaction}
      this.transporteurService.expandContainer(request).subscribe(res=>{
        this.disable=false;
        this.spinner.hide();

        Swal.fire(
          'Success',
          'Conteneur   expédier ',
          'success'
        )
        this.router.navigate(["/dashboard/suivi"])
        },error => {
        this.disable = false;
        this.spinner.hide();
      });
    })


}
  onCreateConfirm(event) {
console.log(this.commande)
    console.log(event)
  let dateExpiration= moment(event.newData.dateExpiration).format("DD/MM/YYYY");

     if(this.qteTotal==this.total){
       Swal.fire(
           'Echec!',
         'Conteneur prêt à expédier !',
         'error'
       )
     }
     else {
       this.spinner.show().then(console.log);
       const data = {...event.newData, tag: this.containerTag, medicament: this.medicament}
       console.log("Create Event In Console");
       console.log(data);
       console.log(this.medicament);
       console.log(this.commande);

       let ethRequest = {
         id:!!this.medicament.id?this.medicament.id:'',
         name: !!this.medicament.name?this.medicament.name:'',
         numeroLot: data.numeroLot,
         amm: !!this.medicament.amm ? this.medicament.amm : 0,
         serialNumber: data.serialNumber,
         dateExpiraetion: dateExpiration,
         saler: this.commande.publicAddress,
         tagContainer: this.containerTag,
         newOwnerName:this.newOwnerName,
         newOwner:this.newOwner,
         addressDestination: this.commande.addressCommande
       }
if((data.serialNumber.length!=8))
{
  Swal.fire(
    'Echec',
    'Numero  de Serie  non Valide!',
    'error'
  )
}
if((data.numeroLot.length!=5))
       {
         Swal.fire(
           'Error!',
           'Numero  de lot  non valide!',
           'success'
         )
       }


       this.medicament.numeroLot = data.numeroLot;
       this.medicament.serialNumber = data.serialNumber;
       this.medicament.dateExpiration = dateExpiration;

       console.log(this.medicament);
if(data.serialNumber.length===8 && data.numeroLot.length===5){
       this._web3.addMedicament(ethRequest).then(signed => {
         console.log(signed);
         let data = {medicament: this.medicament, transaction: signed.rawTransaction, commandeId: this.commandeId,email:this.commande.email};
         this.commandService.addMedicament(data).subscribe(response => {
           console.log(response)
           event.confirm.resolve();

           this.spinner.hide();
           Swal.fire(
             'Success!',
             'Transaction  validée  avec succès!',
             'success'
           )
           this.qte++;
           this.msg.removeCommande(this.commandeId);
           this.total++;

         }, error1 => {
           event.confirm.reject();
           this.spinner.hide();
         })
       });
     }
else {
  this.spinner.hide();
  Swal.fire(
    'Error!',
    'information non valide',
    'error'
  )
}
     }
    }


  onSaveConfirm(event) {

    const data={oldName:event.data.name,newName:event.newData.name};
    this._serviceMedicament.updateClass(data).subscribe(response=>{
      event.confirm.resolve();

    },error1 =>
      event.confirm.reject())
  }

  activeBtn(event) {
if(event!=="")
{this.disable=false;}
  }
}
