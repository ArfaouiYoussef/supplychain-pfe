import { Component, OnInit } from '@angular/core';
import {LocalDataSource} from "ng2-smart-table";
import { ImageUploaderOptions, FileQueueObject } from 'ngx-image-uploader';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MedicamentService} from "../../../service/medicament.service";
import {ClassTh} from 'app/model/ClassTh';
import {Observable} from "rxjs";
import {medicament_validation_messages} from 'app/dashboard/Validators/medicament_validation_messages';
import Swal from 'sweetalert2';
import {Router} from "@angular/router";

@Component({
  selector: 'ngx-livraison',
  templateUrl: './livraison.component.html',
  styleUrls: ['./livraison.component.scss']
})
export class LivraisonComponent implements OnInit{

  constructor(private _serviceMedicament:MedicamentService) {

  }

  ngOnInit(): void {
    this._serviceMedicament.getAllClass().subscribe(response=>{
      this.data=response;
    })

  }
  settings = {
    delete: {

      deleteButtonContent:'<i class="nb-trash"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmDelete: true,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate:true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',

      confirmSave: true,
    },
    columns: {
      name: {
        title: 'Name',
      },
      numeLot:{
        title:'numero Lot'
      },
      reference:{
        title:'référence'
      }
    },
  };

  data = [
    {
      name: "classe 1 ",
    },

  ];

  onDeleteConfirm(event) {
    console.log("Delete Event In Console")
    console.log(event);
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  onCreateConfirm(event) {
    console.log("Create Event In Console")
    console.log(event.newData.name);
    this._serviceMedicament.addClass(event.newData).subscribe(response=>{
      this.data.push(event.newData);

      event.confirm.resolve();


    },error1 => event.confirm.reject())

  }

  onSaveConfirm(event) {
    const data={oldName:event.data.name,newName:event.newData.name};
    this._serviceMedicament.updateClass(data).subscribe(response=>{
      event.confirm.resolve();

    },error1 =>
      event.confirm.reject())
  }

}

