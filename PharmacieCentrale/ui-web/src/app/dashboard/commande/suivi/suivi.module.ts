import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SuiviRoutingModule } from './suivi-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SuiviRoutingModule
  ]
})
export class SuiviModule { }
