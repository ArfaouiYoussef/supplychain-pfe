export class User {
    id: number;
    username: string;
    accountAddress:string;
    email: string;
    roles: any;

    password: string;
    accessToken?: string;
    tokenType:string;
  createdDate:string;
  userPhone:string;
  address:string;
}

