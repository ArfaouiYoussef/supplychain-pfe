import { Injectable, NgZone } from '@angular/core';
import {config} from "../config";
import {catchError} from "rxjs/operators";
import {Observable, throwError} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {User} from 'app/model/User';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private zone: NgZone){}

  /**
   * Subscribe to the teams update Server Sent Event stream
   */
  getUsers(): Observable<any> {
    return new Observable((observer) => {
      let url = 'http://localhost:8000/api/auth/demande';
      let eventSource = new EventSource(url);

      eventSource.onmessage = (event) => {
        let json = JSON.parse(event.data);
        if (json !== undefined && json !== '') {
          this.zone.run(() => observer.next(json));
        }
      };

      eventSource.onerror = (error) => {
        if (eventSource.readyState === 0) {
          console.log('The stream has been closed by the server.');
          eventSource.close();
          observer.complete();
        } else {
          observer.error('EventSource error: ' + error);
        }
      }
    });
  }

  getCommande(): Observable<any> {
    return new Observable((observer) => {
      let url = 'http://localhost:8000/api/command/notification';
      let eventSource = new EventSource(url);

      eventSource.onmessage = (event) => {
        let json = JSON.parse(event.data);
        if (json !== undefined && json !== '') {
          this.zone.run(() => observer.next(json));
        }
      };

      eventSource.onerror = (error) => {
        if (eventSource.readyState === 0) {
          console.log('The stream has been closed by the server.');
          eventSource.close();
          observer.complete();
        } else {
          observer.error('EventSource error: ' + error);
        }
      }
    });
  }


  getContainers(): Observable<any> {
    return new Observable((observer) => {
      let url = 'http://localhost:8000/api/container/notification';
      let eventSource = new EventSource(url);

      eventSource.onmessage = (event) => {
        let json = JSON.parse(event.data);
        if (json !== undefined && json !== '') {
          this.zone.run(() => observer.next(json));
        }
      };

      eventSource.onerror = (error) => {
        if (eventSource.readyState === 0) {
          console.log('The stream has been closed by the server.');
          eventSource.close();
          observer.complete();
        } else {
          observer.error('EventSource error: ' + error);
        }
      }
    });
  }
}
