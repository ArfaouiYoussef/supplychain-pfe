import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {User} from "../model/User";
import {config} from "../config";
import {ClassTh} from "../model/ClassTh";
import {Medicament} from "../model/Medicament";
import {catchError} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class MedicamentService {

  constructor(private _http:HttpClient) { }


  getAllClass(): Observable<ClassTh[]>{
    return this._http.get<ClassTh[]>(config.apiUrl1+"/api/classTherapeutique");
  }
  addClass(data:ClassTh): Observable<Object>{
    return this._http.post(config.apiUrl1+"/api/classTherapeutique",data);
  }
  deleteMedicament(id:any){

    return this._http.delete<Medicament>(config.apiUrl1+"/api/medicament"+"/"+ id);

  }

  deleteClass(data:ClassTh):Observable<Object>{

    return this._http.delete<ClassTh>(config.apiUrl1+"/api/classTherapeutique"+ "/"+ data.name);

  }
  addMedicament(data:any){
    console.log(data);
    return this._http.post(config.apiUrl1+"/api/medicament",data);

  }
  upload(data:any){
    console.log(data)
    return this._http.post(config.apiUrl1+"/api/uploadFile",data);

  }


  updateClass(data:any):Observable<Object>{

    return this._http.put(config.apiUrl1+"/api/classTherapeutique",data);

  }
  updateMedicament(data:any):Observable<Object>{

    return this._http.put(config.apiUrl1+"/api/medicament",data);

  }



  getMedicamentById(id:any){
    return this._http.get(config.apiUrl1+"/api/medicament"+"/"+id)      .pipe(catchError((error: any) => throwError(error.message)));;


  }

  getMedicament():Observable<any[]>{
    return this._http.get<any[]>(config.apiUrl1+"/api/medicament")
      .pipe(catchError((error: any) => throwError(error.message)));;
  }

  delete(deleteRequest: any) {

  }
}
