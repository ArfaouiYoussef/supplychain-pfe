import { Injectable } from '@angular/core';
import { StorageService } from'./secure-storage.service';
@Injectable({
  providedIn: 'root'
})
export class LocalstorageService {
  constructor(private storageService: StorageService) { }
// Set the json data to local storage
  setIem(key,data){
    let key_encoded=  this.storageService.encrypt(key);
    let value_encoded=  this.storageService.encrypt(data);
    localStorage.setItem(key_encoded,value_encoded);
  }
  //sm
  getItem(key){
  let val= this.storageService.encrypt(key);
    return this.storageService.decrypt(localStorage.getItem(val));
  }
// Clear the local storage
  clearStorage() {
localStorage.clear();
  }
}
