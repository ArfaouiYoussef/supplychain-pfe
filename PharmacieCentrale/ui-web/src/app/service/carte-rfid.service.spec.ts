import { TestBed } from '@angular/core/testing';

import { CarteRfidService } from './carte-rfid.service';

describe('CarteRfidService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CarteRfidService = TestBed.get(CarteRfidService);
    expect(service).toBeTruthy();
  });
});
