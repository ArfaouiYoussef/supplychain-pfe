import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {AuthService} from "../service/auth.service";


@Injectable({ providedIn: 'root' })
export class AuthGuardClient implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: AuthService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authenticationService.currentUserValue;
        if (currentUser && currentUser.roles[0]==="ROLE_GROSSISTE") {
            // logged in so return true
            return true;
        }
        else        if (currentUser && currentUser.roles[0]==="ROLE_LABORATOIRE") {
          this.router.navigate(['/dashboard/client']);
        return  false;
        }

          console.log("root") ;      // not logged in so redirect to login page with the return url
        return false;
    }
}
