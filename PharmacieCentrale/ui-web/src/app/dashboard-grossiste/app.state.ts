import {Medicament} from "../model/Medicament";

export interface AppState {
  readonly product: Medicament[];
}
