import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable, of} from 'rxjs';
import * as Medicaments from '../../redux/common/store/products/products.actions';
import {Panier} from 'app/dashboard-grossiste/redux/common/models/panier';
import * as Paniers from '../../redux/common/store/panier/panier.actions';
import {Medicament} from "../../redux/common/models/medicament";
import {User} from "../../../model/user";
import {inflateRaw} from "zlib";
import {map} from "rxjs/operators";
import {MedicamentService} from "../../../service/medicament.service";
import {EthereumProvider} from "../../../ethereumProvider/ethereum";

@Component({
  selector: 'ngx-list-produit',
  templateUrl: './list-produit.component.html',
  styleUrls: ['./list-produit.component.scss']
})
export class ListProduitComponent implements OnInit {
  selectedItem:any;
  medicaments: any;
  checked: boolean;
  @Input() totalLikes = 0;
  @Input() iLike = false;
med:Medicament[];
  @Output() change = new EventEmitter();
panier:Panier;
  filteredItems: any;
  class: import("C:/Users/YOUSSEF/Desktop/PFE/pfe-blockchain/laboratoire/ui-web/src/app/model/ClassTh").ClassTh[];
  onClick() {
    this.iLike = !this.iLike;
    this.totalLikes += this.iLike ? 1 : -1;
    this.change.emit({newValue: this.iLike, newTotal: this.totalLikes});
  }
  constructor(private store: Store<any>,private _medicamentService:MedicamentService,
              private web3:EthereumProvider){


  this.store.dispatch(new Medicaments.GetMedicaments());
  this.store.select('medicament').subscribe(response => {

this.medicaments=response.medicaments;
    console.log(response);
    this.assignCopy();


    setTimeout(() => {
    }, 2000);

  }, error => {
  });
}

  options: string[];
  filteredOptions$: Observable<string[]>;



  private filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(optionValue => optionValue.toLowerCase().includes(filterValue));
  }

  getFilteredOptions(value: string): Observable<string[]> {
    return of(value).pipe(
      map(filterString => this.filter(filterString)),
    );
  }
  assignCopy(){
    this.filteredItems = Object.assign([], this.medicaments);
  }
  onChange(event) {
    let value=event.target.value;
    if(!value){
      this.assignCopy();
    }
    this.filteredItems = Object.assign([], this.medicaments).filter(
      item => item.name.toLowerCase().indexOf(value.toLowerCase()) > -1
    )


  }


  ngOnInit() {
this._medicamentService.getAllClass().subscribe(response=>{
  this.class=response;


} )
  }




check(id){}
  addToPanier(item) {

console.log(item)
    this.store.dispatch(new Paniers.AddPanier({

      medicament:item,
      prixTotal:500,
      panier:JSON.parse(localStorage.getItem("currentUser")).panier.id,
      quantite:10,
      userId:JSON.parse(localStorage.getItem("currentUser")).id
,dispo:item.quantite

    }));
    setTimeout(() => {
    }, 2000);






console.log(this.panier)

  }

  changes(event) {
    console.log(event.target.value)
  }

  onSelect(event) {
console.log(event)
    if(!event){
      this.assignCopy();
    }
    this.filteredItems = Object.assign([], this.medicaments).filter(
      item => item.classeTherapeutique.id.toLowerCase().indexOf(event.toLowerCase()) > -1
    )


    console.log(event)
  }
}
