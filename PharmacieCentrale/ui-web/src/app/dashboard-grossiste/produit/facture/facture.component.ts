import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {Store} from "@ngrx/store";
import * as Paniers from "../../redux/common/store/panier/panier.actions";
import * as jsPDF from 'jspdf';
import {ActivatedRoute} from "@angular/router";
import {CommandService} from "../../../service/command.service";

@Component({
  selector: 'ngx-facture',
  templateUrl: './facture.component.html',
  styleUrls: ['./facture.component.scss']
})
export class FactureComponent implements OnInit {
  commande: any;
  panier: any[];
  list: any[];
  prixTotal: any;
  // @ts-ignore
  @ViewChild('content') content: ElementRef;
  commandId: string;
  userDetails: any;
  email: any;
  username: any;
  address: any;
  telephone: any;

  makePdf() {
    let doc = new jsPDF();
    doc.addHTML(this.content.nativeElement, function() {
      doc.save("facture.pdf");

    });
  }

  constructor(private activatedRoute:ActivatedRoute,private commandeService:CommandService) {

  }

  ngOnInit() {
this.userDetails=JSON.parse(localStorage.getItem("currentUser"))
    this.username=this.userDetails.username;
this.email=this.userDetails.email;
this.address=this.userDetails.address;
this.telephone=this.userDetails.telephone;


   // this.store.dispatch(new Paniers.GetPanierById(JSON.parse(localStorage.getItem("currentUser")).panier.id))
    this.activatedRoute.url.subscribe(res=>{
      this.commandId= res[1].path;
    })
this.commandeService.getCommandeById(this.commandId).subscribe((res:any)=>{
  console.log(res.panier.products)


    this.list=[]
 //   this.store.select("panier").subscribe(res=> {

      this.panier=[];
      if(!!res.panier.products) {
this.prixTotal=res.panier.prixTotal
        this.list=[]

        Object.keys(res.panier.products).forEach((key) => {
          // @ts-ignore
          this.list.push( {...res.panier.products[key],quantity:res.panier.productQuantities[key]})
        });
        // @ts-ignore
        console.log(this.list)

      }});


  }

}
