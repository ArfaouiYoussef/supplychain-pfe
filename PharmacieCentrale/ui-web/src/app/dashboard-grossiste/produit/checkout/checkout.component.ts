import { Component, OnInit } from '@angular/core';
import * as L from 'leaflet';
import {Store} from "@ngrx/store";
import {NbToastrService} from "@nebular/theme";
import {ActivatedRoute, Router} from "@angular/router";
import {PanierEffects} from "../../redux/common/store/panier/panier.effects";
import {Actions, ofType} from "@ngrx/effects";
import * as Command from "../../redux/common/store/commande/commande.actions";
import * as Panier from "../../redux/common/store/panier/panier.actions";
import * as Paniers from '../../redux/common/store/panier/panier.actions';

import * as Medicaments from "../../redux/common/store/products/products.actions";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import Swal from "sweetalert2";
var places = require('places.js');
var latlng;
var address;

@Component({
  selector: 'ngx-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})

export class CheckoutComponent implements OnInit {
  linearMode = true;
  commande: FormGroup;
  panier: any;
  places: any;
  panierExist:any=true;
  toggleLinearMode() {
    this.linearMode = !this.linearMode;
  }


  constructor(private store: Store<any>, private toastrService: NbToastrService, private activatedRoute: ActivatedRoute,
              private panierEffects: PanierEffects, private router: Router,
              private action: Actions,
              private  _formBuilder:FormBuilder,
              ) {
    this.commande=this._formBuilder.group({
  name:[''],
        addressCommande:['',Validators.required],
        publicAddress:[''],
accountAdress:[''],
      email:[''],
        lat:[''],
      userId:[''],
  lng:[''],
    telephone:[''],
panier:[''],
  }
)

  }


  ngOnInit() {




  this.store.select("panier").subscribe(panier=> {
  let user=  JSON.parse(localStorage.getItem("currentUser"));
  this.panierExist=!(JSON.stringify(panier.panier.productQuantities)==='{}');

  this.commande.patchValue({
    publicAddress:!!user.accountAddress?user.accountAddress:'',
    accountAdress:!!user.accountAddress?user.accountAddress:'',
    name:!!user.username?user.username:'',
    email:!!user.email?user.email:'',
    panier:!!panier.panier?panier.panier:'',
    userId:!!user.id?user.id:'',
    telephone:!!user.telephone?user.telephone:''

  });
});

var placesAutocomplete = places({
  appId: 'plKKWNU1YB21',
  apiKey: '1fe0aa35dca2b89085aaccfa7a9834c8',
  container: document.querySelector('#input-map')
});

var map = L.map('map-example-container', {
  scrollWheelZoom: false,
  zoomControl: false
});

var osmLayer = new L.TileLayer(
  'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    minZoom: 1,
    maxZoom: 13,
    attribution: 'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors'
  }
);

    var markers = [];

    map.setView(new L.LatLng(0, 0), 1);
    map.addLayer(osmLayer);

    placesAutocomplete.on('suggestions', handleOnSuggestions);
    placesAutocomplete.on('cursorchanged', handleOnCursorchanged);
    placesAutocomplete.on('change', handleOnChange);
    placesAutocomplete.on('clear', handleOnClear);
    function handleOnSuggestions(e) {
      markers.forEach(removeMarker);
      markers = [];

      if (e.suggestions.length === 0) {
        map.setView(new L.LatLng(0, 0), 1);
        return;
      }

      e.suggestions.forEach(addMarker);
      findBestZoom();
    }

    function handleOnChange(e) {
      markers
        .forEach(function (marker, markerIndex) {
          if (markerIndex === e.suggestionIndex) {
            markers = [marker];
            marker.setOpacity(1);
            findBestZoom();
          } else {
            removeMarker(marker);
          }
        });
    }

    function handleOnClear() {
      map.setView(new L.LatLng(0, 0), 1);
      markers.forEach(removeMarker);
    }

    function handleOnCursorchanged(e) {
      markers
        .forEach(function (marker, markerIndex) {
          if (markerIndex === e.suggestionIndex) {
            marker.setOpacity(1);
            marker.setZIndexOffset(1000);
          } else {
            marker.setZIndexOffset(0);
            marker.setOpacity(0.5);
          }
        });
    }
    function addMarker(suggestion) {
      var marker = L.marker(suggestion.latlng, {opacity: .4});
      console.log(suggestion.latlng)
      latlng=suggestion.latlng;

      address=suggestion.value;
      console.log(address)
      marker.addTo(map);
      markers.push(marker);
    }

    function removeMarker(marker) {
      map.removeLayer(marker);
    }

    function findBestZoom() {
      var featureGroup = L.featureGroup(markers);
      map.fitBounds(featureGroup.getBounds().pad(0.5), {animate: false});
    }
  };






confirmCommande(){
if(this.panierExist){
this.commande.controls["lat"].patchValue(latlng.lat);
  this.commande.controls["lng"].patchValue(latlng.lng);
  this.commande.controls["addressCommande"].patchValue(address);

  console.log(this.commande.value);
  let data={
    panier:this.panier
  }
  console.log( this.commande.value);
this.store.dispatch(new Command.AddCommand(this.commande.value))
  this.action.pipe(
    ofType('ADD_COMMAND_SUCCESS'))
    .
    subscribe(   { next: (value:any)=>{
        Swal.fire(
          'Success!',
          'Commande  Ajouter  avec succès!',
          'success'
        )
     this.router.navigate(["/dashboard-grossiste/produit/commande"])
      },

      error: err => console.error(err),
      complete: () => {
       }});
      }

}
  options = {
    layers: [
      L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: 'DrugsChain' }),
    ],
    zoom: 5,
    center: L.latLng({ lat: 38.991709, lng: -76.886109 }),
  };
function() {
    var placesAutocomplete = places({
      appId: 'plKKWNU1YB21',
      apiKey: '1fe0aa35dca2b89085aaccfa7a9834c8',
      container: document.querySelector('#input-map')
    });

    var map = L.map('map-example-container', {
      scrollWheelZoom: false,
      zoomControl: false
    });

    var osmLayer = new L.TileLayer(
      'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        minZoom: 1,
        maxZoom: 13,
        attribution: 'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors'
      }
    );


}


}
