import { Injectable } from '@angular/core';
import {Actions, createEffect, Effect, ofType} from '@ngrx/effects';



import {switchMap, catchError, map, mergeMap, tap} from 'rxjs/operators';
import { of } from 'rxjs';

import { Panier } from '../../models/panier';
import {TodosService} from '../../services/todos/todos.service';
import {PanierService} from "../../../../../service/panier.service";
import {GetUsersFailed, GetUsersSuccess, UsersActionType} from "./notification.actions";
import {NotificationService} from "../../../../../service/notification.service";
import {User} from 'app/model/User';

@Injectable()
export class NotificationEffects {

  constructor(
    private actions$: Actions,
    private todosService: TodosService,
    private notification:NotificationService,

  ) { }

  getUsers$ =createEffect(() => this.actions$.pipe(
    ofType(UsersActionType.GET_USERS),
    mergeMap(() =>
      this.notification.getUsers().pipe(

        map((users: Array<User>) => new GetUsersSuccess(users),
        catchError(error => of(new GetUsersFailed(error)))
      )
    )
  )));





/*  @Effect()


  @Effect()
  deletePANIER$ = this.actions$.pipe(
    ofType(PANIERActionType.DELETE_PANIER),
    switchMap((action) =>
      this.todosService.updateAPITodo(action['payload']).pipe(
        map((todoId: number) => new DeleteMedicmanetSuccess(todoId)),
        catchError(error => of(new DeleteMedicmanetFailed(error)))
      )
    )
  );
*/
}
