import { ProductsActions, MedicamentsActionType } from './products.actions';
import { Medicament } from '../../models/medicament';

export const initialState = {};

export function productReducer(state = initialState, action: ProductsActions) {

  switch (action.type) {

    case MedicamentsActionType.GET_MEDICAMENTS: {
      return { ...state };
    }
    case MedicamentsActionType.GET_MEDICAMENT_BY_ID: {
      return { ...state };
    }

    case MedicamentsActionType.GET_MEDICAMENTS_SUCCESS: {
      let msgText = '';
      let bgClass = '';

      if (action.payload.length < 1) {
        msgText = 'No data found';
        bgClass = 'bg-danger';
      } else {
        msgText = 'Loading data';
        bgClass = 'bg-info';
      }

      return {
        ...state,
        medicaments: action.payload,
        message: msgText,
        infoClass: bgClass
      };
    }

    case MedicamentsActionType.GET_MEDICAMENTS_FAILED: {
      return { ...state };
    }

    case MedicamentsActionType.ADD_MEDICAMENT: {
      return {
        ...state, message: '',
        infoClass: ''
      };
    }

    case MedicamentsActionType.ADD_MEDICAMENT_SUCCESS: {
      const data = state['medicaments'].push(action.payload);
      return {
        ...state,
        message: 'New todo added',
        infoClass: 'bg-success'
      };
    }

    case MedicamentsActionType.GET_MEDICAMENT_BY_ID_SUCCESS: {
      let msgText = '';
      let bgClass = '';


      return {
        ...state,
        medicament: action.payload,
        message: msgText,
        infoClass: bgClass
      };
    }

    case MedicamentsActionType.UPDATE_MEDICAMENT: {
      return {
        ...state,
        message: '',
        infoClass: ''
      };
    }

    case MedicamentsActionType.UPDATE_MEDICAMENT_SUCCESS: {
      return {
        ...state,
        message: 'Update todo',
        infoClass: 'bg-success'
      };
    }

    case MedicamentsActionType.UPDATE_MEDICAMENT_FAILED: {
      return { ...state };
    }

    case MedicamentsActionType.DELETE_MEDICAMENT: {
      const medicaments = state;
      medicaments['medicaments'].forEach((todo: Medicament, i: number) => {
        if (todo.id === action.payload) {
          medicaments['medicaments'].splice(i, 1);
        }
      });

      return {
        ...state,
        message: '',
        infoClass: ''
      };
    }

    case MedicamentsActionType.DELETE_MEDICAMENT_SUCCESS: {
      return {
        ...state,
        message: 'Todo deleted',
        infoClass: 'bg-warning'
      };
    }

    case MedicamentsActionType.DELETE_MEDICAMENT_FAILED: {
      return { ...state };
    }

    default: {
      return state;
    }

  }

}
