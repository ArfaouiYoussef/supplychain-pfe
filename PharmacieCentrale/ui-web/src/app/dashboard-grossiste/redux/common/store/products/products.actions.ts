import { Action } from '@ngrx/store';
import { Medicament } from '../../models/medicament';


export enum MedicamentsActionType {
  GET_MEDICAMENTS = 'GET_MEDICAMENTS',
  GET_MEDICAMENT_BY_ID= 'GET_MEDICAMENT_BY_ID',
  GET_MEDICAMENT_BY_ID_SUCCESS = 'GET_MEDICAMENT_BY_ID_SUCCESS',

  GET_MEDICAMENTS_SUCCESS = 'GET_MEDICAMENTS_SUCCESS',
  GET_MEDICAMENTS_FAILED = 'GET_MEDICAMENTS_FAILED',
  ADD_MEDICAMENT = 'ADD_MEDICAMENT',
  ADD_MEDICAMENT_SUCCESS = 'ADD_MEDICAMENT_SUCCESS',
  ADD_MEDICAMENT_FAILED = 'ADD_MEDICAMENT_FAILED',
  UPDATE_MEDICAMENT = 'UPDATE_MEDICAMENT',
  UPDATE_MEDICAMENT_SUCCESS = 'UPDATE_MEDICAMENT_SUCCESS',
  UPDATE_MEDICAMENT_FAILED = 'UPDATE_MEDICAMENT_FAILED',
  DELETE_MEDICAMENT = 'DELETE_MEDICAMENT',
  DELETE_MEDICAMENT_SUCCESS = 'DELETE_MEDICAMENT_SUCCESS',
  DELETE_MEDICAMENT_FAILED = 'DELETE_MEDICAMENT_FAILED'
}

export class GetMedicaments implements Action {
  readonly type = MedicamentsActionType.GET_MEDICAMENTS;
}
export class GetMedicamentById implements Action {
  readonly type = MedicamentsActionType.GET_MEDICAMENT_BY_ID;
  constructor(public payload: string) { }


}

export class GetMedicamentByIdSuccess implements Action {
  readonly type = MedicamentsActionType.GET_MEDICAMENT_BY_ID_SUCCESS;
  constructor(public payload:Medicament) { }
}
export class GetMedicamentSuccess implements Action {
  readonly type = MedicamentsActionType.GET_MEDICAMENTS_SUCCESS;
  constructor(public payload: Array<Medicament>) { }
}

export class GetMedicamentFailed implements Action {
  readonly type = MedicamentsActionType.GET_MEDICAMENTS_FAILED;
  constructor(public payload: string) { }
}

export class AddMedicmanet implements Action {
  readonly type = MedicamentsActionType.ADD_MEDICAMENT;
  constructor(public payload: Medicament) { }
}

export class AddMedicmanetSuccess implements Action {
  readonly type = MedicamentsActionType.ADD_MEDICAMENT_SUCCESS;
  constructor(public payload: Medicament) { }
}

export class AddMedicmanetFailed implements Action {
  readonly type = MedicamentsActionType.ADD_MEDICAMENT_FAILED;
  constructor(public payload: string) { }
}

export class UpdateMedicmanet implements Action {
  readonly type = MedicamentsActionType.UPDATE_MEDICAMENT;
  constructor(public payload: Medicament) { }
}

export class UpdateMedicmanetSuccess implements Action {
  readonly type = MedicamentsActionType.UPDATE_MEDICAMENT_SUCCESS;
  constructor(public payload: Medicament) { }
}

export class UpdateMedicmanetFailed implements Action {
  readonly type = MedicamentsActionType.UPDATE_MEDICAMENT_FAILED;
  constructor(public payload: string) { }
}

export class DeleteMedicmanet implements Action {
  readonly type = MedicamentsActionType.DELETE_MEDICAMENT;
  constructor(public payload: number) { }
}

export class DeleteMedicmanetSuccess implements Action {
  readonly type = MedicamentsActionType.DELETE_MEDICAMENT_SUCCESS;
  constructor(public payload: number) { }
}

export class DeleteMedicmanetFailed implements Action {
  readonly type = MedicamentsActionType.DELETE_MEDICAMENT_FAILED;
  constructor(public payload: string) { }
}

export type ProductsActions = GetMedicaments |GetMedicamentById|
  GetMedicamentSuccess |
  GetMedicamentFailed |GetMedicamentByIdSuccess|
  AddMedicmanet |
  AddMedicmanetSuccess |
  AddMedicmanetFailed |
  UpdateMedicmanet |
  UpdateMedicmanetSuccess |
  UpdateMedicmanetFailed |
  DeleteMedicmanet |
  DeleteMedicmanetSuccess |
  DeleteMedicmanetFailed;
