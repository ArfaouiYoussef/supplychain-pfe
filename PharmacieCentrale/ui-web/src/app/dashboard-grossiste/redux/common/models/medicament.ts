import{Deserializable} from './Deserializable';

export class Medicament implements Deserializable {
  dispo: any;
  stock: any;
  amm: any;

  get quantity(): string {
    return this._quantity;
  }

  set quantity(value: string) {
    this._quantity = value;
  }
  get medicamentPrice(): string {
    return this._medicamentPrice;
  }

  set medicamentPrice(value: string) {
    this._medicamentPrice = value;
  }
  private _id: number;
  private _classeTherapeutique: string;
  private _composition: string;
  private _dosage: string;
  private _formegalenique: string;
  private _indication: string;
  private _marque: string;
  private _name: string;
  private _photoUrl: string;
  private _presentation: string;
  private _medicamentPrice: string;
  private _quantity: string;
  private _type: string;


  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get classeTherapeutique(): string {
    return this._classeTherapeutique;
  }

  set classeTherapeutique(value: string) {
    this._classeTherapeutique = value;
  }

  get composition(): string {
    return this._composition;
  }

  set composition(value: string) {
    this._composition = value;
  }

  get dosage(): string {
    return this._dosage;
  }

  set dosage(value: string) {
    this._dosage = value;
  }

  get formegalenique(): string {
    return this._formegalenique;
  }

  set formegalenique(value: string) {
    this._formegalenique = value;
  }

  get indication(): string {
    return this._indication;
  }

  set indication(value: string) {
    this._indication = value;
  }

  get marque(): string {
    return this._marque;
  }

  set marque(value: string) {
    this._marque = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get photoUrl(): string {
    return this._photoUrl;
  }

  set photoUrl(value: string) {
    this._photoUrl = value;
  }

  get presentation(): string {
    return this._presentation;
  }

  set presentation(value: string) {
    this._presentation = value;
  }




  get type(): string {
    return this._type;
  }

  set type(value: string) {
    this._type = value;
  }

  deserialize(input: any): this {
    return Object.assign(this, input);
  }


}
