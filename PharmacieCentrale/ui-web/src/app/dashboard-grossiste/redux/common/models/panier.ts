import {Medicament} from "./medicament";
import {User} from 'app/@core/data/users';
import {Deserializable} from "./Deserializable";

export class Panier implements Deserializable{
  private _createdAt: string;
  private _id: string;
  private _lastModified: string;
  private _medicamentInBaskets: [{}];
  private _prixTotal: string;
  private _quantite:string;


  get createdAt(): string {
    return this._createdAt;
  }

  set createdAt(value: string) {
    this._createdAt = value;
  }

  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get lastModified(): string {
    return this._lastModified;
  }

  set lastModified(value: string) {
    this._lastModified = value;
  }

  get medicamentInBaskets(): [{}] {
    return this._medicamentInBaskets;
  }

  set medicamentInBaskets(value: [{}]) {
    this._medicamentInBaskets = value;
  }

  get prixTotal(): string {
    return this._prixTotal;
  }

  set prixTotal(value: string) {
    this._prixTotal = value;
  }

  get quantite(): string {
    return this._quantite;
  }

  set quantite(value: string) {
    this._quantite = value;
  }

  deserialize(input: any): this {
    return Object.assign(this, input);
  }


}
