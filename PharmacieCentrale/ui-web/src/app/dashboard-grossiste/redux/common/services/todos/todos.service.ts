import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Medicament } from '../../models/medicament';
import { headers } from '../../headers/headers';

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = 'http://localhost:8080';
  }

  getAPITodos() {
    return this.http.get(`${this.baseUrl}/api/medicament`, { headers })
      .pipe(catchError((error: any) => throwError(error.message)));
  }

  addAPITodo(todo: Medicament) {
    const sendTodo = {
      'todoName': todo.name,
      'todoDescription': todo.presentation,
      'todoState': 0
    };

    return this.http.post(
      `${this.baseUrl}/todos`,
      JSON.stringify(sendTodo),
      { headers }
    )
      .pipe(catchError((error: any) => throwError(error.message)));
  }

  updateAPITodo(todo: Medicament) {
    return this.http.put(
      `${this.baseUrl}/todos/${todo.id}`,
      JSON.stringify(todo),
      { headers }
    )
      .pipe(catchError((error: any) => throwError(error.message)));

  }

  deleteAPITodo(todoId: number) {
    return this.http.delete(
      `${this.baseUrl}/todos/${todoId}`,
      { headers }
    )
      .pipe(catchError((error: any) => throwError(error.message)));
  }
}
