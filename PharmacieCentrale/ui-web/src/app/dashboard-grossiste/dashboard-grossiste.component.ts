import { Component, OnInit } from '@angular/core';
import {MENU_ITEMS} from "./dashboard-grossiste-menu";

@Component({
  selector: 'ngx-dashboard-grossiste',
  templateUrl: './dashboard-grossiste.component.html',
  styleUrls: ['./dashboard-grossiste.component.scss']
})
export class DashboardGrossisteComponent implements OnInit {
  menu = MENU_ITEMS;

  constructor() { }

  ngOnInit() {
  }

}
