import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardGrossisteComponent } from './dashboard-grossiste.component';

describe('DashboardGrossisteComponent', () => {
  let component: DashboardGrossisteComponent;
  let fixture: ComponentFixture<DashboardGrossisteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardGrossisteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardGrossisteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
