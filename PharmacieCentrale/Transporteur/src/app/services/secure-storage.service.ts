import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import {environment} from "../../environments/environment.prod";
var crypto = require('crypto');


@Injectable()
export class StorageService {
    constructor() { }

    encrypt(data){
        const SECRET_KEY = localStorage.getItem("hash");

        let mykey = crypto.createCipher('aes-128-cbc', SECRET_KEY);
        let str = mykey.update(data, 'utf8', 'hex')
        str += mykey.final('hex');
        return str;


    }

    decrypt(data){
        const SECRET_KEY = localStorage.getItem("hash");
        let mykey = crypto.createDecipher('aes-128-cbc', SECRET_KEY);
        return  mykey.update(data, 'hex', 'utf8')

    }


}
