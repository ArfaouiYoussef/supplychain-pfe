import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {config} from '../config';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private _http:HttpClient) { }
changePassword(data){
   return    this._http.post(config.apiUrl+"/api/auth/editPassword",data);
}
updateUser(data){
      return this._http.post(config.apiUrl+"/api/user/update",data);
}
getUser(id){
  return this._http.get(config.apiUrl+"/api/user/"+id);
}
}
