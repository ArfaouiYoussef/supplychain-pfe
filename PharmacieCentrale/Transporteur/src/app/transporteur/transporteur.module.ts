import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransporteurRoutingModule } from './transporteur-routing.module';
import {TransporteurComponent} from './transporteur.component';
import {SettingComponent} from './setting/setting.component';
import {IonicModule} from '@ionic/angular';
import {ContainerComponent} from './container/container.component';
import {AvatarModule} from 'ngx-avatar';
import {HttpClientModule} from '@angular/common/http';
import {DetailsContainerComponent} from './details-container/details-container.component';
import {ConfigurationComponent} from './configuration/configuration.component';
import {AboutComponent} from './about/about.component';
import {ConfigurationIotService} from '../services/configuration-iot.service';
import {NgxSpinnerModule} from 'ngx-spinner';
import { NetworkInterface } from '@ionic-native/network-interface/ngx';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [TransporteurComponent,SettingComponent,ContainerComponent,DetailsContainerComponent,
  ConfigurationComponent,AboutComponent],
    imports: [
        CommonModule,
        TransporteurRoutingModule,
        IonicModule,
        AvatarModule,
        HttpClientModule,
        // Specify AvatarModule as an import
        AvatarModule,
        NgxSpinnerModule,
        ReactiveFormsModule,

    ],providers:[ConfigurationIotService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],

})
export class TransporteurModule { }
