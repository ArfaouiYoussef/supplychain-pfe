import { Component, OnInit } from '@angular/core';
import {LivraisonService} from '../../services/livraison.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {EthereumProvider} from '../../ethereumProvider/ethereum';
import Swal from 'sweetalert2'
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-details-container',
  templateUrl: './details-container.component.html',
  styleUrls: ['./details-container.component.scss'],
})
export class DetailsContainerComponent implements OnInit {
    containerId: string;
    containerDetails: any;
    tempreratrue: any;
    humidite: any;
    tagContainer: any;
  constructor(private _livraisonService:LivraisonService,
              private spinner:NgxSpinnerService,
              private _web3:EthereumProvider,
              public alertController: AlertController,
              private router:Router,
              private activatedRoute:ActivatedRoute) {

  }

  ngOnInit() {
     // this._livraisonService.getContainerById()
      this.activatedRoute.url.subscribe(res=>{
          this.containerId=res[1].path;
      })
  this._livraisonService.getContainerById(this.containerId).subscribe((response:any)=>{
      this.containerDetails=response;
      this.tagContainer=response.tagContainer;
  })

  }
  async  containerConfirm(){
        const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Confirmation',
            message: '<p>Vous pouvez charger ce conteneur !</p>',
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Confirmer',
                    handler: () => {
   this.delivredContainer();
                        console.log('Confirm Okay');
                    }
                }
            ]
        });

        await alert.present();
    }




  
delivredContainer(){
this.spinner.show().then(console.log);

this._livraisonService.getContainerById(this.containerId).subscribe((container:any)=>{
    if(!!container&& !!container.temperature && !!container.humidity)
    {//
        this.tempreratrue=container.temperature[Object.keys(container.temperature).length-1];
        console.log(Object.keys(container.humidity))
        this.humidite=container.humidity[Object
            .keys(container.humidity).length-1];
        let ethRequest={tagContainer:this.containerDetails.tagContainer,temperature:this.tempreratrue,humidite:this.humidite}
        this._web3.ReceivedContainer(ethRequest).then(signed=>{
console.log(signed.rawTransaction); let data={
                tagContainer:this.tagContainer,
transaction:signed.rawTransaction,

}
this._livraisonService.delivredContianer(data).subscribe(done=>{
    this.containerDetails=done;

Swal.fire(
   'SUCCESS  ',
   ' Votre conteneur  livrée ',
   'success'
)
this.spinner.hide();
} ,error => {this.spinner.hide();
Swal.fire(
   'Echec  ',
   ' Transaction non valide ',
   'error'
)
})
        })
    }
    else {
        Swal.fire(
            'Rescanner votre conteneur  ',
            ' Verifier que votre appareil connecté  puis confirmer',
            'error'
        )

    }

 });


}

  loadContainer(){
      
      this.spinner.show().then(console.log);
      this._livraisonService.getContainerById(this.containerId).subscribe((container:any)=>{
         if(!!container && container.temperature!='' && container.humidity!='')
         {//
             this.tempreratrue=container.temperature[0];
             this.humidite=container.humidity[0];
             let ethRequest={tagContainer:this.containerDetails.tagContainer,temperature:this.tempreratrue,humidite:this.humidite}
             this._web3.loadContainer(ethRequest).then(signed=>{
 let data={
                     tagContainer:this.tagContainer,
    transaction:signed.rawTransaction,

}
this._livraisonService.shippingContianer(data).subscribe(done=>{
    Swal.fire(
        'SUCCESS  ',
        ' Votre conteneur bien chargé ',
        'success'
    )
    this.router.navigate(["/dashboard/container"])

    this.spinner.hide();
} ,error => {this.spinner.hide();
    Swal.fire(
        'Echec  ',
        ' Transaction non valide / verifier que votre appareil  connecte ',
        'error'
    )
})
             })
         }
         else {
             Swal.fire(
                 'Rescanner votre conteneur  ',
                 ' Verifier que votre appareil connecté  puis confirmer',
                 'error'
             )
this.spinner.hide();
         }

      });



  }
    async presentAlertConfirm() {
        const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Confirmation',
            message: '<p>Vous pouvez charger ce conteneur !</p>',
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Confirmer',
                    handler: () => {
   this.loadContainer();
                        console.log('Confirm Okay');
                    }
                }
            ]
        });

        await alert.present();
    }
}
