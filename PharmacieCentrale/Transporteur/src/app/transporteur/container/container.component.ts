import { Component, OnInit } from '@angular/core';
import {LivraisonService} from '../../services/livraison.service';
import {MessageService} from '../../services/shared.service';
@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss'],
})
export class ContainerComponent implements OnInit {
    listContainer: any;
    filteredItems:any;
    options: string[];

  constructor(private  livraisonService:LivraisonService,
              private messageService:MessageService) {
      this.filteredItems=[];
  }
    state: any[ ] = [
        {
            id: 'Expedie',
            name: 'Conteneurs prets a livrer'
        },
        {
            id: 'Received',
            name:'Conteneurs livrer'
        }
    ];

    compareWithFn = (o1, o2) => {
        return o1 === o2;
    };

    compareWith = this.compareWithFn;
    MyDefaultYearIdValue: any=1;
    SelectedYearIdValue : any ;
    private filter(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.options.filter(optionValue => optionValue.toLowerCase().includes(filterValue));
      }


      assignCopy(){
        this.filteredItems = Object.assign([], this.listContainer);
      }
      filterItem(value){

      }
    ///// In initialisations (in my case it's in ngOnInit() function)

    ///// In functions definitions
    onSelectChange(event) {

    this.assignCopy()
      this.filteredItems=  this.filteredItems.filter(item=>item.state===event.target.value.id)
      this.SelectedYearIdValue=event.target.value.name;
      console.log(this.filterItem)
    }
  ngOnInit() {

    this.SelectedYearIdValue  = "Conteneurs prets a livrer" ;
this.messageService.getMessage().subscribe(response=>{
    this.filteredItems.push(response.container)
})
      this.livraisonService.getcontainers().subscribe((response:any)=>{
this.listContainer=response;
this.assignCopy();
this.filteredItems=response;

      })


  }

}
