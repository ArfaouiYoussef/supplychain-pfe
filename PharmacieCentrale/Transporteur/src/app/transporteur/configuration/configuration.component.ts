import { Component, OnInit } from '@angular/core';
import {ConfigurationIotService} from '../../services/configuration-iot.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {Router} from '@angular/router';
import { NetworkInterface } from '@ionic-native/network-interface/ngx';


@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss'],
})
export class ConfigurationComponent implements OnInit {

connected:any=false;
    done: any=true;
    disable: boolean=false;

        constructor(private configService:ConfigurationIotService,private networkInterface: NetworkInterface,
                private spinner: NgxSpinnerService,private route:Router) {




    }
    ngOnInit(): void {
this.connected=false;

        this.spinner.show().then(console.log);
        setTimeout(() => {

            /** spinner ends after 5 seconds */
            this.spinner.hide();
this.disable=true;
this.networkInterface.getWiFiIPAddress().then(address =>
                {console.log(address.ip)
                    if(address.ip==="192.168.4.2"){
                        this.connected=true;

                        this.disable=true;
                    }else
                    {this.connected=false;
                        this.disable=true;
                    }

                    console.info(`IP: ${address.ip}, Subnet: ${address.subnet}`)})
                .catch(error =>{ console.error(`Unable to get IP: ${error}` );

                    this.connected=false;
                    this.disable=true;
                });
        }, 3000);

console.log(this.connected)

        }
    ngAfterViewInit(): void {  }
    refresh(): void {
this.disable=false;
        this.route.navigateByUrl('/', {skipLocationChange: true}).then(()=>
            this.route.navigate(["/dashboard/configuration"]));

    }


    myLoadEvent() {
        console.log("load");
    }
}
