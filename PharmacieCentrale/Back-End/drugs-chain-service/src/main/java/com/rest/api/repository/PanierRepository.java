package com.rest.api.repository;

import com.rest.api.models.Medicament;
import com.rest.api.models.Panier;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;


public interface PanierRepository extends MongoRepository<Panier, String> {
Panier findByMedicamentInBasketsNotContaining(Medicament medicament);

}
