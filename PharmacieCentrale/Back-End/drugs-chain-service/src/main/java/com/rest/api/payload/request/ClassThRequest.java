package com.rest.api.payload.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class ClassThRequest {

    @NotBlank
    @Size(min = 3, max = 70)
    private String name;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }




}
