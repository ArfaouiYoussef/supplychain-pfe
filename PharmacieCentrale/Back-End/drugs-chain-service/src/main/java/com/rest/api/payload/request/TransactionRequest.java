package com.rest.api.payload.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class
TransactionRequest {

    @NotBlank
    private String transaction;

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }
}
