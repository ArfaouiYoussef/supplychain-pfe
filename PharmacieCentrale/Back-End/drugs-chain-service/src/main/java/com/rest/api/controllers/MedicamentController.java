package com.rest.api.controllers;

import com.rest.api.models.ClasseTherapeutique;
import com.rest.api.models.Medicament;
import com.rest.api.payload.request.MedicamentRequest;
import com.rest.api.payload.response.MessageResponse;
import com.rest.api.repository.ClassTherapeutiqueRepository;
import com.rest.api.repository.MedicamentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;


@RestController
@RequestMapping("/api/medicament")
public class MedicamentController {
@Autowired
    MedicamentRepository medicamentRepository;
@Autowired
ClassTherapeutiqueRepository therapeutiqueRepository;
FileController fileController;
@GetMapping()
    public ResponseEntity<?> getAll(){
   return ResponseEntity.ok(medicamentRepository.findByType("model"));
}

    @GetMapping("/{id}")
    public  ResponseEntity<?> getById(@PathVariable String id){

        if(medicamentRepository.findById(id).isPresent())
        {
            Medicament medicament= medicamentRepository.findById(id).get();
            return ResponseEntity.ok(medicament);

        }
        else
            return ResponseEntity.badRequest().body(new MessageResponse("class not found"));
    }

    @PostMapping()
    public  ResponseEntity<?> addMedicament(@Valid @RequestBody MedicamentRequest medicamentRequest
    ){

if(therapeutiqueRepository.findById(medicamentRequest.getClasseTherapeutique()).isPresent())
{
   ClasseTherapeutique classe= therapeutiqueRepository.findById(medicamentRequest.getClasseTherapeutique()).get();
    Medicament medicament=new Medicament();
    medicament.setStock(medicamentRequest.getStock());

    medicament.setDosage(medicamentRequest.getDosage());
    medicament.setFormegalenique(medicamentRequest.getFormegalenique());
    medicament.setAmm(medicamentRequest.getAmm());
    medicament.setIndication(medicamentRequest.getIndication());
    medicament.setMarque(medicamentRequest.getMarque());
    medicament.setName(medicamentRequest.getName());
    medicament.setMedicamentPrice(medicamentRequest.getMedicamentPrice());
    medicament.setQuantity(medicamentRequest.getQuantity());
    medicament.setType(medicamentRequest.getType());
    medicament.setPresentation(medicamentRequest.getPresentation());
    medicament.setClasseTherapeutique(classe);
    medicament.setComposition(medicamentRequest.getComposition());
medicament.setPhotoUrl(medicamentRequest.getPhotoUrl());
    medicamentRepository.save(medicament);
    return ResponseEntity.ok(new MessageResponse("success"));

}
else
    return ResponseEntity.badRequest().body(new MessageResponse("class not found"));
    }
    @PutMapping()
    public  ResponseEntity<?> updateMedicament(@Valid @RequestBody MedicamentRequest medicamentRequest){

        if(therapeutiqueRepository.findById(medicamentRequest.getClasseTherapeutique()).isPresent()&&medicamentRepository.findById(medicamentRequest.getId()).isPresent())
        {
            ClasseTherapeutique classe= therapeutiqueRepository.findById(medicamentRequest.getClasseTherapeutique()).get();
            Medicament medicament=medicamentRepository.findById(medicamentRequest.getId()).get();
            medicament.setDosage(medicamentRequest.getDosage());
            medicament.setFormegalenique(medicamentRequest.getFormegalenique());
            medicament.setIndication(medicamentRequest.getIndication());
            medicament.setMarque(medicamentRequest.getMarque());
            medicament.setName(medicamentRequest.getName());
            medicament.setMedicamentPrice(medicamentRequest.getMedicamentPrice());
            medicament.setQuantity(medicamentRequest.getQuantity());
            medicament.setAmm(medicamentRequest.getAmm());
medicament.setStock(medicamentRequest.getStock());
            medicament.setType(medicamentRequest.getType());
            medicament.setPresentation(medicamentRequest.getPresentation());
            medicament.setClasseTherapeutique(classe);
            medicament.setPhotoUrl(medicamentRequest.getPhotoUrl());

            medicament.setComposition(medicamentRequest.getComposition());
            medicamentRepository.save(medicament);
            return ResponseEntity.ok(new MessageResponse("update success"));

        }
        else
            return ResponseEntity.badRequest().body(new MessageResponse("class not found"));
    }






@GetMapping("class/{id}/")
public ResponseEntity<?> getByClassId(@PathVariable String id)
{
   if( ! medicamentRepository.findByClasseTherapeutiqueId(id).isEmpty())
   {
       return ResponseEntity.ok(medicamentRepository.findByClasseTherapeutiqueId(id));
   }
   else {
       return ResponseEntity.badRequest().body(new MessageResponse("Medicament not found"));

   }



}
    @DeleteMapping("/{id}")
    public  ResponseEntity<?> delete(@PathVariable String id){

        if(medicamentRepository.findById(id).isPresent())
        {
            medicamentRepository.deleteById(id);
            return ResponseEntity.ok(new MessageResponse("ok"));

        }
        else
            return ResponseEntity.badRequest().body(new MessageResponse("failed"));
    }


}




