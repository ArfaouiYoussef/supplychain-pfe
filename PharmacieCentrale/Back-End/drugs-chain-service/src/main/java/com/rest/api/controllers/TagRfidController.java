package com.rest.api.controllers;

import com.rest.api.models.TagRfid;
import com.rest.api.payload.request.TagRequest;
import com.rest.api.payload.response.MessageResponse;
import com.rest.api.repository.TagRfidRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.HTML;


@RestController

@RequestMapping("/api/tagRfid")
public class TagRfidController {
@Autowired
    TagRfidRepository tagRfidRepository;
    @GetMapping()
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(tagRfidRepository.findAll());
    }
    @PostMapping()
public ResponseEntity<?> addTag(@RequestBody TagRequest tagRequest) {
        if (!tagRfidRepository.findByTag(tagRequest.getTag()).isPresent()) {
            TagRfid tagRfid = new TagRfid();
            tagRfid.setActive(true);
            tagRfid.setTag(tagRequest.getTag());
            tagRfidRepository.save(tagRfid);
            return ResponseEntity.ok(tagRfid);
        }
        else
        return ResponseEntity.badRequest().body(new MessageResponse("exists Tag"));
    }
@PostMapping("/delete")
    public ResponseEntity<?> deleteTag(@RequestBody  TagRequest tagRequest)
{
    if(tagRfidRepository.findById(tagRequest.getId()).isPresent()){
 tagRfidRepository.deleteById(tagRequest.getId());
    return ResponseEntity.ok(new MessageResponse("success"));
    }
    return ResponseEntity.badRequest().body(new MessageResponse("not found"));

}
@PostMapping("/disable")
public ResponseEntity<?> disable(@RequestBody TagRequest tagRequest){
        if(tagRfidRepository.findByTag(tagRequest.getTag()).isPresent()){
            TagRfid tagRfid = tagRfidRepository.findByTag(tagRequest.getTag()).get();
       tagRfid.setActive(false);
       tagRfidRepository.save(tagRfid);
       return ResponseEntity.ok(new MessageResponse("success"));
        }
        return ResponseEntity.badRequest().body(new MessageResponse("failed"));
}
    @PostMapping("/enable")
    public ResponseEntity<?> enable(@RequestBody TagRequest tagRequest){
        if(tagRfidRepository.findById(tagRequest.getId()).isPresent()){
            TagRfid tagRfid=   tagRfidRepository.findById(tagRequest.getId()).get();
            tagRfid.setActive(true);

            tagRfidRepository.save(tagRfid);
            return ResponseEntity.ok(new MessageResponse("success"));
        }
        return ResponseEntity.badRequest().body(new MessageResponse("failed"));
    }
@GetMapping("/{id}")
    public  ResponseEntity<?> findById(@PathVariable String id){
        if (tagRfidRepository.findById(id).isPresent()) {
            return ResponseEntity.ok(tagRfidRepository.findById(id).get());
        }
        else
        {
            return ResponseEntity.badRequest().body(new MessageResponse("not found"));
        }
}

    }
