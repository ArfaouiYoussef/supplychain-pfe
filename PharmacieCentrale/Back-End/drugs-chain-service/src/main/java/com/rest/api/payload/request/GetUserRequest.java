package com.rest.api.payload.request;

import javax.validation.constraints.NotBlank;

public class GetUserRequest {
    @NotBlank
    private String email;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
