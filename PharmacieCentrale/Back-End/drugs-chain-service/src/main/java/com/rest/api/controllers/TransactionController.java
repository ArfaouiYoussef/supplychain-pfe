package com.rest.api.controllers;

import com.rest.api.contracts.Helloworld;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Int;
import org.web3j.abi.datatypes.generated.Uint8;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.Keys;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.http.HttpService;

import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;


@RestController
@RequestMapping("/api")
public class TransactionController {
    @GetMapping("/send/{id}")
public ResponseEntity<?> send(@PathVariable String id){

    return ResponseEntity.ok("hello");

}
    @GetMapping("/web3/{id}")
    public ResponseEntity<?> sendTransaction(@PathVariable String id) throws IOException, ExecutionException, InterruptedException, NoSuchAlgorithmException {
        Web3j web3j = Web3j.build(new HttpService("http://5.135.52.74:8546"));

// Pull all the events for this contract
         final Event MY_EVENT = new Event("MyEvent", Arrays.<TypeReference<?>>asList(new TypeReference<Address>(true) {}, new TypeReference<Int>(true) {}, new TypeReference<Uint8>(false) {}));

// Event definition hash
         final String MY_EVENT_HASH = EventEncoder.encode(MY_EVENT);
        ArrayList<Log> arrayList=new ArrayList<>();
// Filter
        EthFilter filter = new EthFilter(DefaultBlockParameterName.EARLIEST, DefaultBlockParameterName.LATEST, "0x45164C1c4Ed0Ba33A49F3a5Bd6464A63a7F66af3");

        web3j.ethLogFlowable(filter.addSingleTopic(MY_EVENT_HASH)).subscribe(log -> {
            String topic = log.getTopics().get(2); // Index 0 is the event definition hash
            System.out.println(log.getTopics());

           if(topic.equals(id)) { // Only MyEvent. You can also use filter.addSingleTopic(MY_EVENT_HASH)
                // address indexed _arg1
               arrayList.add(log);
               System.out.println(log);



            }

        });


        return ResponseEntity.ok(arrayList);
    }

    @GetMapping("/getCustomerById")
    public String test() throws Exception {

        Web3j web3 = Web3j.build(new HttpService("http://5.135.52.74:8501"));

        Credentials dummyCredentials = Credentials.create(Keys.createEcKeyPair());

        Helloworld contract = Helloworld.load(
                "0xe57B06515dD9085A96eD9c653fd76d922c133237",
                web3,
                dummyCredentials,
                new BigInteger("22000000000"),
                new BigInteger("510000")
        );

        try {
            return contract.getCustomer(new BigInteger("1")).send().toString();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return contract.getCustomer(new BigInteger("1")).send().toString();
    }
}



