package com.rest.api.payload.request;

import org.springframework.data.annotation.Id;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class MedicamentRequest {
	@Id
	private String id;
	@NotBlank
	@Size(max = 20)
	private String name;

	private String  indication;
private String batchNumber;
	public String getComposition() {
		return composition;
	}

	public void setComposition(String composition) {
		this.composition = composition;
	}

		private String  composition;

		@Size(max = 20)
	private String dosage;
		@Size(max = 120)
	private String formegalenique;
	private String classeTherapeutique;
	@Size(max = 200)
	private String presentation;

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	private int stock;
	public double getQuantity() {
		return quantity;
	}

private  String amm;

	public String getAmm() {
		return amm;
	}

	public void setAmm(String amm) {
		this.amm = amm;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	private double quantity;
	@Size(max = 200)
	private String marque;

	public Number getMedicamentPrice() {
		return medicamentPrice;
	}

	public void setMedicamentPrice(Number medicamentPrice) {
		this.medicamentPrice = medicamentPrice;
	}

	private Number medicamentPrice;
	@Size(max = 200)
	private String type;

	private String photoUrl;



	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getDosage() {
		return dosage;
	}

	public void setDosage(String dosage) {
		this.dosage = dosage;
	}

	public String getFormegalenique() {
		return formegalenique;
	}

	public void setFormegalenique(String formegalenique) {
		this.formegalenique = formegalenique;
	}

	public String getClasseTherapeutique() {
		return classeTherapeutique;
	}

	public void setClasseTherapeutique(String classeTherapeutique) {
		this.classeTherapeutique = classeTherapeutique;
	}

	public String getPresentation() {
		return presentation;
	}

	public void setPresentation(String presentation) {
		this.presentation = presentation;
	}


	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}


	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public String getIndication() {
		return indication;
	}

	public void setIndication(String indication) {
		this.indication = indication;
	}

	public String getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}
}
