package com.rest.api.controllers;

import com.rest.api.models.Medicament;
import com.rest.api.models.MedicamentInBasket;
import com.rest.api.models.MedicamentPanier;
import com.rest.api.models.Panier;
import com.rest.api.payload.request.DeleteMedicamentInBasketRequest;
import com.rest.api.payload.request.PanierRequest;
import com.rest.api.payload.response.MessageResponse;
import com.rest.api.repository.MedicamentRepository;
import com.rest.api.repository.MedicamentInBasketRepository;
import com.rest.api.repository.PanierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;



@RestController
@RequestMapping("/api/panier")
public class PanierController {
@Autowired
PanierRepository panierRepository;
@Autowired
MedicamentInBasketRepository panierMedicamentRepository;
@Autowired
MedicamentRepository medicamentRepository;
    private MongoTemplate mongoTemplate;


    @PostMapping()
    public ResponseEntity<?> addPanier(@Valid @RequestBody PanierRequest panierRequest) {
     System.out.println(panierRequest.getQuantite());
        if (panierRepository.findById(panierRequest.getPanierId()).isPresent()) {
        Panier panier=panierRepository.findById(panierRequest.getPanierId()).get();
        Medicament medicament=medicamentRepository.findById(panierRequest.getMedicament().getId()).get();
System.out.println(panierRequest.getQuantite());
if(panierRequest.getQuantite().intValue()>0){
        if(medicament.removeStock(panierRequest.getQuantite().intValue())) {
            System.out.println(medicament.getStock());
            medicament = medicamentRepository.save(medicament);
            panier.addProduct(medicament);
            panier.addProductQuantity(medicament,panierRequest.getQuantite().intValue());
            panierRepository.save(panier);

            return ResponseEntity.ok(panier);

        }
    return ResponseEntity.badRequest().body("failed");

        }
else{
    panier.removeProductQuantity(medicament, panierRequest.getQuantite().intValue() * -1);
medicament.addStock(Math.abs(panierRequest.getQuantite().intValue()));
medicamentRepository.save(medicament);
panierRepository.save(panier);
    return ResponseEntity.ok(panier);
            }




        }

        return ResponseEntity.badRequest().body("");



    }
@PutMapping()
public ResponseEntity<?> updatePanier(@Valid @RequestBody PanierRequest panierRequest) {
    if (panierRepository.findById(panierRequest.getPanierId()).isPresent()) {
        Panier panier=panierRepository.findById(panierRequest.getPanierId()).get();
        Medicament medicament=medicamentRepository.findById(panierRequest.getMedicament().getId()).get();

        if(medicament.removeStock(panierRequest.getQuantite().intValue()))
        {System.out.println(medicament.getStock());
            medicament=  medicamentRepository.save(medicament);

            panier.addProduct(medicament);

            panier.setProductQuantity(medicament,panierRequest.getQuantite().intValue());

            panierRepository.save(panier);

            return ResponseEntity.ok(panier);

        }
        return ResponseEntity.badRequest().body("");


    }

    return ResponseEntity.badRequest().body("");



}

    @PostMapping("/delete")
    public ResponseEntity<?> deletePanierById( @Valid @RequestBody DeleteMedicamentInBasketRequest deleteMedicamentInBasketRequest)
    {
        if(panierRepository.findById(deleteMedicamentInBasketRequest.getPanierId()).isPresent()){
            Panier panier=panierRepository.findById(deleteMedicamentInBasketRequest.getPanierId()).get();
Medicament medicament =medicamentRepository.findById(deleteMedicamentInBasketRequest.getMedicamentInBasket()).get();
System.out.println(deleteMedicamentInBasketRequest.toString());
int quantity=  panier.getProductQuantities().get(deleteMedicamentInBasketRequest.getMedicamentInBasket());

            panier.getProductQuantities().remove(medicament.getId());

            medicament.addStock(quantity);

            panier.removeProduct(deleteMedicamentInBasketRequest.getMedicamentInBasket());

            panier.removeProductQuantity(medicament
                    , quantity );
            medicament.setQuantity(quantity);
            System.out.println(medicament);

            medicamentRepository.save(medicament);
            panierRepository.save(panier);


            return ResponseEntity.ok(panier);
        }
        else {
            return ResponseEntity.badRequest().body(new MessageResponse("panier not found"));
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getPanierById(@PathVariable String id)
    {
        if(panierRepository.findById(id).isPresent()){
            Panier panier=panierRepository.findById(id).get();
            ResponseEntity.ok(panier);
            return ResponseEntity.ok(panier);
        }
        else {
            return ResponseEntity.badRequest().body(new MessageResponse("panier not found"));
        }
    }
@PostMapping("/confirmCommande/{id}")
    public ResponseEntity<?> confirmCommande(@RequestBody PanierRequest panierRequest){
        if(panierRepository.findById(panierRequest.getPanier()).isPresent())
        {
         Panier panier=   panierRepository.findById(panierRequest.getPanier()).get();
         if(panier.getProducts().isEmpty()){

         }
        }
        return ResponseEntity.badRequest().body("");

}

}

