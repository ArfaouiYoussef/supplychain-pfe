package com.rest.api.controllers;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import javax.validation.Valid;

import com.rest.api.models.*;
import com.rest.api.payload.request.*;
import com.rest.api.payload.response.UserResponse;
import com.rest.api.repository.PanierRepository;
import com.rest.api.service.BlockChainService;
import com.rest.api.service.MessageSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import com.rest.api.payload.response.JwtResponse;
import com.rest.api.payload.response.MessageResponse;
import com.rest.api.repository.RoleRepository;
import com.rest.api.repository.UserRepository;
import com.rest.api.security.jwt.JwtUtils;
import com.rest.api.security.services.UserDetailsImpl;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.EthGetTransactionReceipt;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;


@RestController
@RequestMapping("/api/user")

public class UserController {



    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;
@Autowired
    BlockChainService blockChainService;
    public static final Map<String, SseEmitter> sses = new ConcurrentHashMap<>();
    private EthSendTransaction ethSendTransaction=null;



    @GetMapping()
    public ResponseEntity<?> getclientAll() {

        List<User> users = userRepository.findAll();

        return ResponseEntity.ok(users);
    }
    @GetMapping("/activeClient")
    public ResponseEntity<?> getclientActive() {
        Role role = roleRepository.findByName(ERole.ROLE_GROSSISTE)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));

        List<User> users = userRepository.findByisAccountActiveAndRoles(true,role);

        return ResponseEntity.ok(users);
    }
    @GetMapping("/client")
    public ResponseEntity<?> getClient() {
        Role role = roleRepository.findByName(ERole.ROLE_GROSSISTE)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));

        List<User> users = userRepository.findByisAccountActiveAndRoles(false,role);

        return ResponseEntity.ok(users);
    }
    @GetMapping("/{id}")
    public  ResponseEntity<?> getUserById(@PathVariable String id){

        if(userRepository.findById(id).isPresent())
            return 	ResponseEntity.ok(userRepository.findById(id).get());
        else
            return  ResponseEntity.badRequest().body(new MessageResponse("user not found"));
    }

    @PostMapping("/{id}")
    public ResponseEntity<?> updateUser(@RequestBody UserRequest userRequest,@PathVariable String id){
        if(userRepository.findById(id).isPresent()){
            User user=userRepository.findById(id).get();
            user.setPhotoUrl(userRequest.getPhotoUrl());
            user.setAddress(userRequest.getAddress());
            user.setEmail(userRequest.getEmail());
            user.setUsername(userRequest.getUsername());
            user.setTelephone(userRequest.getTelephone());
            userRepository.save(user);
            return ResponseEntity.ok(new MessageResponse("success"));
        }
        else
            return ResponseEntity.badRequest().body(new MessageResponse("failed"));



    }
    @PostMapping("/delete")
    public ResponseEntity<?> deleteFromBlockChain(@RequestBody DeleteRequest deleteRequest ) throws InterruptedException, IOException, ExecutionException {
        if(userRepository.existsById(deleteRequest.getId())){
            TransactionReceipt transactionReceipt=blockChainService.sendTransaction(deleteRequest.getTransaction());
            if (!transactionReceipt.isStatusOK()) {
                return ResponseEntity.badRequest().body(new MessageResponse("Transaction Failed"));

            }

            if (!transactionReceipt.isStatusOK()) {
                return ResponseEntity.badRequest().body(new MessageResponse("Transaction Failed"));

            }



            userRepository.deleteById(deleteRequest.getId());
            return ResponseEntity.ok().body(new MessageResponse("success"));

        };

        return 	 	ResponseEntity.badRequest().body(new MessageResponse("user not found"));

    }
    @DeleteMapping("/client/{id}")
    public ResponseEntity<?> delete(@PathVariable String id ){
        if(userRepository.existsById(id)){
            userRepository.deleteById(id);
            return ResponseEntity.ok().body(new MessageResponse("success"));

        };

        return 	 	ResponseEntity.badRequest().body(new MessageResponse("user not found"));

    }

@PostMapping("/update")
public ResponseEntity<?> editUser(@RequestBody UpdateUserRequest updateUserRequest)
{
    if(userRepository.findById(updateUserRequest.getId()).isPresent()){
        User user=userRepository.findById(updateUserRequest.getId()).get();
        user.setEmail(updateUserRequest.getEmail());
            user.setUsername(updateUserRequest.getUsername());
        userRepository.save(user);
       return ResponseEntity.ok(new MessageResponse("success"));

    }
return     ResponseEntity.badRequest().body(new MessageResponse("user not found"));

}
    @GetMapping("/unblocked")
    public ResponseEntity<?> getClientUnBlocked(){
        Role grossisteRole = roleRepository.findByName(ERole.ROLE_GROSSISTE)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));

        List<User> users=userRepository.findByisAccountActiveAndRoles(true,grossisteRole);

        return 	 	ResponseEntity.ok(users);

    }
    @GetMapping("/blocked")
    public ResponseEntity<?> getClientBlocked(){
        Role role = roleRepository.findByName(ERole.ROLE_GROSSISTE)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));

        List<User> users=userRepository.findByisBlockedAndRoles(true,role);

        return 	 	ResponseEntity.ok(users);

    }
    @PostMapping("/blocked")
    public ResponseEntity<?> Block(@Valid @RequestBody BlockRequest blockRequest) throws InterruptedException, ExecutionException, IOException {
        if(userRepository.existsByAccountAddress(blockRequest.getAccountAddress())){
            TransactionReceipt transactionReceipt=blockChainService.sendTransaction(blockRequest.getTransaction());
            if (!transactionReceipt.isStatusOK()) {
                return ResponseEntity.badRequest().body(new MessageResponse("Transaction Failed"));

            }

            User user=userRepository.findByAccountAddress(blockRequest.getAccountAddress()).get();
            user.setAccountActive(false);
            user.setBlocked(blockRequest.isBlock());
            userRepository.save(user);
            return 	 	ResponseEntity.ok().body(new MessageResponse("success"));

        };

        return 	 	ResponseEntity.badRequest().body(new MessageResponse("user not found"));

    }

    @PostMapping("/activateAccount")
    public ResponseEntity<?> activateAccount(@Valid @RequestBody ActivateRequest activateRequest) throws InterruptedException, IOException, ExecutionException {


        if(userRepository.existsById(activateRequest.getId())){
            if(userRepository.findById(activateRequest.getId()).isPresent()) {

                TransactionReceipt transactionReceipt=blockChainService.sendTransaction(activateRequest.getTransaction());
                if (!transactionReceipt.isStatusOK()) {
                    return ResponseEntity.badRequest().body(new MessageResponse("Transaction Failed"));

                }
                User user = userRepository.findById(activateRequest.getId()).get();
                user.setAccountActive(activateRequest.isActive());
                user.setBlocked(false);
                user.setSmartContract(activateRequest.getSmartContract());

                System.out.println(user.getSmartContract());


                userRepository.save(user);
                return ResponseEntity.ok(transactionReceipt);

            }

            else {
                return ResponseEntity.badRequest().body(new MessageResponse("user not found"));
            }

        }


        return 	 	ResponseEntity.badRequest().body(new MessageResponse("user not found"));

    }

}