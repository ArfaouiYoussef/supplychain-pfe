package com.rest.api.payload.request;

import com.rest.api.models.Medicament;
import com.rest.api.models.Panier;

import java.util.ArrayList;

public class ContainerRequest {

    private String laboAddress;
private String grossisteAddress;
private String commandeId;
private String numeroLot;
private String serialNumber;
private String DateExpiration;
private String emailClient;
    private String seuilTemperature;
    private String seuilHumidite;
    private String destinationAddress;
    private ArrayList<String> temperature=new ArrayList<>();

    private  ArrayList<String> humidity=new ArrayList<>();

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public String getSeuilTemperature() {
        return seuilTemperature;
    }

    public void setSeuilTemperature(String seuilTemperature) {
        this.seuilTemperature = seuilTemperature;
    }

    public String getSeuilHumidite() {
        return seuilHumidite;
    }

    public void setSeuilHumidite(String seuilHumidite) {
        this.seuilHumidite = seuilHumidite;
    }

    public String getEmailClient() {
        return emailClient;
    }

    public void setEmailClient(String emailClient) {
        this.emailClient = emailClient;
    }

    public String getNumeroLot() {
        return numeroLot;
    }

    public void setNumeroLot(String numeroLot) {
        this.numeroLot = numeroLot;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getDateExpiration() {
        return DateExpiration;
    }

    public void setDateExpiration(String dateExpiration) {
        DateExpiration = dateExpiration;
    }

    public Medicament getMedicament() {
        return medicament;
    }

    public void setMedicament(Medicament medicament) {
        this.medicament = medicament;
    }

    private Medicament medicament;
    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public ArrayList<String> getTemperature() {
        return temperature;
    }

    public void setTemperature(ArrayList<String> temperature) {
        this.temperature = temperature;
    }

    public ArrayList<String> getHumidity() {
        return humidity;
    }

    public void setHumidity(ArrayList<String> humidity) {
        this.humidity = humidity;
    }

    private String transaction;

private String tagContainer;
    private String commandeRef;
    private String addressTrasnporteur;

    public String getCommandeRef() {
        return commandeRef;
    }

    public void setCommandeRef(String commandeRef) {
        this.commandeRef = commandeRef;
    }

    public String getTagContainer() {
        return tagContainer;
    }

    public void setTagContainer(String tagContainer) {
        this.tagContainer = tagContainer;
    }

    public String getCommandeId() {
        return commandeId;
    }

    public void setCommandeId(String commandeId) {
        this.commandeId = commandeId;
    }


    public String getGrossisteAddress() {
        return grossisteAddress;
    }

    public void setGrossisteAddress(String grossisteAddress) {
        this.grossisteAddress = grossisteAddress;
    }




    public String getLaboAddress() {
        return laboAddress;
    }

    public void setLaboAddress(String laboAddress) {
        this.laboAddress = laboAddress;
    }

    public String getAddressTrasnporteur() {
        return addressTrasnporteur;
    }

    public void setAddressTrasnporteur(String addressTrasnporteur) {
        this.addressTrasnporteur = addressTrasnporteur;
    }

    @Override
    public String toString() {
        return "ContainerRequest{" +
                "laboAddress='" + laboAddress + '\'' +
                ", grossisteAddress='" + grossisteAddress + '\'' +
                ", temperature='" + temperature + '\'' +
                ", humidity='" + humidity + '\'' +
                ", commandeId='" + commandeId + '\'' +
                ", numeroLot='" + numeroLot + '\'' +
                ", serialNumber='" + serialNumber + '\'' +
                ", DateExpiration='" + DateExpiration + '\'' +
                ", medicament=" + medicament +
                ", transaction='" + transaction + '\'' +
                ", tagContainer='" + tagContainer + '\'' +
                ", commandeRef='" + commandeRef + '\'' +
                '}';
    }
}
