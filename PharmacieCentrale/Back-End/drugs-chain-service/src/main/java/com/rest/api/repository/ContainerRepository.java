package com.rest.api.repository;

import com.rest.api.models.Container;
import com.rest.api.models.Status;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContainerRepository extends MongoRepository<Container, String> {
Boolean existsByTagContainer(String tag);
Optional<Container> findByCommandeRef(String address);
Optional<Container>findByTagContainer(String tag);
List<Container>findByTransporteurAddress(String address);
List<Container>findAllByTransporteurAddress(String address);

List<Container>findAllByState(String state);
}
