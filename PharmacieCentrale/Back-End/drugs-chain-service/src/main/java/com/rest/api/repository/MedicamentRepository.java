package com.rest.api.repository;

import com.rest.api.models.Medicament;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;


public interface MedicamentRepository extends MongoRepository<Medicament, String> {
List<Medicament> findByClasseTherapeutiqueId(String id);
    List<Medicament> findByType(String type);

}
