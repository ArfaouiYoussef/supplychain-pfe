package com.rest.api.payload.response;

import com.rest.api.models.Panier;

import java.util.List;

public class UserResponse {
    private String id;
    private String accountAddress;
    private String username;
    private String email;
    private List<String> roles;
    private Panier panier;

    public Panier getPanier() {
        return panier;
    }

    public void setPanier(Panier panier) {
        this.panier = panier;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public UserResponse(String id, String username, String email, String accountAddress, Panier panier) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.accountAddress=accountAddress;
        this.panier=panier;
    }




    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<String> getRoles() {
        return roles;
    }

    public String getAccountAddress() {
        return accountAddress;
    }

    public void setAccountAddress(String accountAddress) {
        this.accountAddress = accountAddress;
    }
}
