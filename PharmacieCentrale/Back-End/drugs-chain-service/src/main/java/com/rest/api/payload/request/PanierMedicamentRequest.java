package com.rest.api.payload.request;

import com.rest.api.models.Medicament;

public class PanierMedicamentRequest {
Medicament medicament;
private Number quantite;

    public Medicament getMedicament() {
        return medicament;
    }

    public void setMedicament(Medicament medicament) {
        this.medicament = medicament;
    }

    public Number getQuantite() {
        return quantite;
    }

    public void setQuantite(Number quantite) {
        this.quantite = quantite;
    }
}
